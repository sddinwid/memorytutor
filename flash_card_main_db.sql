-- phpMyAdmin SQL Dump
-- version 4.0.10.7
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 25, 2015 at 07:33 PM
-- Server version: 5.5.41-cll-lve
-- PHP Version: 5.4.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `flash_card_main_db`
--
CREATE DATABASE IF NOT EXISTS `flash_card_main_db` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `flash_card_main_db`;

-- --------------------------------------------------------

--
-- Table structure for table `flash_card_cards`
--

CREATE TABLE IF NOT EXISTS `flash_card_cards` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `categorization` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `hint` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `question` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `answer` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `userId` int(40) NOT NULL,
  `cardset_id` int(11) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_on` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=332 ;

--
-- Dumping data for table `flash_card_cards`
--

INSERT INTO `flash_card_cards` (`id`, `name`, `description`, `categorization`, `hint`, `question`, `answer`, `userId`, `cardset_id`, `created_on`, `modified_on`) VALUES
(1, 'Doom', '<p>Doom desc</p>', '0', '<p>DOOM</p>', '<p>Doom?</p>', '<p>doom</p>', 0, 2, '2014-11-10 21:32:59', '2014-12-04 19:46:50'),
(3, 'test card', '<p>test</p>', '0', '<p>hint</p>', '<p>question</p>', '<p>answer</p>', 0, 2, '2014-11-19 14:39:30', '2014-12-04 19:45:34'),
(4, 'test card', '<p>test from view</p>', '0', '<p>view hint test</p>', '<p>view question test mod</p>', '<p>view answer test</p>', 0, 2, '2014-11-19 16:12:12', '2014-12-04 19:39:51'),
(5, 'test card2', '', '0', '', '', '', 0, 2, '2014-11-19 16:12:29', '2014-12-04 19:41:16'),
(6, 'test21', '<p>&nbsp;3rd card for set</p>', '0', '<p>&nbsp;3rd card hint</p>', '<p>&nbsp;3rd card question</p>', '<p>&nbsp;3rd card answer</p>', 0, -1, '2014-11-20 02:29:12', '2014-11-25 08:24:52'),
(7, 'fromView', '<p>tesst</p>', '0', '', '', '', 0, 2, '2014-11-21 22:25:16', '2014-12-04 19:01:55'),
(8, 'scott set fromView', '<p>adding card from view</p>\n<p>modified card</p>', '0', '<p>scott set viewh</p>', '<p>scott set viewq</p>', '<p>scott set viewa</p>', 0, 3, '2014-11-21 22:28:11', '2014-11-29 20:16:27'),
(9, 'scott set fromView2', 'adding card2 from view', '0', 'scott set viewh2', 'scott set viewq2', 'scott set viewa2', 0, 3, '2014-11-21 22:29:46', '2014-11-21 22:29:46'),
(10, 'scott set fromView3', 'adding card3 from view', '1-4', 'scott set viewh3', 'scott set viewq3', 'scott set viewa3', 0, 3, '2014-11-21 22:31:23', '2014-11-21 22:31:23'),
(11, 'scott set fromView4', 'adding card4 from view', '1-4', 'scott set viewh4', 'scott set viewq4', 'scott set viewa4', 0, 3, '2014-11-21 22:31:45', '2014-11-21 22:31:45'),
(12, 'another card', 'test again', '2-5', 'this h', 'this q', 'this a', 0, 2, '2014-11-22 00:27:45', '2014-11-22 00:27:45'),
(13, 'space1', 'space 1', '10', 'Space h1', 'space q1', 'Space a1', 0, 6, '2014-11-22 14:18:57', '2014-11-22 14:18:57'),
(14, '', '', '0', '', 'Ifire', '', 0, -1, '2014-11-23 17:18:39', '2014-11-23 17:18:39'),
(15, '', '', '0', 'Riddle', 'Ifire', 'Smoke', 0, -1, '2014-11-23 17:19:16', '2014-11-23 17:19:16'),
(16, '', '', '0', 'X = +', '6 x 6 =', '12', 0, 2, '2014-11-23 20:51:09', '2014-11-23 20:51:09'),
(17, '', '', '0', 'ICEHAG', 'moons of Jupiter', 'Io, Callisto, Europa, Himalaya, Amalthea and Ganymede', 0, -1, '2014-11-23 20:55:01', '2014-11-23 20:55:01'),
(18, '', '', '0', 'ICEHAG', 'moons of Jupiter', 'Io, Callisto, Europa, Himalaya, Amalthea and Ganymede', 0, -1, '2014-11-23 20:55:45', '2014-11-23 20:55:45'),
(19, '', '', '0', 'ICEHAG', 'moons of Jupiter', 'Io, Callisto, Europa, Himalaya, Amalthea and Ganymede', 0, -1, '2014-11-23 20:57:48', '2014-11-23 20:57:48'),
(20, 'Mars', '', '10', '<p>Fear, Dread</p>', '<p>Moons of Mars</p>', '<p>Phoebos, Deimos</p>', 0, 7, '2014-11-23 20:57:50', '2014-11-27 00:38:32'),
(21, '', '', '0', 'ICEHAG', 'moons of Jupiter', 'Io, Callisto, Europa, Himalaya, Amalthea and Ganymede', 0, -1, '2014-11-23 20:58:16', '2014-11-23 20:58:16'),
(22, 'Jupiter', '', '0', '<p>ICEHAG (acrostic)</p>', '<p>Moons of Jupiter</p>', '<p>Io, Calisto, Europa, Himalia, Amalthea, Ganymede</p>', 0, 7, '2014-11-23 21:02:20', '2014-12-07 04:50:53'),
(23, '', '', '0', 'multiply', '6 x 6 =', '36', 0, -1, '2014-11-23 21:02:20', '2014-11-23 21:02:20'),
(24, 'Saturn', '', '0', '<p>TIRED (acrostic)</p>\n<p>Three pairs</p>\n<p>JAPP (acrostic)</p>\n<p>HPH (acrostic)</p>', '<p>Moons of Saturn</p>', '<p>Titan, Iapetus, Rhea, Enceladus, Dione</p>\n<p>Tethys, Mimas, Telesto, Calypso, Epimethius, Prometheus</p>\n<p>Janus, Atlas, Pan, Pandora</p>\n<p>Hyperion, Phoebe, Helene</p>', 0, 7, '2014-11-23 21:03:08', '2014-12-07 05:18:30'),
(25, '', '', '0', 'multiply', '7 x 7 =', '49', 0, -1, '2014-11-23 21:05:36', '2014-11-23 21:05:36'),
(26, 'Uranus', '', '0', '<p>OTUAMP (acrostic)</p>\n<p>2Bs, 3Cs, and a D</p>\n<p>JORP (acrostic)</p>\n<p>CuSP (acrostic)</p>', '<p>Moons of Uranus</p>', '<p>Oberon, Titania, Umbriel, Ariel, Miranda, Puck</p>\n<p>Bianca, Belinda, Caliban, Cordelia, Cressida, Desdemona</p>\n<p>Juliet, Ophelia, Rosalind, Portia</p>\n<p>Cupid, Sycorax, Prospero</p>', 0, 7, '2014-11-23 21:10:04', '2014-12-07 04:59:49'),
(27, 'Neptune', '', '0', '<p>5 in list</p>\n<p>PLeaSeD (acrostic)</p>', '<p>Moons of Neptune</p>', '<p>Triton, Naiad, Nereid, Thalassa, Galatea,</p>\n<p>Proteus, Larissa, Sao, Despina</p>', 0, 7, '2014-11-27 00:45:02', '2014-12-07 04:53:42'),
(28, 'Pluto', '', '0', '<p>CHiNKS (acrostic)</p>', '<p>Moons of Pluto</p>', '<p>Charon, Hydra, Nyx, Kerberos, Styx</p>', 0, 7, '2014-11-27 00:45:08', '2014-12-07 04:43:26'),
(29, '', '', '0', '<p>reversible acetylcholinesterase inhibitor</p>', '<p>edrophonium</p>', '<p>tensilon</p>', 0, 8, '2014-11-29 18:58:55', '2014-11-29 18:58:55'),
(30, 'Circle Formula', '', '0', '', '<p>sin*sin x + cos*cos x =</p>', '<p>1</p>', 0, 11, '2014-11-29 19:01:43', '1970-01-01 00:00:00'),
(31, 'Euler Equation', '', '0', '', '<p>Euler Equation/Identity:</p>', '<p>E^ (-i*pi) = 0</p>', 0, -1, '2014-11-29 19:03:09', '2014-12-06 22:23:25'),
(32, 'Marine Nav test card', 'Marine Nav test card', '0', 'Marine hint', 'marine question', 'Marine answer', 0, 13, '2014-11-29 19:40:08', '2014-11-29 19:41:20'),
(33, 'Sphere area', '', '0', '', '<p>Formula for area of a sphere''s surface =</p>', '<p>4 pi r squared,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; or&nbsp;&nbsp; 4 * PI * R^2</p>', 0, 11, '2014-11-30 00:52:52', '2014-12-07 05:27:11'),
(34, 'Sphere volume', '', '0', '', '<p>Formula for volume of a sphere =</p>', '<p>4/3 * PI * R^3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; or 4/3*PI*R*R*R</p>', 0, 11, '2014-11-30 00:54:00', '2014-11-30 00:54:00'),
(35, '1', '', '18', '', '<p>Capital of Alaska</p>', '<p>Juneau</p>', 0, -1, '2014-11-30 01:01:17', '2015-04-24 21:33:03'),
(36, '2', '', '18', '', '<p>Capital of Alaska</p>', '<p>Juneau</p>', 0, -1, '2014-11-30 01:02:17', '2014-12-21 21:22:32'),
(37, '1', '', '0', '', '', '', 0, -1, '2014-12-04 11:10:06', '2014-12-04 11:11:19'),
(38, '1', '', '0', '', '<p>1st president</p>', '<p>George Washington</p>', 0, 19, '2014-12-04 11:11:41', '2015-01-25 02:06:46'),
(39, '', '', '17', '', 'FIrst President', 'George Washington', 0, -1, '2014-12-04 11:11:54', '2014-12-04 12:02:50'),
(40, '2', '<p>2nd president</p>', '0', '', '<p>Second president</p>', '<p>John Q Adams</p>', 0, 19, '2014-12-04 12:37:26', '2015-04-06 00:57:09'),
(41, '3', '<p>3rd president</p>', '0', '<p>Signed the Declaration of</p>', '<p>Who was the third president of the US?</p>', '<p>Thomas Jefferson</p>', 0, 19, '2014-12-04 14:36:11', '2014-12-04 14:36:11'),
(42, '4', '<p>4th President</p>', '0', '<p>Try this picture cue:</p>\n<p>Friend <span style="text-decoration: underline;">James</span> walking through <span style="text-decoration: underline;">door</span> (for "four") on <span style="text-decoration: underline;">Madison</span> Ave.</p>', '<p>Who was the 4th US President?</p>', '<p>James Madison</p>', 0, 19, '2014-12-05 00:26:24', '2014-12-05 00:26:24'),
(43, '5', '', '0', '', '<p>5th president</p>', '<p>James Monroe</p>', 0, 19, '2014-12-05 00:27:27', '2015-01-25 02:07:42'),
(44, '6', '6th president', '15-15-17', '', 'Who was the sixth US president?', 'John Adams', 0, -1, '2014-12-06 21:11:47', '2014-12-06 21:11:47'),
(45, '6', '6th president', '15-15-17', '', 'Who was the sixth US president?', 'John Adams', 0, -1, '2014-12-06 21:11:54', '2014-12-06 21:11:54'),
(46, '6', '', '15-17', '', '<p>Who was the sixth US president?</p>', '<p>John Adams.</p>', 0, 19, '2014-12-06 21:13:53', '2014-12-06 21:13:53'),
(47, '7', '', '15-17', '<p>Number of letters in his last name</p>', '<p>Who was the seventh US president?</p>', '<p>Andrew Jackson</p>', 0, -1, '2014-12-06 21:15:47', '2015-01-04 00:18:30'),
(48, '8', '', '15-17', '<p>Number of letters in his last name (Van Buren)</p>', '<p>Who was the eighth US president?</p>', '<p>Martin Van Buren</p>', 0, 19, '2014-12-06 21:15:59', '2014-12-07 04:21:36'),
(49, 'Largest Moons', '', '0', '', '<p>What are the seven largest moons of the Solar System?</p>', '<p>1. Ganymede (Jupiter, main/Galilean group)</p>\r\n<p>2. Titan (Saturn)</p>\r\n<p>3. Callisto (Jupiter, main/Galilean group)</p>\r\n<p>4. Io (Jupiter, main/Galilean group)</p>\r\n<p>moon</p>\r\n<p>Europa</p>\r\n<p>Triton</p>\r\n<p>Titania</p>\r\n<p>Rhea</p>\r\n<p>Oberon</p>\r\n<p>Iapetus</p>\r\n<p>Charon</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>5. Amalthea (Jupiter, inner/Amalthea group)</p>\r\n<p>6. Europa (Jupiter, main/Galilean group)</p>\r\n<p>7. Thebe (Jupiter, inner/Amalthea group) &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>', 0, 7, '2014-12-06 21:29:00', '2015-01-04 04:10:19'),
(50, 'compare moon/planet', '', '0', '', '<p>What''s larger, the planet Mercury or the moon of Jupiter, Ganymede?</p>', '<p>Ganymede</p>', 0, 7, '2014-12-06 21:45:50', '2014-12-06 21:45:50'),
(51, '0', '0', '0', '0', '0', '0', 0, 0, '2014-12-06 22:24:45', '2014-12-06 22:24:45'),
(52, '0', '0', '0', '0', '0', '0', 0, 0, '2014-12-06 22:24:48', '2014-12-06 22:24:48'),
(53, '0', '0', '0', '0', '0', '0', 0, 0, '2014-12-06 22:24:48', '2014-12-06 22:24:48'),
(54, 'cylinder', '', '0', '', '<p>What is the volume of a cylinder?</p>', '<p>height X pi X radius squared</p>\n<p>or</p>\n<p>H * Pi * R^2</p>', 0, 11, '2014-12-06 22:27:12', '2014-12-07 05:33:22'),
(55, 'cone', '', '0', '', '<p>What is the volume of a cylinder?</p>', '<p>height X pi X radius squared</p>\n<p>or</p>\n<p>H * Pi * R^2</p>', 0, 11, '2014-12-06 22:28:11', '2014-12-07 05:33:45'),
(56, '', '', '0', '', '<p>What is the volume of a cone?</p>', '<p>(height X pi X radius squared)/3</p>\n<p>or</p>\n<p>(H * Pi * R^2)/3</p>', 0, -1, '2014-12-06 22:28:11', '2014-12-06 22:28:33'),
(57, '3', '', '18', '<p>An ARK they SAW sitting on a </p>\n<p>LITTLE ROCK</p>\n<p>&nbsp;</p>', '<p>Capital of Arkansas</p>', '<p>Little Rock</p>', 0, -1, '2014-12-06 22:35:11', '2015-04-24 21:33:15'),
(58, '', '', '0', '', '<p>arm</p>', '<p>brazo</p>', 0, 15, '2014-12-06 22:39:26', '2014-12-06 22:39:26'),
(59, '', '', '0', '', '<p>leg</p>', '<p>pierna</p>', 0, 15, '2014-12-06 22:39:59', '2014-12-06 22:39:59'),
(60, '', '', '0', '', '<p>eye</p>', '<p>ojo</p>', 0, 15, '2014-12-06 22:40:23', '2014-12-06 22:40:23'),
(61, '', '', '0', '', '<p>finger</p>', '<p>dedo</p>', 0, 15, '2014-12-06 22:40:48', '2014-12-06 22:40:48'),
(62, '', '', '0', '', '<p>mouth</p>', '<p>boca</p>', 0, 15, '2014-12-06 22:40:58', '2014-12-07 05:46:04'),
(63, '', '', '0', '', '<p>cheek</p>', '<p>mejilla</p>', 0, 15, '2014-12-06 22:42:05', '2014-12-06 22:42:05'),
(64, '', '', '0', '', '<p>beard</p>', '<p>barba</p>', 0, 15, '2014-12-06 22:42:24', '2014-12-06 22:42:24'),
(65, '', '', '0', '', '<p>hair</p>', '<p>pelo</p>', 0, 15, '2014-12-06 22:42:53', '2014-12-06 22:42:53'),
(66, '', '', '0', '', '<p>face</p>', '<p>cara</p>', 0, 15, '2014-12-06 22:43:19', '2014-12-06 22:43:19'),
(67, '', '', '0', '', '<p>ear</p>', '<p>oreja</p>', 0, 15, '2014-12-06 22:43:21', '2014-12-07 05:48:36'),
(68, '', '', '0', '', '<p>stomach</p>', '<p>estomaco</p>', 0, 15, '2014-12-06 22:47:09', '2014-12-06 22:47:09'),
(69, '', '', '0', '', '<p>back</p>', '<p>espalda</p>', 0, 15, '2014-12-06 22:47:28', '2014-12-06 22:47:28'),
(70, '', '', '0', '', '<p>neck</p>', '<p>cuello</p>', 0, 15, '2014-12-06 22:47:49', '2014-12-06 22:47:49'),
(71, '', '', '0', '', '<p>shoulder</p>', '<p>hambre</p>', 0, 15, '2014-12-06 22:48:09', '2014-12-06 22:48:09'),
(72, '', '', '0', '', '<p>hip</p>', '<p>cuerpo</p>', 0, 15, '2014-12-06 22:48:24', '2014-12-06 22:48:24'),
(73, '', '', '0', '<p>First 2 letters the same</p>\n<p>&nbsp;</p>', '<p>Burundi</p>', '<p>Bujumbura</p>', 0, 16, '2014-12-06 22:50:36', '2015-04-26 00:31:25'),
(74, '', '', '0', '', '<p>Burkina Faso</p>', '<p>Ouagodougou</p>', 0, 16, '2014-12-06 22:51:07', '2014-12-06 22:51:07'),
(75, '', '', '0', '<p>The OCRA is GONE</p>\n<p>&nbsp;</p>', '<p>Ghana</p>', '<p>Accra</p>', 0, -1, '2014-12-06 22:51:26', '2015-04-25 17:32:35'),
(76, '', '', '0', '', '<p>Benin</p>', '<p>Porto Novo</p>', 0, -1, '2014-12-06 22:51:53', '2015-04-25 17:31:44'),
(77, '', '', '0', '<p>The C.A.R. explodes with a BANG-ui</p>', '<p>Central African Republic</p>', '<p>Bangui</p>', 0, 16, '2014-12-06 22:52:22', '2015-04-26 00:32:14'),
(78, '', '', '0', '', '<p>Somalia</p>', '<p>Mogadishu</p>', 0, 16, '2014-12-06 22:52:45', '2014-12-06 22:52:45'),
(79, '', '', '0', '', '<p>Sudan</p>', '<p>Khartoum</p>', 0, 16, '2014-12-06 22:53:08', '2014-12-06 22:53:08'),
(80, '', '', '0', '', '<p>Eritrea</p>', '<p>Asmara</p>', 0, 16, '2014-12-06 22:53:28', '2014-12-06 22:53:28'),
(81, '', '', '0', '', '<p>Rwanda</p>', '<p>Kigali</p>', 0, 16, '2014-12-06 22:54:26', '2014-12-06 22:54:26'),
(82, '', '', '0', '', '<p>Kenya</p>', '<p>Nairobi</p>', 0, 16, '2014-12-06 22:54:43', '2014-12-06 22:54:43'),
(83, '', '', '0', '<p>camping in you-CAN-do</p>\n<p>&nbsp;</p>', '<p>Uganda</p>', '<p>Kampala</p>', 0, 16, '2014-12-06 22:55:00', '2015-04-12 04:58:15'),
(84, '', '', '0', '', '<p>Cameroon</p>', '<p>Yaounde</p>', 0, 16, '2014-12-06 22:55:35', '2014-12-06 22:55:35'),
(85, '', '', '0', '', '<p>Algeria</p>', '<p>Algiers</p>', 0, 16, '2014-12-06 22:55:59', '2014-12-06 22:55:59'),
(86, '', '', '0', '', '<p>Tunisia</p>', '<p>Tunis</p>', 0, 16, '2014-12-06 22:56:18', '2014-12-06 22:56:18'),
(87, '', '', '0', '', '<p>Cote d''Ivoire</p>', '<p>Yamosoukro</p>', 0, 16, '2014-12-06 22:57:05', '2014-12-06 22:57:05'),
(88, '', '', '0', '', '<p>Liberia</p>', '<p>Monrovia</p>', 0, 16, '2014-12-06 22:57:39', '2015-04-26 00:09:03'),
(89, '', '', '0', '', '<p>Comoros Islands</p>', '<p>Moroni</p>', 0, 16, '2014-12-06 22:58:03', '2014-12-06 22:58:03'),
(90, '', '', '0', '', '<p>Namibia</p>', '<p>Windhoek</p>', 0, 16, '2014-12-06 22:58:53', '2014-12-06 22:58:53'),
(91, '', '', '0', '', '<p>Lesotho</p>', '<p>Maseru</p>', 0, 16, '2014-12-06 22:59:15', '2014-12-06 22:59:15'),
(92, '', '', '0', '', '<p>Madagascar</p>', '<p>Antananarivo</p>', 0, 16, '2014-12-06 22:59:39', '2014-12-06 22:59:39'),
(93, '', '', '0', '', '<p>Mozambique</p>', '<p>Maputo</p>', 0, 16, '2014-12-06 23:00:23', '2014-12-06 23:00:23'),
(94, '', '', '0', '', '<p>Mauritania</p>', '<p>Nouakchott</p>', 0, 16, '2014-12-06 23:00:46', '2014-12-06 23:00:46'),
(95, '', '', '0', '', '<p>Name the LEPTON group</p>', '<p>Electron, muon, tau particles</p>\n<p>Neutrinos (3 subtypes:&nbsp; electron, muon, tau)</p>', 0, 17, '2014-12-06 23:09:52', '2014-12-06 23:09:52'),
(96, '', '', '0', '', '<p>Name the BOSONs</p>', '<p>Weakons (weak nuclear force)</p>\n<p>Gluons (strong nuclear force)</p>\n<p>Photons (electromagnetic force)</p>\n<p>Gravitons (gravitational force)</p>', 0, 17, '2014-12-06 23:10:55', '2014-12-06 23:10:55'),
(97, '', '', '0', '', '<p>What are the six quark types?</p>', '<p>Up, down (normal matter)</p>\n<p>Charm, strange</p>\n<p>Bottom, top</p>', 0, 17, '2014-12-06 23:11:50', '2014-12-06 23:11:50'),
(98, '', '', '0', '', '<p>What are the baryons nd what are they made of?</p>', '<p>Neutrons (two down and one up quark)</p>\n<p>Protons (two up and one down quark)</p>\n<p>&nbsp;</p>', 0, 17, '2014-12-06 23:12:57', '2014-12-29 04:43:44'),
(99, '', '', '0', '', '<p>What are the hadrons and what are they made of?</p>', '<p>Baryons (made of 3 quarks: neutrons, protons)</p>\n<p>Mesons (made of 2 bosos; includes W and Z)</p>\n<p>&nbsp;</p>', 0, 17, '2014-12-06 23:15:59', '2014-12-29 04:45:09'),
(100, '', '', '0', '', '<p>What do gluons do?</p>', '<p>Hold together quarks inside hadrons (such as neutrons and protons)</p>\n<p>&nbsp;</p>', 0, 17, '2014-12-06 23:17:06', '2014-12-06 23:17:06'),
(101, '', '', '0', '', '<p>Oldest dynasty</p>', '<p>Xia</p>', 0, 18, '2014-12-06 23:18:30', '2014-12-06 23:18:30'),
(102, '', '', '0', '', '<p>2100 BC to 1200 BC</p>', '<p>Shang dynasty</p>', 0, 18, '2014-12-06 23:20:22', '2014-12-06 23:20:22'),
(103, '', '', '0', '', '<p>Which dynasty lasted from 1200 BC to 250 BC and unified China for the first time?</p>', '<p>Zhou Dynasty</p>', 0, 18, '2014-12-06 23:21:05', '2014-12-06 23:21:05'),
(104, '', '', '0', '', '<p>Hebrews 4:12</p>', '<p>The Word of God is living and active, sharper than any two-edged sword, piercing to the division of soul and spirit, of joints and marrow, and is a discerner of the thoughts and intents of the heart.</p>', 0, 20, '2014-12-08 00:11:05', '2014-12-08 00:11:05'),
(105, '', '', '0', '', '<p>Isaiah 55:11</p>', '<p>So shall My word be that proceeds forth from out of My mouth; it shall not return to Me void, but shall accomplish that which I purpose, and shall prosper in that whereto I sent it.</p>', 0, 20, '2014-12-08 00:12:23', '2014-12-08 00:12:23'),
(106, '', '', '0', '', '<p>Isaiah 26:3</p>', '<p>You will keep him in perfect peace whose mind is stayed on You, because he trusts in You.</p>', 0, 20, '2014-12-08 00:13:10', '2014-12-08 00:13:10'),
(107, '', '', '0', '', '<p>Acts 4:12</p>', '<p>Nor is there salvation in any other, for there is no other name under heaven given among men by which we must be saved.</p>', 0, 20, '2014-12-08 00:15:09', '2014-12-08 07:20:15'),
(108, '', '', '0', '', '<p>Romans 12:1-2</p>', '<p>12:1 - I beseech you therefore, brethren, by the mercies of God, to present your bodies a living sacrifice, holy and acceptable to God, which is your reasonable service. 2: Do not be conformed to this world, but be transformed by the renewing of your minds, so that you may prove what is that good, acceptable and perfect will of God.</p>', 0, 20, '2014-12-08 00:17:24', '2014-12-08 00:17:24'),
(109, '', '', '0', '', '<p>Phillipians 4:6-7</p>', '<p>6 - Be anxious for nothing, but in everything by prayer and supplication with thanksgiving, make your requests known to God. 7- And the peace of God which passes all understanding will keep your hearts and your minds in Christ Jesus.</p>', 0, 20, '2014-12-08 00:19:05', '2014-12-08 00:19:05'),
(110, '', '', '0', '', '<p>Micah 6:8</p>', '<p>He has shown you, O man, what is good.&nbsp; And what does the Lord require of you, but to do justice, to love kindness, and to walk humbly with your God?</p>', 0, 20, '2014-12-08 00:20:16', '2014-12-08 00:20:16'),
(111, '', '', '0', '<p>Acrostic (capital letters below):</p>\n<p>RuSseL''s JaZzy ID GaiNed him a JoB.</p>', '<p>The Twelve Sons (later tribes) of Israel.</p>', '<p>Judah, Zebulun, Issachar,</p>\n<p>Dan, Gad, Asher</p>\n<p>Naphtali, Joseph, Benjamin</p>', 0, 21, '2014-12-08 00:26:37', '2014-12-08 00:26:37'),
(112, '', '', '0', '', '<p>Proverbs 3:5-6</p>', '<p>Trust in the Lord with all your heart,</p>\n<p>And lean not unto your own understanding.</p>\n<p>In all your ways acknowledge Him,</p>\n<p>And He will direct your path.</p>', 0, 20, '2014-12-13 23:43:15', '2014-12-13 23:43:15'),
(113, '', '', '0', '', '<p>2 Timothy 3:16</p>', '<p>All scripture is given by inspiration of God and is profitable for doctrine, for reproof, for correction, and for instruction in righteousness</p>', 0, 20, '2014-12-14 15:23:49', '2014-12-14 15:23:49'),
(114, '', '', '0', '', '<p>2 Peter 1:5-8</p>', '<p>But also for this very reason, giving all diligence, add to your faith virtue, to virtue knowledge, to knowledge self-control, to self-control perseverance, to perseverance godliness, to godliness brotherly kindness, and to brotherly kindness, love.&nbsp; For it these things are yours and abound, you will be neither barren or unfruitful in the knowledge of our Lord Jesus Christ.</p>', 0, 20, '2014-12-14 15:27:41', '2014-12-14 15:27:41'),
(115, '2', '', '0', '', '<p>Alabama</p>', '<p>Montgomery</p>', 0, -1, '2014-12-21 21:23:16', '2015-04-24 21:33:50'),
(116, '4', '', '0', '', '<p>California</p>', '<p>Sacramento</p>', 0, -1, '2014-12-21 21:23:55', '2015-04-24 21:34:13'),
(117, '5', '', '0', '', '<p>Colorado</p>', '<p>Denver</p>', 0, 14, '2014-12-21 21:24:35', '2014-12-21 21:24:35'),
(118, '1', '', '0', '', '<p>1</p>', '<p>Hydrogen</p>', 0, 23, '2014-12-25 20:13:53', '2014-12-25 20:13:53'),
(119, '', '', '0', '', '<p>2</p>', '<p>Helium</p>', 0, 23, '2014-12-25 20:14:16', '2014-12-25 20:14:16'),
(120, '', '', '0', '', '<p>3</p>', '<p>Lithium</p>', 0, 23, '2014-12-25 20:14:44', '2014-12-25 20:14:44'),
(121, '', '', '0', '', '<p>4</p>', '<p>Berrylium</p>', 0, 23, '2014-12-25 20:15:08', '2014-12-25 20:15:08'),
(122, '', '', '0', '', '<p>5</p>', '<p>Boron</p>', 0, 23, '2014-12-25 20:15:32', '2014-12-25 20:15:32'),
(123, '', '', '0', '', '<p>6</p>', '<p>Oxygen</p>', 0, 23, '2014-12-25 20:15:47', '2014-12-25 20:15:47'),
(124, '', '', '0', '', '<p>7</p>', '<p>Nitrogen</p>', 0, 23, '2014-12-25 20:16:04', '2014-12-25 20:16:04'),
(125, '', '', '0', '', '<p>In a capacitor, which phase leads:&nbsp; voltage or current?</p>', '<p>Current leads voltage by 90 degrees</p>', 0, 22, '2014-12-25 20:24:00', '2014-12-25 20:24:00'),
(126, '', '', '0', '', '<p>In an inductor, which phase leads:&nbsp; voltage or current?</p>', '<p>Voltage leads current by 90 degrees</p>', 0, 22, '2014-12-25 20:24:33', '2014-12-25 20:24:33'),
(127, '', '', '0', '', '<p>What is impedance?</p>', '<p>The square root of inductance squared + capacitance squares.</p>', 0, 22, '2014-12-25 20:25:41', '2014-12-25 20:25:41'),
(128, '', '', '0', '', '<p>What is Kirchoff''s Current Law?</p>', '<p>The sum of the currents entering a node equals the sum of the current leaving the node</p>', 0, 22, '2014-12-25 20:28:53', '2014-12-25 20:28:53'),
(129, '', '', '0', '', '<p>Name 4 membrane currents</p>', '<p>Slow potassium current (Ik), sodium current (Na), T calcium channel, L calcium channel</p>', 0, 24, '2014-12-25 20:36:46', '2014-12-25 20:36:46'),
(130, '', '', '0', '', '<p>Two facts about mitochondria</p>', '<p>They have the electron transport chance in the cristae, and they carry mother''s DNA on (hence, there are 7 well-known mitochondrial diseases inherited from the mother).</p>', 0, 24, '2014-12-25 20:39:26', '2014-12-25 20:39:26'),
(131, '', '', '0', '', '<p>Next four dynasties after zhou</p>', '<p style="display: inline !important;">Xin, Eastern Ha&nbsp;</p>', 0, -1, '2014-12-28 21:48:06', '2014-12-28 21:49:03'),
(132, '', '', '0', '', '<p>Next four dynasties after zhou</p>', '<p style="display: inline !important;">Qin, Western Han</p>\n<p style="display: inline !important;">&nbsp;</p>\n<p style="display: inline !important;">Xin, Eastern Han&nbsp;</p>', 0, 18, '2014-12-28 21:48:46', '2014-12-28 21:48:46'),
(133, '', '', '0', '', '<p>After Eastern Han</p>', '<p style="display: inline !important;"><span class="Apple-style-span" style="font-family: sans-serif; font-size: medium;">Warring kingdoms</span></p>\n<p><span class="Apple-style-span" style="font-family: sans-serif; font-size: medium;">Wei, Shu, W</span></p>\n<p>&nbsp;</p>', 0, 18, '2014-12-28 21:50:38', '2014-12-28 21:50:38'),
(134, '', '', '0', '', '<p>Delaware</p>', '<p>Dover</p>', 0, -1, '2015-01-03 19:06:04', '2015-01-03 19:34:08'),
(135, '', '', '0', '<p>TALL HOUSes (tallahasse), ie skyscrapers,</p>\n<p>line the beaches of Florida</p>\n<p>&nbsp;</p>', '<p>Florida</p>', '<p>Tallahassee</p>', 0, 14, '2015-01-03 19:06:20', '2015-04-25 04:37:59'),
(136, '', '', '0', '', '<p>Georgia</p>', '<p>Atlanta</p>', 0, 14, '2015-01-03 19:06:42', '2015-01-03 19:06:42'),
(137, '', '', '0', '', '<p>Hawaii</p>', '<p>Honolulu</p>', 0, 14, '2015-01-03 19:07:24', '2015-01-03 19:07:24'),
(138, '', '', '0', '', '<p>Hawaii</p>', '<p>Honolulu</p>', 0, -1, '2015-01-03 19:07:24', '2015-01-03 19:08:59'),
(139, '', '', '0', '', '<p>Iowa</p>', '<p>Des Moines</p>', 0, 14, '2015-01-03 19:07:50', '2015-01-03 19:07:50'),
(140, '', '', '0', '', '<p>Illinois</p>', '<p>Springfield</p>', 0, 14, '2015-01-03 19:08:09', '2015-01-03 19:08:09'),
(141, '', '', '0', '', '<p>Indiana</p>', '<p>Indianapolis</p>', 0, 14, '2015-01-03 19:08:30', '2015-01-03 19:08:30'),
(142, '', '', '0', '', '<p>Kentucky</p>', '<p>Frankfort</p>', 0, 14, '2015-01-03 19:13:02', '2015-01-03 19:13:02'),
(143, '', '', '0', '', '<p>Louisiana</p>', '<p>Baton Rouge</p>', 0, 14, '2015-01-03 19:13:42', '2015-01-03 19:13:42'),
(144, '', '', '0', '<p>the MAIN ruler in Jesus'' time was AUGUSTus Caesar</p>', '<p>Maine</p>', '<p>Augusta</p>', 0, 14, '2015-01-03 19:14:06', '2015-04-13 00:33:46'),
(145, '', '', '0', '', '<p>Massachussetts</p>', '<p>Boston</p>', 0, 14, '2015-01-03 19:14:38', '2015-01-03 19:14:38'),
(146, '', '', '0', '', '<p>Mississippi</p>', '<p>Jackson</p>', 0, 14, '2015-01-03 19:14:57', '2015-01-03 19:14:57'),
(147, '', '', '0', '', '<p>Minnesota</p>', '<p>St Paul</p>', 0, 14, '2015-01-03 19:15:25', '2015-01-03 19:15:25'),
(148, '', '', '0', '', '<p>Missouri</p>', '<p>Jefferson City</p>', 0, 14, '2015-01-03 19:16:45', '2015-01-03 19:16:45'),
(149, '', '', '0', '', '<p>Michigan</p>', '<p>Ann Arbor</p>', 0, 14, '2015-01-03 19:17:12', '2015-01-03 19:17:12'),
(150, '', '', '0', '<p>HE SPOKE to IDA</p>', '<p>Idaho</p>', '<p>Spokane</p>', 0, 14, '2015-01-03 19:17:33', '2015-04-25 04:49:04'),
(151, '', '', '0', '<p>MOUNT ST HELENs</p>', '<p>Montana</p>', '<p>Helena</p>', 0, 14, '2015-01-03 19:18:03', '2015-04-25 04:48:32'),
(152, '', '', '0', '<p>CARS in SIN CITY, NEVADA</p>', '<p>Nevada</p>', '<p>Carson City</p>', 0, 14, '2015-01-03 19:18:45', '2015-04-25 04:48:12'),
(153, '', '', '0', '<p>President LINCOLN NEVERASKED her</p>', '<p>Nebraska</p>', '<p>Lincoln</p>', 0, 14, '2015-01-03 19:19:07', '2015-04-25 04:47:48'),
(154, '', '', '0', '<p>the BEES MARKED his COAT<br /><br /></p>', '<p>North Dakota</p>', '<p>Bismarck</p>', 0, 14, '2015-01-03 19:19:38', '2015-04-25 04:47:13'),
(155, '', '', '0', '<p>a RALLY in CAROLINE''s house</p>', '<p>North Carolina</p>', '<p>Raleigh</p>', 0, 14, '2015-01-03 19:20:25', '2015-04-25 04:46:26'),
(156, '', '', '0', '', '<p>Ohio</p>', '<p>Columbus</p>', 0, 14, '2015-01-03 19:20:51', '2015-01-03 19:20:51'),
(157, '', '', '0', '', '<p>Oregon</p>', '<p>Salem</p>', 0, 14, '2015-01-03 19:21:09', '2015-01-03 19:21:09'),
(158, '', '', '0', '<p>A merry land has an apple</p>', '<p>Maryland</p>', '<p>Annapolis</p>', 0, 14, '2015-01-03 19:21:46', '2015-04-12 04:59:14'),
(159, '', '', '0', '<p>Harry Pencils</p>\n<p>&nbsp;</p>', '<p>Pennsylvania</p>', '<p>Harrisburg</p>', 0, 14, '2015-01-03 19:22:16', '2015-04-25 04:41:43'),
(160, '', '', '0', '<p>PROVIDE a DENSE ROAD for the ISLAND<br /><br /></p>', '<p>Rhode Island</p>', '<p>Providence</p>', 0, 14, '2015-01-03 19:22:40', '2015-04-25 04:45:16'),
(161, '', '', '0', '<p>"I''ll be in" New York</p>', '<p>New York</p>', '<p>Albany</p>', 0, 14, '2015-01-03 19:23:29', '2015-04-25 04:43:56'),
(162, '', '', '0', '<p>Hamster riding on back of a Concorde jet</p>', '<p>New Hampshire</p>', '<p>Concorde</p>', 0, 14, '2015-01-03 19:23:50', '2015-04-25 04:44:19'),
(163, '', '', '0', '', '<p>New Mexico</p>', '<p>Santa Fe</p>', 0, 14, '2015-01-03 19:24:13', '2015-01-03 19:24:13'),
(164, '', '', '0', '', '<p>South Dakota</p>', '<p>Pierre</p>', 0, 14, '2015-01-03 19:24:53', '2015-01-03 19:24:53'),
(165, '', '', '0', '', '<p>South Carolina</p>', '<p>Columbia</p>', 0, 14, '2015-01-03 19:25:48', '2015-01-03 19:25:48'),
(166, '', '', '0', '', '<p>Texas</p>', '<p>Austin</p>', 0, 14, '2015-01-03 19:26:27', '2015-01-03 19:26:27'),
(167, '', '', '0', '', '<p>Utah</p>', '<p>Salt Lake City</p>', 0, 14, '2015-01-03 19:26:57', '2015-01-03 19:26:57'),
(168, '', '', '0', '', '<p>Vermont</p>', '<p>Montpelier</p>', 0, 14, '2015-01-03 19:27:38', '2015-01-03 19:27:38'),
(169, '', '', '0', '', '<p>Virginia</p>', '<p>Richmond</p>', 0, 14, '2015-01-03 19:28:35', '2015-01-03 19:28:35'),
(170, '', '', '0', '', '<p>New Jersey</p>', '<p>Trenton</p>', 0, 14, '2015-01-03 19:29:07', '2015-01-03 19:29:07'),
(171, '', '', '0', '', '<p>Washington</p>', '<p>Olympia</p>', 0, 14, '2015-01-03 19:29:48', '2015-01-03 19:29:48'),
(172, '', '', '0', '', '<p>Arizona</p>', '<p>Phoenix</p>', 0, 14, '2015-01-03 19:30:46', '2015-01-03 19:30:46'),
(173, '', '', '0', '', '<p>Oklahoma</p>', '<p>Oklahoma City</p>', 0, 14, '2015-01-03 19:32:53', '2015-01-03 19:32:53'),
(174, '', '', '0', '', '<p>Kansas</p>', '<p>Topeka</p>', 0, 14, '2015-01-03 19:33:20', '2015-01-03 19:33:20'),
(175, '', '', '0', '', '<p>Delaware</p>', '<p>Dover</p>', 0, 14, '2015-01-03 19:33:24', '2015-04-25 03:49:22'),
(176, '', '', '0', '', '<p>9th president</p>', '<p>William Harrison</p>', 0, 19, '2015-01-03 19:46:07', '2015-01-03 19:46:07'),
(177, '', '', '0', '', '<p>10th president</p>', '<p>John Tyler</p>', 0, 19, '2015-01-03 19:46:58', '2015-01-03 19:46:58'),
(178, '', '', '0', '', '<p>11th president</p>', '<p>James Polk</p>', 0, 19, '2015-01-03 19:47:30', '2015-01-03 19:47:30'),
(179, '', '', '0', '', '<p>12th president</p>', '<p>Zachary Taylor</p>', 0, 19, '2015-01-03 19:47:55', '2015-01-03 19:47:55'),
(180, '', '', '0', '', '<p>13th president</p>', '<p>Millard Fillmore</p>', 0, 19, '2015-01-03 19:48:26', '2015-01-03 19:48:26'),
(181, '', '', '0', '', '<p>14th president</p>', '<p>Franklin Pierce</p>', 0, 19, '2015-01-03 19:48:50', '2015-01-03 19:48:50'),
(182, '', '', '0', '', '<p>15th president</p>', '<p>James Buchanan</p>', 0, 19, '2015-01-03 19:49:26', '2015-01-03 19:49:26'),
(183, '', '', '0', '', '<p>16th president</p>', '<p>Abraham Lincoln</p>', 0, 19, '2015-01-03 19:49:43', '2015-01-03 19:49:43'),
(184, '', '', '0', '', '<p>17th president</p>', '<p>Andrew Johnson</p>', 0, 19, '2015-01-03 19:50:08', '2015-01-03 19:50:08'),
(185, '', '', '0', '', '<p>18th president</p>', '<p>Ulysses Grant</p>', 0, 19, '2015-01-03 19:50:25', '2015-01-03 19:50:25'),
(186, '', '', '0', '', '<p>19th president</p>', '<p>Rutherford B. Hayes</p>', 0, 19, '2015-01-03 19:51:04', '2015-01-03 19:51:04'),
(187, '', '', '0', '', '<p>20th president</p>', '<p>James Garfield</p>', 0, 19, '2015-01-03 19:51:35', '2015-01-03 19:51:35'),
(188, '', '', '0', '', '<p>21st president</p>', '<p>Chester Arthur</p>', 0, 19, '2015-01-03 19:51:53', '2015-01-03 19:51:53'),
(189, '', '', '0', '', '<p>22nd president</p>', '<p>Grover Cleveland</p>', 0, 19, '2015-01-03 19:52:26', '2015-01-03 19:52:26'),
(190, '', '', '0', '<p>Harry Ben franklin</p>\n<p>&nbsp;</p>', '<p>23rd president</p>', '<p>Benjamin Harrison</p>', 0, 19, '2015-01-03 19:52:44', '2015-04-12 04:42:54'),
(191, '', '', '0', '', '<p>24th president</p>', '<p>Grover Cleveland</p>', 0, 19, '2015-01-03 19:53:08', '2015-01-03 19:53:08'),
(192, '', '', '0', '', '<p>25th president</p>', '<p>William McKinley</p>', 0, 19, '2015-01-03 19:53:24', '2015-01-03 19:53:24'),
(193, '', '', '0', '', '<p>26th president</p>', '<p>Theodore Roosevelt</p>', 0, 19, '2015-01-04 00:15:30', '2015-01-04 00:15:30'),
(194, '', '', '0', '', '<p>26th president</p>', '<p>Theodore Roosevelt</p>', 0, -1, '2015-01-04 00:15:30', '2015-04-19 18:15:45'),
(195, '', '', '0', '', '<p>27th president</p>', '<p>William Taft</p>', 0, 19, '2015-01-04 00:15:52', '2015-01-04 00:15:52'),
(196, '', '', '0', '', '<p>28th president</p>', '<p>Woodrow Wilson</p>', 0, 19, '2015-01-04 00:16:14', '2015-01-04 00:16:14'),
(197, '', '', '0', '', '<p>29th president</p>', '<p>Warren Harding</p>', 0, 19, '2015-01-04 00:17:17', '2015-01-04 00:17:17'),
(198, '', '', '0', '', '<p>30th president</p>', '<p>Calvin Coolidge</p>', 0, 19, '2015-01-04 00:17:56', '2015-01-04 00:17:56'),
(199, '', '', '0', '', '<p>A</p>', '<h1>.-</h1>', 0, 25, '2015-01-04 00:20:15', '2015-04-20 01:35:48'),
(200, '', '', '0', '', '<p>B</p>', '<h1>-...</h1>', 0, 25, '2015-01-04 00:20:38', '2015-04-20 01:35:33'),
(201, '', '', '0', '', '<p>The Twelve Apostles/Disciples of Jesus</p>', '<p>Simon Peter, James, John</p>\n<p>Andrew, Levi (Matthew), Bartholemew</p>\n<p>James (of Alphaeus), Judas (Thaddeus),</p>\n<p>Simon the zealot,</p>', 0, 21, '2015-01-04 00:27:59', '2015-01-04 00:27:59'),
(202, '', '', '0', '', '<p>The Twelve Apostles/Disciples of Jesus</p>', '<p>Simon Peter, James, John</p>\n<p>Andrew, Levi (Matthew), Bartholemew</p>\n<p>James (of Alphaeus), Judas (Thaddeus),</p>\n<p>Simon the zealot,</p>', 0, 21, '2015-01-04 00:28:05', '2015-01-04 00:28:05'),
(203, '', '', '0', '', '<p>List five agencies that have long-term track records of helping with disaster relief as well as long-term development assistance.</p>', '<p>CARE</p>\n<p>Food for the Poor</p>\n<p>Gospel for Asia</p>\n<p>World Concern</p>\n<p>World Vision</p>', 0, 26, '2015-01-04 12:41:56', '2015-01-04 12:41:56'),
(204, '', '', '0', '', '<p>List agencies that help with appropriate technologies to the poor, and what they provide.</p>', '<p>Azuri - provides clean solar power, 8hr/day, for light, cell phones, etc - via weekly "cards" that start the user saving 50% of their energy costs immediately, freeing them from the unhealthy and dangerous kerosene lamps used</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>Charity: Water - provides clean water via many techniques in 22 countries</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>Kickstart - provides the "MoneyMaker Hip Pump" allowing a person to irrigate large areas of land and increase crop yields 3-5 X</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<p>Kiva - microfinance made easy, by giving 0 interest $25 incremental loans to 3rd world entrepreneurs</p>\n<p>&nbsp;</p>\n<p>Women for Women, International - helps women break out of cycle of abuse and empowers them to be self-supporting, strong presence in Africa</p>', 0, 26, '2015-01-04 12:42:30', '2015-01-04 20:14:28'),
(205, '', '', '0', '', '<p>List agencies that help with appropriate technologies to the poor, and what they provide.</p>', '<p>Azuri -</p>\n<p>Charity: Water -</p>\n<p>Kickstart</p>\n<p>Kiva</p>\n<p>&nbsp;</p>', 0, -1, '2015-01-04 12:44:24', '2015-01-04 12:47:48'),
(206, '', '', '0', '', '<p>What are five foundational Bible principles?</p>', '<p>1. There is a creator God, all-knowing and all- powerful, who is perfectly loving, holy and just.</p>\n<p>2. We are created beings and have made selfish decisions (sinned)</p>\n<p>3. There is an inseperable chasm between us and God therefore that means we all are destined to eternity without God (hell).</p>\n<p>4. God came as a Man, Christ, who was sinless and died as payment for our sin.</p>\n<p>5. If we turn from our sins, receive Him as Savior, and commit our lives to Him as our Lord, we can live a rich and meaningful life here, and then be with Him forever after death.</p>', 0, 27, '2015-01-04 13:18:44', '2015-01-04 20:39:37'),
(207, '', '', '0', '', '<p>What are the tallest mountains on each of the five major continents?</p>', '<p>Everest: Asia</p>\n<p>Kilamanjaro: Africa</p>\n<p>etc</p>', 0, 28, '2015-01-04 13:20:59', '2015-01-04 13:20:59'),
(208, '', '', '0', '', '<p>What are the tallest mountains on each of the five major continents?</p>', '<p>Everest: Asia</p>\n<p>Kilamanjaro: Africa</p>\n<p>etc</p>', 0, -1, '2015-01-04 13:21:05', '2015-01-04 13:21:19'),
(209, '', '', '0', '', '<p>What are the tallest mountains on each of the five major continents?</p>', '<p>Everest: Asia</p>\n<p>Kilamanjaro: Africa</p>\n<p>etc</p>', 0, -1, '2015-01-04 13:21:07', '2015-01-04 13:21:17'),
(210, '', '', '0', '', '<p>What are the five longest rivers?</p>', '<p>Nile, Yangtze, Amazon, Mississippi, and Yesenev</p>', 0, 28, '2015-01-04 13:22:03', '2015-01-04 13:22:03'),
(211, '', '', '0', '', '<p>What are five fundamentals of the relationship with Christ/God, once saved?</p>', '<p>1. Prayer/conversation with Him is crucial</p>\n<p>&nbsp;</p>\n<p>2. Whenever we do sin, we must confess it to Him, ask for help, and surrender the area of difficulty for His healing.</p>\n<p>3. He gives us His Holy Spirit to guide us very tangibly, comfort us, motivate us to loving behavior and attitudes</p>\n<p>4. We must forgive others, or we can''t be forgiven</p>\n<p>5. Jesus speaks to us through the Bible, which is God''s Word.</p>\n<p>&nbsp;</p>', 0, 27, '2015-01-04 13:41:24', '2015-01-04 13:41:24'),
(212, '', '', '0', '', '<p>What are five Scriptures that clearly present Christ as God?</p>', '<p>Isaiah 9: "Unto us a child is born, a Son is Given, and His name shall be called...Almighty God, everlasting Father..."</p>\n<p>John 1:1 - "In the beginning was the Word, and the Word was with God, and the Word was God"</p>\n<p>Colossians 1:15 - "He is the image of the invisible God" and v. 19: "For it pleased the Father that in Him all the fulness should dwell"</p>\n<p>Hebrews 1:3 - "Who, being the brightness of His glory and the express image of His person"</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>', 0, 27, '2015-01-04 13:49:56', '2015-01-04 13:49:56'),
(213, '', '', '0', '', '<p>What are the "fruits" (characteristics) of the Spirit (and those who are led by the Spirit of Jesus)?</p>', '<p>Gal 5:22 - "But the fruit of the Spirit is love, joy, peace, longsuffering (patience), kindness, goodness, faith, gentleness, and self-control. Against such there is no law".</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>', 0, 27, '2015-01-04 13:52:16', '2015-01-04 13:52:16'),
(214, '', '', '0', '<p>LHON</p>', '<p>LHON</p>', '<p>Leber''s Hereditary Optic Neuropathy</p>', 0, 29, '2015-01-04 13:54:29', '2015-01-04 13:54:29'),
(215, '', '', '0', '<p>LHON</p>', '<p>DAD</p>', '<p>Diabetes and Deafness</p>', 0, 29, '2015-01-04 13:54:58', '2015-01-04 13:54:58'),
(216, '', '', '0', '<p>LHON</p>', '<p>NARP</p>', '<p>Neuropathy</p>\n<p>Ataxia</p>\n<p>Retinitis Pigmentosa</p>\n<p>Ptosis</p>', 0, 29, '2015-01-04 13:56:30', '2015-01-04 13:56:30'),
(217, '', '', '0', '<p>LHON</p>', '<p>Leigh''s</p>', '<p>Subacute sclerosing panencephalitis</p>', 0, 29, '2015-01-04 13:57:10', '2015-01-04 13:57:10'),
(218, '', '', '0', '<p>LHON</p>', '<p>MELAS</p>', '<p>Mitochondrial myopathy</p>\n<p>Encephalopathy</p>\n<p>Lactic acidosis</p>\n<p>Strokelike symptoms</p>', 0, 29, '2015-01-04 13:58:34', '2015-01-04 13:58:34'),
(219, '', '', '0', '<p>LHON</p>', '<p>MERFF</p>', '<p>Myoclonic Epilepsy</p>\n<p>Ragged red fibers (on biopsy)</p>', 0, 29, '2015-01-04 13:59:19', '2015-01-04 13:59:19'),
(220, '', '', '0', '<p>LHON</p>', '<p>MERFF</p>', '<p>Myoclonic Epilepsy</p>\n<p>Ragged red fibers (on biopsy)</p>', 0, -1, '2015-01-04 13:59:22', '2015-01-04 14:00:48'),
(221, '', '', '0', '<p>LHON</p>', '<p>MaNGIE</p>', '<p>MyoNeurogenic </p>\n<p>GastroIntestinal</p>\n<p>Encephalopathy</p>', 0, 29, '2015-01-04 14:00:24', '2015-01-04 14:00:24'),
(222, '', '', '0', '<p>LHON</p>', '<p>What are the seven most well-known mitochondrial diseases?</p>', '<p>DAD, NARP, Leigh''s, LHON, MERFF, MaNGIE, and MELAS</p>', 0, 29, '2015-01-04 14:01:44', '2015-01-04 14:01:44'),
(223, '', '', '0', '<p>SMacK - DIe</p>', '<p>What are the five cities with the greatest populations in the world?</p>', '<p>Shanghai, Mumbai, Karachi, New Delhi, Istanbul</p>', 0, 28, '2015-01-04 14:04:08', '2015-01-04 14:04:08'),
(224, '', '', '0', '<p>SMacK - DIe</p>', '<p>What are the five cities with the greatest populations in the world?</p>', '<p>Shanghai, Mumbai, Karachi, New Delhi, Istanbul</p>', 0, -1, '2015-01-04 14:04:24', '2015-01-04 14:04:46'),
(225, '', '', '0', '<p>SMacK - DIe</p>', '<p>What are the five cities with the greatest populations in the world?</p>', '<p>Shanghai, Mumbai, Karachi, New Delhi, Istanbul</p>', 0, -1, '2015-01-04 14:04:28', '2015-01-04 14:04:40'),
(226, '', '', '0', '<p>SMacK - DIe</p>', '<p>How many island nations are there?</p>', '<p>27</p>', 0, 28, '2015-01-04 14:05:19', '2015-01-04 14:05:19'),
(227, '', '', '0', '<p>SMacK - DIe</p>', '<p>What are the major groups of island nations?</p>', '<p>27</p>', 0, 28, '2015-01-04 14:05:46', '2015-01-04 14:05:46'),
(228, '', '', '0', '<p>"So Little Time People,</p>\n<p>Time To Come Home"</p>\n<p>(acrostic)</p>', '<p>What are the 8 bones of the hand?</p>', '<p>Scaphoid, lunate, triquetral, pisiform,</p>\n<p>Trapezoid, trapezium, capitate, hamate</p>', 0, 30, '2015-01-04 14:13:28', '2015-01-04 21:26:27'),
(229, '', '', '0', '<p>On Old Olympus'' towering tops, a</p>\n<p>Friendly, vibrant german viewed some hops</p>', '<p>What are the 12 cranial nerves, in order?</p>', '<p>Olfactory, optic, oculomotor,</p>\n<p>Trochlear, Trigeminal, Abducens,</p>\n<p>Facial, Vestibulocochlear, Glossopharyngeal</p>\n<p>Vagus, Spinal Accessory, Hypoglossal</p>', 0, 30, '2015-01-04 14:15:51', '2015-01-04 21:24:12'),
(230, '', '', '0', '', '<p>What is dark energy?</p>', '', 0, 31, '2015-01-04 14:26:56', '2015-01-04 14:26:56'),
(231, '', '', '0', '', '<p>Name five facts about dark energy</p>', '', 0, 31, '2015-01-04 14:27:22', '2015-01-04 14:27:22'),
(232, '', '', '0', '', '<p>What is the evidence for dark energy?</p>', '', 0, 31, '2015-01-04 14:27:38', '2015-01-04 14:27:38'),
(233, '', '', '0', '', '<p>What is dark matter?</p>', '', 0, 31, '2015-01-04 14:27:52', '2015-01-04 14:27:52'),
(234, '', '', '0', '', '<p>List five facts about dark matter</p>', '', 0, 31, '2015-01-04 14:28:24', '2015-01-04 14:28:24'),
(235, '', '', '0', '', '<p>List five facts about dark matter</p>', '', 0, -1, '2015-01-04 14:28:27', '2015-01-04 14:29:47'),
(236, '', '', '0', '', '<p>What is evidence for dark matter?</p>', '', 0, 31, '2015-01-04 14:28:56', '2015-01-04 14:28:56'),
(237, '', '', '0', '', '<p>How many light-years is one parsec?</p>', '<p>3.2</p>', 0, 31, '2015-01-04 14:29:30', '2015-01-04 14:29:30'),
(238, '', '', '0', '', '<p>About how many stars are in our Milky Way Galaxy?</p>', '<p>300 billion</p>', 0, 31, '2015-01-04 14:30:35', '2015-01-04 14:30:35'),
(239, '', '', '0', '', '<p>What percent of the stars in our Milky Way Galaxy are Red dwarf stars?</p>', '<p>70%</p>', 0, 31, '2015-01-04 14:31:14', '2015-01-04 14:31:14'),
(240, '', '', '0', '', '<p>What are quasars?</p>', '<p>70%</p>', 0, 31, '2015-01-04 14:32:07', '2015-01-04 14:32:07'),
(241, '', '', '0', '', '<p>Tianenman Square crisis</p>', '<p>6-4-89</p>', 0, 32, '2015-01-04 14:35:22', '2015-01-04 14:35:22'),
(242, '', '', '0', '', '<p>D-Day</p>', '<p>6-6-44</p>', 0, 32, '2015-01-04 14:35:43', '2015-01-04 14:35:43'),
(243, '', '', '0', '', '<p>JFK assassinated</p>', '<p>11-22-63</p>', 0, 32, '2015-01-04 14:36:21', '2015-01-04 14:36:21'),
(244, '', '', '0', '', '<p>Sputnik orbited Earth</p>', '<p>10-4-57</p>', 0, 32, '2015-01-04 14:36:46', '2015-01-04 14:36:46'),
(245, '', '', '0', '', '<p>Moon landing by Neil Armstrong, Buzz Aldrin</p>', '<p>7-21-69</p>', 0, 32, '2015-01-04 14:37:17', '2015-01-04 14:37:17'),
(246, '', '', '0', '', '<p>Pearl Harbor</p>', '<p>12-7-41</p>', 0, 32, '2015-01-04 14:37:47', '2015-01-04 14:37:47'),
(247, '', '', '0', '', '<p>First private commercial vessel to reach International Space Station (SpaceX)</p>', '<p>5-27-12</p>', 0, 32, '2015-01-04 14:38:55', '2015-01-04 14:38:55'),
(248, '26', '', '0', '', '<p>26th president</p>', '<p>Theodore Roosevelt</p>', 0, -1, '2015-01-24 16:00:35', '2015-04-19 18:16:06'),
(249, '26', '', '0', '', '<p>27th president</p>', '<p>William Taft</p>', 0, -1, '2015-01-24 16:01:01', '2015-04-19 18:16:23'),
(250, '26', '', '0', '', '<p>28th president</p>', '<p>Woodrow Wilson</p>', 0, -1, '2015-01-24 16:01:22', '2015-04-19 18:16:29'),
(251, '26', '', '0', '', '<p>29th president</p>', '<p>Warren Harding</p>', 0, -1, '2015-01-24 16:01:38', '2015-04-19 18:16:36'),
(252, '26', '', '0', '', '<p>30th president</p>', '<p>Calvin Coolidge</p>', 0, -1, '2015-01-24 16:01:55', '2015-04-19 18:16:54'),
(253, '', '', '0', '', '<p>31st US President</p>\n<p>&nbsp;</p>', '<p>Herbert Hoover</p>\n<p>&nbsp;</p>', 0, 19, '2015-01-24 16:02:13', '2015-04-20 01:22:13'),
(254, '', '', '0', '', '<p>32nd US President</p>\n<p>&nbsp;</p>', '<p>Franklin Delano Roosevelt</p>\n<p>&nbsp;</p>', 0, 19, '2015-01-24 16:02:33', '2015-04-20 01:23:52'),
(255, '', '', '0', '', '<p>33rd US President</p>\n<p>&nbsp;</p>', '<p>Harry Truman</p>\n<p>&nbsp;</p>', 0, 19, '2015-01-24 16:02:57', '2015-04-20 01:19:16'),
(256, '', '', '0', '', '<p>34th US President</p>\n<p>&nbsp;</p>', '<p>Dwight Eisenhower</p>\n<p>&nbsp;</p>', 0, 19, '2015-01-24 16:03:14', '2015-04-20 01:24:20'),
(257, '', '', '0', '', '<p>35th US President</p>\n<p>&nbsp;</p>', '<p>John F. Kennedy</p>\n<p>&nbsp;</p>', 0, 19, '2015-01-24 16:03:34', '2015-04-20 01:24:47'),
(258, '', '', '0', '', '<p>7th president</p>', '<p>Andrew Jackson</p>', 0, 19, '2015-01-24 19:12:29', '2015-01-24 19:12:29'),
(259, '', '', '0', '', '<p>36th president</p>', '<p>Lyndon B. Johnson</p>', 0, 19, '2015-01-24 19:13:08', '2015-01-24 19:13:08'),
(260, '', '', '0', '', '<p>37th president</p>', '<p>Richard M. Nixon</p>', 0, 19, '2015-01-24 19:13:26', '2015-01-24 19:13:26'),
(261, '', '', '0', '', '<p>38th president</p>', '<p>Gerald Ford</p>', 0, 19, '2015-01-24 19:13:48', '2015-01-24 19:13:48'),
(262, '', '', '0', '', '<p>39th president</p>', '<p>Jimmy Carter</p>', 0, 19, '2015-01-24 19:14:11', '2015-01-24 19:14:11'),
(263, '', '', '0', '', '<p>40th president</p>', '<p>Ronald Reagan</p>', 0, 19, '2015-01-24 19:14:54', '2015-01-24 19:14:54'),
(264, '', '', '0', '', '<p>2 ^ 2 =</p>', '<p>4</p>', 0, 33, '2015-03-10 12:32:28', '2015-03-10 12:32:28'),
(265, '', '', '0', '', '<p>3 ^ 3 =</p>', '<p>27</p>', 0, 33, '2015-03-10 12:32:46', '2015-03-10 12:32:46'),
(266, '', '', '0', '', '<p>3 ^ 3 =</p>', '<p>27</p>', 0, 33, '2015-03-10 12:32:53', '2015-03-10 12:32:53'),
(267, '', '', '0', '', '<p>Paul Dirac, facts<br /><br /></p>', '<p>Taught at Cambridge</p>\n<p>Developed DIRAC equation, which described FERMIONS (like electrons) and predicted antimatter.</p>\n<p>Was taciturn, and odd, very "logical".</p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>', 0, 34, '2015-04-12 18:20:57', '2015-04-13 01:23:11'),
(268, '', '', '0', '', '<p>Albert Einstein - facts</p>', '<p>At age 26, published four major papers while working as a patent clerk:</p>\n<p>&nbsp; Special relativity</p>\n<p>&nbsp; Matter-energy equivalence:&nbsp; E = mc^2</p>\n<p>&nbsp; Brownian motion, evidence for atoms</p>\n<p>&nbsp; Photoelectri effect, proving quantum nature</p>\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; of subatomic particles</p>\n<p>&nbsp;</p>', 0, 34, '2015-04-12 18:31:16', '2015-04-12 18:31:16'),
(269, '', '', '0', '', '<p>41st US President</p>\n<p>&nbsp;</p>', '<p>George Bush, Sr. </p>', 0, -1, '2015-04-19 18:25:07', '2015-04-19 18:26:44'),
(270, '', '', '0', '', '<p>41st US President</p>\n<p>&nbsp;</p>', '<p>George Bush, Sr. </p>', 0, 19, '2015-04-19 18:25:14', '2015-04-19 18:25:14'),
(271, '', '', '0', '', '<p>42nd US President</p>\n<p>&nbsp;</p>', '<p>William Clinton</p>', 0, 19, '2015-04-19 18:25:41', '2015-04-19 18:25:41'),
(272, '', '', '0', '', '<p>43rd US President</p>\n<p>&nbsp;</p>', '<p>George W. Bush</p>', 0, 19, '2015-04-19 18:26:04', '2015-04-19 18:26:04'),
(273, '', '', '0', '', '<p>44th US President</p>\n<p>&nbsp;</p>', '<p>Barak H. Obama</p>', 0, 19, '2015-04-19 18:26:24', '2015-04-19 18:26:24'),
(274, '', '', '0', '', '<p>C</p>', '<h1>-.-.</h1>', 0, 25, '2015-04-19 18:32:47', '2015-04-20 01:35:14'),
(275, '', '', '0', '', '<p>D</p>', '<h1>-..</h1>', 0, 25, '2015-04-19 18:33:38', '2015-04-20 01:36:32'),
(276, '', '', '0', '', '<p>E</p>', '<h1>.</h1>', 0, 25, '2015-04-19 18:34:55', '2015-04-19 18:34:55'),
(277, '', '', '0', '', '<p>F</p>', '<h1>..-.</h1>', 0, 25, '2015-04-19 18:35:38', '2015-04-19 18:35:38'),
(278, '', '', '0', '', '<p>There is no God</p>', '<p>Impossible to disprove God</p>\n<p>What are chances of the 38+ physical constants having perfect precision to create this order?</p>\n<p>Chances of 3 amino acids connecting = 1:10,000, of 10 = 1:quintillion, and shortest protein is 400 amino acids long</p>', 0, 36, '2015-04-20 23:43:26', '2015-04-20 23:43:26'),
(279, '', '', '0', '', '<p>Hello</p>', '<p>Goodbye</p>', 0, 38, '2015-04-23 12:19:48', '2015-04-23 12:19:48'),
(280, '', '', '0', '', '<p>West Virginia</p>', '<p>Charleston</p>', 0, 14, '2015-04-24 20:55:32', '2015-04-24 20:55:32'),
(281, '', '', '0', '<p>His HART is CONNECTED<br /><br /></p>', '<p>Connecticutt</p>', '<p>Hartford</p>', 0, 14, '2015-04-24 21:06:59', '2015-04-25 04:50:32'),
(282, '', '', '0', '', '<p>Tennessee</p>', '<p>Nashville</p>', 0, 14, '2015-04-24 21:21:02', '2015-04-24 21:21:02'),
(283, '', '', '0', '', '<p>Wisconsin</p>', '<p>Madison</p>', 0, 14, '2015-04-24 21:26:36', '2015-04-24 21:26:36'),
(284, '', '', '0', '', '<p>Wyoming</p>', '<p>Cheyenne</p>', 0, 14, '2015-04-24 21:31:06', '2015-04-24 21:31:06'),
(285, '', '', '0', '', '', '', 0, 14, '2015-04-24 21:31:33', '2015-04-24 21:31:33'),
(286, '', '', '0', '', '', '', 0, 14, '2015-04-24 21:31:33', '2015-04-24 21:31:33'),
(287, '', '', '0', '', '', '', 0, 14, '2015-04-24 21:31:36', '2015-04-24 21:31:36'),
(288, '', '', '0', '', '', '', 0, 14, '2015-04-24 21:31:37', '2015-04-24 21:31:37'),
(289, '', '', '0', '<p>ROBOTs working on MORE ROCKs in quarry</p>', '<p>Morocco</p>', '<p>Rabat</p>', 0, 16, '2015-04-25 17:04:39', '2015-04-25 17:04:39'),
(290, '', '', '0', '<p>ROBOTs working on MORE ROCKs in quarry</p>', '<p>Eqypt</p>', '<p>Cairo</p>', 0, 16, '2015-04-25 17:05:05', '2015-04-25 17:05:05'),
(291, '', '', '0', '<p>ROBOTs working on MORE ROCKs in quarry</p>', '<p>Libya</p>', '<p>Tripoli</p>', 0, 16, '2015-04-25 17:06:07', '2015-04-25 17:06:07'),
(292, '', '', '0', '', '<p>Angola</p>', '<p>Luanda</p>', 0, 16, '2015-04-25 17:08:02', '2015-04-25 17:08:02'),
(293, '', '', '0', '', '<p>Botswana</p>', '<p>Gaborone</p>', 0, 16, '2015-04-25 17:09:19', '2015-04-25 17:09:19'),
(294, '', '', '0', '', '<p>Botswana</p>', '<p>Gaborone</p>', 0, -1, '2015-04-25 17:09:28', '2015-04-25 17:20:44'),
(295, '', '', '0', '', '<p>Cape Verde</p>', '<p>Praia</p>', 0, 16, '2015-04-25 17:09:59', '2015-04-25 17:09:59'),
(296, '', '', '0', '', '<p>Chad</p>', '<p>N''Djamena</p>', 0, 16, '2015-04-25 17:10:25', '2015-04-25 17:10:25'),
(297, '', '', '0', '', '<p>Congo, Republic of the</p>', '<p>Brazzaville</p>', 0, 16, '2015-04-25 17:11:07', '2015-04-25 17:11:07'),
(298, '', '', '0', '', '<p>Congo, Democratic Republic of the</p>', '<p>Kinshasa</p>', 0, 16, '2015-04-25 17:12:05', '2015-04-25 17:12:05'),
(299, '', '', '0', '', '<p>Djibouti</p>', '<p>Djibouti</p>', 0, 16, '2015-04-25 17:12:35', '2015-04-25 17:12:35'),
(300, '', '', '0', '', '<p>Equatorial Guinea</p>', '<p>Malabo</p>', 0, 16, '2015-04-25 17:12:55', '2015-04-25 17:12:55'),
(301, '', '', '0', '', '<p>Ethiopia</p>', '<p>Addis Ababa</p>', 0, 16, '2015-04-25 17:13:19', '2015-04-25 17:13:19'),
(302, '', '', '0', '', '<p>Gabon</p>', '<p>Libreville</p>', 0, 16, '2015-04-25 17:13:41', '2015-04-25 17:13:41'),
(303, '', '', '0', '', '<p>Gambia</p>', '<p>Banjul</p>', 0, 16, '2015-04-25 17:14:09', '2015-04-25 17:14:09'),
(304, '', '', '0', '', '<p>Guinea</p>', '<p>Conakry</p>', 0, 16, '2015-04-25 17:14:29', '2015-04-25 17:14:29');
INSERT INTO `flash_card_cards` (`id`, `name`, `description`, `categorization`, `hint`, `question`, `answer`, `userId`, `cardset_id`, `created_on`, `modified_on`) VALUES
(305, '', '', '0', '', '<p>Guinea</p>', '<p>Conakry</p>', 0, 16, '2015-04-25 17:14:31', '2015-04-25 17:14:31'),
(306, '', '', '0', '', '<p>Guinea-Bissau</p>', '<p>Bissau</p>', 0, 16, '2015-04-25 17:14:57', '2015-04-25 17:14:57'),
(307, '', '', '0', '', '<p>Malawi</p>', '<p>Lilongwe</p>', 0, 16, '2015-04-25 17:15:28', '2015-04-25 17:15:28'),
(308, '', '', '0', '', '<p>Mali</p>', '<p>Bamako</p>', 0, 16, '2015-04-25 17:15:48', '2015-04-25 17:15:48'),
(309, '', '', '0', '', '<p>Mauritius</p>', '<p>Port Louis</p>', 0, 16, '2015-04-25 17:16:11', '2015-04-25 17:16:11'),
(310, '', '', '0', '', '<p>Mayotte</p>', '<p>Mamoudzou</p>', 0, 16, '2015-04-25 17:16:33', '2015-04-25 17:16:33'),
(311, '', '', '0', '', '<p>Namibia</p>', '<p>Windhoek</p>', 0, 16, '2015-04-25 17:16:55', '2015-04-25 17:16:55'),
(312, '', '', '0', '', '<p>Niger</p>', '<p>Niamey</p>', 0, 16, '2015-04-25 17:17:57', '2015-04-25 17:17:57'),
(313, '', '', '0', '', '<p>Nigeria</p>', '<p>Abuja</p>', 0, 16, '2015-04-25 17:18:01', '2015-04-26 00:20:41'),
(314, '', '', '0', '', '<p>Rwanda</p>', '<p>Kigali</p>', 0, 16, '2015-04-25 17:19:22', '2015-04-25 17:19:22'),
(315, '', '', '0', '', '<p>Reunion</p>', '<p>St. Denis</p>', 0, 16, '2015-04-25 17:19:46', '2015-04-25 17:19:46'),
(316, '', '', '0', '', '<p>Senegal</p>', '<p>Dakar</p>', 0, 16, '2015-04-25 17:20:10', '2015-04-25 17:20:10'),
(317, '', '', '0', '', '<p>Seychelles</p>', '<p>Victoria</p>', 0, 16, '2015-04-25 17:21:36', '2015-04-25 17:21:36'),
(318, '', '', '0', '', '<p>Sierra Leone</p>', '<p>Freetown</p>', 0, 16, '2015-04-25 17:22:02', '2015-04-25 17:22:02'),
(319, '', '', '0', '', '<p>Somaliland</p>', '<p>Hargeisa</p>', 0, 16, '2015-04-25 17:22:30', '2015-04-25 17:22:30'),
(320, '', '', '0', '', '<p>South Africa</p>', '<p>Pretoria - executive</p>\n<p>Bloemfontein - judicial</p>\n<p>Cape Town - legislative</p>', 0, 16, '2015-04-25 17:23:39', '2015-04-25 17:23:39'),
(321, '', '', '0', '', '', '', 0, -1, '2015-04-25 17:23:55', '2015-04-25 17:33:01'),
(322, '', '', '0', '', '<p>South Sudan</p>', '<p>Juba</p>', 0, 16, '2015-04-25 17:24:11', '2015-04-25 17:24:11'),
(323, '', '', '0', '', '<p>Swaziland</p>', '<p>Mbabane</p>', 0, 16, '2015-04-25 17:24:35', '2015-04-25 17:24:35'),
(324, '', '', '0', '', '<p>Sao Tome and Principe</p>', '<p>Sao Tome</p>', 0, 16, '2015-04-25 17:24:56', '2015-04-25 17:24:56'),
(325, '', '', '0', '', '<p>Tanzania</p>', '<p>Dodoma</p>', 0, 16, '2015-04-25 17:25:14', '2015-04-25 17:25:14'),
(326, '', '', '0', '', '<p>Togo</p>', '<p>Lome</p>', 0, 16, '2015-04-25 17:25:29', '2015-04-25 17:25:29'),
(327, '', '', '0', '', '<p>Western Sahara</p>', '<p>El Aaiun</p>', 0, 16, '2015-04-25 17:26:09', '2015-04-25 17:26:09'),
(328, '', '', '0', '', '<p>Zambia</p>', '<p>Lusaka</p>', 0, 16, '2015-04-25 17:26:27', '2015-04-25 17:26:27'),
(329, '', '', '0', '', '<p>Zimbabwe</p>', '<p>Harare</p>', 0, 16, '2015-04-25 17:26:51', '2015-04-25 17:26:51'),
(330, 'idchk', 'test', '0', 'test chk', 'test chk', 'test chk', 1, 3, '2015-05-09 07:52:36', '2015-05-09 07:52:36'),
(331, 'idchk2', 'test2', '3-1-35', 'test chk2', 'test chk2', 'test chk2', 1, 3, '2015-05-09 07:58:12', '2015-05-09 07:58:12');

-- --------------------------------------------------------

--
-- Table structure for table `flash_card_cardsets`
--

CREATE TABLE IF NOT EXISTS `flash_card_cardsets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `categorization` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `userId` int(40) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_on` datetime NOT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=41 ;

--
-- Dumping data for table `flash_card_cardsets`
--

INSERT INTO `flash_card_cardsets` (`id`, `name`, `description`, `categorization`, `userId`, `created_on`, `modified_on`, `active`) VALUES
(1, 'test cardset', '<p>&nbsp;test desc</p>', '8-9-10', 1, '2014-11-18 21:02:49', '2014-11-18 21:02:49', 0),
(2, 'test cardset 2', '<p>test desc 2 - mod</p>', '2-3-6', 1, '2014-11-18 21:03:19', '2015-02-01 18:43:57', 0),
(3, 'test cardset scott', '<p>Moons of the Solar System</p>', '4-3', 1, '2014-11-19 12:20:43', '2015-04-25 18:19:42', 0),
(6, 'Scott New Space Set', '<p>Moons of the Solar System</p>', '10', 1, '2014-11-21 22:45:10', '2015-04-25 18:20:13', 0),
(7, 'Solar System Moons', '<p>&nbsp;test desc 2 - mod</p>', '2-10', 8, '2014-11-23 20:53:28', '2015-04-25 18:06:31', 0),
(8, 'Scott test set sat', 'testing creation of a new cardset', '3-2-4', 1, '2014-11-29 18:26:33', '2014-11-29 18:26:33', 0),
(9, 'Scott Computer quiz', 'cardset for computer questions', '3', 1, '2014-11-29 18:31:13', '2014-11-29 18:31:13', 0),
(10, 'Scott Science quiz', 'cardset for Science questions', '2', 1, '2014-11-29 18:38:21', '2014-11-29 18:38:21', 0),
(11, 'Geometric Formulas', '<p>Formulas for shapes</p>', '1', 8, '2014-11-29 18:59:59', '2014-11-06 21:21:50', 1),
(12, 'Navigation By the Stars', '<p>Quiz on nocturnal navigation.</p>', '11-12-21-10', 1, '2014-11-29 19:25:24', '2015-03-20 20:43:17', 1),
(13, 'Marine Navigation Set', '', '11-12-14', 1, '2014-11-29 19:28:05', '2015-00-21 15:45:20', 0),
(14, 'States and Capitals', '<p>States and their Capitals</p>', '18', 8, '2014-11-30 00:59:06', '2014-11-06 21:36:10', 1),
(15, 'Body Parts in Spanish', '<p>Body Parts in Spanish</p>', '19-20', 8, '2014-11-30 00:59:42', '2014-11-06 21:49:05', 1),
(16, 'African Nations & Capitals', '', '18', 8, '2014-12-03 12:01:48', '2014-11-10 23:11:40', 1),
(17, 'Fundamental Particles', '', '2-21', 8, '2014-12-04 00:12:26', '2015-03-20 22:53:33', 1),
(18, 'Chinese Dynasties', '<p>Facts about Chinese Dynasties</p>', '15', 8, '2014-12-04 11:07:32', '2014-11-06 22:22:41', 1),
(19, 'USA Presidents', '<p>US presidents by order of occurrence</p>', '15-17', 8, '2014-12-04 11:08:00', '2015-03-19 18:28:11', 1),
(20, 'Favorite Verses', '', '22', 8, '2014-12-08 00:09:25', '2014-11-20 22:54:10', 1),
(21, 'Bible Facts and Figures', '', '22', 8, '2014-12-08 00:21:20', '2014-12-08 00:21:20', 1),
(22, 'Electronics', '', '2', 1, '2014-12-25 20:05:58', '2014-11-25 19:31:34', 0),
(23, 'Periodic Table of Elements', '<p>Elements, numbers, and abbreviations</p>', '2-23', 8, '2014-12-25 20:08:09', '2014-11-25 19:17:30', 1),
(24, 'Cell Biology', '<p>Cell organelles, membrane processes, chemical sequences</p>', '2-25', 8, '2014-12-25 20:12:09', '2014-11-25 19:40:42', 1),
(25, 'Morse Code', '<p>International Morse Code - letters &amp; numbers</p>', '19', 8, '2015-01-04 00:19:32', '2015-03-19 18:37:04', 1),
(26, 'Agencies for the Poor', '', '26', 8, '2015-01-04 12:39:48', '2015-01-04 12:39:48', 1),
(27, 'Bible Principles', '', '22-28', 8, '2015-01-04 13:14:51', '2015-00-04 12:39:14', 1),
(28, 'Geography questions', 'Assorted geography questions', '18', 8, '2015-01-04 13:19:49', '2015-01-04 13:19:49', 1),
(29, 'Mitochondrial Diseases', '', '2-29', 8, '2015-01-04 13:24:22', '2015-00-04 12:28:10', 1),
(30, 'Anatomy', 'Various anatomy questions', '2-29', 8, '2015-01-04 14:11:29', '2015-01-04 14:11:29', 1),
(31, 'Astrophysics', '<p>Various astrophysics questions</p>\n<p>&nbsp;</p>', '2-10', 8, '2015-01-04 14:26:08', '2015-00-04 13:30:32', 1),
(32, 'Famous dates', 'Various well-known or important dates', '15', 8, '2015-01-04 14:33:12', '2015-01-04 14:33:12', 1),
(33, 'Test', 'Dummy test', '0', 10, '2015-03-10 12:31:34', '2015-03-10 12:31:34', 0),
(34, 'Physicists - 20th century', '', '2-21', 8, '2015-04-12 18:05:20', '2015-03-20 22:51:11', 1),
(35, 'African History', '', '15', 8, '2015-04-18 20:01:31', '2015-03-20 22:54:58', 1),
(36, 'Apologetics', '', '22', 8, '2015-04-20 23:40:33', '2015-03-20 23:42:51', 1),
(37, 'Apologetics', 'Arguments against Christianity and best answers', '22', 8, '2015-04-20 23:41:04', '2015-04-20 23:41:04', 0),
(38, 'Test One', '', '0', 8, '2015-04-23 12:18:45', '2015-03-23 12:20:25', 1),
(39, 'Planet facts', '', '2-21-10', 8, '2015-04-24 22:38:34', '2015-03-24 22:41:25', 1),
(40, 'Forgotten', '', '34-33', 8, '2015-04-25 18:12:59', '2015-03-25 18:15:31', 1);

-- --------------------------------------------------------

--
-- Table structure for table `flash_card_categories`
--

CREATE TABLE IF NOT EXISTS `flash_card_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `categorization` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `userId` int(40) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_on` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=36 ;

--
-- Dumping data for table `flash_card_categories`
--

INSERT INTO `flash_card_categories` (`id`, `name`, `description`, `categorization`, `userId`, `created_on`, `modified_on`) VALUES
(1, 'Mathematics', '<p>Mathematics stuff </p>\n<p>Calculus, Trig, etc</p>', '-1', 0, '2014-11-10 20:54:44', '2014-11-10 20:54:44'),
(2, 'Science', '<p>Science stuff</p>', '-1', 0, '2014-11-10 21:02:50', '2014-11-10 21:02:50'),
(3, 'Computer', '<p>Computer stuff</p>', '-1', 0, '2014-11-10 21:07:40', '2014-11-10 21:07:40'),
(4, 'Hiking', '<p>Things about Hiking</p>', '-1', 0, '2014-11-10 21:09:21', '2014-11-10 21:09:21'),
(5, 'Camping', '<p>Things about Camping</p>', '-1', 0, '2014-11-10 21:21:24', '2014-11-10 21:21:24'),
(6, 'Cooking', '<p>Things about Cooking</p>', '-1', 0, '2014-11-10 21:23:45', '2014-11-10 21:23:45'),
(7, 'Knitting', '<p>Things about Knitting</p>', '-1', 0, '2014-11-10 21:27:27', '2014-11-10 21:27:27'),
(8, 'Gaming', '<p>Things about Gaming</p>', '-1', 0, '2014-11-10 21:30:03', '2014-11-10 21:30:03'),
(9, 'Cats', '<p>Things about Cats</p>', '-1', 0, '2014-11-10 21:32:28', '2014-11-10 21:32:28'),
(10, 'Space', '', '-1', 0, '2014-11-15 13:45:10', '2014-11-15 13:45:10'),
(11, 'Outdoors', 'Outdoor based topics', '-1', 0, '2014-11-29 18:44:49', '2014-11-29 18:44:49'),
(12, 'Navigation', 'Talking about ways to navigate', '11', 0, '2014-11-29 18:52:22', '2014-11-29 18:52:22'),
(13, 'Biking', 'Tips when biking', '-1', 0, '2014-11-29 18:58:25', '2014-11-29 18:58:25'),
(14, 'Marine', 'Tips on navigation while on the ocean', '11-12', 0, '2014-11-29 19:18:52', '2014-11-29 19:18:52'),
(15, 'History', '', '-1', 0, '2014-11-30 00:57:12', '2014-11-30 00:57:12'),
(16, 'Geometry', '', '1', 0, '2014-11-30 00:58:23', '2014-11-30 00:58:23'),
(17, 'US History', 'United States History', '15', 0, '2014-12-04 11:06:15', '2014-12-04 11:06:15'),
(18, 'Geography', 'Various geographic quizzes', '-1', 0, '2014-12-06 22:30:52', '2014-12-06 22:30:52'),
(19, 'Languages', '', '-1', 0, '2014-12-06 22:37:15', '2014-12-06 22:37:15'),
(20, 'Spanish', '', '-1', 0, '2014-12-06 22:37:46', '2014-12-06 22:37:46'),
(21, 'Physics', '', '-1', 0, '2014-12-06 23:06:45', '2014-12-06 23:06:45'),
(22, 'Bible', '', '-1', 0, '2014-12-06 23:07:44', '2014-12-06 23:07:44'),
(23, 'Chemistry', '', '2', 0, '2014-12-25 20:06:18', '2014-12-25 20:06:18'),
(24, 'Movie Stars', '', '-1', 0, '2014-12-25 20:17:17', '2014-12-25 20:17:17'),
(25, 'Biology', '', '2', 0, '2014-12-25 20:32:25', '2014-12-25 20:32:25'),
(26, 'Poverty and Hunger', 'Issues, statistics, and ways/agencies to help make a solid impact for the poor', '-1', 0, '2015-01-04 12:26:30', '2015-01-04 12:26:30'),
(27, 'Clean Energy', 'Solar and other renewable energy, conservation, clean tech, stats and agencies', '-1', 0, '2015-01-04 12:27:45', '2015-01-04 12:27:45'),
(28, 'Bible Principles', 'Core principles from the Bible, and supporting verse references', '22', 0, '2015-01-04 12:28:46', '2015-01-04 12:28:46'),
(29, 'Medicine', '', '-1', 0, '2015-01-04 13:22:46', '2015-01-04 13:22:46'),
(30, 'Apologetics', 'Defending one''s Christian Faith', '22-28', 0, '2015-03-22 21:49:31', '2015-03-22 21:49:31'),
(31, 'Animal Groups', 'Names for groups of animals', '2-25', 0, '2015-03-22 21:50:42', '2015-03-22 21:50:42'),
(32, 'Animal Groups', 'Names for groups of animals', '2-25', 0, '2015-03-22 21:50:42', '2015-03-22 21:50:42'),
(33, 'Forgotten Items', 'Things I have recently wanted to remember, but forgot', '34', 8, '2015-04-25 18:10:38', '2015-04-25 18:10:38'),
(34, 'MISCELLANEOUS', 'General Category for difficult to categorize categories and sets', '-1', 0, '2015-04-25 18:11:30', '2015-04-25 18:11:30'),
(35, 'testA', '', '1', 0, '2015-04-29 11:10:40', '2015-04-29 11:10:40');

-- --------------------------------------------------------

--
-- Table structure for table `flash_card_code_table`
--

CREATE TABLE IF NOT EXISTS `flash_card_code_table` (
  `id` int(40) NOT NULL AUTO_INCREMENT,
  `code` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `reason_used` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_used` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `flash_card_code_table`
--

INSERT INTO `flash_card_code_table` (`id`, `code`, `reason_used`, `date_used`) VALUES
(1, 'feca9-20aae-e19e7-8e218-5a4dc-f275f-9d5b6', NULL, NULL),
(2, 'b6579-5bfb8-5bc26-d75a5-68c82-f1ad7-c76fe', NULL, NULL),
(3, '40ebc-34cf3-a5e8b-d057f-39ef0-061cf-1bb6b', NULL, NULL),
(4, 'aa4cd-9a718-63d78-e41b2-b9181-15a0b-dc94d', NULL, NULL),
(5, '70521-4e9bd-15238-0ddc6-90f76-a6a88-91006', NULL, NULL),
(6, 'a9a80-60ac6-a1ba4-6e646-9b22b-c614a-ffe6c', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `flash_card_quizzes`
--

CREATE TABLE IF NOT EXISTS `flash_card_quizzes` (
  `id` int(40) NOT NULL AUTO_INCREMENT,
  `cardset_id` int(40) NOT NULL,
  `userId` int(40) NOT NULL,
  `date_taken` date NOT NULL,
  `quiz_length` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `end_time` time NOT NULL,
  `quiz_time` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `percent_correct` decimal(5,2) NOT NULL,
  `number_of_questions` int(5) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=24 ;

--
-- Dumping data for table `flash_card_quizzes`
--

INSERT INTO `flash_card_quizzes` (`id`, `cardset_id`, `userId`, `date_taken`, `quiz_length`, `end_time`, `quiz_time`, `percent_correct`, `number_of_questions`) VALUES
(1, 3, 1, '0000-00-00', '4 cards viewed', '16:52:32', '00:00:00', '9.99', 4),
(2, 3, 1, '0000-00-00', '4 cards viewed', '16:54:14', '00:00:00', '9.99', 4),
(3, 3, 1, '2015-01-02', '4 cards viewed', '17:05:00', '00:00:00', '75.00', 4),
(4, 19, 1, '2015-01-02', '8 cards viewed', '17:09:13', '00:00:00', '62.50', 8),
(5, 18, 1, '2015-01-02', '5 cards viewed', '17:31:08', '00:00:00', '20.00', 5),
(6, 28, 1, '2015-01-10', '5 cards viewed', '22:10:39', '00:00:00', '80.00', 5),
(7, 19, 1, '0000-00-00', '2 cards viewed', '00:00:00', '00:00:00', '100.00', 30),
(8, 19, 1, '0000-00-00', '2 cards viewed', '00:00:00', '00:00:00', '100.00', 46),
(9, 7, 1, '2015-03-13', '8 cards viewed', '13:59:31', '00:00:00', '50.00', 8),
(10, 32, 1, '2015-03-14', '7 cards viewed', '11:16:15', '00:00:00', '42.86', 7),
(11, 7, 1, '2015-03-14', '8 cards viewed', '11:26:04', '00:00:39', '62.50', 8),
(12, 21, 1, '2015-03-14', '3 cards viewed', '11:35:43', '00:00:00', '66.67', 3),
(13, 24, 1, '2015-03-14', '2 cards viewed', '11:55:45', '00:00:00', '100.00', 2),
(14, 11, 1, '2015-03-14', '5 cards viewed', '11:59:46', '0:0:0', '80.00', 5),
(15, 11, 1, '2015-03-14', '5 cards viewed', '12:21:19', '00:01:53', '80.00', 5),
(16, 17, 8, '0000-00-00', '1 card viewed', '00:00:00', '', '100.00', 6),
(17, 28, 1, '2015-03-18', '2 cards viewed', '08:05:52', '00:00:29', '50.00', 5),
(18, 15, 8, '2015-03-21', '1 card viewed', '22:17:21', '00:00:49', '100.00', 15),
(19, 14, 8, '2015-04-11', '3 cards viewed', '22:04:09', '00:00:55', '100.00', 45),
(20, 19, 8, '2015-04-12', '5 cards viewed', '17:42:41', '00:01:38', '100.00', 46),
(21, 14, 8, '2015-04-18', '1 card viewed', '17:52:05', '00:03:05', '100.00', 45),
(22, 19, 8, '2015-04-20', '2 cards viewed', '22:48:40', '00:00:59', '50.00', 44),
(23, 16, 8, '2015-04-26', '59 cards viewed', '17:28:36', '00:14:58', '89.83', 59);

-- --------------------------------------------------------

--
-- Table structure for table `flash_card_quiz_ratings`
--

CREATE TABLE IF NOT EXISTS `flash_card_quiz_ratings` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_on` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `flash_card_quiz_response_data`
--

CREATE TABLE IF NOT EXISTS `flash_card_quiz_response_data` (
  `quiz_id` int(40) NOT NULL,
  `card_id` int(40) NOT NULL,
  `draw_order` int(5) NOT NULL,
  `time_on_card` varchar(50) NOT NULL,
  `answer` text NOT NULL,
  `right_wrong` tinyint(1) NOT NULL,
  `rating` varchar(100) NOT NULL,
  `hint_viewed` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `flash_card_quiz_response_data`
--

INSERT INTO `flash_card_quiz_response_data` (`quiz_id`, `card_id`, `draw_order`, `time_on_card`, `answer`, `right_wrong`, `rating`, `hint_viewed`) VALUES
(2, 11, 0, '2.092 secs', 'hgsadva', 0, 'challenging', 0),
(2, 8, 1, '1.243 secs', 'safasfsAF', 0, 'challenging', 0),
(2, 9, 2, '3.036 secs', 'ASDFSAFSADFdasfas', 0, 'hard', 0),
(2, 10, 3, '2.052 secs', 'asfasfasdfav s', 0, 'challenging', 0),
(3, 11, 0, '4.572 secs', 'asf sadfas', 0, 'challenging', 0),
(3, 10, 1, '22.077 secs', 'sadfasdf', 0, 'hard', 0),
(3, 9, 2, '3.591 secs', 'asdfasfdafsa', 0, 'challenging', 0),
(3, 8, 3, '4.545 secs', 'asfas fasdfas', 0, 'hard', 0),
(4, 46, 0, '4.879 secs', 'asdfas', 1, 'challenging', 0),
(4, 47, 1, '10.505 secs', 'Madison', 0, 'hard', 0),
(4, 48, 2, '2.944 secs', 'asfasf', 1, 'challenging', 0),
(4, 43, 3, '10.799 secs', 'monroe', 1, 'challenging', 0),
(4, 41, 4, '7.498 secs', 'jefferson', 1, 'challenging', 0),
(4, 40, 5, '5.859 secs', 'asfasdf sca', 0, 'hard', 0),
(4, 38, 6, '4.025 secs', 'washingotn', 1, 'challenging', 0),
(4, 42, 7, '2.622 secs', 'safasdf', 0, 'challenging', 0),
(5, 133, 0, '8.625 secs', 'noo idea', 0, 'hard', 1),
(5, 132, 1, '8.997 secs', 'asfdasdfa', 0, 'challenging', 0),
(5, 102, 2, '20.039 secs', 'shang', 1, 'hard', 1),
(5, 103, 3, '6.38 secs', 'shang', 0, 'hard', 0),
(5, 101, 4, '10.336 secs', 'chow', 0, 'impossible', 1),
(6, 226, 0, '7.106 secs', '', 1, 'easy', 0),
(6, 227, 1, '4.107 secs', '', 0, 'hard', 0),
(6, 210, 2, '3.405 secs', '', 1, 'easy', 0),
(6, 207, 3, '6.425 secs', '', 1, 'easy', 0),
(6, 223, 4, '3.175 secs', '', 1, 'challenging', 0),
(7, 182, 0, '14.747 secs', '', 1, 'easy', 0),
(7, 42, 1, '8.919 secs', 'JAmes Madisno', 1, 'easy', 0),
(8, 261, 0, '17.806 secs', 'Gerald Ford', 1, 'easy', 0),
(8, 254, 1, '27.647 secs', 'Franklin Delano Roosevelt', 1, 'easy', 0),
(9, 50, 0, '1.457 secs', '', 1, 'challenging', 0),
(9, 49, 1, '4.641 secs', '', 0, 'hard', 0),
(9, 26, 2, '0.697 secs', '', 0, 'challenging', 0),
(9, 24, 3, '3.213 secs', '', 0, 'hard', 0),
(9, 20, 4, '3.044 secs', '', 1, 'challenging', 0),
(9, 22, 5, '10.602 secs', '', 1, 'hard', 0),
(9, 27, 6, '0.754 secs', '', 0, 'hard', 0),
(9, 28, 7, '1.68 secs', '', 1, 'challenging', 0),
(10, 246, 0, '9.908 secs', 'dec 7 1941', 1, 'easy', 0),
(10, 243, 1, '22.417 secs', '1963', 1, 'challenging', 0),
(10, 244, 2, '1.475 secs', '', 0, 'hard', 0),
(10, 245, 3, '6.65 secs', '1969', 1, 'challenging', 0),
(10, 247, 4, '2.49 secs', '', 0, 'challenging', 0),
(10, 241, 5, '3.157 secs', '', 0, 'hard', 0),
(10, 242, 6, '8.679 secs', '', 0, 'challenging', 0),
(11, 20, 0, '1.465 secs', '', 1, 'challenging', 0),
(11, 49, 1, '0.62 secs', '', 0, 'hard', 0),
(11, 50, 2, '0.78 secs', '', 1, 'hard', 0),
(11, 28, 3, '0.477 secs', '', 0, 'hard', 0),
(11, 26, 4, '2.61 secs', '', 0, 'challenging', 0),
(11, 24, 5, '1.852 secs', '', 1, 'challenging', 0),
(11, 27, 6, '3.186 secs', '', 1, 'hard', 0),
(11, 22, 7, '1.126 secs', '', 1, 'challenging', 0),
(12, 202, 0, '0.482 secs', '', 0, 'challenging', 0),
(12, 201, 1, '36.514 secs', '', 1, 'challenging', 0),
(12, 111, 2, '38.86 secs', '', 1, 'hard', 0),
(13, 129, 0, '35.162 secs', '', 1, 'challenging', 0),
(13, 130, 1, '32.572 secs', '', 1, 'challenging', 0),
(14, 34, 0, '21.284 secs', '', 0, 'hard', 0),
(14, 30, 1, '19.979 secs', '', 1, 'challenging', 0),
(14, 54, 2, '21.853 secs', '', 1, 'challenging', 0),
(14, 55, 3, '18.71 secs', '', 1, 'challenging', 0),
(14, 33, 4, '21.075 secs', '', 1, 'challenging', 0),
(15, 34, 0, '19.737 secs', '', 1, 'challenging', 0),
(15, 55, 1, '19.919 secs', '', 1, 'challenging', 0),
(15, 30, 2, '18.887 secs', '', 1, 'challenging', 0),
(15, 54, 3, '20.123 secs', '', 1, 'challenging', 0),
(15, 33, 4, '16.382 secs', '', 0, 'hard', 0),
(16, 98, 0, '2.471 secs', '', 1, 'challenging', 0),
(17, 227, 0, '5.524 secs', '', 0, 'impossible', 0),
(17, 210, 1, '5.391 secs', '', 1, 'challenging', 0),
(18, 60, 0, '0.946 secs', '', 1, 'easy', 0),
(19, 117, 0, '4.036 secs', '', 1, 'easy', 0),
(19, 116, 1, '2.248 secs', '', 1, 'easy', 0),
(19, 168, 2, '9.471 secs', 'Montpelier', 1, 'challenging', 0),
(20, 185, 0, '20.629 secs', '', 1, 'easy', 1),
(20, 181, 1, '1.716 secs', '', 1, 'easy', 0),
(20, 189, 2, '12.402 secs', '', 1, 'easy', 0),
(20, 190, 3, '0.227 secs', '', 1, 'easy', 0),
(20, 198, 4, '0.01 secs', '', 1, 'easy', 0),
(21, 164, 0, '12.3 secs', '', 1, 'challenging', 0),
(22, 273, 0, '2.331 secs', '', 1, 'easy', 0),
(22, 48, 1, '2.518 secs', '', 0, 'hard', 1),
(23, 311, 0, '1.384 secs', '', 1, 'easy', 0),
(23, 324, 1, '3.147 secs', '', 0, 'hard', 0),
(23, 315, 2, '1.326 secs', '', 1, 'easy', 0),
(23, 317, 3, '1.322 secs', '', 1, 'easy', 0),
(23, 84, 4, '6.488 secs', '', 0, 'easy', 0),
(23, 290, 5, '1.975 secs', '', 1, 'easy', 0),
(23, 320, 6, '11.57 secs', '', 0, 'easy', 0),
(23, 313, 7, '2.092 secs', '', 1, 'easy', 0),
(23, 87, 8, '0.689 secs', '', 1, 'easy', 0),
(23, 308, 9, '7.643 secs', '', 1, 'easy', 0),
(23, 79, 10, '2.905 secs', '', 1, 'easy', 0),
(23, 89, 11, '5.172 secs', '', 1, 'easy', 0),
(23, 328, 12, '15.217 secs', '', 1, 'easy', 0),
(23, 325, 13, '1.515 secs', '', 1, 'easy', 0),
(23, 293, 14, '2.652 secs', '', 1, 'easy', 0),
(23, 92, 15, '7.106 secs', '', 1, 'easy', 0),
(23, 307, 16, '4.397 secs', '', 1, 'easy', 0),
(23, 299, 17, '1.953 secs', '', 1, 'easy', 0),
(23, 326, 18, '4.558 secs', '', 1, 'easy', 0),
(23, 309, 19, '4.715 secs', '', 1, 'easy', 0),
(23, 83, 20, '1.857 secs', '', 1, 'easy', 0),
(23, 300, 21, '2.185 secs', '', 1, 'easy', 0),
(23, 85, 22, '2.086 secs', '', 1, 'easy', 0),
(23, 318, 23, '4.103 secs', '', 1, 'easy', 0),
(23, 296, 24, '2.166 secs', '', 1, 'easy', 0),
(23, 304, 25, '4.516 secs', '', 1, 'easy', 0),
(23, 305, 26, '6.875 secs', '', 1, 'easy', 0),
(23, 314, 27, '0.682 secs', '', 1, 'easy', 0),
(23, 298, 28, '7.343 secs', '', 1, 'easy', 0),
(23, 289, 29, '2.152 secs', '', 1, 'easy', 0),
(23, 91, 30, '0.932 secs', '', 1, 'easy', 0),
(23, 323, 31, '22.569 secs', '', 1, 'easy', 0),
(23, 306, 32, '3.727 secs', '', 1, 'easy', 0),
(23, 312, 33, '2.825 secs', '', 1, 'easy', 0),
(23, 297, 34, '44.734 secs', '', 1, 'easy', 0),
(23, 88, 35, '2.793 secs', '', 1, 'easy', 0),
(23, 80, 36, '4.005 secs', '', 1, 'easy', 0),
(23, 303, 37, '3.73 secs', '', 1, 'easy', 0),
(23, 310, 38, '4.007 secs', '', 1, 'easy', 0),
(23, 86, 39, '0.856 secs', '', 1, 'easy', 0),
(23, 82, 40, '1.609 secs', '', 1, 'easy', 0),
(23, 90, 41, '2.039 secs', '', 1, 'easy', 0),
(23, 329, 42, '4.316 secs', '', 1, 'easy', 0),
(23, 94, 43, '4.684 secs', '', 1, 'easy', 0),
(23, 302, 44, '22.902 secs', '', 1, 'easy', 0),
(23, 319, 45, '3.229 secs', '', 0, 'challenging', 0),
(23, 327, 46, '5.092 secs', '', 0, 'impossible', 0),
(23, 74, 47, '4.215 secs', '', 1, 'easy', 0),
(23, 292, 48, '7.628 secs', '', 1, 'easy', 0),
(23, 322, 49, '4.947 secs', '', 1, 'easy', 0);

-- --------------------------------------------------------

--
-- Table structure for table `flash_card_roles`
--

CREATE TABLE IF NOT EXISTS `flash_card_roles` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `secVal` int(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `flash_card_roles`
--

INSERT INTO `flash_card_roles` (`id`, `name`, `description`, `secVal`) VALUES
(1, 'User', 'Basic User', 10),
(2, 'Moderator', 'Basic Moderator', 100),
(3, 'Admin', 'Basic Admin', 500),
(4, 'SuperAdmin', 'SuperAdmin', 1000);

-- --------------------------------------------------------

--
-- Table structure for table `flash_card_users`
--

CREATE TABLE IF NOT EXISTS `flash_card_users` (
  `id` int(40) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `roleId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=13 ;

--
-- Dumping data for table `flash_card_users`
--

INSERT INTO `flash_card_users` (`id`, `username`, `password`, `roleId`) VALUES
(1, 'sddinwid@gmail.com', 'edcd0929abea8f25969fc4f5014db552', 4),
(4, 'Mistydenson', '76f6ad55122a3e536971b15ca0cb41bb', 1),
(5, 'sddinwid@yahoo.com', 'edcd0929abea8f25969fc4f5014db552', 1),
(6, 'Kenstar777', '35d4f92e22b1813b65471bcd9d19004a', 1),
(7, 'testuser@test.com', '05a671c66aefea124cc08b76ea6d30bb', 1),
(8, 'Kenstar777@aol.com', 'b62828b696f224327e341d6fe20007a3', 4),
(9, 'testme@test.com', '05a671c66aefea124cc08b76ea6d30bb', 1),
(10, 'Kenstars777@gmail.com', 'b8c151684feda3d3f9ac2d02e1b12af7', 1),
(11, 'DrKErickson@gmail.com', '23c3417df3b916f7147555cdab0cbb70', 1),
(12, 'denson.misty@gmail.com', '76f6ad55122a3e536971b15ca0cb41bb', 1);

-- --------------------------------------------------------

--
-- Table structure for table `flash_card_user_preferences_quiz`
--

CREATE TABLE IF NOT EXISTS `flash_card_user_preferences_quiz` (
  `userid` int(15) NOT NULL,
  `correctKey` int(8) NOT NULL,
  `incorrectKey` int(8) NOT NULL,
  `easyKey` int(8) NOT NULL,
  `challengingKey` int(8) NOT NULL,
  `hardKey` int(8) NOT NULL,
  `impossibleKey` int(8) NOT NULL,
  `last_saved` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`userid`)
) ENGINE=MyISAM DEFAULT CHARSET=ascii;

--
-- Dumping data for table `flash_card_user_preferences_quiz`
--

INSERT INTO `flash_card_user_preferences_quiz` (`userid`, `correctKey`, `incorrectKey`, `easyKey`, `challengingKey`, `hardKey`, `impossibleKey`, `last_saved`) VALUES
(1, 49, 50, 51, 87, 69, 82, '2015-04-25 19:27:43'),
(8, 71, 72, 74, 75, 76, 59, '2015-04-25 23:54:31'),
(0, 0, 0, 0, 0, 0, 0, '2015-04-25 04:04:18');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
