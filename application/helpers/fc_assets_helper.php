<?php

if (!function_exists('assets_url')) {

    function assets_url ()
    {

        $CI =& get_instance();
        return $CI->config->item('assets_url');

    }

}
if (!function_exists('media_url')) {

    function media_url ()
    {

        $CI =& get_instance();
        return $CI->config->item('media_url');

    }

}