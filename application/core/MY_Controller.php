<?php
/**
 * Created by PhpStorm.
 * User: Scott
 * Date: 11/2/2014
 * Time: 11:50 AM
 */

class MY_Controller extends CI_Controller
{

    protected $the_user;

    public function __construct()
    {

        parent::__construct();

    }
}

//        $this->load->config('app_config');

        /*		$this->session->set_userdata('controller_name', $this->router->fetch_directory() . $this->router->fetch_class());
                $this->session->set_userdata('action_name', $this->router->fetch_method());
        */
//        if (!$this->input->is_ajax_request()) {
//            $this->session->set_userdata('uri_string', uri_string());
//        }
//
//        if ($this->config->item('maintenance_mode') == true && $this->session->userdata('site_key') == false) {
//            redirect('site-closed');
//        }

        /*
         * if logged in get the user data, else goto the login page
         */
//        if ($this->ion_auth->logged_in()) {
//
//            $data = new stdClass();
//
//            $user_id = $this->session->userdata('adminUser');
//            $data->the_user = $this->the_user = $this->ion_auth->user($user_id)->row();
//            $data->the_user->inOverride = $this->the_user->inOverride = $user_id != false;
//            $user_groups = $this->ion_auth->get_users_groups($this->the_user->id)->result();
//
//            $this->the_user->groups = array_map(function ($value) {
//                return $value->name;
//            }, $user_groups);
//            $this->load->model('Partner_model');
//            $partner_info = $this->Partner_model->getPartnerDetails($this->the_user->partner_id);
//            $this->the_user->branded = $partner_info->branded;
//            $this->the_user->logo = $this->the_user->partner_id == 0 || $partner_info->branded === 'N' ?
//                assets_url() . '/images/sep-logo-01.png' :
//                media_url() . '/partners/logos/' . $partner_info->logo_image;
//
//            $this->the_user->partner_name = $partner_info->partner_name;
//            $this->the_user->partner_referral = $partner_info->referral;
//
//            $this->data['the_user'] = $this->the_user;
//
//            $this->load->vars($data);
//
//            if ($this->session->userdata('password_flag') === 'Y') {
//                redirect('/auth/change_password');
//            }
//
//        } else {
//            redirect('login');
//        }
//    }
//
//}

/**
 * The Admin_Controller is used for all of the SEP admin section
 *
 */
//class Admin_Controller extends MY_Controller
//{
//
//    public function __construct() {
//
//        parent::__construct('admin');
//
//        $group = array('admin', 'super');
//
//        if ($this->ion_auth->in_group($group)) {
//            $this->load->get_var('the_user')->user_type = 'admin';
//
//            // Set a session admin structure
//            $data = array(
//                'admin_override' => array(
//                    'partner_id' => '',
//                    'student_id' => '',
//                ),
//            );
//
//            $this->session->set_userdata($data);
//
//        } else {
//            redirect('/');
//        }
//    }
//
//}
//
///**
// * The Corp_Controller is used for the corporate admin section where the corporate
// * clients can manage and monitor their employees progress.
// *
// */
//class Corp_Controller extends MY_Controller
//{
//
//    public function __construct() {
//
//        parent::__construct();
//
//        $group = array('admin', 'super', 'corp_adm', 'corp_man');
//
//        if ($this->ion_auth->in_group($group)) {
//            $this->load->model('Partner_model');
//            $options = $this->Partner_model->getManagerOptions($this->the_user->user_id);
//            if (!empty($options)) {
//                $this->load->get_var('the_user')->manage_branch = $options->branch_admin;
//                $this->load->get_var('the_user')->manage_orders = $options->place_orders;
//                $this->load->get_var('the_user')->needs_approval = $options->order_approval;
//            }
//            else {
//                $this->load->get_var('the_user')->manage_branch = 0;
//                $this->load->get_var('the_user')->manage_orders = 1;
//                $this->load->get_var('the_user')->needs_approval = 0;
//            }
//
//            $this->load->get_var('the_user')->user_type = 'corp';
//        } else {
//            redirect('/');
//        }
//    }
//
//}
//
///**
// * The User_Controller is use in the student portal and when login is needed to the
// * public facing side of the website.
// *
// */
//class User_Controller extends MY_Controller
//{
//
//    public function __construct() {
//
//        parent::__construct();
//
//        $group = array('students', 'admin', 'super', 'corp_adm', 'corp_man');
//
//        if ($this->ion_auth->in_group($group)) {
//            $this->load->get_var('the_user')->user_type = 'user';
//
//            $className = $this->router->fetch_directory() . $this->router->fetch_class();
//            $actionName = $this->router->fetch_method();
//
//            $this->load->get_var('the_user')->controller_name = $className;
//            $this->load->get_var('the_user')->action_name = $actionName;
//
//        } else {
//            $this->ion_auth->logout();
//            redirect('/');
//        }
//    }
//
//}
//
///**
// * Public controller is used for sections of the site that does not need to be logged in to use.
// * If logged in need to get the user information so it can be used int these controllers.
// */
//class Public_Controller extends CI_Controller
//{
//
//    protected $the_user;
//
//    public function __construct() {
//
//        parent::__construct();
//
//        $this->load->config('app_config');
//
//        if ($this->config->item('maintenance_mode') == true && $this->session->userdata('site_key') == false) {
//            redirect('site-closed');
//        }
//
//        /*		$this->session->set_userdata('controller_name', $this->router->fetch_directory() . $this->router->fetch_class());
//                $this->session->set_userdata('action_name', $this->router->fetch_method());*/
//
//        if (!$this->input->is_ajax_request()) {
//            $this->session->set_userdata('uri_string', uri_string());
//        }
//
//        if ($this->ion_auth->logged_in()) {
//
//            $user_id = $this->session->userdata('adminUser');
//            $data->the_user = $this->the_user = $this->ion_auth->user($user_id)->row();
//
//            $data->the_user = $this->the_user;
//            $data->the_user->inOverride = $this->the_user->inOverride = $user_id != false;
//
//            $user_groups = $this->ion_auth->get_users_groups($this->the_user->id)->result();
//
//            $this->the_user->groups = array_map(function ($value) {
//                return $value->name;
//            }, $user_groups);
//            $this->load->model('Partner_model');
//            $partner_info = $this->Partner_model->getPartnerDetails($this->the_user->partner_id);
//            $this->the_user->branded = $partner_info->branded;
//            $this->the_user->logo = $this->the_user->partner_id == 0 || $partner_info->branded === 'N' ?
//                assets_url() . '/images/sep-logo-01.png' :
//                media_url() . '/partners/logos/' . $partner_info->logo_image;
//
//            $this->the_user->partner_name = $partner_info->partner_name;
//
//            $this->data['the_user'] = $this->the_user;
//
//            $this->load->vars($data);
//
//            $controller = $this->router->fetch_directory();
//            if ($controller == 'welcome') {
//                redirect('student');
//            }
//
//        }
//        else {
//            $this->the_user = new stdClass();
//            $data =  new stdClass();
//
//            $this->load->model('Partner_model');
//            $partner_id = $this->session->userdata('partner_id') ?: 0;
//            $this->the_user->partner_id = $partner_id;
//            $partner_info = $this->Partner_model->getPartnerDetails($partner_id);
//            $this->the_user->branded = $partner_info->branded;
//            $this->the_user->logo = !$partner_id || $partner_info->branded === 'N' ?
//                assets_url() . '/images/sep-logo-01.png' :
//                media_url() . '/partners/logos/' . $partner_info->logo_image;
//            $this->the_user->partner_referral = $partner_info->referral;
//
//            $data->the_user = $this->the_user;
//
//            $this->load->vars($data);
//
//        }
//
//    }
//
//}