<?
if(isset($this->session->userdata['fc_validated'])) {
    ?>

<div id="viewCardSetBody" class="view-cardsets-page col-md-12">
    <div class="view-container">

        <div class="view-body overflow-hidden">
            <div id="cardsetSelector" class="cardset-selector">
                <div>
                    <label for="selectCardset">Select Cardset: </label>
                    <select id="selectCardset" class="" style="min-width:200px;" name="selectCardset" placeholder="Select Cardset" size="1">
                        <option value="-1" selected="selected">Select one</option>
                        <option value="0">Orphaned Cards(no cardset)</option>
                        <?php  

                            foreach ($cardsets as $single) {
                                # code...
                                echo "<option id=\"cardset_".$single->id."\" class=\"\" value=\"".$single->id."\">".$single->name."</option>";
                            }

                        ?>  
                    </select>
                </div>
            </div>
            <div id="cardsetInfo" class="cardset-info">
              <form id="updateCardsetInfoForm" name="updateCardsetInfoForm">
              <input id="updateCardsetId" type="hidden" name="id">
              <input id="updateCardsetModified" type="hidden" name="modified_on">
                <div class="info-short-items name-info">
                    <label for="name">Name: </label><br />
                    <input id="name" name="name" size="25" />
                </div>
                <div class="info-short-items description-info">
                    <label for="description">Description: </label><br />
                    <textarea id="viewCardSetDescription" name="description"></textarea>
                </div>
                <div class="info-short-items categorization-container">
                    <label for="categorization"><span class="form-label">Categorization: </span></label><br>
                    <div id="selcModDiv" class="selc-mod-div">
                        <select id="cardSetCategorization" class="" style="min-width:120px;" name="categorization" placeholder="" size="5">

                        </select>
                        <div id="modBtns" class="mod-buttons">
                            <span id="moveTop" class="fa fa-angle-double-up"></span>
                            <span id="moveUp" class="fa fa-angle-up"></span>
                            <span id="deleteCat" class="fa fa-times"></span>
                            <span id="moveDown" class="fa fa-angle-down"></span>
                            <span id="moveBottom" class="fa fa-angle-double-down"></span>
                        </div>
                    </div>
                    <br /><br />
                    <label for="categorySelect"><span class="form-label" >Add Category</span>:</label>
                    <select id="categorySelect" class="">
                    <? 
                        foreach ($category as $single) {
                            # code...
                            echo "<option id=\"category_".$single->id."\" class=\"activeHover\" value=\"".$single->id."\" >".$single->name."</option>";
                        }
                    ?>
                    </select>
                    <input type="hidden" name="active" value="1" />
                    <br /><br />
                    <button id="updateCardsetSubmitButton" type="submit" class="btn btn-primary" >Save changes</button>
                    
                </div>
              </form>
            </div>
            <div id="cardsInSet" class="cards-in-set">
                <div id="cardsInSetHeader" class="cards-in-set-header">
                    <div>
                        <span>Cards in set</span>
                    </div>
                    <div style="float:right;">
                        <button id="addNewCard" type="button" class="btn btn-info"  data-toggle="modal" data-target="#newCardWindow">Add Card</button>
                    </div>
                </div>       
                <div id="cardsInSetListing" class="cards-in-set-listing">
                    <table id="cardsInSetTable">
                        <thead>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>             
            </div>
        </div>

    </div>
</div>
<div class="view-footer">

</div>
<div id="newCardWindow" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Add a card</h4>
            </div>
            <div class="modal-body">
                <form id="newCardForm" name="newCardForm" class="new-card-form">
                    <div>
                        <ul class="nav nav-tabs" role="tablist">
                          <li role="presentation" class="active"><a href="#general" role="tab" data-toggle="tab">General</a></li>
                          <li role="presentation"><a href="#question" role="tab" data-toggle="tab">Question</a></li>
                          <li role="presentation"><a href="#answer" role="tab" data-toggle="tab">Answer</a></li>
                          <li role="presentation"><a href="#hint" role="tab" data-toggle="tab">Hint</a></li>
                            <li role="presentation"><a href="#prompt" role="tab" data-toggle="tab">Prompt</a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="general" role="tabpanel" class="tab-pane active">
                                <div class="top-left-form-div">
                                    <div class="short-items">
                                        <label for=""><span class="form-label">Name: </span></label>
                                        <input id="addCardName" name="cardname">
                                    </div>
                                    <div class="short-items categorization-container">
                                        <label for="categorization"><span class="form-label">Categorization: </span></label>
                                        <select id="categorization" class="firstselect" style="" name="categorization" placeholder="Add Category" size="3">

                                        </select>
                                        <label for="categorySelect">Select <span class="form-label" > Main Category</span>:</label>
                                        <select id="categorySelect" class="secselect" style="">
                                        <? 
                                            foreach ($category as $single) {
                                                # code...
                                                echo "<option id=\"category_".$single->id."\" class=\"\" value=\"".$single->id."\" >".$single->name."</option>";
                                            }
                                        ?>
                                        </select>
                                    </div>
                                    <div class="short-items">
                                        <label for="cardset_id"><span class="form-label">CardSet Selection</span></label>
                                        <select id="addCardCardset_id"   class="" name="cardset_id" style="width: 150px;">
                                            <option value="-1">None</option>
                                            <?php  

                                            foreach ($cardsets as $single) {
                                                # code...
                                                echo "<option id=\"cardset_".$single->id."\" class=\"\" value=\"".$single->id."\">".$single->name."</option>";
                                            }

                                            ?>  
                                        </select>
                                    </div>
                                </div>
                                <div class="description-div">
                                    <label for="description"><span class="form-label">Description: </span></label>
                                    <br />
                                    <textarea id="addCardDescription" name="description"></textarea>
                                </div>
                            </div>
                            <div id="hint" role="tabpanel" class="tab-pane new-card-data new-card-hint-div">
                                <div class="pull-left col-lg-2">
                                    <label for="hint"><span class="form-label">Hint: </span></label>
                                </div>
                                <div class="pull-right col-lg-10">
                                    <textarea id="addCardHint" name="hint"></textarea>
                                </div>
                            </div>
                            <div id="question" role="tabpanel"  class="tab-pane new-card-data new-card-question-div">
                                <div class="pull-left col-lg-2">
                                    <label for="question"><span class="form-label">Question:</span></label>
                                </div>
                                <div class="pull-right col-lg-10">
                                    <textarea id="addCardQuestion" name="question"></textarea>
                                </div>
                            </div>
                            <div id="answer" role="tabpanel"  class="tab-pane new-card-data new-card-answer-div">
                                <div class="pull-left col-lg-2">
                                    <label for="answer"><span class="form-label">Answer</span></label>
                                </div>
                                <div class="pull-right col-lg-10">
                                    <textarea id="addCardAnswer" name="answer"></textarea>
                                </div>
                            </div>
                            <div id="prompt" role="tabpanel"  class="tab-pane new-card-data new-card-prompt-div">
                                <div class="prompt-items">
                                    <input type="hidden" id="promptid" name="promptid" >
                                    <div id="textareadiv" class="textareadiv">
                                        <label for="prompt"><span class="form-label">Prompt</span></label>
                                        <textarea id="addCardPrompt" name="prompt"></textarea>
                                    </div>
                                    <div id="checks" class="checks">
                                        <div id="infoPrivate"><span >Private: </span>
                                            <div id="prvWrap" class="prvWrap">
                                                <input type="checkbox" id="private" name="private" checked="" style="visibility:hidden;"/>
                                                <label for="private"></label>
                                            </div>
                                        </div>
                                        <div id="infoActive"><span >Active: </span>
                                            <div id="chkWrap" class="chkWrap">
                                                <input type="checkbox" id="active" name="active" checked="" style="visibility:hidden;"/>
                                                <label for="active"></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="alarmCountDiv">
                                        <div id="alarmon">
                                            <table>
                                                <tr>
                                                    <th>Alarm On: </th>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td><input id="alarm" type="text" name="alarmTime" size="3" />
                                                        <label> secs</label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td><input id="wrong" type="text" name="alarmWrong" size="3" />
                                                        <label> wrong</label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <div id="dates" class="dates">
                                        <div id="date"><label for="date">Date: </label>
                                            <input id="prompt_added_date" name="prompt_added_date" size="18" readonly /></div>

                                        <div id="modified_datediv"><label for="modified_date">Last Modified Date: </label>
                                            <input id="modified_date" name="modified_date" size="18" readonly /></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?/**
                    <div style="display:none;">
                        
                        <input type="hidden" id="cardModified" value="" name="modified_on">
                    </div>**/
    ?>
                </form>

            </div>
            <div class="modal-footer">
                <button id="cardResetButton" type="button" class="btn btn-warning" >Reset</button>
                <button id="cardCloseButton" type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                <button id="cardSubmitButton" type="submit" class="btn btn-primary">Save changes</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?
} else {
    header('Location: /');
}
?>