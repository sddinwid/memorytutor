<div class="prompts-content">
    <div id="allPromptsTableDiv" class="all-prompts-table-div">
        <div style="float:right;">
            <button id="addNewPrompt" type="button" class="btn btn-info"  onclick="javascript:openAddPrompt();">Add Prompt</button>
        </div>
        <table id="allPromptsTable" class="all-prompts-table">
            <thead>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>


<div id="modifyPromptWindow" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Edit Prompt</h4>
            </div>
            <!--/**
            *  1   id	            int(12)
            2	prompt_text	    text
            3	prompt_category	int(11)
            4	prompt_date	    date
            5	modified_date	date
            6	userid_added	int(11)
            7	private	        tinyint(1)
            8	active          tinyint(1)
            */-->
            <div class="modal-body overflow-hidden">
                <form id="updatePromptInfoForm" name="promptForm" class="new-prompt-form" >
                    <div id="infoCont">
                        <div id="promptCont">
                            <label for="prompt">Prompt: </label><br />
                            <textarea id="prompt" name="prompt_text"></textarea>
                            <input type="hidden" id="id" name="id" />
                        </div>
                        <div id="category"><label for="category">Category: </label>
                            <input id="category" name="prompt_category" size="18" /></div>

                        <div id="date"><label for="date">Date: </label>
                            <input id="date" name="prompt_added_date" size="18" readonly /></div>

                        <div id="modified_datediv"><label for="modified_date">Last Modified Date: </label>
                            <input id="modified_date" name="modified_date" size="18" readonly /></div>

                        <div id="infoPrivate"><span >Private: </span>
                            <div id="prvWrap" class="prvWrap">
                                <input type="checkbox" id="private" name="private" checked="" style="visibility:hidden;"/>
                                <label for="private"></label>
                            </div>
                        </div>

                        <div id="infoActive"><span >Active: </span>
                            <div id="chkWrap" class="chkWrap">
                                <input type="checkbox" id="active" name="active" checked="" style="visibility:hidden;"/>
                                <label for="active"></label>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button id="newPromptCloseButton" type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                <button id="newPromptSubmitButton" type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->