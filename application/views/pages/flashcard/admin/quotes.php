<div class="quotes-content">
    <div id="allQuotesTableDiv" class="all-quotes-table-div">
        <div style="float:right;">
            <button id="addNewQuote" type="button" class="btn btn-info"  onclick="javascript:openAddQuote();">Add Quote</button>
        </div>
        <table id="allQuotesTable" class="all-quotes-table">
            <thead>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>


<div id="modifyQuoteWindow" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Edit Quote</h4>
            </div>
            <div class="modal-body overflow-hidden">
                <form id="updateQuoteInfoForm" name="quoteForm" class="new-quote-form" >
                    <div id="infoCont">
                        <div id="quoteCont">
                            <label for="quote">Quote: </label><br />
                            <textarea id="quote" name="quote_text"></textarea>
                            <input type="hidden" id="id" name="id" />
                        </div>
                        <div id="credit"><label for="credit">Credit: </label>
                            <input id="credit" name="quote_credit" size="18" /></div>

                        <div id="date"><label for="date">Date: </label>
                            <input id="date" name="quote_date" size="18" /></div>

                        <div id="infoActive"><span >Active: </span>
                            <div id="chkWrap" class="chkWrap">
                                <input type="checkbox" id="active" name="active" checked="" style="visibility:hidden;"/>
                                <label for="active"></label>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button id="newQuoteCloseButton" type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                <button id="newQuoteSubmitButton" type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->