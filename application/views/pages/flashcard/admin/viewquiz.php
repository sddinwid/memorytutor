<div class="viewquiz-content">
	<div id="backToQuizzes">
		<button class="btn btn-primary" onclick="javascript:window.location.href = '/fcadmin/quizzes/';">Back to Quizzes</button>
	</div>
	
	<div id="quizContent">
		<div id="quizData">
			<div>
				<div id="quizId">
					<label for="id">Quiz ID: </label>
					<input id="id" type="text" value="<? echo $quiz[0]->id; ?>" size="<? echo strlen($quiz[0]->id); ?>" disabled />
				</div>
				<div id="cardset_Name">
					<label for="cardsetname">Cardset Name: </label>
					<input id="cardsetname" type="text" value="<? echo $quiz[0]->cardsetname; ?>" size="<? echo strlen($quiz[0]->cardsetname); ?>" disabled />
				</div>
				<div id="cardsetId">
					<label for="cardset_id">Cardset ID: </label>
					<input id="cardset_id" type="text" value="<? echo $quiz[0]->cardset_id; ?>" size="<? echo strlen($quiz[0]->cardset_id); ?>" disabled />
				</div>
			</div>
			<div>
				<div id="cardset_user">
					<label for="username">User Email (Username): </label>
					<input id="username" type="text" value="<? echo $quiz[0]->username; ?>" size="<? echo strlen($quiz[0]->username); ?>" disabled />
				</div>
				<div id="dateTaken">
					<label for="date_taken">Date Taken: </label>
					<input id="date_taken" type="text" value="<? echo $quiz[0]->date_taken; ?>" size="<? echo strlen($quiz[0]->date_taken); ?>" disabled />
				</div>
				<div id="quizendtime">
					<label for="end_time">Quiz End Time: </label>
					<input id="end_time" type="text" value="<? echo $quiz[0]->end_time; ?>" size="<? echo strlen($quiz[0]->end_time); ?>" disabled />
				</div>
			</div>
			<div>
				<div id="quizcardlength">
					<label for="quiz_length">Cards Viewed: </label>
					<input id="quiz_length" type="text" value="<? echo $quiz[0]->quiz_length; ?>" size="<? echo strlen($quiz[0]->quiz_length); ?>" disabled />
				</div>
				<div id="questionCount">
					<label for="number_of_questions">Number of Questions: </label>
					<input id="number_of_questions" type="text" value="<? echo $quiz[0]->number_of_questions; ?>" size="<? echo strlen($quiz[0]->number_of_questions); ?>" disabled />
				</div>
				<div id="quizendtime">
					<label for="quiz_time">Quiz Time Length: </label>
					<input id="quiz_time" type="text" value="<? echo $quiz[0]->quiz_time; ?>" size="<? echo strlen($quiz[0]->quiz_time); ?>" disabled />
				</div>
				<div id="quizpercentcorrect">
					<label for="percent_correct">Percent Correct: </label>
					<input id="percent_correct" type="text" value="<? echo $quiz[0]->percent_correct; ?>%" size="<? echo strlen($quiz[0]->percent_correct); ?>" disabled />
				</div>
			</div>
		</div>
		<div id="quizResponse">
			<table class='draw-order' >
				
				<thead>
					<tr class='draw-order card header-top row' >
						<td colspan="100%" style="textalign: center;">Card Draw Order (Click row to view card.)</td>
					</tr>
					<tr class='draw-order card header row' >
						<td class='draw-order card header id' >Card ID</td>
						<td class='draw-order card header drw-ordr' >Draw Order</td>
						<td class='draw-order card header hnt-vwd' >Hint_Viewed</td>
						<td class='draw-order card header rtng' >Rating</td>
						<td class='draw-order card header rgt-wrng' >Right/Wrong</td>
						<td class='draw-order card header usr-ansr' >Users Answer</td>
						<td class='draw-order card header time-on-card' >Time spent on card</td>
					</tr>
				</thead>
				<tbody>
					<?
						foreach ($quiz['drawOrder'] as $card) {
							# code...
							echo "<tr class='draw-order card row' >";
							echo "<td class='draw-order card id' >".$card->card_id."</td>";
							echo "<td class='draw-order card drw-ordr' >".(((int)($card->draw_order))+1)."</td>";
							echo "<td class='draw-order card hnt-vwd' >".(($card->hint_viewed)? "Yes" : "No")."</td>";
							echo "<td class='draw-order card rtng' >".$card->rating."</td>";
							echo "<td class='draw-order card rgt-wrng' >".(($card->right_wrong)? "Correct" : "Not correct")."</td>";
							echo "<td class='draw-order card usr-ansr' >".(($card->answer)? $card->answer : "No answer given")."</td>";
							echo "<td class='draw-order card time-on-card' >".$card->time_on_card."</td>";
							echo "</tr>";
						}

					?>
				</tbody>
			</table>			
			<table class='cards' >
				
				<thead>
					<tr class='cards card header-top row' >
						<td colspan="100%" style="textalign: center;">Cards Drawn (Shown by clicking card above)</td>
					</tr>
					<tr class='cards card header row' >
						<td class='cards card header id' >Card ID</td>
						<td class='cards card header name' >Card Name</td>
						<td class='cards card header desc' >Card Description</td>
						<td class='cards card header categorization' >Categorization</td>
						<td class='cards card header hint' >Hint</td>
						<td class='cards card header question' >Question</td>
						<td class='cards card header answer' >Answer</td>
					</tr>
				</thead>
				<tbody>
					<?
						foreach ($cards as $card) {
							# code...
							echo "<tr class='cards card row' >";
							echo "<td class='cards card id' >".$card->id."</td>";
							echo "<td class='cards card name' >".$card->name."</td>";
							echo "<td class='cards card desc' >".$card->description."</td>";
							echo "<td class='cards card categorization' >".$card->categorization."</td>";
							echo "<td class='cards card hint' >".$card->hint."</td>";
							echo "<td class='cards card question' >".$card->question."</td>";
							echo "<td class='cards card answer' >".$card->answer."</td>";
							echo "</tr>";
						}

					?>
				</tbody>
			</table>	
		</div>
	</div>
</div>