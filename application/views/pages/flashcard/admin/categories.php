<div class="categories-content">
	<div id="allCategoriesTableDiv" class="all-categories-table-div">
        <table id="allCategoriesTable" class="all-categories-table">
            <thead>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

<div id="modifyCategoryWindow" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Edit Category</h4>
            </div>
            <div class="modal-body overflow-hidden">
                <form id="updateCategoryInfoForm" class="new-category-form" action="addNewCategory/">
                    <div>
                        <label for="name">Name: </label><br />
                        <input id="name" name="name" size="25" />
                        <input type="hidden" id="id" name="id" />
                    </div>
                    <div>
                        <label for="description">Description: </label><br />
                        <textarea id="categoryDescription" name="description"></textarea>
                    </div>
                    <div class="short-items categorization-container">
                        <label for="categorization"><span class="form-label">Categorization: </span></label>
                        <select id="categoryCategorization" class="" style="width:200px;" name="categorization" placeholder="Add Category" size="5">

                        </select>
                        <label for="categorySelect">Select <span class="form-label" >Main Category</span>:</label>
                        <select id="categorySelect" class="" style="width:100px;">
                            <?
                            // foreach ($category as $single) {
                            //     # code...
                            //     echo "<option id=\"category_".$single->id."\" class=\"activeHover\" value=\"".($single->categorization>0? $single->categorization."-".$single->id : $single->id)."\" >".$single->name."</option>";
                            // }
                            ?>
                        </select>
                    </div>
                    <div>
                        <label for="created_on">Created On: </label>
                        <input id="created_on" name="created_on" size="15" />
                        <br />
                        <label for="modified_on">Modified On: </label>
                        <input id="modified_on" name="modified_on" size="15" />

                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button id="newCategoryCloseButton" type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                <button id="newCategorySubmitButton" type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->