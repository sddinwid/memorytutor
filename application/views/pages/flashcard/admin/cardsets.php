<div class="cardsets-content">
	<div id="allCardsetsTableDiv" class="all-cardsets-table-div">
        <table id="allCardsetsTable" class="all-cardsets-table">
            <thead>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>
<div id="modifyCardSetWindow" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Edit Cardset</h4>
            </div>
            <div class="modal-body overflow-hidden">
                <form id="updateCardsetInfoForm" class="new-cardset-form" action="addNewCardSet/">
                    <div id="nameCont">
                        <label for="name">Name: </label><br />
                        <input id="name" name="name" size="25" />
                        <input type="hidden" id="id" name="id" />
                    </div>
                    <div id="descCont">
                        <label for="description">Description: </label><br />
                        <textarea id="cardSetDescription" name="description"></textarea>
                    </div>
                    <div id="catCont" class="short-items categorization-container">
                        <label for="categorization"><span class="form-label">Categorization: </span></label>
                        <select id="cardSetCategorization" class="" style="width:200px;" name="categorization" placeholder="Add Category" size="5">

                        </select>
                        <div id="catchange">
                            <span id="moveTop" class="fa fa-angle-double-up fa-lg"></span>
                            <span id="moveUp" class="fa fa-angle-up fa-lg"></span>
                            <span id="deleteCat" class="fa fa-times fa-lg"></span>
                            <span id="moveDown" class="fa fa-angle-down fa-lg"></span>
                            <span id="moveBottom" class="fa fa-angle-double-down fa-lg"></span>
                        </div>
                        <label for="categorySelect">Select <span class="form-label" >Category</span>:</label>
                        <select id="categorySelect" class="" style="width:100px;">
                        <? 
                             foreach ($category as $single) {
                                 # code...
                                 echo "<option id=\"category_".$single->id."\" class=\"activeHover\" value=\"".($single->categorization>0? $single->categorization."-".$single->id : $single->id)."\" >".$single->name."</option>";
                             }
                        ?>
                        </select>
                    </div>
                    <div id="infoCont">
                        <div id="infoCreated"><label for="created_on">Created On: </label>
                        <input id="created_on" name="created_on" size="18" /></div>
                        
                        <div id="infoModified"><label for="modified_on">Modified On: </label>
                        <input id="modified_on" name="modified_on" size="18" /></div>
                        
                        <div id="infoActive"><span >Active: </span>
                        <div id="chkWrap" class="chkWrap">
                            <input type="checkbox" id="active" name="active" checked="" style="visibility:hidden;"/>
                            <label for="active"></label>
                        </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button id="newCardsetCloseButton" type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                <button id="newCardsetSubmitButton" type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
var cardsets = '';
<?

if(isset($allCardsets)) { ?>
console.log("set");
	cardsets = <? echo $allCardsets; ?>; <?
} else {
	?>
	console.log("not set");
	cardsets = 0; <?	
}
?>
</script>