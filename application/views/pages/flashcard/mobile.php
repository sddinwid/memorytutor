<?php
/**
 * Created by PhpStorm.
 * User: Scott
 * Date: 11/1/2014
 * Time: 9:26 PM
 */

$brow_str = $_SERVER['HTTP_USER_AGENT'];
$brow = "";
$mobile = 0;
if(strpos($brow_str, "Mobile")) {
    $mobile = 1;
    if(strpos($brow_str, "Android")) {
        $brow = "android";
    }else if(strpos($brow_str, "Firefox")) {
        $brow = "firefox-mobile";
    }else if(strpos($brow_str, "AppleWebKit") &&
        strpos($brow_str, "Safari") &&
        !strpos($brow_str, "Chrome")) {
        $brow = "safari-mobile";
    }else if(strpos($brow_str, "Chrome")) {
        $brow = "chrome-mobile";
    }
}else if(strpos($brow_str, "Edge")) {
    $brow = "edge";
}else if(strpos($brow_str, "Firefox")) {
    $brow = "firefox";
}else if(strpos($brow_str, "Chrome")) {
    $brow = "chrome";
}else if(strpos($brow_str, "Safari")) {
    $brow = "safari";
}else if(strpos($brow_str, "Trident/7.0") && strpos($brow_str, "rv:11.0")){
    $brow = "IE11";
}

if(($this->session->userdata('fc_validated'))) {


}
?>

<!DOCTYPE html >
<head>
    <title>Memory Tutor | <?php echo $title ?></title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <link href="<?php echo FCASSETS ?>bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="<?php echo FCJS ?>jquery-ui-1.11.1/jquery-ui.min.css" type="text/css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.4/css/jquery.dataTables.min.css" type="text/css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.4/css/jquery.dataTables_themeroller.css" type="text/css" />
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet" />
    <link href='http://fonts.googleapis.com/css?family=Permanent+Marker' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="/assets/flashcard/css/mobile.css" type="text/css" />
    <?
    //<link rel="stylesheet" href="/assets/flashcard/css/mobile/<?php echo $page; .css" type="text/css" />
    ?>

    <script src="<?php echo FCJS?>jquery-1.11.1.min.js" type="text/javascript"></script>

    <script type="text/javascript">


        $(document).ready(function () {


            <?
            if(!isset($this->session->userdata['fc_validated'])) {

                ?>

            <?
        }
        ?>


        });

        var logos = <? echo $logo_images ?> ;

    </script>
</head>
<body class="page body <? echo $brow?" ".$brow:""; ?>" style="text-align: center;background-color: rgb(220, 220, 255);">
<div id="topMenu" class="top-menu">
    <span id="mobilemenu" class="menu-icon fa fa-bars"></span>
    <div id="info"></div>
    <div id="user"><? if(($this->session->userdata("username"))) echo($this->session->userdata("username")); ?></div>
</div>

<div id="mbody" class="mbody">
    <div id="topcontainer" class="top-container">
        <div class="logo-container">
            <span id="logo" class="site-logo"><img class="logo-image" alt="" src=""></span>
        </div>
        <div class="title-container">
            <span id="siteTitle" class="site-title">Memory Tutor</span>
        </div>
    </div>
    <div id="login" class="login">
        <div id="loginFormDiv" class="login-form-div">
            <form id="loginForm" class="login-form"  action='/fclogin/process' method='post' name='process'  onSubmit="return validateloginForm(this)">
                <input type="hidden" id="mobile" name="mobile" value="<?=$mobile ?>">
                <div id="unamed" class="lunamed">
                    <label for="username">Username: </label>
                    <input type="text" id="username" name="username">
                </div>
                <div id="upassd" class="lupassd">
                    <label for="password">Password: </label>
                    <input type="password" id="password" name="password">
                </div>
                <div id="formBtns" class="login-form-buttons">
                    <input id="loginCancel" class="login-cancel" type="button" value="Cancel">
                    <input id="loginSubmit" class="login-submit" type="submit" name="submit" value="Login">
                </div>
            </form>
        </div>
    </div>
    <?
    if(isset($this->session->userdata['fc_validated'])) {

        ?>
        <div id="home" class="home">

        </div>
        <div id="quiz" class="quiz">
            <div id="quizPage" class="quiz-page">
                <div id="quizMenuDiv" class="quiz-menu-div">
                    <label for="selectCardset">Select Cardset for Quiz:
                        <select id="selectCardset" class="" style="min-width:200px;" name="selectCardset"
                                placeholder="Select Cardset" size="1">
                            <option value="0" selected="selected">Select one</option>
                            <?php

                            foreach ($cardsets as $single) {
                                # code...
                                echo "<option id=\"cardset_" . $single->id . "\" class=\"\" value=\"" . $single->id . "\">" . $single->name . "</option>";
                            }

                            ?>
                        </select>
                    </label>
                </div>
                <div id="quizContent" class="quiz-content">
                    <div id='deckBlind' class='deckblind'>
                        <div id="quizStartDiv" class="quiz-start-div">
                            <div>
                                <span class="welcomequiz">Click to Start quiz.</span>
                            </div>
                            <div>
                                <span id="loadquiz"></span>
                            </div>
                        </div>
                    </div>
                    <div id='cardDeck' class='carddeck'></div>
                    <div id="showHint" class="show-hint">Show Hint</div>
                </div>
                <div id="userInteractionDiv" class="user-interaction-div">
                    <div id="cardRatingDiv" class="card-rating-div">
                        <div id="rightWrong" class="right-wrong">
                            <div id="correctKey" class="button-correct button-success">Correct</div>
                            <div id="incorrectKey" class="button-incorrect button-danger">Incorrect</div>
                        </div>
                        <div id="ratings">
                            <div id="easyKey" class="rating-button rating-button-1 button-primary">Easy</div>
                            <div id="challengingKey" class="rating-button rating-button-2 button-success">Challenging</div>
                            <div id="hardKey" class="rating-button rating-button-3 button-warning">Hard</div>
                            <div id="impossibleKey" class="rating-button rating-button-4 button-danger">Impossible</div>
                        </div>
                    </div>
                    <div id="scoresDiv" class="scores-div">
                        <div id="scoresRow1" class="scores-rows scores-row-1">
                            <div id="displayCorrect" class="">
                                <span>Correct: </span>
                                <span id="text">0</span>
                            </div>
                            <div id="displayIncorrect" class="">
                                <span>Incorrect: </span>
                                <span id="text">0</span>
                            </div>
                            <div id="displayPercent" class="">
                                <span>Percent: </span>
                                <span id="text">0</span>
                            </div>
                        </div>
                        <div id="scoresRow2" class="scores-rows scores-row-2">
                            <div id="displayButton1" class="">
                                <span>Easy: </span>
                                <span id="text">0</span>
                            </div>
                            <div id="displayButton2" class="">
                                <span>Challenging: </span>
                                <span id="text">0</span>
                            </div>
                            <div id="displayButton3" class="">
                                <span>Hard: </span>
                                <span id="text">0</span>
                            </div>
                            <div id="displayButton4" class="">
                                <span>Impossible: </span>
                                <span id="text">0</span>
                            </div>
                        </div>
                        <div id="scoresRow3" class="scores-rows scores-row-3">
                            <div id="totalAnswered" class="">
                                <span>Total Answered: </span>
                                <span id="text">0</span>
                            </div>
                            <div id="displayTimer">
                                <span>Timer: </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="scoresBtnDiv" class="scores-button-div">
                <div id="scoresBtn" class="scores-button">
                    <span>Scores</span>
                </div>
            </div>
        </div>
        <div id="allcardset" class="allcardset">

        </div>
        <div id="addcardset" class="addcardset">
            <div class="header">Add Cardset</div>
            <form id="newCardSetForm" class="new-cardset-form" name="newCardSetForm">
                <div>
                    <label for="name">Name: </label><br/>
                    <input id="name" name="name"/>
                </div>
                <div class="description-div">
                    <label for="description">Description: </label><br/>
                    <textarea id="cardSetDescription" name="description"></textarea>
                </div>
                <div class="short-items categorization-container">
                    <label for="cardSetCategorization"><span class="form-label">Categorization: </span></label>

                    <div class="sameline">
                        <input id="catcode" type="hidden" name="categorization" value="">
                        <ul id="cardSetCategorization" class="">
                        </ul>
                        <div id="modBtns" class="mod-buttons">
                            <span id="moveTop" class="fa fa-angle-double-up"></span>
                            <span id="moveUp" class="fa fa-angle-up"></span>
                            <span id="deleteCat" class="fa fa-times"></span>
                            <span id="moveDown" class="fa fa-angle-down"></span>
                            <span id="moveBottom" class="fa fa-angle-double-down"></span>
                        </div>
                    </div>
                    <label for="categorySelect">Select&nbsp;<span class="form-label">Main Category</span>:</label>
                    <select id="categorySelect" class="">
                        <option value="-1">Select One</option>
                        <?
                        foreach ($category as $single) {
                            # code...
                            echo "<option id=\"category_" . $single->id . "\" class=\"activeHover\" value=\"" . $single->id . "\" >" . $single->name . "</option>";
                        }
                        ?>
                    </select>
                </div>
                <div id="newCardsetSubmitContainer" class="newcardset-submit-container">
                    <button id="newCardsetSubmitButton" type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
        <div id="editcardset" class="editcardset">
            <div class="header">Edit Cardset</div>
            <form id="editCardSetForm" class="edit-cardset-form" name="editCardSetForm">
                <div>
                    <label for="name">Name: </label><br/>
                    <input id="name" name="name"/>
                    <input type="hidden" id="id" name="id"/>
                    <input type="hidden" id="userId" name="userId"/>
                </div>
                <div class="description-div">
                    <label for="description">Description: </label><br/>
                    <textarea id="cardSetDescription" name="description"></textarea>
                </div>
                <div class="short-items categorization-container">
                    <label for="cardSetCategorization"><span class="form-label">Categorization: </span></label>

                    <div class="sameline">
                        <input id="catcode" type="hidden" name="categorization" value="">
                        <ul id="cardSetCategorization" class="">
                        </ul>
                        <div id="modBtns" class="mod-buttons">
                            <span id="moveTop" class="fa fa-angle-double-up"></span>
                            <span id="moveUp" class="fa fa-angle-up"></span>
                            <span id="deleteCat" class="fa fa-times"></span>
                            <span id="moveDown" class="fa fa-angle-down"></span>
                            <span id="moveBottom" class="fa fa-angle-double-down"></span>
                        </div>
                    </div>
                    <label for="categorySelect">Select&nbsp;<span class="form-label">Main Category</span>:</label>
                    <select id="categorySelect" class="">
                        <option value="-1">Select One</option>
                        <?
                        foreach ($category as $single) {
                            # code...
                            echo "<option id=\"category_" . $single->id . "\" class=\"activeHover\" value=\"" . $single->id . "\" >" . $single->name . "</option>";
                        }
                        ?>
                    </select>
                    <div>
                        <label name="created_on"></label>
                        <label name="modified_on"></label>
                    </div>
                    <div id="checks" class="checks">
                        <div id="infoActive"><span>Active: </span>

                            <div id="chkWrap" class="chkWrap">
                                <input type="checkbox" id="active" name="active" checked=""
                                       style="visibility:hidden;"/>
                                <label for="active"></label>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="viewcards-cont" class="viewcard-container">
                    <button id="viewcardsbtn" class="viewcards-button" type="button">View Cards in Cardset</button>
                    <div id='cardscontainer' style="display: none;"></div>
                </div>
                <div id="editCardsetSubmitContainer" class="editcardset-submit-container">
                    <button id="editCardsetSubmitButton" type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
        <div id="addcard" class="addcard">
            <div class="header">Add Card</div>
            <form id="newCardForm" class="new-card-form">
                <div>
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#general" role="tab"
                                                                  data-toggle="tab">General</a></li>
                        <li role="presentation"><a href="#question" role="tab" data-toggle="tab">Question</a></li>
                        <li role="presentation"><a href="#answer" role="tab" data-toggle="tab">Answer</a></li>
                        <li role="presentation"><a href="#hint" role="tab" data-toggle="tab">Hint</a></li>
                        <li role="presentation"><a href="#prompt" role="tab" data-toggle="tab">Prompt</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="general" role="tabpanel" class="tab-pane active">
                            <div class="top-left-form-div">
                                <div class="short-items">
                                    <label for=""><span class="form-label">Name: </span></label>
                                    <input id="addCardName" name="cardname">
                                </div>
                                <div class="description-div">
                                    <label for="description"><span class="form-label">Description: </span></label>
                                    <textarea id="addCardDescription" name="description"></textarea>
                                </div>
                                <div class="short-items">
                                    <label for="cardset_id"><span class="form-label">CardSet Selection</span></label>
                                    <select id="addCardCardset_id" class="" name="cardset_id">
                                        <option value="-1">None</option>
                                        <?php

                                        foreach ($cardsets as $single) {
                                            # code...
                                            echo "<option id=\"cardset_" . $single->id . "\" class=\"\" value=\"" . $single->id . "\">" . $single->name . "</option>";
                                        }

                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div id="hint" role="tabpanel" class="tab-pane new-card-data new-card-hint-div">
                            <div class="col-lg-2">
                                <label for="hint"><span class="form-label">Hint: </span></label>
                            </div>
                            <div class="col-lg-10">
                                <textarea id="addCardHint" name="hint"></textarea>
                            </div>
                        </div>
                        <div id="question" role="tabpanel" class="tab-pane new-card-data new-card-question-div">
                            <div class="col-lg-2">
                                <label for="question"><span class="form-label">Question:</span></label>
                            </div>
                            <div class="col-lg-10">
                                <textarea id="addCardQuestion" name="question"></textarea>
                            </div>
                        </div>
                        <div id="answer" role="tabpanel" class="tab-pane new-card-data new-card-answer-div">
                            <div class="col-lg-2">
                                <label for="answer"><span class="form-label">Answer</span></label>
                            </div>
                            <div class="col-lg-10">
                                <textarea id="addCardAnswer" name="answer"></textarea>
                            </div>
                        </div>
                        <div id="prompt" role="tabpanel" class="tab-pane new-card-data new-card-prompt-div">
                            <div class="prompt-items">
                                <input type="hidden" id="promptid" name="promptid">

                                <div id="textareadiv" class="textareadiv">
                                    <label for="prompt"><span class="form-label">Prompt</span></label>
                                    <textarea id="addCardPrompt" name="prompt" rows="5"></textarea>
                                </div>
                                <div id="checks" class="checks">
                                    <div id="infoPrivate"><span>Private: </span>

                                        <div id="prvWrap" class="prvWrap">
                                            <input type="checkbox" id="private" name="private" checked=""
                                                   style="visibility:hidden;"/>
                                            <label for="private"></label>
                                        </div>
                                    </div>
                                    <div id="infoActive"><span>Active: </span>

                                        <div id="chkWrap" class="chkWrap">
                                            <input type="checkbox" id="active" name="active" checked=""
                                                   style="visibility:hidden;"/>
                                            <label for="active"></label>
                                        </div>
                                    </div>
                                </div>
                                <div id="alarmCountDiv">
                                    <div id="alarmon">
                                        <div class="row">
                                            <div class="left">
                                                <span>Alarm On:</span>
                                            </div>
                                            <div class="right">
                                                <div class="alarm-setting-table">
                                                    <div class="alarm-setting-time">
                                                        <input id="alarm" type="text" name="alarmTime" size="3"/>
                                                        <label> secs</label>
                                                    </div>
                                                    <div class="alarm-setting-wrong">
                                                        <input id="wrong" type="text" name="alarmWrong" size="3"/>
                                                        <label> wrong</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="dates" class="dates">
                                    <div id="date"><label for="date">Date: </label>
                                        <span id="prompt_added_date" > </span>
                                    </div>

                                    <div id="modified_datediv"><label for="modified_date">Last Modified Date: </label>
                                        <span id="modified_date"  ></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="newCardSubmitContainer" class="newcard-submit-container">
                    <button id="newCardSubmitButton" type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
        <div id="editcard" class="editcard">
            <div class="header">Edit Card</div>
            <form id="editCardForm" class="edit-card-form">
                <div>
                    <ul class="nav nav-tabs" role="tablist" >
                        <li role="presentation" class="active"><a href="#generalpane" aria-controls="general" role="tab" data-toggle="tab" >General</a></li>
                        <li role="presentation" ><a href="#questionpane" aria-controls="question" role="tab" data-toggle="tab">Question</a></li>
                        <li role="presentation" ><a href="#answerpane" aria-controls="answer" role="tab" data-toggle="tab">Answer</a></li>
                        <li role="presentation" ><a href="#hintpane" aria-controls="hint" role="tab" data-toggle="tab">Hint</a></li>
                        <li role="presentation" ><a href="#promptpane" aria-controls="prompt" role="tab" data-toggle="tab">Prompt</a></li>
                    </ul>
                    <div class="tab-content">
                        <div id="generalpane" role="tabpanel" class="tab-pane active" >
                            <div class="top-left-form-div">
                                <div class="short-items">
                                    <label for=""><span class="form-label">Name: </span></label>
                                    <input id="addCardName" name="name">
                                </div>
                                <div class="description-div">
                                    <label for="description"><span class="form-label">Description: </span></label>
                                    <textarea id="addCardDescription" name="description"></textarea>
                                </div>
                                <div class="short-items">
                                    <label for="cardset_id"><span class="form-label">CardSet Selection</span></label>
                                    <select id="addCardCardset_id" class="" name="cardset_id">
                                        <option value="-1">None</option>
                                        <?php

                                        foreach ($cardsets as $single) {
                                            # code...
                                            echo "<option id=\"cardset_" . $single->id . "\" class=\"\" value=\"" . $single->id . "\">" . $single->name . "</option>";
                                        }

                                        ?>
                                    </select>
                                </div>
                                <div class="short-items">
                                    <label for=""><span class="form-label">Created: </span></label>
                                    <br />
                                    <label id="cardCreated" name="created_on">
                                </div>
                                <div class="short-items">
                                    <label for=""><span class="form-label">Last Modified: </span></label>
                                    <br />
                                    <label id="lastmodified" name="modified_on">
                                </div>
                            </div>
                        </div>
                        <div id="hintpane" role="tabpanel" class="tab-pane new-card-data new-card-hint-div">
                            <div class="col-lg-2">
                                <label for="hint"><span class="form-label">Hint: </span></label>
                            </div>
                            <div class="col-lg-10">
                                <textarea id="addCardHint" name="hint"></textarea>
                            </div>
                        </div>
                        <div id="questionpane" role="tabpanel" class="tab-pane">
                            <div class="col-lg-2">
                                <label for="question"><span class="form-label">Question:</span></label>
                            </div>
                            <div class="col-lg-10">
                                <textarea id="addCardQuestion" name="question"></textarea>
                            </div>
                        </div>
                        <div id="answerpane" role="tabpanel" class="tab-pane new-card-data new-card-answer-div">
                            <div class="col-lg-2">
                                <label for="answer"><span class="form-label">Answer</span></label>
                            </div>
                            <div class="col-lg-10">
                                <textarea id="addCardAnswer" name="answer"></textarea>
                            </div>
                        </div>
                        <div id="promptpane" role="tabpanel" class="tab-pane new-card-data new-card-prompt-div">
                            <div class="prompt-items">
                                <input type="hidden" id="promptid" name="promptid">

                                <div id="textareadiv" class="textareadiv">
                                    <label for="prompt"><span class="form-label">Prompt</span></label>
                                    <textarea id="addCardPrompt" name="prompt" rows="5"></textarea>
                                </div>
                                <div id="checks" class="checks">
                                    <div id="infoPrivate"><span>Private: </span>

                                        <div id="prvWrap" class="prvWrap">
                                            <input type="checkbox" id="private" name="private" checked=""
                                                   style="visibility:hidden;"/>
                                            <label for="private"></label>
                                        </div>
                                    </div>
                                    <div id="infoActive"><span>Active: </span>

                                        <div id="chkWrap" class="chkWrap">
                                            <input type="checkbox" id="active" name="active" checked=""
                                                   style="visibility:hidden;"/>
                                            <label for="active"></label>
                                        </div>
                                    </div>
                                </div>
                                <div id="alarmCountDiv">
                                    <div id="alarmon">
                                        <div class="row">
                                            <div class="left">
                                                <span>Alarm On:</span>
                                            </div>
                                            <div class="right">
                                                <div class="alarm-setting-table">
                                                    <div class="alarm-setting-time">
                                                        <input id="alarm" type="text" name="alarmTime" size="3"/>
                                                        <label> secs</label>
                                                    </div>
                                                    <div class="alarm-setting-wrong">
                                                        <input id="wrong" type="text" name="alarmWrong" size="3"/>
                                                        <label> wrong</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="dates" class="dates">
                                    <div id="date"><label for="date">Date: </label>
                                        <span id="prompt_added_date" > </span>
                                    </div>

                                    <div id="modified_datediv"><label for="modified_date">Last Modified Date: </label>
                                        <span id="modified_date"  ></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="newCardSubmitContainer" class="newcard-submit-container">
                    <button id="editCardSubmitButton" type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
        <div id="addcategory" class="addcategory">
            <div class="header">Add Category</div>
            <form id="newCategoryForm" class="" role="form">
                <div class="form-group">
                    <label for="newCategoryName" class="control-label">Name :</label>
                    <input type="text" id="newCategoryName" name="cat_name" class="formInput">
                </div>
                <div class="form-group description-div">
                    <label for="newCategoryDescription" class="control-label">Description :</label>
                        <textarea id="newCategoryDescription" name="description" class="formInput formTextArea"
                                  rows="5" style="resize: none"
                                  placeholder="Enter Description"></textarea>
                </div>
                <div class="form-group">
                    <label for="newCategoryParent" class="control-label">Subcategory of </label>
                    <select id="newCategoryParent" name="categorization"
                            class="formSelect">
                        <option value="-1">None (New Parent)</option>
                        <?
                        foreach ($category as $single) {
                            # code...
                            if ($single->name)
                                echo "<option id=\"" . $single->id . "\" name=\"" . $single->name . "\" class=\"activeHover\" value=\"" . ($single->categorization > 0 ? $single->categorization . "-" . $single->id : $single->id) . "\">" . $single->name . "</option>";
                        }
                        ?>
                    </select>
                </div>
                <div id="btnContainer" class="button-container">
                    <button id="newCategorySubmitButton" type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
        <div id="editcategory" class="editcategory">
            <div class="header">Edit Category</div>
            <form id="editCategoryForm" class="" role="form">
                <div class="form-group">
                    <label for="newCategoryName" class="control-label">Name :</label>
                    <input type="text" id="newCategoryName" name="cat_name" class="formInput">
                </div>
                <div class="form-group description-div">
                    <label for="newCategoryDescription" class="control-label">Description :</label>
                        <textarea id="newCategoryDescription" name="description" class="formInput formTextArea"
                                  rows="5" style="resize: none"
                                  placeholder="Enter Description"></textarea>
                </div>
                <div class="form-group">
                    <label for="newCategoryParent" class="control-label">Subcategory of </label>
                    <select id="newCategoryParent" name="categorization"
                            class="formSelect">
                        <option value="-1">None (New Parent)</option>
                        <?
                        foreach ($category as $single) {
                            # code...
                            if ($single->name)
                                echo "<option id=\"" . $single->id . "\" name=\"" . $single->name . "\" class=\"activeHover\" value=\"" . ($single->categorization > 0 ? $single->categorization . "-" . $single->id : $single->id) . "\">" . $single->name . "</option>";
                        }
                        ?>
                    </select>
                </div>
                <div id="btnContainer" class="button-container">
                    <button id="newCategorySubmitButton" type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
        <?
    }
    ?>
</div>

<?php


?>

<div id="footerDiv" class="footer-div">
    <strong>Memory Tutor &copy; 2014 - <script>document.write(new Date().getFullYear());</script></strong>
</div>
<div id="popoutmenu" class="popoutmenu" style="display: none;">
    <div id="menuHome">Home</div>
    <div id="menuTakeQuiz">Take Quiz</div>
    <div id="menuMyCardsets">View My Cardsets</div>
    <div id="menuAllCardsets">View All Cardsets</div>
    <div id="menuAddCardset">Add Cardset</div>
    <div id="menuAddCard">Add Card</div>
    <div id="menuAddCategory">Add Category</div>
    <div id="menuLogout">Logout</div>
</div>

<!-- -------------------------
                post page script
        ---------------------   -->


<script src="<?php echo FCASSETS ?>bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo FCJS ?>jquery-ui-1.11.1/jquery-ui.min.js" type="text/javascript"></script>
<script src="https://cdn.tinymce.com/4/tinymce.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/1.10.4/js/jquery.dataTables.js" type="text/javascript"></script>

<?
if(isset($mycardsets) && $mycardsets !=="false") {
    ?>
    <script>
        var mycardsets = <?= json_encode($mycardsets) ?>;
    </script>
    <?
}
?>
<script src="<?php echo FCJS ?>mobile.js" type="text/javascript"></script>


<script src="<?php echo FCJS ?>formValidator.js"></script>
<script src="<?php echo FCJS ?>card_formv.js"></script>
<script src="<?php echo FCJS ?>cardset_formv.js"></script>
<script src="<?php echo FCJS ?>category_formv.js"></script>
<script src="<?php echo FCJS ?>login_register_formv.js"></script>
<? if(($this->session->userdata("username"))) { ?>
    <script>
        $('div#mbody > div').slice(1).each(function(){if($(this).attr("id")!="home") $(this).hide();$(this).removeClass("hidden");});

    </script>
<?
}else { ?>
    <script>
        $('div#mbody > div').slice(1).each(function(){if($(this).attr("id")!="login") $(this).hide();});

    </script>
<?
} ?>
<script>

</script>
<?

if(($this->session->userdata('fc_validated'))) {

    ?>

    <?
}

//if(trim($page) == "viewcardsets") {
//    <script src="<?php echo FCJS card_formv.js"></script>
//    <script src="<?php echo FCJS cardset_formv.js"></script>
// } else if(trim($page) == "index") {
//    <script src="<?php echo FCJS card_formv.js"></script>
//    <script src="<?php echo FCJS cardset_formv.js"></script>
//    <script src="<?php echo FCJS category_formv.js"></script>
//    } else {
//
////<script>console.log('=$page ');</script>
//
//    //validateCategoryForm
//} ?>

<!-- -------------------------
     end post page script
---------------------   -->
</body>
</html>