
<div id="mainbody" class="main-body">

    <div id="quotesDiv" class="quotes-div"><div id="quotes" class="quotes"></div></div>
	<div id="centerArea" class="">
        <?
        if(isset($this->session->userdata['fc_validated'])) {
                    ?>

            <div id="leftsidebar" class="left-sidebar">
                <div id="sidebarmenu" class="sidebar-menu">
                    <ul>
                        <li id="addCategory" class="activeHover"  data-toggle="modal" data-target="#newCategoryWindow">Add a Category</li>
                        <li id="categoryNewCardSet" class="activeHover"  data-toggle="modal" data-target="#newCardSetWindow">Make New CardSet</li>
                        <li id="categoryAddCard" class="activeHover"  data-toggle="modal" data-target="#newCardWindow">Add Card</li>
                    </ul>
                </div>
            </div>
            <?
        }
            ?>

        <div id="content" class="content" >
            <div id="dataLoaded" class="data-loaded"></div>

        </div>
    </div>
</div>

<div id="newCategoryWindow" class="modal fade ">
    <div id="newCatContainer" class="modal-dialog modal-sm">
        <div class="modal-content ">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                        class="sr-only">Close</span></button>
                <h4 class="modal-title">Add New Category</h4>
            </div>
            <div id="newCategoryFormDiv" class="modal-body">
                <form id="newCategoryForm" class="" role="form">
                    <div class="form-group">
                        <label for="newCategoryName" class="control-label">Name :</label>
                        <input type="text" id="newCategoryName" name="cat_name" class="formInput" size="35">
                    </div>
                    <div class="form-group description-div">
                        <label for="newCategoryDescription" class="control-label">Description :</label>
                        <textarea id="newCategoryDescription" name="description" class="formInput formTextArea"
                                  cols="35" rows="5" style="resize: none"
                                  placeholder="Enter Description"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="newCategoryParent" class="control-label">Subcategory of </label>
                        <select id="newCategoryParent" name="categorization"
                                class="formSelect">
                            <option value="-1">None (New Parent)</option>
                            <?
                            foreach ($category as $single) {
                                # code...
                                if($single->name)
                                echo "<option id=\"" . $single->id . "\" name=\"" . $single->name . "\" class=\"activeHover\" value=\"" . ($single->categorization > 0 ? $single->categorization . "-" . $single->id : $single->id) . "\">" . $single->name . "</option>";
                            }
                            ?>
                        </select>
                    </div>
                    <br/>
                </form>
            </div>
            <div class="modal-footer">
                <button id="newCategoryFormQuit" type="button" class="btn btn-danger" data-dismiss="modal">Close
                </button>
                <button id="newCategoryFormSubmit" type="submit" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>
<div id="newCardWindow" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                        class="sr-only">Close</span></button>
                <h4 class="modal-title">Add a card</h4>
            </div>
            <div class="modal-body">
                <form id="newCardForm" class="new-card-form" action="addNewCard/">
                    <div>
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#general" role="tab" data-toggle="tab">General</a></li>
                            <li role="presentation"><a href="#question" role="tab" data-toggle="tab">Question</a></li>
                            <li role="presentation"><a href="#answer" role="tab" data-toggle="tab">Answer</a></li>
                            <li role="presentation"><a href="#hint" role="tab" data-toggle="tab">Hint</a></li>
                            <li role="presentation"><a href="#prompt" role="tab" data-toggle="tab">Prompt</a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="general" role="tabpanel" class="tab-pane active">
                                <div class="top-left-form-div">
                                    <div class="short-items">
                                        <label for=""><span class="form-label">Name: </span></label>
                                        <input id="addCardName" name="cardname">
                                    </div>
                                    <div class="description-div">
                                        <label for="description"><span class="form-label">Description: </span></label>
                                        <br />
                                        <textarea id="addCardDescription" name="description"></textarea>
                                    </div>
                                    <div class="short-items">
                                        <label for="cardset_id"><span class="form-label">CardSet Selection</span></label>
                                        <br>
                                        <select id="addCardCardset_id"   class="" name="cardset_id" style="width: 60%;">
                                            <option value="-1">None</option>
                                            <?php

                                            foreach ($cardsets as $single) {
                                                # code...
                                                echo "<option id=\"cardset_".$single->id."\" class=\"\" value=\"".$single->id."\">".$single->name."</option>";
                                            }

                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div id="hint" role="tabpanel" class="tab-pane new-card-data new-card-hint-div">
                                <div class="pull-left col-lg-2">
                                    <label for="hint"><span class="form-label">Hint: </span></label>
                                </div>
                                <div class="pull-right col-lg-10">
                                    <textarea id="addCardHint" name="hint"></textarea>
                                </div>
                            </div>
                            <div id="question" role="tabpanel"  class="tab-pane new-card-data new-card-question-div">
                                <div class="pull-left col-lg-2">
                                    <label for="question"><span class="form-label">Question:</span></label>
                                </div>
                                <div class="pull-right col-lg-10">
                                    <textarea id="addCardQuestion" name="question"></textarea>
                                </div>
                            </div>
                            <div id="answer" role="tabpanel"  class="tab-pane new-card-data new-card-answer-div">
                                <div class="pull-left col-lg-2">
                                    <label for="answer"><span class="form-label">Answer</span></label>
                                </div>
                                <div class="pull-right col-lg-10">
                                    <textarea id="addCardAnswer" name="answer"></textarea>
                                </div>
                            </div>
                            <div id="prompt" role="tabpanel"  class="tab-pane new-card-data new-card-prompt-div">
                                <div class="prompt-items">
                                    <input type="hidden" id="promptid" name="promptid" >
                                    <div id="textareadiv" class="textareadiv">
                                        <label for="prompt"><span class="form-label">Prompt</span></label>
                                        <textarea id="addCardPrompt" name="prompt"></textarea>
                                    </div>
                                    <div id="checks" class="checks">
                                        <div id="infoPrivate"><span >Private: </span>
                                            <div id="prvWrap" class="prvWrap">
                                                <input type="checkbox" id="private" name="private" checked="" style="visibility:hidden;"/>
                                                <label for="private"></label>
                                            </div>
                                        </div>
                                        <div id="infoActive"><span >Active: </span>
                                            <div id="chkWrap" class="chkWrap">
                                                <input type="checkbox" id="active" name="active" checked="" style="visibility:hidden;"/>
                                                <label for="active"></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="alarmCountDiv">
                                        <div id="alarmon">
                                            <table>
                                                <tr>
                                                    <th>Alarm On: </th>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td><input id="alarm" type="text" name="alarmTime" size="3" />
                                                        <label> secs</label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td><input id="wrong" type="text" name="alarmWrong" size="3" />
                                                        <label> wrong</label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <div id="dates" class="dates">
                                        <div id="date"><label for="date">Date: </label>
                                            <input id="prompt_added_date" name="prompt_added_date" size="18" readonly /></div>

                                        <div id="modified_datediv"><label for="modified_date">Last Modified Date: </label>
                                            <input id="modified_date" name="modified_date" size="18" readonly /></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                <button id="newCardSubmitButton" type="submit" class="btn btn-primary">Save changes</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="newCardSetWindow" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                        class="sr-only">Close</span></button>
                <h4 class="modal-title">Create a New Cardset</h4>
            </div>
            <div class="modal-body overflow-hidden">
                <form id="newCardSetForm" class="new-cardset-form" action="addNewCardSet/">
                    <div>
                        <label for="name">Name: </label><br/>
                        <input id="name" name="name" size="25"/>
                    </div>
                    <div class="description-div">
                        <label for="description">Description: </label><br/>
                        <textarea id="cardSetDescription" name="description"></textarea>
                    </div>
                    <div class="short-items categorization-container">
                        <label for="cardSetCategorization"><span class="form-label">Categorization: </span></label>
                        <br>
                        <div class="sameline">
                            <select id="cardSetCategorization" class="" style="width:200px;" name="categorization"
                                    placeholder="Add Category" size="5">

                            </select>
                            <div id="modBtns" class="mod-buttons">
                                <span id="moveTop" class="fa fa-angle-double-up"></span>
                                <span id="moveUp" class="fa fa-angle-up"></span>
                                <span id="deleteCat" class="fa fa-times"></span>
                                <span id="moveDown" class="fa fa-angle-down"></span>
                                <span id="moveBottom" class="fa fa-angle-double-down"></span>
                            </div>
                        </div>
                        <br>
                        <br>
                        <label for="categorySelect">Select&nbsp;<span class="form-label">Main Category</span>:</label>
                        <select id="categorySelect" class="" style="width:100px;">
                            <option value="-1">Select One</option>
                            <?
                            foreach ($category as $single) {
                                # code...
                                echo "<option id=\"category_" . $single->id . "\" class=\"activeHover\" value=\"" . ($single->categorization > 0 ? $single->categorization . "-" . $single->id : $single->id) . "\" >" . $single->name . "</option>";
                            }
                            ?>
                        </select>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button id="newCardsetCloseButton" type="button" class="btn btn-danger" data-dismiss="modal">Close
                </button>
                <button id="newCardsetSubmitButton" type="submit" class="btn btn-primary">Save changes</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
    var newUser = '';


    <?
    if(isset($this->session->userdata['fc_validated'])) {
        ?>
    $("#centerArea").addClass("border-top-black3px");
    <?
    }
    if((isset($this->session->userdata['fc_validated'])) && (isset($this->session->userdata['newUser']))) {

    ?>

    newUser = <? echo $this->session->userdata['newUser'] ?>;


    <?
    } else {
    ?>
    newUser = false;
    <?
    }
    ?>
    //alert(newUser);
</script>