<?
if (isset($this->session->userdata['fc_validated'])) {
    ?>
    <div>
        <?
        /** linkage for prompts should be here as $linkage **/
        if(isset($linkage)){

        }

        ?>
    </div>
    <div class="quiz-container col-md-12">
        <div id="quizMenuBar" class="quiz-menu-bar hidden">
            <ul>
                <li>
                    <label for="selectCardset">Select Cardset for Quiz:
                        <select id="selectCardset" class="" style="min-width:200px;" name="selectCardset"
                                placeholder="Select Cardset" size="1">
                            <option value="0" selected="selected">Select one</option>
                            <?php

                            foreach ($cardsets as $single) {
                                # code...
                                echo "<option id=\"cardset_" . $single->id . "\" class=\"\" value=\"" . $single->id . "\">" . $single->name . "</option>";
                            }

                            ?>
                        </select>
                    </label>
                </li>
                <?
                //<li><a id="reviewQuiz" class="">Review Quiz</a></li>
                //<li><a id="home" class="">Return to Flashcard Home</a></li>
                ?>
                <li><a id="quizRatings" class="">Preferences</a></li>
            </ul>
        </div>
        <div>
            <div style="display:none">
                <input type="checkbox" name="speedEn" id="speedEnabled" class="speed-check-box" checked="checked"/>
                <label for="speedMode"> Speed Mode </label>
                <span id="enabledText"></span>
            </div>
        </div>
        <div id="quizIndexContent" class="quiz-content">
            <div id="cardDeck" class="card-deck-shuffled">

            </div>
            <div id="currentCardDiv" class="current-card-div">
                <div id="cardHolder" class="card-holder"></div>
            </div>
            <div id="answerRatingDiv" class="answer-rating-div">
                <div id="answeredCardsDiv" class="answered-carddeck-div">

                </div>
                <div class="scoring">
                    <div id="cardRatingDiv" class="card-rating-div">
                        <div id="rightWrong" class="right-wrong">
                            <div id="correctKey" class="button-correct btn btn-success">Correct</div>
                            <div id="incorrectKey" class="button-incorrect btn btn-danger">Incorrect</div>
                        </div>
                        <div id="ratings">
                            <div id="easyKey" class="rating-button rating-button-1 btn btn-primary">Easy</div>
                            <div id="challengingKey" class="rating-button rating-button-2 btn btn-success">Challenging</div>
                            <div id="hardKey" class="rating-button rating-button-3 btn btn-warning">Hard</div>
                            <div id="impossibleKey" class="rating-button rating-button-4 btn btn-danger">Impossible
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="stats" class="stats">

                <div id="scoresDiv" class="scores-div">
                    <div id="scoresRow1" class="scores-rows scores-row-1">
                        <div id="displayCorrect" class="">
                            <span>Correct: </span>
                            <span id="text">0</span>
                        </div>
                        <div id="displayIncorrect" class="">
                            <span>Incorrect: </span>
                            <span id="text">0</span>
                        </div>
                        <div id="displayPercent" class="">
                            <span>Percent: </span>
                            <span id="text">0</span>
                        </div>
                    </div>
                    <div id="scoresRow2" class="scores-rows scores-row-2">
                        <div id="displayButton1" class="">
                            <span>Easy: </span>
                            <span id="text">0</span>
                        </div>
                        <div id="displayButton2" class="">
                            <span>Challenging: </span>
                            <span id="text">0</span>
                        </div>
                        <div id="displayButton3" class="">
                            <span>Hard: </span>
                            <span id="text">0</span>
                        </div>
                        <div id="displayButton4" class="">
                            <span>Impossible: </span>
                            <span id="text">0</span>
                        </div>
                    </div>
                    <div id="scoresRow3" class="scores-rows scores-row-3">
                        <div id="totalAnswered" class="">
                            <span>Total Answered: </span>
                            <span id="text">0</span>
                        </div>
                        <div id="displayTimer">
                            <span>Timer: </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="notificationsDiv" class="notification-div hidden">
        <div id="notify">
            <div id="header" class="notify-header">
                <div id="title" class="notify-title">
                    <span id="titletext"></span>
                </div>
                <div id="closewindowbutton" class="closewindowbutton">
                    <div id="closebutton" class="closebutton">
                        <span id="closetext" class="closetext">x</span>
                    </div>
                </div>
            </div>
            <div id="body" class="notify-body">
                <div id="bodytext" class="notify-body-text">
                    <span id="textbody" class="textbody"></span>
                </div>
            </div>
            <div id="footer" class="notify-footer">
                <div id="buttonsDiv">
                    <button id="save" class="notify-save">Save</button>
                    <button id="close" class="notify-save">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div id="retestWin" class="retest-window hidden" >
        <div id="retestCont" class="retest-container">
            <span id="retest" class="retest"></span>
        </div>
    </div>

    <div id="newPromptWin" role="tabpanel"  class="tab-pane new-card-data new-card-prompt-div hidden">
        <form id="addPromptForm" class="addpromptform" >
            <div class="pull-right col-lg-10">
                <div class="pull-left col-lg-2">
                    <label for="prompt_text"><span class="form-label">Prompt</span></label>
                </div>

                <div id="textareadiv" class="textareadiv">
                    <textarea id="addCardPrompt" name="prompt_text" rows="5" cols="40"></textarea>
                </div>
                <div id="checks" class="checks">
                    <div id="infoPrivate"><span >Private: </span>
                        <div id="prvWrap" class="prvWrap">
                            <input type="checkbox" id="private" name="private" checked="" style="visibility:hidden;"/>
                            <label for="private"></label>
                        </div>
                    </div>
                    <div id="infoActive"><span >Active: </span>
                        <div id="chkWrap" class="chkWrap">
                            <input type="checkbox" id="active" name="active" checked="" style="visibility:hidden;"/>
                            <label for="active"></label>
                        </div>
                    </div>
                </div>
                <div id="alarmCountDiv">
                    <div id="alarmon">
                        <table class="alarm-table">
                            <tr>
                                <th>Alarm On: </th>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td><input id="alarm" type="text" name="alarmTime" size="3" />
                                    <label> secs</label>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td><input id="wrong" type="text" name="alarmWrong" size="3" />
                                    <label> wrong</label>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div id="dates" class="dates">
                    <div id="date"><label for="date">Date: </label>
                        <input id="prompt_added_date" name="prompt_added_date" size="18" readonly />
                    </div>

                    <div id="modified_datediv"><label for="modified_date">Last Modified Date: </label>
                        <input id="modified_date" name="modified_date" size="18" readonly />
                        <input id="cardid" name="cardId" type="hidden" value="" />
                    </div>
                </div>
            </div>
        </form>
        <div class="newpromptfooter">
            <button id="promptCloseButton" type="button" onclick="$('#newPromptWin').toggle()" class="btn btn-danger" data-dismiss="modal">Close</button>
            <button id="promptSubmitButton" type="button" class="btn btn-primary">Save prompt</button>
        </div>
    </div>

    <script src="<?php echo FCJS ?>formValidator.js"></script>
    <script src="<?php echo FCJS ?>validateUserQuizPrefsFrm.js"></script>
    <div id="configWin" class="config-window">
        <div id="formContainer" class="form-container">
            <?
            $this->load->helper('form');
            $attributes = array('class' => 'user_prefs', 'id' => 'user_prefs_form');
            $hidden = array('userid' => $this->session->userdata['id']);

            echo form_open('', $attributes, $hidden);

            $closebtn_data = array(
                'id' => 'closePrefs',
                'content' => 'X',
                'type' => 'button'
            );

            echo form_fieldset('Quiz Key Configuration <button id="closePrefs" type="button">X</button>');

            echo form_label("Correct Key: ", 'correctKey');
            $attributes = array(
                'class' => 'key-config-input',
                'id' => 'correctKey',
                'name' => 'correctKey',
                'size' => '4',
                'maxlength' => '6',
                'value' => (isset($this->session->userdata['preferences']['quiz'][0]->correctKey) ? ucfirst(chr($this->session->userdata['preferences']['quiz'][0]->correctKey)) : "1")//,
                //'value' => (((isset($this->session->userdata['preferences']['correctKey']))? echo $this->session->userdata['preferences']['correctKey'] : echo ""))
            );
            echo form_input($attributes);

            echo form_label("Incorrect Key: ", 'incorrectKey');
            $attributes = array(
                'class' => 'key-config-input',
                'id' => 'incorrectKey',
                'name' => 'incorrectKey',
                'size' => '4',
                'maxlength' => '6',
                'value' => (isset($this->session->userdata['preferences']['quiz'][0]->incorrectKey) ? ucfirst(chr($this->session->userdata['preferences']['quiz'][0]->incorrectKey)) : "2")//,
                //'value' => (((isset($this->session->userdata['preferences']['correctKey']))? echo $this->session->userdata['preferences']['correctKey'] : echo ""))
            );
            echo form_input($attributes);

            echo form_label("Easy Key: ", 'easyKey');
            $attributes = array(
                'class' => 'key-config-input',
                'id' => 'easyKey',
                'name' => 'easyKey',
                'size' => '4',
                'maxlength' => '6',
                'value' => (isset($this->session->userdata['preferences']['quiz'][0]->easyKey) ? ucfirst(chr($this->session->userdata['preferences']['quiz'][0]->easyKey)) : "Q")//,
                //'value' => (((isset($this->session->userdata['preferences']['correctKey']))? echo $this->session->userdata['preferences']['correctKey'] : echo ""))
            );
            echo form_input($attributes);

            echo form_label("Challenging Key: ", 'challengingKey');
            $attributes = array(
                'class' => 'key-config-input',
                'id' => 'challengingKey',
                'name' => 'challengingKey',
                'size' => '4',
                'maxlength' => '6',
                'value' => (isset($this->session->userdata['preferences']['quiz'][0]->challengingKey) ? ucfirst(chr($this->session->userdata['preferences']['quiz'][0]->challengingKey)) : "W")//,
                //'value' => (((isset($this->session->userdata['preferences']['correctKey']))? echo $this->session->userdata['preferences']['correctKey'] : echo ""))
            );
            echo form_input($attributes);

            echo form_label("Hard Key: ", 'hardKey');
            $attributes = array(
                'class' => 'key-config-input',
                'id' => 'hardKey',
                'name' => 'hardKey',
                'size' => '4',
                'maxlength' => '6',
                'value' => (isset($this->session->userdata['preferences']['quiz'][0]->hardKey) ? ucfirst(chr($this->session->userdata['preferences']['quiz'][0]->hardKey)) : "E")//,
                //'value' => (((isset($this->session->userdata['preferences']['correctKey']))? echo $this->session->userdata['preferences']['correctKey'] : echo ""))
            );
            echo form_input($attributes);

            echo form_label("Impossible Key: ", 'impossibleKey');
            $attributes = array(
                'class' => 'key-config-input',
                'id' => 'impossibleKey',
                'name' => 'impossibleKey',
                'size' => '4',
                'maxlength' => '6',
                'value' => (isset($this->session->userdata['preferences']['quiz'][0]->impossibleKey) ? ucfirst(chr($this->session->userdata['preferences']['quiz'][0]->impossibleKey)) : "R")//,
                //'value' => (((isset($this->session->userdata['preferences']['correctKey']))? echo $this->session->userdata['preferences']['correctKey'] : echo ""))
            );
            echo form_input($attributes);

            echo form_fieldset_close();


            $btn_data = array(
                'name' => 'prefSubmit',
                'id' => 'prefSubmit',
                'content' => 'Save Preferences',
                'type' => 'button'
            );
            echo form_button($btn_data);

            ?>
        </div>
    </div>

    <script>
        var incQuiz = null;
        var linkage = null;

        <?php
            if(isset($linkage)) {
                if(is_array($linkage)) {
                ?>
                    linkage = <? echo is_array($linkage)?json_encode($linkage):0;
                }
        } else {
            ?>
                        linkage = <? echo 0; ?>;
        <?
        }
        ?>


        var prefs = <? echo json_encode($this->session->userdata['preferences']); ?>

            <?php
                if(isset($incQuiz)) {
                    if(!is_nan($incQuiz)) {
                        ?>
            incQuiz = <? echo $incQuiz; ?>;
        <?
        } else {
            ?>
        incQuiz = <? echo "not num"; ?>;
        <?
        }
    } else {
        ?>
        incQuiz = <? echo "false"; ?>;
        <?
        }
    ?>
        //alert(incQuiz);
        var prefs = new Array();
        prefs = <? echo json_encode($this->session->userdata['preferences']['quiz']); ?>;
    </script>

    <?
} else {
    header('Location: /');
}
?>