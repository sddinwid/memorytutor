<?php
/**
 * Created by PhpStorm.
 * User: Scott
 * Date: 11/12/2014
 * Time: 2:21 PM
 */
?>

<script src="<?php echo FCJS ?>formValidator.js"></script>
<script src="<?php echo FCJS ?>login_register_formv.js"></script>

<div id='login_form' class="login-form">
    <form id="loginForm" action='/fclogin/process' method='post' name='process'  onSubmit="return validateloginForm(this)">
        <? if(! is_null($msg)) //echo $msg; ?>
        <div>
            <label for='username'>Username</label>
            <input type='text' name='username' id='username' size='25' /><br />
        </div>

        <div>
            <label for='password'>Password</label>
            <input type='password' name='password' id='password' size='25' /><br />
        </div>
        <div>
            <input type="hidden" value="<? echo $msg; ?>" name="code" />
        </div>
        <div id='loginRegisterButtons'>
            <div class="login-div">
                <br>
                <button type='Submit' name='submit' class="login-submit-button btn btn-primary" value='Login' >Login</button>
            </div>
            <div class="register-div">
                <span>If no account,</span>
                <button type='Submit' name='submit' class="login-register-button btn btn-primary" value='Register' >Register</button>
            </div>
        </div>
    </form>
</div>
