<?php
/**
 * Created by PhpStorm.
 * User: Scott
 * Date: 11/1/2014
 * Time: 9:26 PM
 */

$brow_str = $_SERVER['HTTP_USER_AGENT'];
$brow = "";
$mobile = 0;
if(strpos($brow_str, "Mobile")) {
    header('Location: /m');
    exit;
    $mobile = 1;
    if(strpos($brow_str, "Android")) {
        $brow = "android";
    }else if(strpos($brow_str, "Firefox")) {
        $brow = "firefox-mobile";
    }else if(strpos($brow_str, "AppleWebKit") &&
        strpos($brow_str, "Safari") &&
        !strpos($brow_str, "Chrome")) {
        $brow = "safari-mobile";
    }else if(strpos($brow_str, "Chrome")) {
        $brow = "chrome-mobile";
    }
}else if(strpos($brow_str, "Edge")) {
    $brow = "edge";
}else if(strpos($brow_str, "Firefox")) {
    $brow = "firefox";
}else if(strpos($brow_str, "Chrome")) {
    $brow = "chrome";
}else if(strpos($brow_str, "Safari")) {
    $brow = "safari";
}else if(strpos($brow_str, "Trident/7.0") && strpos($brow_str, "rv:11.0")){
    $brow = "IE11";
}
?>

<!DOCTYPE html>
      <html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
    <title>Memory Tutor | <?php echo $title ?></title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <link href="<?php echo FCASSETS ?>bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="<?php echo FCJS ?>jquery-ui-1.11.1/jquery-ui.min.css" type="text/css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.4/css/jquery.dataTables.min.css" type="text/css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.4/css/jquery.dataTables_themeroller.css" type="text/css" />
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet" />
	<link href='http://fonts.googleapis.com/css?family=Permanent+Marker' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="/assets/flashcard/css/flashCard.css" type="text/css" />
    <link rel="stylesheet" href="/assets/flashcard/css/<?php echo $page; ?>.css" type="text/css" />

    <script src="<?php echo FCJS?>jquery-1.11.1.min.js" type="text/javascript"></script>

    <script type="text/javascript">


        $(document).ready(function () {
            $("#hintArea").hide();
            $("#changingTable").hide();

            <?
            if(!isset($this->session->userdata['fc_validated'])) {
                ?>
            $('#homeButton').unbind("click");
            $("#homeButtonDiv").unbind("click");
            <?
        }
        ?>
            

        });

        var logos = <? echo $logo_images ?> ;
    </script>
</head>
<body class="page body container-fluid<? echo $brow?" ".$brow:""; ?>" style="text-align: center;background-color: rgb(220, 220, 255);">
<div id='header'>
	<div id="menuBar" class="menu-bar container-fluid col-md-12">
		<div id="navBar" class="nav-bar">
            <div id="homeButtonDiv" class="category-button-div">
                <div id="homeButton" class="category-button mtbutton">Home</div>
            </div>

                <div id="viewMyCardsetsButtonDiv" class="view-my-cardsets-btn-div">
                        <?
                        if(isset($this->session->userdata['fc_validated'])) {
                            ?>
                        <div id="viewMyCardsetsButton" class="view-my-cardsets-btn mtbutton">
                            <a class="a-viewcset" href="/flashcard/viewcardsets/">Edit Cardsets</a>
                        </div>
                            <?
                        }
                        ?>
                </div>
                <div id="quizzesButtonDiv" class="quizzes-btn-div">
                        <?
                        if(isset($this->session->userdata['fc_validated'])) {
                            ?>
                        <div id="quizzesButton" class="quizzes-btn mtbutton">
                            <a class="a-quiz" href="/flashcard/quizzes/">Take Quizzes</a>
                        </div>
                            <?
                        }
                        ?>
                </div>
        </div>
        <div class="nav-bottom"></div>
        <?
        if(isset($this->session->userdata['fc_validated'])) {
        ?>
        <div id="settingsButtonDiv" class="cogbutton">
            <div id="settingsButton"><span class="glyphicon glyphicon-cog"></div>
        </div>
        <?
        } else {
            ?>
        <div id="loginButtonDiv" class="login-button-div">
            <button id="loginLink">Login/Register</button>
        </div>
        <?
        }
        ?>
	</div>
	<div id="headerBar" class="header-bar col-md-12">
		<span id="logo" class="logo col-md-2"><img class="logo-image" alt="" src=""></span>
		<span id="siteTitle" class="site-title col-md-6">Memory Tutor</span>
	</div>

</div>
<div id="settingsMenuDiv" style="display: none;">
    <ul id="settingsMenu" class="category">

        <li>Options</li>
        <?

            if(isset($this->session->userdata['secVal'])) {
                if($this->session->userdata['secVal']>'500') {
                ?>
                <li id="adminLink" onclick="window.location='/fcadmin'">Admin</li>
                <?
                }
                ?>
                <li id="logoutLink">Logout</li>
                <?
            } else {
                ?>

                <?
            }

        ?>
    </ul>
</div>
<div id="categoryMenuDiv" style="display: none;">
    <ul id="categoryMenu" class="category">
        <li id='review'>Load CardSet

                <?php
              echo $cardsetmenu;



                ?>
        </li>

        <?
        if(isset($this->session->userdata['fc_validated'])) {
            ?>
        <li id="quiz">Load CardSet for Quiz

                <?php
                 echo $cardsetmenu;
                ?>
        </li>
        <li id="addCategory" class="activeHover"  data-toggle="modal" data-target="#newCategoryWindow">Add a Category</li>

        <li id="categoryNewCardSet" class="activeHover"  data-toggle="modal" data-target="#newCardSetWindow">Make New CardSet</li>
        <li id="categoryAddCard" class="activeHover"  data-toggle="modal" data-target="#newCardWindow">Add Card</li>
        <?
            }
        ?>
    </ul>
</div>
