<?php
/**
 * Created by PhpStorm.
 * User: Scott
 * Date: 11/1/2014
 * Time: 9:26 PM
 */
?>
<div id="footerDiv" class="footer-div col-md-12">
<strong>Memory Tutor &copy; 2014 - <script>document.write(new Date().getFullYear());</script></strong>
</div>

<div id="loginFormDiv" class="login-form-container fade">
    
</div>
<!-- -------------------------
                post page script
        ---------------------   -->


<script src="<?php echo FCASSETS ?>bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo FCJS ?>jquery-ui-1.11.1/jquery-ui.min.js" type="text/javascript"></script>
<script src="https://tinymce.cachefly.net/4.1/tinymce.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/1.10.4/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo FCJS ?>flashCard.js" type="text/javascript"></script>
<script src="<?php echo FCJS; echo $page; ?>.js" type="text/javascript"></script>


<script src="<?php echo FCJS ?>formValidator.js"></script>
<?
if(trim($page) == "viewcardsets") { ?>
    <script src="<?php echo FCJS ?>card_formv.js"></script>
    <script src="<?php echo FCJS ?>cardset_formv.js"></script>
<? } else if(trim($page) == "index") { ?>
    <script src="<?php echo FCJS ?>card_formv.js"></script>
    <script src="<?php echo FCJS ?>cardset_formv.js"></script>
    <script src="<?php echo FCJS ?>category_formv.js"></script>
<?    } else {

//<script>console.log('=$page ');</script>

    //validateCategoryForm
} ?>

<!-- -------------------------
     end post page script
---------------------   -->
</body>
</html>