<html>
<head>
    <title>Flash Card Project | <?php echo $title ?></title>

    <link href="<?php echo FCASSETS ?>bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo FCJS ?>jquery-ui-1.11.1/jquery-ui.min.css" type="text/css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.4/css/jquery.dataTables.min.css" type="text/css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.4/css/jquery.dataTables_themeroller.css" type="text/css" />
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

    <link rel="stylesheet" href="<?php echo FCCSS;?>admin/main.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo FCCSS;?>admin/<? echo $page; ?>.css" type="text/css" />


    <script src="<?php echo FCJS?>jquery-1.11.1.min.js"></script>
    
</head>
<body style="text-align: center">
<div>
    <div id="container">