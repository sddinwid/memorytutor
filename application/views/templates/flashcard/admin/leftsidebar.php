<div id="logos" style="height:30px;">
    
</div>
<div class="left-sidebar">
	<ul>
		<li id="users"><span class="fa fa-user fa-2x" data-toggle="tooltip" title="Manage Users"></span>
			
		</li>
		<li id="content"><span class="fa fa-share-alt fa-2x" data-toggle="tooltip" title="Manage Content"></span>
			
		</li>
		<li id="quizzes"><span class="fa fa-question-circle fa-2x" data-toggle="tooltip" title="Manage Quizzes"></span>
			
		</li>
		<li id="quotes"><span class="fa fa-quote-left fa-2x" data-toggle="tooltip" title="Manage Quotes"></span>
			<span class="fa fa-quote-right fa-2x" data-toggle="tooltip" title="Manage Quotes"></span>
		</li>
		<li id="prompts"><span class="fa fa-exclamation-circle fa-2x" data-toggle="tooltip" title="Manage Prompts"></span>
		</li>
		<li id="promptlinkage"><span class="fa fa-exclamation-circle fa-2x" data-toggle="tooltip" title="Manage Prompt Linkage"></span>
			<span class="fa fa-link fa-2x" data-toggle="tooltip" title="Manage Prompt Linkage"></span>
		</li>
		<li id="comments"><span class="fa fa-comments fa-2x" data-toggle="tooltip" title="Manage Comments"></span>
			
		</li>
		<li id="site"><span class="fa fa-sitemap fa-2x" data-toggle="tooltip" title="Manage Site"></span>
			
		</li>
		<?
		/*
		<li id="blog"><span class="fa fa-rss fa-2x" data-toggle="tooltip" title="Manage Blog"></span>
			
		</li>
		<li id="messages"><span class="fa fa-envelope-o fa-2x" data-toggle="tooltip" title="Manage Messages"></span>
			
		</li>
		<li id="payments"><span class="fa fa-money fa-2x" data-toggle="tooltip" title="Manage Payments"></span>
			
		</li>
		*/
		?>
		<li id="support"><span class="fa fa-ticket fa-2x" data-toggle="tooltip" title="Manage Support"></span>
			
		</li>
	</ul>
</div>
<?
/*
<div id="userAdminOptions" option="users" class="user-admin-options" style="display:none;">
	<ul>
		<li><span>All Users</span></li>
		<li><span>Users 1-2</span></li>
		<li><span>Users 1-3</span></li>
	</ul>
</div>
*/
?>
<div id="userAdminOptions" option="content" class="user-admin-options" style="display:none;">
	<ul>
		<li><span>Categories</span></li>
		<li><span>Card Sets</span></li>
		<li><span>Cards</span></li>
	</ul>
</div>
<div id="userAdminOptions" option="site" class="user-admin-options" style="display:none;">
	<ul>
		<li><span>Site Roles</span></li>
		<li><span>Site Codes</span></li>
	</ul>
</div>
<?
/*
<div id="userAdminOptions" option="news" class="user-admin-options" style="display:none;">
	<ul>
		<li><span>Admin News Content</span></li>
		<li><span>Add News Content</span></li>
		<li><span>News 1-3</span></li>
	</ul>
</div>
<div id="userAdminOptions" option="blog" class="user-admin-options" style="display:none;">
	<ul>
		<li><span>Blog Posts</span></li>
		<li><span>Add Blog Post</span></li>
		<li><span>Blog 1-3</span></li>
	</ul>
</div>
<div id="userAdminOptions" option="messages" class="user-admin-options" style="display:none;">
	<ul>
		<li><span>Messages 1-1</span></li>
		<li><span>Messages 1-2</span></li>
		<li><span>Messages 1-3</span></li>
	</ul>
</div>
<div id="userAdminOptions" option="payments" class="user-admin-options" style="display:none;">
	<ul>
		<li><span>Payments 1-1</span></li>
		<li><span>Payments 1-2</span></li>
		<li><span>Payments 1-3</span></li>
	</ul>
</div>
*/
?>
<div id="userAdminOptions" option="support" class="user-admin-options" style="display:none;">
	<ul>
		<li><span>Support 1-1</span></li>
		<li><span>Support 1-2</span></li>
		<li><span>Support 1-3</span></li>
	</ul>
</div>