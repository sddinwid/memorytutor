<?php
/**
 * Created by PhpStorm.
 * User: Scott
 * Date: 11/1/2014
 * Time: 9:26 PM
 */

$brow_str = $_SERVER['HTTP_USER_AGENT'];
$brow = "";
$mobile = 0;
if(strpos($brow_str, "Mobile")) {
    $mobile = 1;
    if(strpos($brow_str, "Android")) {
        $brow = "android";
    }else if(strpos($brow_str, "Firefox")) {
        $brow = "firefox-mobile";
    }else if(strpos($brow_str, "AppleWebKit") &&
        strpos($brow_str, "Safari") &&
        !strpos($brow_str, "Chrome")) {
        $brow = "safari-mobile";
    }else if(strpos($brow_str, "Chrome")) {
        $brow = "chrome-mobile";
    }
}else if(strpos($brow_str, "Edge")) {
    $brow = "edge";
}else if(strpos($brow_str, "Firefox")) {
    $brow = "firefox";
}else if(strpos($brow_str, "Chrome")) {
    $brow = "chrome";
}else if(strpos($brow_str, "Safari")) {
    $brow = "safari";
}else if(strpos($brow_str, "Trident/7.0") && strpos($brow_str, "rv:11.0")){
    $brow = "IE11";
}
?>

<!DOCTYPE html >
<head>
    <title>Memory Tutor | <?php echo $title ?></title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <link href="<?php echo FCASSETS ?>bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="<?php echo FCJS ?>jquery-ui-1.11.1/jquery-ui.min.css" type="text/css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.4/css/jquery.dataTables.min.css" type="text/css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.4/css/jquery.dataTables_themeroller.css" type="text/css" />
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet" />
	<link href='http://fonts.googleapis.com/css?family=Permanent+Marker' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="/assets/flashcard/css/mobile.css" type="text/css" />
    <?
    //<link rel="stylesheet" href="/assets/flashcard/css/mobile/<?php echo $page; .css" type="text/css" />
?>


    <script src="<?php echo FCJS?>jquery-1.11.1.min.js" type="text/javascript"></script>

    <script type="text/javascript">


        $(document).ready(function () {


            <?
            if(!isset($this->session->userdata['fc_validated'])) {
                ?>

            <?
        }
        ?>
            

        });

        var logos = <? echo $logo_images ?> ;
    </script>
</head>
<body class="page body <? echo $brow?" ".$brow:""; ?>" style="text-align: center;background-color: rgb(220, 220, 255);">
