

    <div id="footerHolder" class="footerHolder" >
        <div id="footer" class="footer ui-corner-bottom">
            <?
            /*
            <div id="footerMenu" class="footer-menu" >
                <ul id="footerMenuList" class="footer-menu-list" >
                    <li>Home</li>
                    <li>Category</li>
                    <li>Advertise</li>
                    <li>Terms</li>
                    <li>Privacy</li>
                    <li>FAQ</li>
                    <li>About</li>
                    <li>Help</li>
                </ul>
            </div>
            */
            ?>
            <div id="footerSepBar" class="footer-sep-bar"></div>
            <div id="copyright" class="copyright" >
                <p>
                    <strong>&copy; 2014</strong>
                </p>
                <br />
                
            </div>
        </div>
    </div>
    <div id="popupModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title">Modal title</h4>
                </div>
                <div class="modal-body">
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</div>


<!-- -------------------------
                post page script
        ---------------------   -->



<script src="<?php echo FCJS ?>jquery.cookie.js"></script>
<script src="<?php echo FCASSETS ?>bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo FCJS ?>jquery-ui-1.11.1/jquery-ui.min.js"></script>
<script src="https://tinymce.cachefly.net/4.1/tinymce.min.js"></script>
<script src="https://cdn.datatables.net/1.10.4/js/jquery.dataTables.js"></script>

<script src="<?php echo FCJS ?>admin/main.js"></script>
<script src="<?php echo FCJS;?>admin/<? echo $page; ?>.js"></script>


<!-- -------------------------
     end post page script
---------------------   -->
</body>
</html>