<?php //if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Scott
 * Date: 11/12/2014
 * Time: 2:18 PM
 */

class M extends CI_Controller {
    function __construct() {
        parent::__construct();
    }

    public function index($page = NULL){
//        $this->load->model('flashcard/cardmodel');
        $this->load->model('flashcard/cardsetmodel');
        $this->load->model('flashcard/categorymodel');
        if ( ! file_exists(APPPATH.'/views/pages/flashcard/mobile.php'))
        {
            // Whoops, we don't have a page for that!
            show_404();
        }
        $data['page'] = "mobile";
        if(($this->session->userdata("username"))) {
            $this->load->model('flashcard/cardsetmodel');
            $mycardsets = $this->cardsetmodel->getMyCardSetsById($this->session->userdata("id"));
            if(is_array($mycardsets))
                $data['mycardsets'] = $mycardsets;
        }

//        $this->session->set_userdata($this->session->flashdata('item'));
//        var_dump($this->session->all_userdata());
        $data['logo_images'] = $this->getAllImages(FCIMG."logos/");
        $data['title'] = "Mobile"; // Capitalize the first letter
        $data['cardsets'] = $this->cardsetmodel->getAllCardSets();
        $data['category'] = $this->categorymodel->getAllCategories();
//        $data['logo_images'] = $this->getAllImages(FCIMG."logos/");
//        $data['fc_assets'] = FCASSETS;
//        $data['cards'] = $this->cardmodel->getAllCards();
//        $data['category'] = $this->categorymodel->getAllCategories();
//        $data['rootcategory'] = $this->getRootCategories();
//        $data['cardsetmenu'] = $this->buildCategoryMenu();

//        $this->load->view('templates/mobile/header', $data);
        $this->load->view('pages/flashcard/mobile', $data);
//        $this->load->view('templates/mobile/footer', $data);
    }

    public function quizzes($quiz = 'none')
    {
        $page = 'quizzes';
        $this->load->model('flashcard/quizmodel');
        $this->load->model('flashcard/cardsetmodel');
        $this->load->model('flashcard/categorymodel');
        $this->load->model('flashcard/promptmodel');
        if ( ! file_exists(APPPATH.'/views/pages/flashcard/'.$page.'.php'))
        {
            // Whoops, we don't have a page for that!
            show_404();
        }
        $data['page'] = $page;
        $data['title'] = ucfirst($page); // Capitalize the first letter
        $data['logo_images'] = $this->getAllImages(FCIMG."logos/");
        $data['fc_assets'] = FCASSETS;
        $data['quizzes'] = $this->quizmodel->getAllQuizzes();
        $data['cardsets'] = $this->cardsetmodel->getActiveCardSets();
        $data['category'] = $this->categorymodel->getAllCategories();
        $data['cardsetmenu'] = $this->buildCategoryMenu();




        if($quiz != 'none') {
            $data['incQuiz'] = $quiz;
            $allCards = $this->getCardsByCardsetID($quiz);
//            echo "<pre>";var_dump($allCards);echo "</pre>";die();
            $ids = array();
            foreach($allCards as $card) {

                foreach ($card as $k=>$v) {
                    if ($k == "id") {
                        if(!array_key_exists($v, $ids)){
                            array_push($ids, $v);
                        }
                    }
                }

            }


            $data['linkage'] = $this->getLinkage(array("idarr"=>$ids));
        }
//        echo "<pre>";var_dump($data['linkage']);echo "</pre>";die();
        $this->load->view('templates/flashcard/header', $data);
        $this->load->view('pages/flashcard/mobile'.$page, $data);
        $this->load->view('templates/flashcard/footer', $data);
    }


    /**
     * Public functions
     */



    public function getAllImages() {
        $imageArr = array();
        foreach(glob('./assets/flashcard/images/logos/*.*') as $filename){
            //echo basename($filename)."\n";

            $imageArr[] = basename($filename);
        }
        return json_encode($imageArr);
        //return $imageArr;
    }

    public function getCardsetById() {
        $cardset_id = $this->input->post('id');
        // Take 'categorization' and split on dashes '-'

        $this->load->model('flashcard/Cardsetmodel');
        $cardset = $this->Cardsetmodel->getCardSetByID($cardset_id);

        echo "{\"success\":\"1\",\"data\":".json_encode($cardset)."}";
    }

}