<?php

class Quizzes extends CI_Controller {

	function __construct() {
        
        parent::__construct();

        $this->load->helper('url');
        
        $this->load->model('flashcard/quizmodel');
        $quiz = array();
    }

    function index ($page = 'index') {


        $this->load->model('flashcard/cardsetmodel');
        $this->load->model('flashcard/categorymodel');
        if ( ! file_exists(APPPATH.'/views/pages/flashcard/'.$page.'.php'))
        {
            // Whoops, we don't have a page for that!
            show_404();
        }
        $data['page'] = $page;
        $data['title'] = ucfirst($page); // Capitalize the first letter
        $data['fc_assets'] = FCASSETS;
        // $data['cards'] = $this->cardmodel->getAllCards();
        $data['cardsets'] = $this->cardsetmodel->getAllCardSets();
        // $data['category'] = $this->categorymodel->getAllCategories();

        $this->load->view('templates/flashcard/header', $data);
        $this->load->view('pages/flashcard/'.$page, $data);
        $this->load->view('templates/flashcard/footer', $data);

    }

    public function getCardsByCardsetAJAX() {

        $cardset_id = $this->input->post('selectCardset');
        // Take 'categorization' and split on dashes '-'
        
// var_dump($categorization);
        $this->load->model('flashcard/Cardmodel');
        $cardsInCardset = $this->Cardmodel->getCardsByCardsetIDAJAX($cardset_id);
        
        echo json_encode($cardsInCardset);

    }

    public function storeQuiz() {

        $response = $this->security->xss_clean($_POST['data']);
        $cardData = $this->security->xss_clean($_POST['card']);


        $quiz = array();

        $card = array(
            'info' => $response,
            'data' => $cardData
        );
        if($this->session->userdata('quiz')) {
            $quiz = $this->session->userdata('quiz');
        }
        $qtemp = $quiz;

        $quiz[] = $card;
        ($quiz);
        // $cardString = substr($cardString, 0, -1);
         try {
             $this->session->set_userdata('quiz', $quiz);
         } catch (Exception $ex) {
             $err = $ex;
         }
        if(isset($err))
            var_dump($err);

        echo "{\"success\":\"1\",\"data\": ".json_encode($quiz).",\"session\": " .json_encode($this->session->userdata('quiz')).",\"qtemp\": " .json_encode($qtemp). ")";

    }

    public function saveQuiz() {

        $quiz = $this->security->xss_clean($_POST['quiz']);
//        $incCard = $this->security->xss_clean($_POST['card']);

        $quiz = json_decode($quiz);
        $cards =$quiz->cards;
        $quizdata = get_object_vars($quiz->quizdata);
        $quizdata["userId"] = $this->session->userdata('id');



        $this->load->model('quizmodel');
        $quizid = $this->quizmodel->insertQuiz($quizdata);

        $drawOrder = array();

        for ($i=0; $i < sizeof($cards); $i++) {
            # code...

        $drawOrder[$i] = array(
                'quiz_id' => $quizid,
                'card_id' => $cards[$i]->id,
                'draw_order' => $i,
                'right_wrong' => $cards[$i]->response->rightWrong,
                'rating' => $cards[$i]->response->rating,
                'time_on_card' => $cards[$i]->response->timeToAnswer,
                'hint_viewed' => $cards[$i]->response->hintUsed,
                'answer' => $cards[$i]->response->answer
            );

        }

        $result = $this->quizmodel->insertQuizResponse($drawOrder);
// var_dump($drawOrder);
        /*
            array(
                'quiz_id' => '',
                'card_id' => '',
                'draw_order' => '',
                'time_on_card' => '',
                'answer' => '',
                'right_wrong' => '',
                'rating' => '',
                'hint_viewed' => ''
            )

            
            {info, data},{info, data},{info, data}

        */


        echo "{\"success\":\"1\" , \"data\":\"".json_encode($result)."\"}";
    }
    function clearQuiz() {
        if($this->session->userdata('quiz')) {
            $this->session->set_userdata('quiz', null);
        }
        if(!($this->session->userdata('quiz'))) {
            echo "{\"success\":\"1\" , \"data\":\"cleared\"}";
        }
    }

}