<?php //if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Scott
 * Date: 11/12/2014
 * Time: 2:18 PM
 */

class Fclogin extends CI_Controller {
    function __construct() {
        parent::__construct();
    }

    public function index($msg = NULL){
        // Load our view to be displayed
        // to the user
        $data['msg'] = $msg;
        $this->load->view('pages/flashcard/login_view', $data);
    }


    /**
     * Public functions
     */

    public function process(){
        // Load the model
        $this->load->model('flashcard/fcloginmodel');
        // Validate the user can login
        $result = $this->fcloginmodel->validate();


        // Now we verify the result
        if(! $result){
            // If user did not validate, then show them login page again
            // $msg = '<font color=red>Invalid username and/or password.</font><br />';
            // $this->index($msg);
            // redirect('/flashcard');
            echo "<font color=red>Not authorized for login.</font><br />";
        }elseif($result==true){
            // If user did validate,
            // Send them to members area
            // redirect('home');
            // echo "success";
            $mobile = $this->security->xss_clean($this->input->post('mobile'));

            if(!empty($mobile)) {
//                var_dump($this->session->all_userdata());


                header("Location:http://memorytutor.scottdinwiddie.com/m", true, 301);
                exit();
            }else {

                header("Location:http://memorytutor.scottdinwiddie.com/", true, 301);
                exit();
            }
        }else{
            echo "bii";
            echo $result;
        }
    }

    public function logout() {
        $this->session->sess_destroy();
        redirect(URL);
    }
}

?>