<?php //if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Scott
 * Date: 11/12/2014
 * Time: 2:18 PM
 */

class Codechecker extends CI_Controller {
    function __construct() {
        parent::__construct();
    }

    public function index($code=null){
        // Load our view to be displayed
        // to the user
        
        if($code!=null) {
            echo $this->checkCode($code);
        } else {
            echo "nope";
        }
        
    }


    /**
     * Public functions
     */

    public function checkCode($code=null){
        // Load the model
        $this->load->model('sitecode_model');
        // Validate the user can login
        $result = $this->sitecode_model->validate($code);
        // var_dump($result);
        // Now we verify the result
        return $result;//($result)? "true" : "false";
    }

    // public function logout() {
    //     $this->session->sess_destroy();
    //     redirect($_SERVER['HTTP_REFERER']);
    // }
}