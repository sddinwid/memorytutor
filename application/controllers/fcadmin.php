<?php



class FCAdmin extends CI_Controller {

	function __construct() {
        parent::__construct();
    }

	public function index($page = 'central') {
		/*
			Load needed models
		*/
	 	// $this->load->model("Dalliouser_model");

	 	/*
			404 if requested page isn't in directory
	 	*/
	 	if ( ! file_exists(APPPATH.'/views/pages/flashcard/admin/'.$page.'.php') || ((int)($this->session->userdata('secVal'))<500))
        {
            // Whoops, we don't have a page for that!
            show_404();
        }
        
        /*
			Load data to pass to view
        */
		// $name = "";
		// if ($this->session->userdata('firstName')!='') {
		// 	$name = $this->session->userdata('firstName');
		// } else if (($this->session->userdata('username')!='')) {
		// 	$name = $this->session->userdata('username');
		// }
        $data['title'] = " Admin Central";
	 	$data["page"] = $page;
	 	// $data["myTopics"] = $this->getMyTopics();
	 	// $data["myTopicCount"] = $this->getMyTopicCount();
	 	// $data["topicNav"] = $this->getTopicNavigator();

	 	/*
			Load views
	 	*/
	 	$this->load->view('templates/flashcard/admin/header', $data);
	 	$this->load->view('templates/flashcard/admin/leftsidebar', $data);
	 	$this->load->view('templates/flashcard/admin/admin_header', $data);
        $this->load->view('pages/flashcard/admin/'.$page, $data);
        $this->load->view('templates/flashcard/admin/footer', $data);
	}

	public function users($option = 'all') {
		/*
			Load needed models
		*/
	 	// $this->load->model("Dalliouser_model");

	 	/*
			404 if requested page isn't in directory
	 	*/
	 	// if ( ! file_exists(APPPATH.'/views/pages/admin/user.php') || ((int)($this->session->userdata('value'))<500))
   //      {
   //          // Whoops, we don't have a page for that!
   //          show_404();
   //      }
        
        /*
			Load data to pass to view
        */
		// $name = "";
		// if ($this->session->userdata('firstName')!='') {
		// 	$name = $this->session->userdata('firstName');
		// } else if (($this->session->userdata('username')!='')) {
		// 	$name = $this->session->userdata('username');
		// }
        $data['title'] = " Admin Central | Manage User | ".ucfirst($option);
	 	$data["page"] = "user";
	 	$data["roles"] = $this->getAllRoles();
	 	// $data["myTopicCount"] = $this->getMyTopicCount();
	 	// $data["topicNav"] = $this->getTopicNavigator();

	 	/*
			Load views
	 	*/

		$this->load->view('templates/flashcard/admin/header', $data);
	 	$this->load->view('templates/flashcard/admin/leftsidebar', $data);
	 	$this->load->view('templates/flashcard/admin/admin_header', $data);
        $this->load->view('pages/flashcard/admin/user', $data);
        $this->load->view('templates/flashcard/admin/footer', $data);

	}

	public function codes($option = 'all') {
		/*
			Load needed models
		*/
	 	// $this->load->model("Dalliouser_model");

	 	/*
			404 if requested page isn't in directory
	 	*/
	 	// if ( ! file_exists(APPPATH.'/views/pages/admin/user.php') || ((int)($this->session->userdata('value'))<500))
   //      {
   //          // Whoops, we don't have a page for that!
   //          show_404();
   //      }
        
        /*
			Load data to pass to view
        */
		// $name = "";
		// if ($this->session->userdata('firstName')!='') {
		// 	$name = $this->session->userdata('firstName');
		// } else if (($this->session->userdata('username')!='')) {
		// 	$name = $this->session->userdata('username');
		// }
        $data['title'] = " Admin Central | Manage Codes | ".ucfirst($option);
	 	$data["page"] = "codes";
	 	// $data["myTopics"] = $this->getMyTopics();
	 	// $data["myTopicCount"] = $this->getMyTopicCount();
	 	// $data["topicNav"] = $this->getTopicNavigator();

	 	/*
			Load views
	 	*/
	 	$this->load->view('templates/flashcard/admin/header', $data);
	 	$this->load->view('templates/flashcard/admin/leftsidebar', $data);
	 	$this->load->view('templates/flashcard/admin/admin_header', $data);
        $this->load->view('pages/flashcard/admin/codes', $data);
        $this->load->view('templates/flashcard/admin/footer', $data);
	}

	public function categories($option = 'all') {
		/*
			Load needed models
		*/
	 	// $this->load->model("Dalliouser_model");

	 	/*
			404 if requested page isn't in directory
	 	*/
	 	// if ( ! file_exists(APPPATH.'/views/pages/admin/user.php') || ((int)($this->session->userdata('value'))<500))
   //      {
   //          // Whoops, we don't have a page for that!
   //          show_404();
   //      }
        
        /*
			Load data to pass to view
        */
		// $name = "";
		// if ($this->session->userdata('firstName')!='') {
		// 	$name = $this->session->userdata('firstName');
		// } else if (($this->session->userdata('username')!='')) {
		// 	$name = $this->session->userdata('username');
		// }
        $data['title'] = " Admin Central | Manage Categories | ".ucfirst($option);
	 	$data["page"] = "categories";
	 	// $data["myTopics"] = $this->getMyTopics();
	 	// $data["myTopicCount"] = $this->getMyTopicCount();
	 	// $data["topicNav"] = $this->getTopicNavigator();

	 	/*
			Load views
	 	*/
	 	$this->load->view('templates/flashcard/admin/header', $data);
	 	$this->load->view('templates/flashcard/admin/leftsidebar', $data);
	 	$this->load->view('templates/flashcard/admin/admin_header', $data);
        $this->load->view('pages/flashcard/admin/categories', $data);
        $this->load->view('templates/flashcard/admin/footer', $data);
	}

	public function cards($option = 'all') {
		/*
			Load needed models
		*/
	 	// $this->load->model("Dalliouser_model");

	 	/*
			404 if requested page isn't in directory
	 	*/
	 	// if ( ! file_exists(APPPATH.'/views/pages/admin/user.php') || ((int)($this->session->userdata('value'))<500))
   //      {
   //          // Whoops, we don't have a page for that!
   //          show_404();
   //      }
        
        /*
			Load data to pass to view
        */
		// $name = "";
		// if ($this->session->userdata('firstName')!='') {
		// 	$name = $this->session->userdata('firstName');
		// } else if (($this->session->userdata('username')!='')) {
		// 	$name = $this->session->userdata('username');
		// }
        $data['title'] = " Admin Central | Manage Cards | ".ucfirst($option);
	 	$data["page"] = "cards";
	 	// $data["myTopics"] = $this->getMyTopics();
	 	// $data["myTopicCount"] = $this->getMyTopicCount();
	 	// $data["topicNav"] = $this->getTopicNavigator();

	 	/*
			Load views
	 	*/
	 	$this->load->view('templates/flashcard/admin/header', $data);
	 	$this->load->view('templates/flashcard/admin/leftsidebar', $data);
	 	$this->load->view('templates/flashcard/admin/admin_header', $data);
        $this->load->view('pages/flashcard/admin/cards', $data);
        $this->load->view('templates/flashcard/admin/footer', $data);
	}

	public function cardsets($option = 'all') {
		/*
			Load needed models
		*/
	 	 $this->load->model("/flashcard/categorymodel");

	 	/*
			404 if requested page isn't in directory
	 	*/
	 	// if ( ! file_exists(APPPATH.'/views/pages/admin/user.php') || ((int)($this->session->userdata('value'))<500))
   //      {
   //          // Whoops, we don't have a page for that!
   //          show_404();
   //      }
        
        /*
			Load data to pass to view
        */
		// $name = "";
		// if ($this->session->userdata('firstName')!='') {
		// 	$name = $this->session->userdata('firstName');
		// } else if (($this->session->userdata('username')!='')) {
		// 	$name = $this->session->userdata('username');
		// }
        $data['title'] = " Admin Central | Manage Cardsets | ".ucfirst($option);
	 	$data["page"] = "cardsets";
	 	$data["allCardsets"] = $this->getAllCardsets();
		$data['category'] = $this->categorymodel->getAllCategories();
	 	// $data["topicNav"] = $this->getTopicNavigator();

	 	/*
			Load views
	 	*/
	 	$this->load->view('templates/flashcard/admin/header', $data);
	 	$this->load->view('templates/flashcard/admin/leftsidebar', $data);
	 	$this->load->view('templates/flashcard/admin/admin_header', $data);
        $this->load->view('pages/flashcard/admin/cardsets', $data);
        $this->load->view('templates/flashcard/admin/footer', $data);
	}

	public function quizzes($option = 'all') {


		if($option=='all'){
			/*
				Load needed models
			*/
		 	// $this->load->model("Dalliouser_model");

		 	/*
				404 if requested page isn't in directory
		 	*/
		 	// if ( ! file_exists(APPPATH.'/views/pages/admin/user.php') || ((int)($this->session->userdata('value'))<500))
	   //      {
	   //          // Whoops, we don't have a page for that!
	   //          show_404();
	   //      }
	        
	        /*
				Load data to pass to view
	        */
			// $name = "";
			// if ($this->session->userdata('firstName')!='') {
			// 	$name = $this->session->userdata('firstName');
			// } else if (($this->session->userdata('username')!='')) {
			// 	$name = $this->session->userdata('username');
			// }
	        $data['title'] = " Admin Central | Manage Quizzes | ".ucfirst($option);
		 	$data["page"] = "quizzes";
		 	// $data["myTopics"] = $this->getMyTopics();
		 	// $data["myTopicCount"] = $this->getMyTopicCount();
		 	// $data["topicNav"] = $this->getTopicNavigator();

		 	/*
				Load views
		 	*/
		 	$this->load->view('templates/flashcard/admin/header', $data);
		 	$this->load->view('templates/flashcard/admin/leftsidebar', $data);
		 	$this->load->view('templates/flashcard/admin/admin_header', $data);
	        $this->load->view('pages/flashcard/admin/quizzes', $data);
	        $this->load->view('templates/flashcard/admin/footer', $data);
        } elseif (is_numeric($option)) {
        	# code...

        	$data['title'] = " Admin Central | View Quiz | Quiz #".ucfirst($option);
		 	$data["page"] = "viewquiz";

		 	$data["quiz"] = $this->getQuizByID($option);

		 	$cardsArray = array();
	        foreach ($data['quiz']['drawOrder'] as $card) {
	        	# code...
	        	array_push($cardsArray, $card->card_id);
	        }

	        // $requestedQuiz['cards'] = $this->getCardsByID($cardsArray);

		 	$data["cards"] = $this->getCardsByID($cardsArray);
		 	// $data["quizCardset"] = $this->getTopicNavigator();

		 	/*
				Load views
		 	*/
		 	$this->load->view('templates/flashcard/admin/header', $data);
		 	$this->load->view('templates/flashcard/admin/leftsidebar', $data);
		 	$this->load->view('templates/flashcard/admin/admin_header', $data);
	        $this->load->view('pages/flashcard/admin/viewquiz', $data);
	        $this->load->view('templates/flashcard/admin/footer', $data);
        }
	}

	public function quotes($option = 'all') {
		/*
			Load needed models
		*/
		$this->load->model("/flashcard/quotemodel");

		/*
            404 if requested page isn't in directory
         */
		// if ( ! file_exists(APPPATH.'/views/pages/admin/user.php') || ((int)($this->session->userdata('value'))<500))
		//      {
		//          // Whoops, we don't have a page for that!
		//          show_404();
		//      }

		/*
            Load data to pass to view
        */
		// $name = "";
		// if ($this->session->userdata('firstName')!='') {
		// 	$name = $this->session->userdata('firstName');
		// } else if (($this->session->userdata('username')!='')) {
		// 	$name = $this->session->userdata('username');
		// }
		$data['title'] = " Admin Central | Manage Quotes | ".ucfirst($option);
		$data["page"] = "quotes";
		$data["allQuotes"] = $this->getAllQuotes();
		//$data['category'] = $this->categorymodel->getAllCategories();
		// $data["topicNav"] = $this->getTopicNavigator();

		/*
            Load views
         */
		$this->load->view('templates/flashcard/admin/header', $data);
		$this->load->view('templates/flashcard/admin/leftsidebar', $data);
		$this->load->view('templates/flashcard/admin/admin_header', $data);
		$this->load->view('pages/flashcard/admin/quotes', $data);
		$this->load->view('templates/flashcard/admin/footer', $data);
	}

	public function prompts($option = 'all') {
		/*
			Load needed models
		*/
		$this->load->model("/flashcard/promptmodel");

		/*
            404 if requested page isn't in directory
         */
		// if ( ! file_exists(APPPATH.'/views/pages/admin/user.php') || ((int)($this->session->userdata('value'))<500))
		//      {
		//          // Whoops, we don't have a page for that!
		//          show_404();
		//      }

		/*
            Load data to pass to view
        */
		// $name = "";
		// if ($this->session->userdata('firstName')!='') {
		// 	$name = $this->session->userdata('firstName');
		// } else if (($this->session->userdata('username')!='')) {
		// 	$name = $this->session->userdata('username');
		// }
		$data['title'] = " Admin Central | Manage Prompts | ".ucfirst($option);
		$data["page"] = "prompts";
		$data["allPrompts"] = $this->getAllPrompts();
		//$data['category'] = $this->categorymodel->getAllCategories();
		// $data["topicNav"] = $this->getTopicNavigator();

		/*
            Load views
         */
		$this->load->view('templates/flashcard/admin/header', $data);
		$this->load->view('templates/flashcard/admin/leftsidebar', $data);
		$this->load->view('templates/flashcard/admin/admin_header', $data);
		$this->load->view('pages/flashcard/admin/prompts', $data);
		$this->load->view('templates/flashcard/admin/footer', $data);
	}

	public function promptlinkage($option = 'all') {
		/*
			Load needed models
		*/
		$this->load->model("/flashcard/promptmodel");

		/*
            404 if requested page isn't in directory
         */
		// if ( ! file_exists(APPPATH.'/views/pages/admin/user.php') || ((int)($this->session->userdata('value'))<500))
		//      {
		//          // Whoops, we don't have a page for that!
		//          show_404();
		//      }

		/*
            Load data to pass to view
        */
		// $name = "";
		// if ($this->session->userdata('firstName')!='') {
		// 	$name = $this->session->userdata('firstName');
		// } else if (($this->session->userdata('username')!='')) {
		// 	$name = $this->session->userdata('username');
		// }
		$data['title'] = " Admin Central | Manage Prompt Linkage | ".ucfirst($option);
		$data["page"] = "promptlinkage";
		$data["allPrompts"] = $this->getLinkage(null);
		//$data['category'] = $this->categorymodel->getAllCategories();
		// $data["topicNav"] = $this->getTopicNavigator();

		/*
            Load views
         */
		$this->load->view('templates/flashcard/admin/header', $data);
		$this->load->view('templates/flashcard/admin/leftsidebar', $data);
		$this->load->view('templates/flashcard/admin/admin_header', $data);
		$this->load->view('pages/flashcard/admin/promptlinkage', $data);
		$this->load->view('templates/flashcard/admin/footer', $data);
	}
	

	/**

		Site functions

	**/

	public function getAllQuotesAJAX() {

		$this->load->model('flashcard/quotemodel');
		$allQuotes = $this->quotemodel->getAllQuotes();

		echo "{\"success\":\"1\",\"data\":".json_encode($allQuotes)."}";

	}

	public function getLinkage($data) {

		$this->load->model('flashcard/promptmodel');
		$linkage = $this->promptmodel->getLinkage($data);
		return $linkage;

	}

	public function getLinkageAJAX($data=null) {
		echo "{\"success\":\"1\",\"data\":".json_encode($this->getLinkage($data))."}";
	}


	public function getAllPrompts() {
		$this->load->model('flashcard/promptmodel');
		$prompts = $this->promptmodel->getPromptsAdmin();
		return $prompts;
	}

	public function addPrompt() {
		$data = $this->security->xss_clean($this->input->post());

		$this->load->model('flashcard/promptmodel');
		$prompts = $this->promptmodel->insertPrompt($data);
		echo "{\"success\":\"1\",\"data\":".json_encode($prompts)."}";
	}

	public function updatePrompt() {
		$data = $this->security->xss_clean($this->input->post());
//var_dump($data);die();
		$this->load->model('flashcard/promptmodel');
		$prompts = $this->promptmodel->updatePrompt($data);
		echo "{\"success\":\"1\",\"data\":".json_encode($prompts)."}";
	}

	public function getAllQuotes() {

		$this->load->model('flashcard/quotemodel');
		$allQuotes = $this->quotemodel->getAllQuotes();

		return $allQuotes;

	}

	public function updateQuote() {

		$data["id"] = $this->security->xss_clean($_POST['id']);
		$data["quote_text"] = $this->security->xss_clean($_POST['quote_text']);
		$data["quote_credit"] = $this->security->xss_clean($_POST['quote_credit']);
		$data["quote_date"] = $this->security->xss_clean($_POST['quote_date']);
		$data['active'] = ($_POST['active']=='on')?1:0;


		$this->load->model('flashcard/quotemodel');
		$allQuotes = $this->quotemodel->updateQuote($data);

		echo "{\"success\":\"1\",\"data\":".json_encode($allQuotes)."}";

	}

	public function addQuote() {

		$data["quote_text"] = $this->security->xss_clean($_POST['quote_text']);
		$data["quote_credit"] = $this->security->xss_clean($_POST['quote_credit']);
		$data["quote_date"] = $this->security->xss_clean($_POST['quote_date']);
		$data['active'] = ($_POST['active']=='on')?1:0;


		$this->load->model('flashcard/quotemodel');
		$allQuotes = $this->quotemodel->insertQuote($data);

		echo "{\"success\":\"1\",\"data\":".json_encode($allQuotes)."}";

	}

	public function getAllCodesAJAX() {

		$this->load->model('flashcard/fcsitecodemodel');
        $allCodes = $this->fcsitecodemodel->getSiteCodes();

        echo "{\"success\":\"1\",\"data\":".json_encode($allCodes)."}";

	}

	public function createCodes() {

		$this->load->model('flashcard/fcsitecodemodel');
        $allCodes = $this->fcsitecodemodel->createSiteCodes();

        echo "{\"success\":\"1\",\"data\":".json_encode($allCodes)."}";

	}

	public function getAllUsersAJAX() {

		$this->load->model('flashcard/fcuser_model');
        $allUsers = $this->fcuser_model->getAllUsers();

        echo "{\"success\":\"1\",\"data\":".json_encode($allUsers)."}";

	}

	public function getAllCategoriesAJAX() {

		$this->load->model('flashcard/categorymodel');
        $allCategories = $this->categorymodel->getAllCategories();

        echo "{\"success\":\"1\",\"data\":".json_encode($allCategories)."}";

	}

	public function getAllCardsAJAX() {

		$this->load->model('flashcard/cardmodel');
        $allCards = $this->cardmodel->getAllCards();

        echo "{\"success\":\"1\",\"data\":".json_encode($allCards)."}";

	}

	public function getCardsByID($card_id) {

		$this->load->model('flashcard/cardmodel');
        $cards = $this->cardmodel->getCardsByID($card_id);

        return $cards;

	}

	public function getAllCardsetsAJAX() {

		$this->load->model('flashcard/cardsetmodel');
        $allCardsets = $this->cardsetmodel->getAllCardSetsAdmin();

        echo "{\"success\":\"1\",\"data\":".json_encode($allCardsets)."}";

	}
	
	public function getAllCardsets() {
	
		$this->load->model('flashcard/cardsetmodel');
		$allCardsets = $this->cardsetmodel->getAllCardSetsAdmin();
	
		return json_encode($allCardsets);
	
	}

	public function getAllQuizzesAJAX() {

		$this->load->model('flashcard/quizmodel');
        $allQuizzes = $this->quizmodel->getAllQuizzes();

        echo "{\"success\":\"1\",\"data\":".json_encode($allQuizzes)."}";

	}

	public function getAdminQuizzesAJAX() {

		$this->load->model('flashcard/quizmodel');
        $allQuizzes = $this->quizmodel->getAdminQuizzes();

        echo "{\"success\":\"1\",\"data\":".json_encode($allQuizzes)."}";

	}

	public function getQuizByID($quiz_id) {

		$this->load->model('flashcard/quizmodel');
        $requestedQuiz = $this->quizmodel->getQuizByID($quiz_id);

        $requestedQuiz['drawOrder'] = $this->quizmodel->getQuizResponse($quiz_id);

        return $requestedQuiz;
        //echo "{\"success\":\"1\",\"data\":".json_encode($requestedQuiz)."}";

	}

	public function getQuizResponse($quiz_id) {

		$this->load->model('flashcard/quizmodel');
        $requestedQuiz = $this->quizmodel->getQuizResponse($quiz_id);

        return $requestedQuiz;
        //echo "{\"success\":\"1\",\"data\":".json_encode($requestedQuiz)."}";

	}

	public function getAllRoles() {

		$this->load->model('flashcard/fcuser_model');
        $allUsers = $this->fcuser_model->getAllRoles();

        return $allUsers;

	}

	public function updateUserRole() {

		$this->load->model('flashcard/fcuser_model');
        $result = $this->fcuser_model->updateUserRole();

        echo "{\"success\":\"1\",\"data\":".json_encode($result)."}";

	}

	public function recover() {

		$user = $this->security->xss_clean($_POST['user']);

		$this->load->model('flashcard/fcloginmodel');
		$data = $this->fcloginmodel->recoverPass($user);

		echo "{\"success\":\"1\",\"data\":".json_encode($data)."}";

	}

}