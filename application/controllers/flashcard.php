<?php
/**
 * Created by PhpStorm.
 * User: Scott
 * Date: 11/1/2014
 * Time: 9:24 PM
 */

class Flashcard extends CI_Controller {



    function __construct() {
        parent::__construct();

        $this->load->helper('url');


//        $this->load->model('Bios_model');
//        $this->load->model('Pages_model');

//        $userID = $this->session->userdata('user_id');
//        if (empty($userID)) {
//            $this->logged_in = 'welcome';
//        }
//        else {
//            $this->logged_in = 'student';
//        }

    }

    public function index($page = 'index')
    {
        $this->load->model('flashcard/cardmodel');
        $this->load->model('flashcard/cardsetmodel');
        $this->load->model('flashcard/categorymodel');
        if ( ! file_exists(APPPATH.'/views/pages/flashcard/'.$page.'.php'))
        {
            // Whoops, we don't have a page for that!
            show_404();
        }
        $data['page'] = $page;
        $data['title'] = ucfirst($page); // Capitalize the first letter
        $data['logo_images'] = $this->getAllImages(FCIMG."logos/");
        $data['fc_assets'] = FCASSETS;
        $data['cards'] = $this->cardmodel->getAllCards();
        $data['cardsets'] = $this->cardsetmodel->getAllCardSets();
        $data['category'] = $this->categorymodel->getAllCategories();
        $data['rootcategory'] = $this->getRootCategories();
        $data['cardsetmenu'] = $this->buildCategoryMenu();

        $this->load->view('templates/flashcard/header', $data);
        $this->load->view('pages/flashcard/'.$page, $data);
        $this->load->view('templates/flashcard/footer', $data);
    }

    public function viewcardsets($page = 'viewcardsets')
    {
        $this->load->model('flashcard/cardmodel');
        $this->load->model('flashcard/cardsetmodel');
        $this->load->model('flashcard/categorymodel');
        if ( ! file_exists(APPPATH.'/views/pages/flashcard/'.$page.'.php'))
        {
            // Whoops, we don't have a page for that!
            show_404();
        }
        $data['page'] = $page;
        $data['title'] = ucfirst($page); // Capitalize the first letter
        $data['logo_images'] = $this->getAllImages(FCIMG."logos/");
        $data['fc_assets'] = FCASSETS;
        $data['cards'] = $this->cardmodel->getAllCards();
        $data['cardsets'] = $this->cardsetmodel->getMyCardSets();
        $data['category'] = $this->categorymodel->getAllCategories();
        $data['cardsetmenu'] = $this->buildCategoryMenu();

        $this->load->view('templates/flashcard/header', $data);
        $this->load->view('pages/flashcard/'.$page, $data);
        $this->load->view('templates/flashcard/footer', $data);
    }


    public function quizzes($quiz = 'none')
    {
        $page = 'quizzes';
        $this->load->model('flashcard/quizmodel');
        $this->load->model('flashcard/cardsetmodel');
        $this->load->model('flashcard/categorymodel');
        $this->load->model('flashcard/promptmodel');
        if ( ! file_exists(APPPATH.'/views/pages/flashcard/'.$page.'.php'))
        {
            // Whoops, we don't have a page for that!
            show_404();
        }
        $data['page'] = $page;
        $data['title'] = ucfirst($page); // Capitalize the first letter
        $data['logo_images'] = $this->getAllImages(FCIMG."logos/");
        $data['fc_assets'] = FCASSETS;
        $data['quizzes'] = $this->quizmodel->getAllQuizzes();
        $data['cardsets'] = $this->cardsetmodel->getActiveCardSets();
        $data['category'] = $this->categorymodel->getAllCategories();
        $data['cardsetmenu'] = $this->buildCategoryMenu();




        if($quiz != 'none') {
            $data['incQuiz'] = $quiz;
            $allCards = $this->getCardsByCardsetID($quiz);
//            echo "<pre>";var_dump($allCards);echo "</pre>";die();
            $ids = array();
            foreach($allCards as $card) {

                foreach ($card as $k=>$v) {
                    if ($k == "id") {
                        if(!array_key_exists($v, $ids)){
                            array_push($ids, $v);
                        }
                    }
                }

            }


            $data['linkage'] = $this->getLinkage(array("idarr"=>$ids));
        }
//        echo "<pre>";var_dump($data['linkage']);echo "</pre>";die();
        $this->load->view('templates/flashcard/header', $data);
        $this->load->view('pages/flashcard/'.$page, $data);
        $this->load->view('templates/flashcard/footer', $data);
    }

    public function testing($page = 'testing') {

        $data['page'] = $page;
        $data['title'] = ucfirst($page); // Capitalize the first letter
        $data['fc_assets'] = FCASSETS;
        // $data['cards'] = $this->cardmodel->getAllCards();
        // $data['cardsets'] = $this->cardsetmodel->getAllCardSets();
        // $data['category'] = $this->categorymodel->getAllCategories();

        // $this->load->view('templates/flashcard/header', $data);
        //$this->load->view('pages/flashcard/'.$page, $data);
        // $this->load->view('templates/flashcard/footer', $data);
echo "<pre>";
        var_dump($this->getLinkage(array("promptid"=>6)));
        echo "</pre>";
        
    }


    /***********************************************
     *      Functions for Flashcard app
     **********************************************/

    public function getFCAssets() {
        return $this->fc_assets;
    }

    public function getQuotes() {
        $this->load->model('flashcard/quotemodel');
        $quotes = $this->quotemodel->getActiveQuotes();
        echo "{\"success\":\"1\",\"data\":".json_encode($quotes)."}";
    }

    public function assignPrompt($data) {

        $this->load->model('flashcard/promptmodel');
        $prompts = $this->promptmodel->linkPrompt($data);
        return $prompts;

    }

    public function getWrongAJAX() {

        $data = $_POST;

        $this->load->model('flashcard/promptmodel');
        $wrong = $this->promptmodel->getWrongByUserCard($data);
        echo "{\"success\":\"1\",\"data\":".json_encode($wrong)."}";

    }

    public function getLinkage($data) {

        $this->load->model('flashcard/promptmodel');
        $linkage = $this->promptmodel->getLinkage($data);
        return $linkage;

    }

    public function getLinkageAJAX() {
        $resp = $this->getLinkage($_POST);
        echo "{\"success\":\"1\",\"data\":".json_encode($resp)."}";
    }

    public function getAllPromptsAJAX() {
        $this->load->model('flashcard/promptmodel');
        $prompts = $this->promptmodel->getPromptsAdmin();
        echo "{\"success\":\"1\",\"data\":".json_encode($prompts)."}";
    }

    public function getAllPrompts() {
        $this->load->model('flashcard/promptmodel');
        $prompts = $this->promptmodel->getPromptsAdmin();
        return $prompts;
    }

    public function addNewPrompt($incdata = null)
    {
        if (!$incdata) {
            $incdata = $this->security->xss_clean($this->input->post());
        }

        $this->load->model('flashcard/promptmodel');
        $prompts = $this->promptmodel->insertPrompt($incdata);
        return $prompts;
    }

    public function insertPromptWrongCard($incdata = null)
    {
        if (!$incdata) {
            $incdata = $this->security->xss_clean($this->input->post());
        }

        $this->load->model('flashcard/promptmodel');
        $prompts = $this->promptmodel->insertPromptWrongCard($incdata);
        return $prompts;
    }

    public function updatePrompt($incdata) {
        if (!$incdata) {
            $incdata = $this->security->xss_clean($this->input->post());
        }

        $this->load->model('flashcard/promptmodel');
        $prompts = $this->promptmodel->updatePrompt($incdata);
        return $prompts;
    }

    public function buildCategoryMenu() {


        $allCardsets = $this->getActiveCardsets();
        $rootCategories = $this->getRootCategories();
        $allCategories = $this->getAllCategories();

        foreach ($allCardsets as $cardset) {
            # code...
            $cardset->description = strip_tags($cardset->description);
            $cardset->description = trim($cardset->description);
            $cardset->categorization = explode('-', $cardset->categorization);

        }

        // $this->categorizationSort($allCardsets);
        usort($allCardsets, array('flashcard', 'cmp'));

        $returnHTML = "<ul id='categoryListing' class='subCategory'>";
        $previousRoot = null;
        $previousSub = null;
        $prevHowDeep = 0;
        $allCategories = (array)$allCategories;
        $adv = 0;
        $onedeep = null;
        // $this->showData($allCategories);
        /*
            Loop through root categories (they are alphabetized by name so list will be alphabetized)
            if there is a cardset with the root category for it's categorization create root category html
            if a cardset has subcategories for this root, then build subategory menu within root menu.
        */

        for ($i=0; $i < sizeof($rootCategories); $i++) {
            # code...
            $subcategory = null;


            foreach ($allCardsets as $cardset) {
                # code...

                $itemCategories = $this->getMyCategories(implode("-",$cardset->categorization));


                if (($cardset->categorization[0])==$rootCategories[$i]->id) {
                    // found root category for this cardset
                    # code...

                    $tempArr = $cardset->categorization;
                    array_shift($tempArr);
                    $howdeep = sizeof($tempArr);// size of array without root category "how deep do the subcategories go"

                    // if($rootCategories[$i]->name=="Science" && $itemCategories[1]->name=="Space") {
                    //     $this->showData($previousRoot);
                    //     $this->showData($previousSub);
                    //     $this->showData($howdeep);
                    //     $this->showData($prevHowDeep);
                    //     $this->showData($onedeep);
                    // }
                    if($onedeep) {
                        if($previousSub && $previousRoot && sizeof($itemCategories)>0) {

                            if(sizeof($itemCategories)>2) {
                                // more than one subcategory deep
                                if(($previousSub->id==$itemCategories[1]->id) && ($previousRoot->id==$itemCategories[0]->id)) {
                                    // current cardset is going to multi
                                    //  AND matches root and 1st sub
                                    $returnHTML .= "";
                                } elseif(($previousSub->id!=$itemCategories[1]->id) && ($previousRoot->id==$itemCategories[0]->id)) {
                                    // current cardset is going to multi
                                    //  AND matches root but NOT 1st sub
                                    $returnHTML .= "</li></ul>";
                                } else {
                                    // current is multi with new root
                                    //  previous was a onedeep
                                    // close cardset ul and sub li and ul
                                    $returnHTML .= "</ul></li></ul>";
                                }
                            } elseif(sizeof($itemCategories)==1) {
                                // current is root, previous had subcategory
                                // (this may need touched for howdeep prev was)
                                if($previousRoot->id==$itemCategories[0]->id) {
                                    // current cardset is root
                                    //  AND matches last root
                                    $returnHTML .= "</li></ul></li>";
                                } else {
                                    // current is new root
                                    //
                                    $returnHTML .= "</ul></li></ul>";
                                }
                            } elseif(sizeof($itemCategories)==2) {
                                // current has root and one subcategory
                                if(($previousSub->id==$itemCategories[1]->id) && ($previousRoot->id==$itemCategories[0]->id)) {
                                    // current cardset is in same root AND subcategory as last cardset
                                } elseif(($previousSub->id!=$itemCategories[1]->id) && ($previousRoot->id==$itemCategories[0]->id)) {
                                    // current cardset is in same root but different subcategory as last cardset
                                    $returnHTML .= "</ul></li>";
                                } else {
                                    // current cardset is in different root
                                    $returnHTML .= "</li></ul>";
                                }
                            }
                        } elseif(!$previousSub && $previousRoot) {
                            // no previoussub but previous root
                            //      (last cardset was a root category cardset)
                        }
                    } else {
                        if($previousRoot && !$previousSub) {
                            if($previousRoot->id==$itemCategories[0]->id) {
                                // current cardset matches last root
                                if(sizeof($itemCategories)==2) {
                                    // current is onedeep
                                    if($prevHowDeep>2) {
                                        // after a multi
                                        $returnHTML .= "";
                                    } else {
                                        $returnHTML .= "";
                                    }

                                } elseif(sizeof($itemCategories)>2) {
                                    $returnHTML .= "";
                                } else {
                                    $returnHTML .= "";
                                }
                            } else {
                                // current is new root
                                //
                                $returnHTML .= "</ul>";
                            }
                        } elseif($previousRoot && $previousSub) {
                            if($previousRoot->id==$itemCategories[0]->id) {
                                // current cardset is root
                                //  AND matches last root
                                if(sizeof($itemCategories)==2) {
                                    // current is onedeep
                                    if($prevHowDeep>2) {
                                        // after a multi
                                        $returnHTML .= "";
                                    } elseif($prevHowDeep==2){
                                        $returnHTML .= "</li>";
                                    } else {
                                        $returnHTML .= "";
                                    }

                                } elseif(sizeof($itemCategories)>2) {
                                    if($prevHowDeep==2){
                                        $returnHTML .= "</li>";
                                    } else {
                                        $returnHTML .= "</li>";
                                    }

                                } else {
                                    if($prevHowDeep>2) {
                                        // after a multi
                                        $returnHTML .= "</li>";
                                    } elseif($prevHowDeep==2){
                                        $returnHTML .= "</li>";
                                    } else {
                                        $returnHTML .= "</li>";
                                    }
                                }
                            } else {
                                // current is new root
                                //
                                if(sizeof($itemCategories)==2) {
                                    // current is onedeep
                                    if($prevHowDeep>2) {
                                        // after a multi
                                        $returnHTML .= "";
                                    } elseif($prevHowDeep==2){
                                        $returnHTML .= "";
                                    } else {
                                        $returnHTML .= "";
                                    }

                                } elseif(sizeof($itemCategories)>2) {
                                    if($prevHowDeep==2){
                                        $returnHTML .= "</li></ul>";
                                    } else {
                                        $returnHTML .= "";
                                    }

                                } else {
                                    if($prevHowDeep>2) {
                                        // after a multi
                                        $returnHTML .= "</li></ul>";
                                    } elseif($prevHowDeep==2){
                                        $returnHTML .= "</li></ul>";
                                    } else {
                                        $returnHTML .= "</li></ul>";
                                    }
                                }
                                // $returnHTML .= "</li></ul></li>";
                            }
                        }
                    }

                    if($previousRoot){
                        if($previousRoot->id!=$rootCategories[$i]->id) {
                            // new root category
                            $returnHTML .= "</li>";
                            $returnHTML .= "<li class='root-category root_".$rootCategories[$i]->id."'>".$rootCategories[$i]->name;
                        }
                    } else {

                        $returnHTML .= "<li class='root-category root_".$rootCategories[$i]->id."'>".$rootCategories[$i]->name;
                    }



                    if($howdeep>=2) {
                        //multiple subcategories
                        // echo "prevSub = $previousSub->name";
                        $onedeep = 0;
                        // $this->showData($itemCategories);
                        for ($l=0; $l <= $howdeep; $l++) {
                            //loop through subcategories starting with first
                            // loop must go through ALL of howdeep
                            # code...
                            $adv = 1;
                            if($l==0) {
                                foreach ($itemCategories as $key => $value) {
                                    # code...

                                    if($previousSub) {
                                        if($value->id==$previousSub->id) {
                                            $l = $key;
                                            $adv .= $key;
                                            break;
                                        }
                                    }
                                }
                            } else {
                                if($previousRoot && $previousSub) {
                                    if(($previousRoot->id==$itemCategories[0]->id) &&
                                        ($previousSub->id==$itemCategories[1]->id) )
                                    { // current has same previous root and 1st sub
                                        $returnHTML .= "<li class='multi'>".$itemCategories[$l]->name;
                                        // $adv--;
                                    }
                                    elseif(($previousRoot->id==$itemCategories[0]->id) &&
                                        ($previousSub->id!=$itemCategories[1]->id))
                                    {  // current has same previous root but differet first sub
                                        $returnHTML .= "<ul><li class='multi'>".$itemCategories[$l]->name;
                                        // $adv--;
                                    }
                                    else { // current has diff root
                                        $returnHTML .= "<ul><li class='multi'>".$itemCategories[$l]->name;
                                    }
                                } elseif($previousRoot && !$previousSub) {
                                    if(($previousRoot->id==$itemCategories[0]->id) && $l==1 )
                                    { // current has same root as previous
                                        $returnHTML .= "<li class='multi'>".$itemCategories[$l]->name;
                                        // $adv--;
                                    }
                                    else {  // current has different root
                                        $returnHTML .= "<ul><li class='multi'>".$itemCategories[$l]->name;
                                    }
                                } else { // no previous root - no previous sub
                                    $returnHTML .= "<ul><li class='multi'>".$itemCategories[$l]->name;
                                }

                            }

                            if($l==$howdeep) {
                                $returnHTML .= "<ul><li class='cardset' data-cardsetid='".$cardset->id."' >".$cardset->name."</li></ul>";

                                for ($m=$l-$adv; $m > 0; $m--) {
                                    # code...

                                    $returnHTML .= "</li></ul>";
                                }
                                $adv = 0;
                            }
                        }
                        $prevHowDeep = $howdeep;
                        $previousSub = $itemCategories[$howdeep];
                    } elseif($howdeep==1) {
                        //
                        $adv = 0;

                        if($previousRoot && $previousSub) {
                            if(($itemCategories[0]->id==$previousRoot->id) &&
                                ($previousSub->id==$itemCategories[1]->id))
                            {
                                $returnHTML .= "<li class='cardset' data-cardsetid='".$cardset->id."' >".$cardset->name."</li>";
                            } elseif(($itemCategories[0]->id==$previousRoot->id) &&
                                ($previousSub->id!=$itemCategories[1]->id))
                            {
                                $returnHTML .= "<li class='onedeep'>".$itemCategories[$howdeep]->name."";

                                $returnHTML .= "<ul><li class='cardset' data-cardsetid='".$cardset->id."' >".$cardset->name."</li>";
                            } else {
                                $returnHTML .= "<ul><li class='onedeep'>".$itemCategories[$howdeep]->name."";

                                $returnHTML .= "<ul><li class='cardset' data-cardsetid='".$cardset->id."' >".$cardset->name."</li>";
                            }
                        } elseif($previousRoot && !$previousSub) {
                            if($itemCategories[0]->id==$previousRoot->id)
                            {
                                $returnHTML .= "<li class='onedeep'>".$itemCategories[$howdeep]->name."";

                                $returnHTML .= "<ul><li class='cardset' data-cardsetid='".$cardset->id."' >".$cardset->name."</li>";
                            } else {
                                $returnHTML .= "<ul><li class='onedeep'>".$itemCategories[$howdeep]->name."";

                                $returnHTML .= "<ul><li class='cardset' data-cardsetid='".$cardset->id."' >".$cardset->name."</li>";
                            }
                        } else {
                            $returnHTML .= "<ul><li class='onedeep'>".$itemCategories[$howdeep]->name."";

                            $returnHTML .= "<ul><li class='cardset' data-cardsetid='".$cardset->id."' >".$cardset->name."</li>";
                        }

                        $onedeep = 1;
                        $prevHowDeep = $howdeep;
                        $previousSub = $itemCategories[$howdeep];
                    } elseif($howdeep==0) {
                        // root category cardset
                        $onedeep = 0;

                        if($previousRoot) {
                            if($previousRoot->id==$itemCategories[0]->id) {
                                // cardset is root cardset in same root as previous

                                $returnHTML .= "<li class='cardset' data-cardsetid='".$cardset->id."' >".$cardset->name."</li>";
                            } else {
                                // cardset is root cardset in diff root from prev

                                $returnHTML .= "<ul><li class='cardset' data-cardsetid='".$cardset->id."' >".$cardset->name."</li>";
                            }
                        } else {
                            // cardset is root cardset in diff root from prev

                            $returnHTML .= "<ul><li class='cardset' data-cardsetid='".$cardset->id."' >".$cardset->name."</li>";
                        }
                        $previousSub = null;
                        $prevHowDeep = $howdeep;
                    }
                    $previousRoot = $rootCategories[$i];
                    $prevHowDeep = $howdeep;
                }
                // $returnHTML .= "</li>";
            }
        }
        for ($i=0; $i < $prevHowDeep; $i++) {
            # code...
            $returnHTML .= "</ul></li></ul>";
        }
        $returnHTML .= "</li></ul>";
        return $returnHTML;
    }
    
    public function getAllImages() {
    	$imageArr = array();
	    foreach(glob('./assets/flashcard/images/logos/*.*') as $filename){
		     //echo basename($filename)."\n";
		     
		     $imageArr[] = basename($filename);
	 	}
	 	return json_encode($imageArr);
    	//return $imageArr;
    }

    public function saveQuizPrefs() {
        $data = $_POST['data'];

        //$toSess = $data;
        //unset($toSess['userid']);
//        foreach ($toSess as $k => $v) {
//
//            $toSess[$k] = char($v);
//            $data[$k] = ord($v);
//        }
//var_dump($data);
        $this->session->userdata['preferences']['quiz'] = $data;

        $this->load->model('flashcard/preferencesmodel');
        $result = $this->preferencesmodel->saveQuizPrefs($data);

        echo "{\"success\":\"1\",\"data\":".json_encode($data).",\"result\":".json_encode($this->session->userdata['preferences']['quiz'])."}";
    }

    public function cmptest() {
        $arr1 = array(
            "name" => "test1",
            "categorization" => array("1","2"),
            "id" => "1"
        );
        $arr2 = array(
            "name" => "test2",
            "categorization" => array("1","2","3"),
            "id" => "2"
        );

        $arr1 = (object) $arr1;
        $arr2 = (object) $arr2;

        
        // echo $this->cmp($arr1, $arr2);
        $this->showData($this->cmp($arr1, $arr2));
        $this->showData($arr1);
        $this->showData($arr2);
    }
    public function cmp($a, $b)
    {
        if(sizeof($a->categorization) > sizeof($b->categorization)) {
            $x = $b->categorization;
        } else {
            $x = $a->categorization;
        }

        for ($i=0; $i < sizeof($x) ; $i++) { 
            # code...
            if(intval($a->categorization[$i]) > intval($b->categorization[$i])) {
                return 1;
            } elseif (intval($a->categorization[$i]) < intval($b->categorization[$i])) {
                # code...
                return -1;
            } else {
                if($i == (int)(sizeof($x)-1)) {
                    if(sizeof($a->categorization) > sizeof($b->categorization)) {
                        return 2;
                    } elseif (sizeof($a->categorization) < sizeof($b->categorization)) {
                        # code...
                        return -2;
                    } else { 
                        return 0;
                    }
                }
                
            }

            
        }

    }


    private function categorizationSort(&$array) {

        $tempArr = $array;
        $first = $array[0];
        $swapped = false;

        for ($i=1; $i < sizeof($tempArr); $i++) { 
            # code...
            if(sizeof($tempArr[$i]->categorization) > sizeof($tempArr[$i-1]->categorization)) {
                $x = $tempArr[$i-1];
            } else {
                $x = $tempArr[$i];
            }

            for ($j=0; $j < sizeof($x->categorization); $j++) { 
                # code...
                if((intval($tempArr[$i]->categorization[$j])) < (intval($tempArr[$i-1]->categorization[$j]))){
                    $holder = $tempArr[$i-1];
                    $tempArr[$i-1] = $tempArr[$i];
                    $tempArr[$i] = $holder;
                    $swapped = true;
                } else {

                }
            }
        }
        $array = $tempArr;
        if($swapped) { 
            $this->categorizationSort($tempArr);
        } else {  }

        $this->showData($tempArr);

    }

    private function recur_sort(&$array) {
       foreach ($array as &$value) {
          if (is_array($value)) recur_sort($value);
       }
       return sort($array);
    }

    private function showData($data) {
        echo "<pre>";
        var_dump($data);
        echo "</pre>";
    }

    /**********************************************
     *  Card-based functions
     **********************************************/

    public function addNewCard() {

//        var_dump($this->input->post());die();
        $uid = $this->session->userdata('id');
        $data = array();
        $data['name'] = $this->input->post('name');
        $data['description'] = $this->input->post('description');
        $data['cardset_id'] = $this->input->post('cardset_id');
        $data['hint'] = $this->input->post('hint');
        $data['question'] = $this->input->post('question');
        $data['answer'] = $this->input->post('answer');

        $promptdata['prompt_text'] = $this->input->post('prompt');
        $promptdata['private'] = $this->input->post('private')?1:0;
        $promptdata['active'] = $this->input->post('active')?1:0;
        $promptdata['userid_added'] = $uid;
//
        $this->load->model('flashcard/promptmodel');
//        $prompt_id = $this->promptmodel->insertPrompt($promptdata);

        // foreach ($tempData as $key => $value) {
        //     # code...
        //     $data[$key] = $value;
        // }
        $this->load->model('flashcard/Cardmodel');
//        $card_id = $this->Cardmodel->insertNewCard($data);

//        $link["cardId"] = $card_id;
//        $link["promptId"] = $prompt_id;
        $link["userId"] = $uid;
//
//        $link_id = $this->promptmodel->linkPrompt($link);
//        $link["linkid"] = $link_id;

//        $data['id'] = $card_id;
        $data['promptdata'] = $promptdata;
        $data['link'] = $link;

        // var_dump($data) ;
        echo "{\"success\":\"1\",\"data\":".json_encode($data)."}";
        
         // return $card_id;
    }

    public function getCardsByCardsetID($cardset_id) {

        // Take 'categorization' and split on dashes '-'

// var_dump($categorization);
        $this->load->model('flashcard/Cardmodel');
        $allCategories = $this->Cardmodel->getCardsByCardsetID($cardset_id);

        return $allCategories;

    }

    public function getCardsByCardset() {

        $cardset_id = $this->input->post('selectCardset');
        // Take 'categorization' and split on dashes '-'
        
// var_dump($categorization);
        $this->load->model('flashcard/Cardmodel');
        $allCategories = $this->Cardmodel->getCardsByCardsetID($cardset_id);

        echo "{\"success\":\"1\",\"data\":".json_encode($allCategories)."}";

    }

    public function getCardsByCardsetAJAX() {

        $cardset_id = $this->input->post('selectCardset');
        // Take 'categorization' and split on dashes '-'
        
// var_dump($categorization);
        $this->load->model('flashcard/Cardmodel');
        $allCategories = $this->Cardmodel->getCardsByCardsetID($cardset_id);
        
        echo "{\"data\":".json_encode($allCategories)."}";

    }

    public function getCardsByCardsetDT() {

        $cardset_id = $this->input->post('selectCardset');

        if($cardset_id==null) $cardset_id = 0;
        // Take 'categorization' and split on dashes '-'
        
// var_dump($categorization);
        $this->load->model('flashcard/Cardmodel');
        $allCategories = $this->Cardmodel->getCardsByCardsetID($cardset_id);

        echo json_encode($allCategories);

    }

    public function removeCardFromCardset() {

        $card_id = $this->input->post('data');
// var_dump($card_id);
        if($card_id==null) return ;
        // Take 'categorization' and split on dashes '-'
        
// var_dump($categorization);
        $this->load->model('flashcard/Cardmodel');
        $result = $this->Cardmodel->removeCardFromCardset($card_id);

        echo "{\"success\":\"1\"}";

    }

    public function updateCard() {

        $pidbool = 0;
        if(isset($_POST["promptid"])) {
            $prompt_id = $_POST["promptid"];
            $pidbool = 1;
        }else { $prompt_id = null; }

        $inc = array();
        foreach($this->input->post() as $k=>$v){
            $inc[$k] = $v;
        }

//        var_dump($inc);die();
        $data = array();
        $data['id'] = $this->input->post('id');
        $data['name'] = $this->input->post('cardname');
        $data['description'] = $this->input->post('description');
        $data['categorization'] = $this->input->post('categorization');
        $data['cardset_id'] = $this->input->post('cardset_id');
        $data['hint'] = $this->input->post('hint');
        $data['question'] = $this->input->post('question');
        $data['answer'] = $this->input->post('answer');
        $data['modified_on'] = date("Y-m-d H:i:s", strtotime($this->input->post('modified_on')));
//var_dump($this->input->post());
        $promptdata['prompt_text'] = $inc['prompt'];
        $promptdata['private'] = $this->input->post('private')?1:0;
        $promptdata['active'] = $this->input->post('active')?1:0;
        $promptdata['userid_added'] = $this->session->userdata['id'];
//var_dump($data);die();
        $prmpt = 0;
        if($prompt_id) {
            $prmpt = 1;
            $promptdata["modified_date"] = $this->input->post('modified_date');
        }else {
            $promptdata["prompt_added_date"] = $this->input->post('prompt_added_date');
            $promptdata["modified_date"] = $this->input->post('modified_date');
        }

        if($prompt_id) {
            $promptdata['promptid'] = $prompt_id;
            $result = $this->updatePrompt($promptdata);
        }else {
//            echo "dead 803";var_dump($promptdata);die();
            $prompt_id = $this->addNewPrompt($promptdata);
            $promptdata['promptid'] = $prompt_id;
            $result = $prompt_id;
        }

        /**
         * ["promptid"]=>
        string(0) ""
        ["prompt"]=>
        string(11) "<p>test</p>"
        ["prompt_added_date"]=>
        string(19) "2015-08-07 11:09:11"
        ["modified_date"]=>
        string(19) "2015-08-07 11:09:11"
        ["modified_on"]=>
        string(19) "2015-08-07 11:09:11"
         */

        /** Update card */

        $this->load->model('flashcard/Cardmodel');
        $card_id = $data['id'];
        $cardresult = $this->Cardmodel->updateCard($data);
        /** End Card update */

        /**
         *  1   id	            int(11)
        2	promptId	    int(11)
        3	userId	        int(11)
        4	cardId	        int(11)
        5	alarmWrong	    int(11)
        6	last_modified	datetime
         */

        /** Setup linkage package  **/
        $linkage["promptId"] = $promptdata['promptid'];
        $linkage["userId"] = $this->session->userdata['id'];
        $linkage["cardId"] = $card_id;
        $linkage["last_modified"] = $promptdata["modified_date"];
        $linkage["alarmWrong"] = $this->input->post('alarmWrong');

        if($pidbool) {
            echo "{\"success\":\"1\",\"cardresult\":\"$cardresult\"}";
        }else if(!$pidbool) {

            $linkres = $this->assignPrompt($linkage);
            echo "{\"success\":\"1\",\"linkId\":\""+$linkres+"\"}";
        }


        
        // var_dump($data) ;
//        if($prmpt) {

//        }else {
//          echo "{\"success\":\"1\",\"linkId\":\""+$linkres+"\"}";
//        }

        
         // return $card_id;
    }

    // End card-based functions
    /**********************************************
     *  Category-based functions
     **********************************************/

    public function updateCategory() {

        $data = array();
        $data['id'] = $this->input->post('id');
        $data['name'] = $this->input->post('name');
        $data['description'] = $this->input->post('description');
        $data['categorization'] = $this->input->post('categorization');
        $data['modified_on'] = $this->input->post('modified_on');
        $data['active'] = $this->input->post('active');

        // foreach ($tempData as $key => $value) {
        //     # code...
        //     $data[$key] = $value;
        // }
        $this->load->model('flashcard/Categorymodel');
        $card_id = $this->Categorymodel->updateCategory($data);


        if ($card_id) {
            echo "{\"success\":\"1\"}";
        } else {
            echo "{\"success\":\"0\"}";
        }
    }

    public function getAllCategories() {

        $this->load->model('flashcard/Categorymodel');
        $allCategories = $this->Categorymodel->getAllCategories();

        return $allCategories;

    }

    public function getRootCategories() {
        $this->load->model('flashcard/Categorymodel');
        $rootCategories = $this->Categorymodel->getRootCategories();

        return $rootCategories;
    }

    public function getSubCategories() {
        $this->load->model('flashcard/Categorymodel');
        $subCategories = $this->Categorymodel->getSubCategories();

        return $subCategories;
    }

    public function addNewCategory() {

        $data = array();
        $data['name'] = $this->input->post('cat_name');
        $data['description'] = $this->input->post('description');
        $data['categorization'] = $this->input->post('categorization');

        $this->load->model('flashcard/Categorymodel');
        $category_id = $this->Categorymodel->insertNewCategory($data);
        $data['id'] = $category_id;
//         var_dump($data) ;die();
        echo "{\"success\":\"1\",\"data\":".json_encode($data)."}";
        
         // return $card_id;
    }

    /**
        function getCategoriesById

        This function takes a categorization string in a
        (categoryId)(dash)(categoryId) pattern ("13-5-2")
        and returns with the categories in the cat string.
    **/
    public function getCategoriesById() {
        $categorization = $this->input->post('categorization');
        // Take 'categorization' and split on dashes '-'
        $categories = explode('-', $categorization);

        $this->load->model('flashcard/Categorymodel');
        $allCategories = $this->Categorymodel->getCategoriesById($categories);
        // var_dump($allCategories);
        $ordered = array();

        for ($i=0; $i < count($categories); $i++) { 
            # code...

            foreach ($allCategories as $category) {
                # code...
                if($categories[$i]===$category->id) {
                    array_push($ordered, $category);
                }
            }
        }

        echo "{\"success\":\"1\",\"data\":".json_encode($ordered)."}";
    }

    public function getMyCategories($catcode) {
        
        // Take 'categorization' and split on dashes '-'
        $categories = explode('-', $catcode);

        $this->load->model('flashcard/Categorymodel');
        $allCategories = $this->Categorymodel->getCategoriesById($categories);
        // var_dump($allCategories);
        $ordered = array();

        for ($i=0; $i < count($categories); $i++) { 
            # code...

            foreach ($allCategories as $category) {
                # code...
                if($categories[$i]===$category->id) {
                    array_push($ordered, $category);
                }
            }
        }

        return $ordered;
    }
    // End category-based functions
    /**********************************************
     *  Cardset-based functions
     **********************************************/

    public function getAllCardsets() {

        $this->load->model('flashcard/Cardsetmodel');
        $allCardsets = $this->Cardsetmodel->getAllCardsets();

        return $allCardsets;

    }

    public function getActiveCardsets() {

        $this->load->model('flashcard/Cardsetmodel');
        $activeCardsets = $this->Cardsetmodel->getActiveCardsets();

        return $activeCardsets;

    }

    public function addNewCardSet() {
        $data = array();
        $data['name'] = $this->input->post('name');
        $data['description'] = $this->input->post('description');
        $data['categorization'] = $this->input->post('categorization');
        $data['active'] = 1;

        $this->load->model('flashcard/Cardsetmodel');
        $cardset_id = $this->Cardsetmodel->insertNewCardSet($data);
        $data['id'] = $cardset_id;
        // var_dump($data) ;
        echo "{\"success\":\"1\",\"data\":".json_encode($data)."}";
        
         // return $card_id;
    }

    

    public function getCardsetsById() {
        $cardset_id = $this->input->post('selectCardset');
        // Take 'categorization' and split on dashes '-'
        
        $this->load->model('flashcard/Cardsetmodel');
        $cardset = $this->Cardsetmodel->getCardSetsById($cardset_id);

        echo "{\"success\":\"1\",\"data\":".json_encode($cardset)."}";
    }

    public function updateCardset() {

        $data = array();
        $data['id'] = $this->input->post('id');
        $data['name'] = $this->input->post('name');
        $data['description'] = $this->input->post('description');
        $data['categorization'] = $this->input->post('categorization');
        $data['modified_on'] = gmdate('Y-m-d h:i:s');
        $data['active'] = $this->input->post('active');

        $this->load->model('flashcard/Cardsetmodel');
        $card_id = $this->Cardsetmodel->updateCardset($data);
        
        
        if ($card_id) echo "{\"success\":\"1\"}";
        
         // return $card_id;
    }
} 