<?php //if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Scott
 * Date: 11/12/2014
 * Time: 1:55 PM
 */

class FCLoginmodel extends CI_Model {
    function __construct() {
        parent::__construct();

    }

    public function validate() {
        // $usersdb = $this->load->database('users', TRUE);

        $regCode = $this->security->xss_clean($this->input->post('code'));
        $submitType = $this->security->xss_clean($this->input->post('submit'));
        $codeExist = $this->db->select('id')->where('code', $regCode)->limit(1)->get('flash_card_code_table');


        $username = $this->security->xss_clean($this->input->post('username'));
        $password = $this->security->xss_clean($this->input->post('password'));

//        var_dump($this->input->post());
//        var_dump(array(md5($password),$this->crypt("encrypt", $password)));
        
        $defaultSecLevel = 1;

        // Prep the query
        $this->db->select("*")->where('username', $username);
        $this->db->where_in('password', array(md5($password),$this->crypt("encrypt", $password)));
        // var_dump($password);
        // Run the query
        $query = $this->db->get('flash_card_users');


        if($submitType=="Login") {

            // Let's check if there are any results
            if($query->num_rows == 1) {
                // If there is a user, then create session data
                $row1 = $query->row();


                if($row1->password==md5($password)) {

                    $this->db->where('id',$row1->id)->update("flash_card_users", array('password'=>$this->crypt("encrypt", $password)));
                }

                $secQuery = $this->db->select('name, secVal')->where('id', $row1->roleId)->get('flash_card_roles');
                
                // var_dump($secData);
                $secData = $secQuery->row();

                $this->load->model('/flashcard/preferencesmodel');
                //$this->session->userdata['preferences']['quiz'] = $this->Preferencesmodel->getQuizPrefs($this->session->userdata['id']);
                $quizPrefs = $this->preferencesmodel->getQuizPrefs($row1->id);

                $prefs = array('quiz' => $quizPrefs);

                $sessdata = array(
                    'id' => $row1->id,
                    'username' => $row1->username,
                    'secVal' => $secData->secVal,
                    'role' => $secData->name,
                    'fc_validated' => true,
                    'preferences' => $prefs
                );

//                 var_dump($sessdata);

                $this->session->set_userdata($sessdata);

                return true;
            } else {
                // no user currently in database
                return "no user found";
            }

        } elseif ($submitType=="Register") {
            // 

            // Let's check if there are any results
            if($query->num_rows == 1) {
                // If there is a user, then create session data
                //      This causes the register button to act as the Login button if 
                //          user is found but they clicked Register.
                $row1 = $query->row();

                $secQuery = $this->db->select('name, secVal')->where('id', $row1->roleId)->get('flash_card_roles');
                
                // var_dump($secData);
                $secData = $secQuery->row();

                $this->load->model('/flashcard/preferencesmodel');
                //$this->session->userdata['preferences']['quiz'] = $this->Preferencesmodel->getQuizPrefs($sessdata['id']);
                $quizPrefs = $this->preferencesmodel->getQuizPrefs($row1->id);
                $prefs = array('quiz' => $quizPrefs);

                $sessdata = array(
                    'id' => $row1->id,
                    'username' => $row1->username,
                    'secVal' => $secData->secVal,
                    'role' => $secData->name,
                    'fc_validated' => true,
                    'preferences' => $prefs
                );

                // var_dump($sessdata);
                
            } else {
                // no user currently in database
                
                //  register user
                $userdata = array(
                    'username' => $username,
                    'password' => $this->crypt("encrypt", $password),
                    'roleId' => '1'
                );

                $userid = $this->db->insert('flash_card_users', $userdata);
                // $result = $usersdb->set('userId',$userid)->insert('vel_db_user_info');
                // if($userid) {

                //     $usersdb->set('userId',$userid)->set('roleId', 4)->insert('vel_db_user_roles');

                // }
                $secQuery = $this->db->select('name, secVal')->where('id', $userid)->get('flash_card_roles');
                $secData = $secQuery->row();



                $sessdata = array(
                    'id' => $userid,
                    'username' => $username,
                    'secVal' => $secData->secVal,
                    'role' => $secData->name,
                    'fc_validated' => true,
                    'newUser' => true
                );
            }

//            die("die");
            // is there a special registration code attached?
            // if($codeExist->result()!=null) {
            //     // special registration code found
                                
            //     /******   HANDLE REGISTRATION CODE LINKAGE HERE ****************************
            //     **                                                                        **
            //     **                                                                        **
            //     ***************************************************************************/

            //     // var_dump($sessdata);
                                
            // }

            $this->session->set_userdata($sessdata);

            return true;
        }

        // If the previous process did not validate
        // then return false.
        return false;
    }
    public function crypt($_oper,$_data) {
        // init
        {
            if(!defined("AES_ENCRYPTION_KEY"))
                define("AES_ENCRYPTION_KEY","\xA4\x94\x52\xA4\xE2\x29\x34\xB7\xD2\x61\x8A\x76\x52\xE3\xA7\x7F\xD1\x5B\x29\x70\x48\x38\x67\x25\xB4\x64\x73\x34\xB4\xB8\x62\x3D");
            mcrypt_generic_init(($_ciph = mcrypt_module_open(MCRYPT_RIJNDAEL_256,"",MCRYPT_MODE_ECB,"")),AES_ENCRYPTION_KEY,mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC), MCRYPT_RAND));
        }
        // crypt
        {
            switch($_oper) {
                case "encrypt" :
                    $_resl = strtoupper(bin2hex(mcrypt_generic($_ciph,$_data)));
                    break;
                case "decrypt" :
                    if(!trim($_data))
                        return "";
                    $_data = join("",array_map("chr",array_map("hexdec",str_split($_data,2))));
                    $_resl = trim(mdecrypt_generic($_ciph,$_data));
                    break;
            }
        }
        // clos
        {
            mcrypt_generic_deinit($_ciph);
        }
        return $_resl;
    }

    public function recoverPass($userid) {

        $this->db->select("*")->where('id', $userid);
        $query = $this->db->get('flash_card_users');

        if($query->num_rows == 1) {
            // If there is a user, then create session data
            $row1 = $query->row();


            return $this->crypt("decrypt", $row1->password);

        }else {
            return "no user found";
        }

    }

}