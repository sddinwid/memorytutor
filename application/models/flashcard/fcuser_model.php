<?php

class Fcuser_model extends CI_Model {

	function __construct() {
    	parent::__construct();
    }
    function getUserTest() {
    	$usersdb = $this->load->database();
		$query = $this->db->get("dall_io_db_user");
		return $query->result();
	}

	function getAllUsers() {

		$this->db->select('flash_card_users.id, flash_card_users.username, flash_card_roles.name, flash_card_roles.secVal');
                    
            $this->db->from('flash_card_users');

            $this->db->join('flash_card_roles', 'flash_card_roles.id=flash_card_users.roleId', 'left');
            
            $query2 = $this->db->get();

            return $query2->result();

	}

    function updateUserProfile($data) {

        
        

        return $data;

    }

    function getAllRoles(){
        $query = $this->db->get("flash_card_roles");
        return json_encode($query->result());
    }

    function updateUserRole(){
        $userid = $this->security->xss_clean($this->input->post('user'));
        $secLvl = $this->security->xss_clean($this->input->post('secLvl'));
        
        $result = $this->db->set('roleId', $secLvl)->where('id', $userid)->limit(1)->update('flash_card_users');

        return $result;
    }
}