<?php
/**
 * Created by PhpStorm.
 * User: Scott
 * Date: 11/2/2014
 * Time: 1:15 PM
 */

class Cardmodel extends CI_Model
{
    var $name = '';

    function __construct()
    {
        parent::__construct();
    }

    function getAllCards()
    {
        $allCards = '';
        $query = $this->db->order_by('flash_card_cards.name')->get('flash_card_cards');
        // foreach ($query->result() as $row)
        // {
        //     $allCards .= json_encode($row);
        // }
        return $query->result();

    }

    function insertNewCard($data)
    {
        $categorization = $data['categorization'];

        $this->db->set($data);
        $this->db->set('userId', $this->session->userdata['id']);
        $this->db->set('created_on', 'NOW()', false);
        $this->db->set('modified_on', 'NOW()', false);
        $this->db->insert('flash_card_cards');
        $this->db->limit(1);
        $new_id = $this->db->insert_id();

        return $new_id;

    }

    function updateCard ($data) {
// var_dump($data);
        $this->db->set($data);
        
        $this->db->where('id', $data['id']);
        $this->db->limit(1);
        return $this->db->update('flash_card_cards');

    } 

    function removeCardFromCardset ($card_id)
      {

        $this->db->set('cardset_id', '-1');
        $this->db->set('modified_on', 'NOW()', false);
        $this->db->where('id', $card_id);
        $this->db->limit(1);
        $this->db->update('flash_card_cards');

        return 1;

      }

    function getCardsByCardsetID($cardset_id) {

        $this->db->select('*');
        
        if ($cardset_id) {
          $this->db->where_in('cardset_id',$cardset_id);
        }
        $this->db->from('flash_card_cards')->order_by('flash_card_cards.name');
        
        $query = $this->db->get();
        return $query->result();
    }

    function getCardsByID($card_id) {

        $this->db->select('*');
        
        if ($card_id) {
          $this->db->where_in('id',$card_id);
        }
        $this->db->from('flash_card_cards')->order_by('flash_card_cards.name');
        
        $query = $this->db->get();
        return $query->result();
    }

    function getCardsByCardsetIDAJAX($cardset_id) {

        $this->db->select('*');
        
        if ($cardset_id) {
          $this->db->where_in('cardset_id',$cardset_id);
        }
        $this->db->from('flash_card_cards')->order_by('flash_card_cards.name');
        
        $query = $this->db->get();
        return $query->result();
    }

} 