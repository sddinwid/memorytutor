<?php //if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Scott
 * Date: 11/12/2014
 * Time: 1:55 PM
 */

class Preferencesmodel extends CI_Model {
    function __construct() {
        parent::__construct();

    }

    // public function validate($code=null) {

    // $sddb = $this->load->database('users', TRUE);

    // $query = $sddb->select('*')->where('code', $code)->get('sd_code_table');

    // $result = ($query->num_rows>0) ? 1 : 0;

    // return $result;

    // }

    public function saveQuizPrefs($data=null) {
        $uid = $this->session->userdata['id'];
        if(isset($data)){
            $count = $this->db->select('*')->where('userid', $uid)->get('flash_card_user_preferences_quiz');
            $count = $count->result();
            if($count) {


                $this->db->set($data);
                $this->db->set('last_saved', 'NOW()', false);

                $this->db->where('userid',$uid);
                $query = $this->db->update('flash_card_user_preferences_quiz');

                return $query;

            } else {
                // insert

                $this->db->set($data);
                $this->db->set('last_saved', 'NOW()', false);
                $this->db->set('userid', $uid);
                $query = $this->db->insert('flash_card_user_preferences_quiz');

                return $query;
            }

        } else {
            return array("Error" => "No data");
        }

    }

    public function getQuizPrefs($userid=null) {

        if(isset($userid)) {

            $count = $this->db->select('correctKey,incorrectKey,easyKey,challengingKey,hardKey,impossibleKey')->where('userid', $userid)->get('flash_card_user_preferences_quiz');
            $data = $count->result();

            return $data;

        }
    }
}