<?php


class Quizmodel extends CI_Model {

	function __construct()
    {
        parent::__construct();
    }

    function insertQuiz ($data) {

    	$this->db->set($data);
        $this->db->insert('flash_card_quizzes');
        $this->db->limit(1);
        $new_id = $this->db->insert_id();

        return $new_id;

    }

    function insertQuizResponse ($data) {

        // $this->db->set($data);
        
        // $this->db->limit(1);
        $new_id = $this->db->insert_batch('flash_card_quiz_response_data', $data);//$this->db->insert_id();

        return $new_id;

    }

    function getQuizResponse ($quiz_id) {

        // $this->db->set($data);
        $this->db->where('quiz_id', $quiz_id);
        
        // $this->db->limit(1);
        $drawOrder = $this->db->get('flash_card_quiz_response_data');//$this->db->insert_id();

        return $drawOrder->result();

    }

    function updateQuiz ($data) {

    	$this->db->set($data);
        
        $this->db->where('id', $data['id']);
        $this->db->limit(1);
        return $this->db->update('flash_card_quizzes');

    }

    function getQuizByID ($quiz_id) {

    	$this->db->select("
                flash_card_quizzes.id,
                flash_card_quizzes.cardset_id,
                flash_card_cardsets.name as cardsetname,
                flash_card_quizzes.userId,
                flash_card_users.username,
                flash_card_quizzes.date_taken,
                flash_card_quizzes.quiz_length,
                flash_card_quizzes.end_time,
                flash_card_quizzes.quiz_time,
                flash_card_quizzes.percent_correct,
                flash_card_quizzes.number_of_questions
            ");

        
        $this->db->where_in('flash_card_quizzes.id',$quiz_id);
        
        $this->db->from("flash_card_quizzes")->join('flash_card_cardsets', 'flash_card_cardsets.id = flash_card_quizzes.cardset_id');
        $this->db->join('flash_card_users', 'flash_card_users.id = flash_card_quizzes.userId');
        
        $query = $this->db->get();
        return $query->result();

    }

    function getAllQuizzes () {

    	$query = $this->db->get('flash_card_quizzes');

    	return $query->result();
    }

    function getAdminQuizzes () {

        $this->db->select("
                flash_card_quizzes.id,
                flash_card_quizzes.cardset_id,
                flash_card_cardsets.name as cardsetname,
                flash_card_quizzes.userId,
                flash_card_users.username,
                flash_card_quizzes.date_taken,
                flash_card_quizzes.quiz_length,
                flash_card_quizzes.end_time,
                flash_card_quizzes.quiz_time,
                flash_card_quizzes.percent_correct,
                flash_card_quizzes.number_of_questions
            ");

        $this->db->from("flash_card_quizzes")->join('flash_card_cardsets', 'flash_card_cardsets.id = flash_card_quizzes.cardset_id');
        $this->db->join('flash_card_users', 'flash_card_users.id = flash_card_quizzes.userId');
        $this->db->order_by('date_taken', 'desc');
        $query = $this->db->get();

        return $query->result();
    }

    function getQuizRatings () {

    	$query = $this->db->get('flash_card_quiz_ratings');

    	return $query->result();
    }

    function insertQuizRating () {

    	$this->db->set($data);
        $this->db->set('created_on', 'NOW()', false);
        $this->db->set('modified_on', 'NOW()', false);
        $this->db->insert('flash_card_quiz_ratings');
        $this->db->limit(1);
        $new_id = $this->db->insert_id();

        return $new_id;

    }

}