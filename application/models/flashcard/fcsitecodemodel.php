<?php //if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Scott
 * Date: 11/12/2014
 * Time: 1:55 PM
 */

class Fcsitecodemodel extends CI_Model {
    function __construct() {
        parent::__construct();

    }

    // public function validate($code=null) {

        // $sddb = $this->load->database('users', TRUE);

        // $query = $sddb->select('*')->where('code', $code)->get('sd_code_table');

        // $result = ($query->num_rows>0) ? 1 : 0;

        // return $result;

    // }

    public function getSiteCodes($option='all') {

        // $sddb = $this->load->database('users', TRUE);

        $query = $this->db->select('*')->get('flash_card_code_table');

        return $query->result();

    }

    public function createSiteCodes() {

        // $sddb = $this->load->database('users', TRUE);

        $howmany = $this->input->post('howmany');

        // $codetest = true;
        $data = array();
        $code = "";
        for ($i=0; $i < $howmany; $i++) { 
            # code...
            for ($j=0; $j < 35; $j++) { 
                # code...
                if($j%5==0 && $j!=0) $code.= "-";
                $code .= dechex(mt_rand(0, 15));
                
                if($j==34) {
                    // if($codetest) {
                        
                    //     $code = "feca9-20aae-e19e7-8e218-5a4dc-f275f-9d5b6";
                    //     $codetest = false;
                    // }
                    $testcode = $this->db->select('*')->where('code', $code)->get('flash_card_code_table');
                    if($testcode->num_rows>0) {
                        $j=0;
                        $code='';
                    }
                }
            }
            $data[$i] = array('code' => $code);
            $code = '';
        }


        $result = $this->db->insert_batch('flash_card_code_table', $data);

        return $result;

    }
}