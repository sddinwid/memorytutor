<?php
/**
 * Created by PhpStorm.
 * User: Scott
 * Date: 11/2/2014
 * Time: 8:43 PM
 */

class Quotemodel extends CI_Model{

    function __construct()
    {
        parent::__construct();
    }

    function getAllQuotes() {
        $allCardSets = '';

        $query = $this->db->get('flash_card_insp_quotes');


        // foreach ($query->result() as $row)
        // {
        //     $allCardSets .= json_encode($row);
        // }
        return $query->result();
    }

    function getAllCardSetsAdmin() {
        $allCardSets = '';
        $this->db->select('*');

        $query = $this->db->get('flash_card_insp_quotes');


        // foreach ($query->result() as $row)
        // {
        //     $allCardSets .= json_encode($row);
        // }
        return $query->result();
    }

    function getActiveQuotes() {
        $allCardSets = '';

        $query = $this->db->where('active', 1)->get('flash_card_insp_quotes');

        return $query->result();
    }

    function getQuotesById($where) {
        $this->db->select('*');
        
        if ($where) {
          $this->db->where_in('id',$where);
        }
        if (isset($this->session->userdata['secVal'])) {
            if($this->session->userdata['secVal']>='500') {

            } else {
                $this->db->where('userId', $this->session->userdata['id']);
            }
        } else {
            // $this->db->where('userId', '23169451513213546456');
        }
        $this->db->from('flash_card_insp_quotes')->order_by('flash_card_insp_quotes.id');
        
        $query = $this->db->get();
        return $query->result();
        // return $where;//$this->db->last_query();
    }

    function insertQuote($data)
    {
        $this->db->set($data);
        $this->db->insert('flash_card_insp_quotes');
        $this->db->limit(1);
        $new_id = $this->db->insert_id();

        return $new_id;

    }

    function updateQuote($data)
    {
        $dateArr = date_parse($data['quote_date']);
        $data['quote_date'] = $dateArr["year"]."-".$dateArr["day"]."-".$dateArr["month"];

        $this->db->set($data);
        $this->db->where('id', $data['id']);
        $this->db->limit(1);
        unset($data['id']);
        return $this->db->update('flash_card_insp_quotes');

    }

} 