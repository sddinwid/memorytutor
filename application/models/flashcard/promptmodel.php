<?php
/**
 * Created by PhpStorm.
 * User: Scott
 * Date: 11/2/2014
 * Time: 8:43 PM
 **

 */

class Promptmodel extends CI_Model{

    function __construct()
    {
        /**
         *  1   id	            int(12)
            2	prompt_text	    text
            3	prompt_category	int(11)
            4	prompt_added_date	    date
            5	modified_date	date
            6	userid_added	int(11)
            7	private	        tinyint(1)
            8	active          tinyint(1)
         */
        parent::__construct();
    }

    function getAllPrompts() {
        $allCardSets = '';

        $query = $this->db->get('flash_card_prompts');


        // foreach ($query->result() as $row)
        // {
        //     $allCardSets .= json_encode($row);
        // }
        return $query->result();
    }

    function getPromptsAdmin() {
        $allCardSets = '';
        $this->db->select('
            flash_card_prompts.id,
            flash_card_prompts.prompt_text,
            flash_card_prompts.prompt_category,
            flash_card_prompts.prompt_added_date,
            flash_card_prompts.modified_date,
            flash_card_users.username as userid_added,
            flash_card_prompts.private,
            flash_card_prompts.active,
        ');

        /**
         * $this->db->select('flash_card_cardsets.id,
        flash_card_cardsets.name,
        flash_card_cardsets.description,
        flash_card_cardsets.categorization,
        flash_card_users.username,
        flash_card_cardsets.created_on,
        flash_card_cardsets.modified_on,
        flash_card_cardsets.active,
        (SELECT COUNT(*)
        FROM `flash_card_cards`
        WHERE `flash_card_cards`.`cardset_id` = `flash_card_cardsets`.`id`) as count
        ');
        $this->db->join('flash_card_users', 'flash_card_users.id = flash_card_cardsets.userId');

        $query = $this->db->order_by('flash_card_cardsets.name')->get('flash_card_cardsets');
         */

        $this->db->join('flash_card_users', 'flash_card_users.id = flash_card_prompts.userid_added');

        $query = $this->db->get('flash_card_prompts');


        // foreach ($query->result() as $row)
        // {
        //     $allCardSets .= json_encode($row);
        // }
        return $query->result();
    }

    function getActivePrompts() {
        $allCardSets = '';

        $query = $this->db->where('active', 1)->get('flash_card_prompts');

        return $query->result();
    }

    function getPromptsByCategory($where) {
        $this->db->select('*');

        if ($where) {
            $this->db->where('prompt_category',$where);

            if (isset($this->session->userdata['secVal'])) {
                if($this->session->userdata['secVal']>='500') {

                } else {
                    $this->db->where('userId', $this->session->userdata['id']);
                }
            } else {
                // $this->db->where('userId', '23169451513213546456');
            }
            $this->db->from('flash_card_prompts')->order_by('flash_card_prompts.id');
        }

        try {
            $query = $this->db->get();
            return $query->result();
        } catch (Exception $e) {
            return array("error"=>"An errored occurred. ".print_r($e, 1));
        }
        // return $where;//$this->db->last_query();
    }

    function getPromptsById($where) {
        $this->db->select('*');

        if ($where) {
            $this->db->where_in('id',$where);
        }
        if (isset($this->session->userdata['secVal'])) {
            if($this->session->userdata['secVal']>='500') {

            } else {
                $this->db->where('userId', $this->session->userdata['id']);
            }
        } else {
            // $this->db->where('userId', '23169451513213546456');
        }
        $this->db->from('flash_card_prompts')->order_by('flash_card_prompts.id');

        $query = $this->db->get();
        return $query->result();
        // return $where;//$this->db->last_query();
    }

    function getPromptsByUserId($uid) {
        $this->db->select('*');

        if($uid){
            if (isset($this->session->userdata['secVal'])) {
                if($this->session->userdata['secVal']>='500') {
                    $this->db->where('userId', $uid);
                } else {
                    $this->db->where('userId', $uid);
                }
            } else {
                // $this->db->where('userId', '23169451513213546456');
            }
            $this->db->from('flash_card_prompts')->order_by('flash_card_prompts.id');
        }

        try {
            $query = $this->db->get();
            return $query->result();
        } catch (Exception $e) {
            return array("error"=>"Something went wrong. ".print_r($e));
        }
        // return $where;//$this->db->last_query();
    }

    function insertPrompt($data)
    {

        /**
         * array(6) {
        ["prompt_text"]=>
        string(4) "test"
        ["active"]=>
        string(2) "on"
        ["alarmTime"]=>
        string(0) ""
        ["alarmWrong"]=>
        string(1) "4"
        ["prompt_added_date"]=>
        string(0) ""
        ["modified_date"]=>
        string(0) ""
        }
         */

        if(isset($data["private"])) {
            $data["private"] = 1;
        }else {
            $data["private"] = 0;
        }
        if(isset($data["active"])) {
            $data["active"] = 1;
        }else {
            $data["active"] = 0;
        }

        if(isset($data["promptid"])) {
            unset($data["promptid"]);
        }
        $data["userid_added"] = $this->session->userdata['id'];

//        var_dump($data);die();

        $this->db->set($data);
        $this->db->insert('flash_card_prompts');
        $this->db->limit(1);
        $new_id = $this->db->insert_id();

        return $new_id;

    }

    function insertPromptWrongCard($data)
    {
        $prompt = null;

        /**
         * array(6) {
        ["prompt_text"]=>
        string(4) "test"
        ["active"]=>
        string(2) "on"
        ["alarmTime"]=>
        string(0) ""
        ["alarmWrong"]=>
        string(1) "4"
        ["prompt_added_date"]=>
        string(0) ""
        ["modified_date"]=>
        string(0) ""
        }
         */

        if(isset($data["private"])) {
            $data["private"] = 1;
            $prompt["private"] = 1;
        }else {
            $data["private"] = 0;
            $prompt["private"] = 0;
        }
        if(isset($data["active"])) {
            $data["active"] = 1;
            $prompt["active"] = 1;
        }else {
            $data["active"] = 0;
            $prompt["active"] = 0;
        }

        if(isset($data["promptid"])) {
            unset($data["promptid"]);
        }
        $data["userid_added"] = $this->session->userdata['id'];
        $prompt["userid_added"] = $this->session->userdata['id'];

        $prompt["prompt_added_date"] = date("Y-m-d H:i:s");
        $prompt["modified_date"] = date("Y-m-d H:i:s");

        $prompt["prompt_text"] = $data["prompt_text"];

//        var_dump($data);die();

        $this->db->set($prompt);
        $this->db->insert('flash_card_prompts');
        $this->db->limit(1);
        $new_id = $this->db->insert_id();


        $promptlink["last_modified"] = date("Y-m-d H:i:s");
        $promptlink["alarmTime"] = $data["alarmTime"];
        $promptlink["alarmWrong"] = $data["alarmWrong"];

        $promptlink["userId"] = $this->session->userdata['id'];
        $promptlink["promptId"] = $new_id;


//        var_dump($data);die();

        $this->db->set($promptlink);
        $this->db->insert('flash_card_prompt_user_card');
        $this->db->limit(1);
        $new_id = $this->db->insert_id();

        return $new_id;

    }

    function updatePrompt($promptdata) {

//        var_dump($promptdata);

        if(isset($promptdata["private"]) && $promptdata["private"] == 1) {
            $promptdata["private"] = 1;
        }else {
            $promptdata["private"] = 0;
        }
        if(isset($promptdata["active"]) && $promptdata["active"] == 1) {
            $promptdata["active"] = 1;
        }else {
            $promptdata["active"] = 0;
        }
//        $data["result"] = "update";
//        var_dump($promptdata);
//        die();

        $pid = $promptdata['promptid'];
        unset($promptdata['promptid']);
        unset($promptdata['prompt_added_date']);
        $this->db->set($promptdata);
        $this->db->where('id', $pid);
        $this->db->limit(1);
        $result = $this->db->update('flash_card_prompts');
        return $result;
    }

    function linkPrompt($data) {

        /**
         *  1   id	            int(11)
            2	promptId	    int(11)
            3	userId	        int(11)
            4	cardId	        int(11)
            5	alarmWrong	    int(11)
            6	last_modified	datetime
         */

//        $data["result"] = "linkprimpt";
//        var_dump($data);die();
        if(!isset($data["userId"]) || (isset($data["flag"])) && $data['flag']=="self")
        $data["userId"] = $this->session->userdata['id'];

        $this->db->set($data);
//        $this->db->where('id', $data['id']);
        $this->db->limit(1);
//        unset($data['id']);
        return $this->db->insert('flash_card_prompt_user_card');

    }

    public function getLinkage($data) {

       $pid = (isset($data['promptid']))?$data['promptid']:null;
        $cid = (isset($data['cardId']))?$data['cardId']:null;
        $uid = (isset($data['userId']))?$data['userId']:null;
        $ida = (isset($data['idarr']))?$data['idarr']:null;
//var_dump($ida);die();
        /**
         * SELECT  `flash_card_prompts` . * ,  `flash_card_users`.username,  `flash_card_cards` . *
        FROM  `flash_card_prompts`
        JOIN  `flash_card_prompt_user_card` ON  `flash_card_prompt_user_card`.promptid =  `flash_card_prompts`.id
        JOIN  `flash_card_users` ON  `flash_card_users`.id =  `flash_card_prompts`.userid_added
        JOIN  `flash_card_cards` ON  `flash_card_cards`.id =  `flash_card_prompt_user_card`.cardId
        WHERE  `flash_card_prompt_user_card`.userId =1
         */

        $this->db->select('
            flash_card_prompts.id as promptid,
            flash_card_prompts.prompt_text,
            flash_card_prompts.prompt_added_date,
            flash_card_prompts.modified_date as prompt_modified_date,
            flash_card_prompts.userid_added as prompt_userid_added,
            flash_card_prompts.private as prompt_private,
            flash_card_prompts.active as prompt_active,
            flash_card_users.username,
            flash_card_prompt_user_card.alarmWrong,
            flash_card_prompt_user_card.alarmTime,
            flash_card_prompt_user_card.last_modified as alarm_last_modified,
            flash_card_cards.question as card_question,
            flash_card_cards.id as card_id
        ');
////,
//            flash_card_quiz_response_data.*
        $this->db->join("flash_card_prompt_user_card", "flash_card_prompt_user_card.promptid=flash_card_prompts.id");
        $this->db->join("flash_card_users", "flash_card_users.id=flash_card_prompts.userid_added");
        $this->db->join("flash_card_cards", "flash_card_cards.id=flash_card_prompt_user_card.cardId");
//        $this->db->join("flash_card_quizzes", "flash_card_quizzes.userId=flash_card_prompt_user_card.userId");
//        $this->db->join("flash_card_quiz_response_data", "flash_card_quiz_response_data.card_id=flash_card_prompt_user_card.cardId", "LEFT");

        if($pid) {

            $this->db->where("flash_card_prompt_user_card.promptId", $pid);
        }
        if($cid) {

            $this->db->where("flash_card_prompt_user_card.cardId", $cid);
        }
        if($uid) {

            $this->db->where("flash_card_prompt_user_card.userId", $uid);
        }
        if($ida) {

            $this->db->where_in("flash_card_prompt_user_card.cardId", $ida);
        }
//        if($data===null){
//
//        }
//        var_dump($data);die();
        $query = $this->db->get("flash_card_prompts");

//        var_dump($this->db->last_query());
//        var_dump($query->result());
//        die();

        return $query->result();

    }

    public function getWrongByUserCard($data) {

        $uid = $this->session->userdata['id'];
        $cid = (isset($data['cardset_ID']))?$data['cardset_ID']:null;

        $this->db->select('id')
                ->from("flash_card_quizzes")
                    ->where("userId", $uid)
                        ->where("cardset_id",$cid);
        $ids = $this->db->get()->result();
//        $ids = $ids[0];
        /**
         * SELECT  `flash_card_prompts` . * ,  `flash_card_users`.username,  `flash_card_cards` . *
        FROM  `flash_card_prompts`
        JOIN  `flash_card_prompt_user_card` ON  `flash_card_prompt_user_card`.promptid =  `flash_card_prompts`.id
        JOIN  `flash_card_users` ON  `flash_card_users`.id =  `flash_card_prompts`.userid_added
        JOIN  `flash_card_cards` ON  `flash_card_cards`.id =  `flash_card_prompt_user_card`.cardId
        WHERE  `flash_card_prompt_user_card`.userId =1
         */

        $this->db->select('
            flash_card_quiz_response_data.*
        ');

        $tids = array();
        foreach($ids as $row) {
            array_push($tids, $row->id);
        }

        if($tids)
        $this->db->where_in("quiz_id", array_values($tids));

        $query = $this->db->get("flash_card_quiz_response_data");

//        var_dump($this->db->last_query());
//        var_dump($query->result());
//        die();

        return $query->result();

    }

} 