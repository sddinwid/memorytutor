<?php
/**
 * Created by PhpStorm.
 * User: Scott
 * Date: 11/2/2014
 * Time: 8:44 PM
 */


class Categorymodel extends MY_Model{

    function __construct()
    {
        parent::__construct();
    }

    function testCategory() {
        return "Hi from Category";
    }

    function getAllCategories() {
        $allCategories = '';

        $query = $this->db->order_by('flash_card_categories.name')->get('flash_card_categories');


        // foreach ($query->result() as $row)
        // {
        //     $allCategories .= $row;
        // }
        return $query->result();
    }

    function getRootCategories() {
        $allCategories = '';

        $query = $this->db->where('categorization',"-1")->order_by('name')->get('flash_card_categories');


        // foreach ($query->result() as $row)
        // {
        //     $allCategories .= $row;
        // }
        return $query->result();
    }

    function getSubCategories() {
        $allCategories = '';

        $query = $this->db->where('categorization !=',"-1")->order_by('name')->get('flash_card_categories');


        // foreach ($query->result() as $row)
        // {
        //     $allCategories .= $row;
        // }
        return $query->result();
    }

    function getCategoriesById($where) {
        $this->db->select('*');
        
        if ($where) {
          $this->db->where_in('id',$where);
        }
        $this->db->from('flash_card_categories')->order_by('flash_card_categories.name');
        
        $query = $this->db->get();
        return $query->result();
        // return $where;//$this->db->last_query();
    }

    function insertNewCategory($data)
    {
        $this->db->set($data);
        $this->db->set('userId', $this->session->userdata['id']);
        $this->db->set('created_on', 'NOW()', false);
        $this->db->set('modified_on', 'NOW()', false);
        $this->db->insert('flash_card_categories');
        $this->db->limit(1);
        $new_id = $this->db->insert_id();

        return $new_id;

    }

    function updateCategory($data)
    {
        $data['active'] = 1;//($data['active']=='on')?1:0;
        $id = $data['id'];
        unset($data['id']);
        // var_dump($data);
        $this->db->set($data);
        $this->db->set('userId', $this->session->userdata['id']);
        $this->db->set('modified_on', 'NOW()', false);
        $this->db->where('id', $id);
        $this->db->limit(1);
        return $this->db->update('flash_card_categories');

    }

}

