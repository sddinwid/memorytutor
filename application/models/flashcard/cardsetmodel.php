<?php
/**
 * Created by PhpStorm.
 * User: Scott
 * Date: 11/2/2014
 * Time: 8:43 PM
 */

class Cardsetmodel extends CI_Model{

    function __construct()
    {
        parent::__construct();
    }

    function testCardset() {
        return "Ho cardset";
    }

    function getAllCardSets() {
        $allCardSets = '';

        $query = $this->db->order_by('flash_card_cardsets.name')->get('flash_card_cardsets');


        // foreach ($query->result() as $row)
        // {
        //     $allCardSets .= json_encode($row);
        // }
        return $query->result();
    }

    function getAllCardSetsAdmin() {
        $allCardSets = '';
        $this->db->select('flash_card_cardsets.id,
                flash_card_cardsets.name,
                flash_card_cardsets.description,
                flash_card_cardsets.categorization,
                flash_card_users.username,
                flash_card_cardsets.created_on,
                flash_card_cardsets.modified_on,
                flash_card_cardsets.active,
        		(SELECT COUNT(*) 
        			FROM `flash_card_cards` 
        			WHERE `flash_card_cards`.`cardset_id` = `flash_card_cardsets`.`id`) as count
            ');
        $this->db->join('flash_card_users', 'flash_card_users.id = flash_card_cardsets.userId');

        $query = $this->db->order_by('flash_card_cardsets.name')->get('flash_card_cardsets');


        // foreach ($query->result() as $row)
        // {
        //     $allCardSets .= json_encode($row);
        // }
        return $query->result();
    }

    function getMyCardSets() {
        if(isset($this->session->userdata['id'])) {

            return $this->getMyCardSetsById($this->session->userdata['id']);
        } else {
            return "No User signed in";
        }
    }

    function getMyCardSetsById($id) {
        if(isset($id)) {

            $allCardSets = '';

            $query = $this->db->order_by('flash_card_cardsets.name')->where('userId', $id)->get('flash_card_cardsets');

            return $query->result();
        } else {
            return "No ID passed";
        }
    }

    function getActiveCardSets() {
        $allCardSets = '';

        $query = $this->db->order_by('flash_card_cardsets.name')->where('active', 1)->get('flash_card_cardsets');

        return $query->result();
    }

    function getCardSetByID($id) {
        $allCardSets = '';

        $query = $this->db->order_by('flash_card_cardsets.name')->where('id', $id)->get('flash_card_cardsets');

        $result = $query->result();

        $cardquery = $this->db->order_by('flash_card_cards.name')->where('cardset_id', $id)->get('flash_card_cards');

        $cards = $cardquery->result();
        $result[0]->cards = $cards;

        return $result[0];
    }

    function getCardSetsById($where) {
        $this->db->select('*');
        
        if ($where) {
          $this->db->where_in('id',$where);
        }
        if (isset($this->session->userdata['secVal'])) {
            if($this->session->userdata['secVal']>='500') {

            } else {
                $this->db->where('userId', $this->session->userdata['id']);
            }
        } else {
            // $this->db->where('userId', '23169451513213546456');
        }
        $this->db->from('flash_card_cardsets')->order_by('flash_card_cardsets.name');
        
        $query = $this->db->get();
        return $query->result();
        // return $where;//$this->db->last_query();
    }

    function insertNewCardSet($data)
    {
        $this->db->set($data);
        $this->db->set('userId', $this->session->userdata['id']);
        $this->db->set('created_on', 'NOW()', false);
        $this->db->set('modified_on', 'NOW()', false);
        $this->db->insert('flash_card_cardsets');
        $this->db->limit(1);
        $new_id = $this->db->insert_id();

        return $new_id;

    }

    function updateCardSet($data)
    {
        $data['active'] = ($data['active']=='on')?1:0;
//        var_dump($data);die();
        $this->db->set($data);
        $this->db->where('id', $data['id']);
        $this->db->limit(1);
        return $this->db->update('flash_card_cardsets');

    }

} 