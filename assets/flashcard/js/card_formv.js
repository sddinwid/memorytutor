// JavaScript Document
function validateCardForm(formNode){
  var formValidatorInstance = new FormValidator(formNode);
  formValidatorInstance.addRule('question', 'Question is required.', 'required');
  formValidatorInstance.addRule('answer', 'Answer is required.', 'required');

  if(document.getElementById("addCardPrompt") && document.getElementById("addCardPrompt").value) {
    if(document.getElementById("wrong") && !document.getElementById("wrong").value &&
        document.getElementById("alarm") && !document.getElementById("alarm").value) {
      formValidatorInstance.addRule('alarmTime', 'You must select the number to alert on or the number of seconds before the prompt is shown.', 'oneorother', 'alarmWrong');
    }else if(document.getElementById("wrong") && document.getElementById("wrong").value &&
        document.getElementById("alarm") && document.getElementById("alarm").value) {
      formValidatorInstance.addRule('alarmTime', 'You cannot select both alarm on time and alarm wrong.', 'notboth', 'alarmWrong');
    }else if(document.getElementById("wrong") && document.getElementById("wrong").value) {
      formValidatorInstance.addRule('alarmWrong', 'Alarm wrong should be a positive integer of incorrect tries.', 'positiveinteger');
    }else if(document.getElementById("alarm") && document.getElementById("alarm").value) {
      formValidatorInstance.addRule('alarmTime', 'Alarm time should be a positive integer of seconds.', 'positiveinteger');
    }
  }

  return formValidatorInstance.validate();
}