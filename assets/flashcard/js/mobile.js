var currLogo = "";
var currLogoNum = 0;
var screenWidth = window.screen.width;
var screenHeight = window.screen.height;

var inactiveY = 3;

//$("html").css({
//    "height" : screenHeight,
//    "width" : screenWidth
//});



$("#popoutmenu").css({
    "top" : $("#topMenu").height()+"px"
});

$(document).ready(function() {
    /*

     vars

     */
    var cardIndex = 0;
    var cardsInSet = "";
    var currentCard = [];
    var currentCardHTML;
    var firstCard = [];
    var wrongCards = [];
    var quiz = {};
    var userAnswer = '';
    var drawOrder = [];
    var hintUsed = 0;
    var quizTimer = new cardTimer();
    var qtStarted = false;
    var speedBool = true;
    var speed = 1;
    var modifierKey = null;
    var lastKey = null;
    var prefsWidth = $('#configWin').width();
    var prefsHeight = $('#configWin').height();
    var allreadyWrong = null;
    var firstprompt = 0;
    var answered = 0;
    var linked = false;
    var returnedCardsHTML;
    var cardtimer = new cardTimer($('#scoresRow3 #displayTimer>span'));


    //$("div#info").text("H: "+screenHeight+" - W: "+screenWidth);

    if(typeof mycardsets != "undefined") {
    	// my cardsets is defined if a user is logged in
        if (mycardsets.length) {
        	// we have a cardset, build html to display it
            var html = "";

            html += "<div id='mycardsets' class='mycardsets'><div class='mcs_header'>My cardsets</div>";
            for (var i = 0; i < mycardsets.length; i++) {
                var theString = mycardsets[i].description;
                var desc = $('<div />').html(theString).text();

                html += "<div id='mcs_" + mycardsets[i].id + "' class='cardset'>";
                html += "<div id='cs_name' class='cs-name'><b>Name:</b>" + (mycardsets[i].name ? mycardsets[i].name : "(None)") + "</div>";
                html += "<div id='cs_desc' class='cs-desc'><b>Description:</b> " + desc + "</div>";
                html += "<div id='cs_created' class='cs-created'><b>Date Created:</b> " + (mycardsets[i].created_on ? new Date(mycardsets[i].created_on).toUTCString() : "(None)") + "</div>";
                html += "<div id='cs_modified' class='cs-modified'><b>Last Modified:</b> " + (mycardsets[i].modified_on ? new Date(mycardsets[i].modified_on).toUTCString() : '(None)') + "</div>";
                html += "</div>";

            }
            html += "</div>";

            $("div#home").append(html);
            $("div#menuHome").trigger("click");
        }
    }else {//no user so show login form and set cursor to username field
        $("div#login").show();
        $("input#username").focus();

    }

    $("#categorySelect").on("change", function(e) {

        var option = this.options[this.selectedIndex];

        categorize($("#categorySelect"), $("#cardSetCategorization"), option);
    });

    $(".mcs_header").on("click", function(e) {
        $("div.mycardsets").find("div.cardset").toggle();
    });

    $('#quizPage #selectCardset').on('change', function (e) {


        $('#quizPage #cardDeck').empty();


        var optSelected = $(this).find("option:selected");
        var selectCardset = $(optSelected).val();

        $.ajax({
            type: "POST",
            data: {"selectCardset": selectCardset},
            cache: false,
            url: "/flashcard/getCardsByCardset",
            success: function (data) {

                //window.location.href = "/flashcard/quizzes/" + $('#selectCardset').val();
            },
            dataType: 'json'
        }).done(function(data) {

            cardsInSet = data.data;

            if(cardsInSet.length > 0) {
                cardsInSet = shuffleDeck(cardsInSet);

                var cardCount = quiz['number_of_questions'] = cardsInSet.length;
                $("#totalcardnum").text(cardCount);
                quiz['cardset_id'] = $('#selectCardset').val();

                resetCardDeck();

                sizeForHint(cardsInSet[cardIndex]);

                $('span.welcomequiz').html("Click to Start quiz.");
                $('span#loadquiz').html("Quiz <br>\"" + $(optSelected).text() + "\"<br> loaded.");
                $("#quizStartDiv").bind("click", function (e) {
                    if($('span#loadquiz').text().trim()=="Quiz finished.") {
                        quizFinished();
                        return;
                    }
                    $("#deckBlind").hide();

                    setTimeout(function() {
                        cardtimer.start();
                        quizTimer.start(); },
                        500
                    );
                    //start quiz
                });

                $(".card .hint").each(function (e) {
                    $(this).hide()
                });
            }else {
                $('span.welcomequiz').html("");
                $('span#loadquiz').html("Quiz <br>\"" + $(optSelected).text() + "\"<br> currently has no cards.<br> Please try another cardset.");
            }
            console.log("ajax done");
        });
        //}

        var judge = {"cardset_ID":selectCardset};
//judge.push({"cardset_ID":incQuiz});

        /** how many this user can get wrong on this cardset **/
        $.ajax({
            type: "POST",
            url: "/flashcard/getWrongAJAX",
            data: judge,
            success: function (data) {

                console.log(data);

            },
            dataType: 'json'
        }).done(function(data) {
            var wrongcards = data.data;
            ids = new Array();
            for(var i = 0; i < wrongcards.length; i++) {
                //console.log(wrongcards[i].card_id);
                ids.push(wrongcards[i].card_id);
            }

            //console.log(ids);

            allreadyWrong = ids.reduce(function (acc, curr) {
                if (typeof acc[curr] == 'undefined') {
                    acc[curr] = 1;
                } else {
                    acc[curr] += 1;
                }

                return acc;
            }, {});
            //console.log(allreadyWrong);
        });


    });

    $("#showHint").on("click", function(e) {
        var effect = 'slide';

        // Set the options for the effect type chosen
        var options = {direction: "down"};

        // Set the duration (default: 400 milliseconds)
        var duration = 500;

        switch ($(this).text()) {
            case "Show Hint":
                if(!hintUsed) hintUsed = 1;
                $("#cardDeck > div:first-child > #hint").css('z-index', 1000).toggle(effect, options, duration);
                $(this).text("Hide Hint");
                break;
            case "Hide Hint":

                $("#cardDeck > div:first-child > #hint").css('z-index', 800).toggle(effect, options, duration);

                $(this).text("Show Hint");
                break;
        }
    });

    $("#cardDeck").on("click", ".front, .back", function(e) {

        if($(e.toElement).hasClass("front")) {
            //alert("front");
            $(e.toElement).parents(".panel").addClass("flip");
            //$(e.toElement).siblings(".back").show();
        }else {
            //alert("back");
            $(e.toElement).parents(".panel").removeClass("flip");
            //$(e.toElement).siblings(".front").show();
        }
    });

    $("div#mycardsets.mycardsets div[id^='mcs_']").on("click", function() {
        HandleForm(this, "edit");
    });

    $("div#popoutmenu").on("click", "div[id^='menu']", function(e) {
        //alert("Menu item clicked");

        var elmval;

        hideElements();

        $("#topcontainer").show();

        switch($(this).attr("id")) {
            case "menuMyCardsets":
            case "menuHome":
                elmval = "div#home";
                break;
            case "menuTakeQuiz":
                elmval = "div#quiz";
                $("#scoresDiv").hide();
                $("#topcontainer").hide();
                break;
            case "menuAddCardset":
                elmval = "div#addcardset";
                HandleForm(document.getElementById("addcardset"), "add");
                break;
            case "menuAddCard":
                elmval = "div#addcard";
                $('div#mbody').find(elmval).find("#dates.dates").hide();
                HandleForm(document.getElementById("addcard"), "add");
                break;
            case "menuAddCategory":
                elmval = "div#addcategory";
                HandleForm(document.getElementById("addcategory"), "add");
                break;
            case "menuLogout":
                window.location = "/fclogin/logout";
                break;
        }
        if(elmval)
            $('div#mbody').find(elmval).show();

        $(".menu-icon").trigger("click");

    });

    $("#scoresBtn").on("click", function(e) {
        var effect = 'slide';

        // Set the options for the effect type chosen
        var options = {direction: "up"};

        // Set the duration (default: 400 milliseconds)
        var duration = 500;

        $('#scoresDiv').toggle(effect, options, duration);
    });

    $(".menu-icon").on("click", function(e) {
        if(typeof mycardsets != "undefined") {
            var effect = 'slide';

            // Set the options for the effect type chosen
            var options = {direction: "left"};

            // Set the duration (default: 400 milliseconds)
            var duration = 500;

            $('#popoutmenu').toggle(effect, options, duration);
            //$("#popoutmenu").slideToggle({direction: 'left'});
        }
    });

    $('#cardRatingDiv>div>div').on('click', function (e) {


        if($('span#loadquiz').text().trim()=="Quiz finished.") {
            alert("Quiz finished.");
            return;
        }

        if($("#selectCardset option:selected").val() > 0 &&
            !($("#cardDeck").find("div.card").length)){
            alert("This cardset currently contains no cards.\nPlease select another.");
            return;
        }else if(!($("#cardDeck").find("div.card").length)) {
            alert("Must select a cardset first.");
            return;
        }else if($("#deckBlind").is(":visible")) {
            if($('span#loadquiz').text().trim()=="Quiz finished.") {
                alert("Quiz finished.");
                return;
            }
            alert("Tap card that says 'Click to Start' to begin the quiz.");
            return;
        }

        var which = $(this).text().toLowerCase().trim();

        switch (which) {
            case "correct":
            case "incorrect":

                $('#rightWrong>div').each(function () {
                    $(this).removeClass('active');
                });
                $(this).addClass('active');


                break;
            case "easy":
            case "challenging":
            case "hard":
            case "impossible":

                $('#ratings>div').each(function () {
                    $(this).removeClass('active');
                });
                $(this).addClass('active');

                break;
        }

        if ($('#ratings').find('.active').length != 0 &&
            $('#rightWrong').find('.active').length != 0) {
            cardtimer.stop();

            var selectedRightText = $('#rightWrong').find('.active').text();
            var selectedRatingText = $('#ratings').find('.active').text();

            var rightWrongElem = $('#scoresRow1').find('span:contains("' + (selectedRightText) + '")').next();
            rightWrongElem.text(parseInt(rightWrongElem.text()) + 1);

            var ratingElem = $('#scoresRow2').find('span:contains("' + (selectedRatingText) + '")').next();
            ratingElem.text(parseInt(ratingElem.text()) + 1);

            var numAnswered = $('#scoresRow3').find('#totalAnswered').find(':last-child');
            numAnswered.text(parseInt(numAnswered.text()) + 1);

            var percentValueElem = $('#scoresRow1').find('span:contains("Percent")').next();
            var currCorrect = $('#scoresRow1').find('span:contains("Correct")').next().text();

            var currAnswered = cardIndex + 1;

            var currPercent = currCorrect / currAnswered;

            currPercent = currPercent * 10000;
            currPercent = Math.round(currPercent);
            currPercent = currPercent / 100;
            percentValueElem.text(parseFloat(currPercent) + "%");

            var rwBool = 0;
            if ($('#rightWrong').find('.active').text().toLowerCase() == "correct") {
                rwBool = 1;
            }

            if(rwBool==0) {
                /** Add in here a prompt if user
                 *   would like to add a prompt for this card.
                 */

                //if($('#newHolder').parent().find('.card').length && (!linked)){
                //
                //    checkToCreatePrompt();
                //}

                wrongCards.push(currentCard);
            }

            //if($("div#promptWin").length==1) {
            //    $("div#promptWin").remove();
            //}

            /**
             * reset buttons to unclicked state.
             * We give it a delay so the user can register
             * the button change on the second button clicked.
             */
            setTimeout(function () {
                $("#cardRatingDiv").find(".active").each(function () {
                    $(this).removeClass('active hover')
                });
            }, 500);

            var quizdata = {
                    "rightWrong": (rwBool),
                    "rating": ($('#ratings').find('.active').text().toLowerCase()),
                    "timeToAnswer": (cardtimer.getTime()),
                    "correct": ($('#scoresRow1').find('span:contains("Correct")').next().text()),
                    "incorrect": ($('#scoresRow1').find('span:contains("Incorrect")').next().text()),
                    "percent": (percentValueElem.text()),
                    "hintUsed": (hintUsed),
                    "answer": (userAnswer),
                    "qsAnswered": (numAnswered.text())
            };

            console.log(quizdata);
            console.log(returnedCardsHTML);
            //
            /**
             *
             */
            storeQuiz(quizdata, cardIndex);

            $('span#loadquiz').html("Next card loaded.");
            $("#deckBlind").show();
            quizTimer.stop();


            if(currAnswered == cardsInSet.length) {
                $('span.welcomequiz').html("");
                $('span#loadquiz').html("Quiz finished.");
                quizFinished();

                //checkWrongCards(wrongCards);
                $("#deckBlind").show();
            }else {
                sizeForHint(cardsInSet[cardIndex]);

                var el = getNextCard();
                console.log(cardsInSet);
                $("#cardDeck").empty().append(el);
                hintUsed = 0;
            }

            cardtimer.reset();
        }
    });

    $("#newCardSubmitButton").click(function (e) {
        // To Do
        var form = $(e.toElement).parents("form")

        e.preventDefault();

        if(!(validateCardForm(document.getElementById("newCardForm"))))
        {
            return;
        }

        $.ajax({
            type: "POST",
            url: "/flashcard/addNewCard",
            data: $('#newCardForm').serialize(),
            success: function (data) {
                console.log(data);
                console.log(data.success);

                if(data.success) window.location.reload();

                alert("Error. Try again later.");
                window.location.reload();

            },
            dataType: 'json'
        });
    });

    $("#newCardsetSubmitButton").click(function (e) {
        // To Do
        var form = $(e.toElement).parents("form")

        e.preventDefault();

        if(!(validateLICategorization(document.getElementById("newCardSetForm"))))
        {
            return;
        }

        $(form).find("input[name='categorization']").val($('#cardSetCategorization li:last-child').data("cat"));

        $.ajax({
            type: "POST",
            url: "/flashcard/addNewCardSet",
            data: $('#newCardSetForm').serialize(),
            success: function (data) {
                 console.log(data.success);

                if(data.success) window.location.reload();

                alert("Error. Try again later.");
                window.location.reload();

            },
            dataType: 'json'
        });
    });

    $("#viewcardsbtn").on("click", function (e) {


        $("#cardscontainer").slideToggle( 3000 , function () {

        });
    });

    $("#editCardsetSubmitButton").click(function (e) {
        // To Do
        var form = $(e.toElement).parents("form")

        e.preventDefault();

        if(!(validateCardsetForm(form)))
        {
            return;
        }

        $(form).find("input[name='categorization']").val($(form).find('#cardSetCategorization li:last-child').attr("value"));


        $.ajax({
            type: "POST",
            url: "/flashcard/updateCardset",
            data: $('#editCardSetForm').serialize(),
            success: function (data) {
                console.log(data.success);

                if(data.success) {
                    window.location.reload();
                }else {
                    alert("Error. Try again later.");
                    window.location.reload();
                }
            },
            dataType: 'json'
        });
    });

    $("#editCardSubmitButton").click(function (e) {
        // To Do
        var form = $(e.toElement).parents("form")

        e.preventDefault();
        alert("Currently disabled");
        return;

        if(!(validateLICategorization(document.getElementById("newCardSetForm"))))
        {
            return;
        }

        $(form).find("input[name='categorization']").val($('#cardSetCategorization li:last-child').data("cat"));


        $.ajax({
            type: "POST",
            url: "/flashcard/addNewCardSet",
            data: $('#newCardSetForm').serialize(),
            success: function (data) {
                console.log(data.success);

                if(data.success) window.location.reload();

                alert("Error. Try again later.");
                window.location.reload();

            },
            dataType: 'json'
        });
    });

    $('#newCategorySubmitButton').on('click', function(e) {
        e.preventDefault();

        if(!(validateCategoryForm(document.getElementById("newCategoryForm"))))
            return;

        $.ajax({
            data: $('#newCategoryForm').serialize(),
            type: "POST",
            url: '/flashcard/addNewCategory',
            dataType: 'json',
            'success': function (resp) {

                console.log(resp);
                if(resp.success) window.location.reload();

                alert("Error. Try again later.");
                window.location.reload();
            },
            error: function(xhr, status, error) {
                console.log("(" + xhr.responseText + ")");

            }
        }).done(function(resp) {
            console.log(resp);
        });
    });


    /**
     * function to keep quiz data stored and up-to-date
     *
     * @param qData - quiz data for the current card
     */
    function storeQuiz(qData) {



        console.log(qData);

        cardsInSet[cardIndex]["response"] = qData;

    }
    /**
     * function to save quiz data to the database
     *
     * @param qData - quiz data for the current card
     */
    function saveQuiz() {
        var save = {};

        quiz['percent_correct'] = $("div#displayPercent > #text").text();
        quiz['quiz_length'] = $("div#totalAnswered > #text").text();
        quiz['quiz_length'] += (parseInt($("div#totalAnswered > #text").text()) > 1) ? " cards viewed" : " card viewed";

        save['quizdata'] = quiz;
        save['cards'] = cardsInSet;

        console.log(save);

        $.ajax({
            type: "POST",
            data: {"quiz": JSON.stringify(save)},
            url: "/quizzes/saveQuiz",
            success: function (data) {
                console.log(data);

                alert("Quiz has been saved.");
            },
            dataType: 'json'
        });

    }
    function getNextCard() {
        currentCardHTML = $(returnedCardsHTML).find("div[id^='card']").eq(++cardIndex);
        currentCard = cardsInSet[cardIndex];
        return currentCardHTML;
    }
    function quizFinished() {
        getQuizTime();
        alert("Quiz finished at "+quiz['end_time']+" on " +quiz['date_taken']);
        saveQuiz();

    }

    function resetCardDeck() {
        returnedCardsHTML = getCardDeckHTML(cardsInSet);
        currentCardHTML = firstCard = $(returnedCardsHTML).find("div[id^='card']:first-child");
        cardIndex = 0;
        currentCard = cardsInSet[cardIndex];
        $('#quizPage #cardDeck').empty().html(currentCardHTML);
    }

    function hideElements() {
        var elms = $('div#mbody > div').slice(2);
        console.log(elms);
        $(elms).each(function() {
            $(this).hide();
        });
    }
    
    function HandleForm(form, action) {
    	// incoming 'form' is the parent div of the actual 
    	// that also contains a div.header with the Add/Edit <Item> text.
    	
    	action = action.toLowerCase();

        console.log(action);


    	if(action=="add"){
            var myform = $(form).find("form");
            var modifying = (form.id).substring(3);
            // set header
            var text = action.charAt(0).toUpperCase() + action.slice(1) + " " + modifying.charAt(0).toUpperCase() + modifying.slice(1);;
            $(form).find("div.header").text(text);
            document.getElementById($(myform).attr("id")).reset();
    	}else if(action=="edit"){
            // form is incoming form (which is actually a div with a child form

            var csid;
            var url;
            var inctype;
            if($(form).hasClass("cardset")) {
                csid = $(form).attr("id").split("_");
                csid = csid[1];
                url = "/m/getCardsetById";
                inctype = "cardset";
            }else if($(form).hasClass("card")) {
                csid = $(form).attr("id").split("_");
                csid = csid[1];
                url = "/m/getCardById";
                inctype = "card";
            }else if($(form).hasClass("category")) {
                csid = $(form).attr("id").split("_");
                csid = csid[1];
                url = "/m/getCategoryById";
                inctype = "category";
            }else { return -1; }

            $.ajax({
                type: "POST",
                data: {"id": csid},
                url: url,
                success: function (data) {


                },
                dataType: 'json'
            }).done(function (data) {

                data = data.data;

                switch (inctype) {
                    case "cardset" :
                        console.log(data);
                        var cards = data.cards;
                        delete data.cards;
                        console.log(cards);
                        console.log(data);

                        for(var key in data){

                            var elem = $("#editCardSetForm [name='"+key+"']");

                            if(key=="active") {
                                console.log(data[key]);
                                $(elem).prop("checked", data[key] ? true : false);
                            }else if(key=="categorization") {
                                getSelectCategorization(data[key], 0, elem);
                            }else {
                                $(elem).val(data[key]);
                            }

                        }

                        if(cards.length && $("#cardscontainer").children().length == 0)
                        for(var card in cards) {
                            console.log(card);
                            var title;
                            if(cards[card].question) {
                                title = cards[card].question;
                            }else if(cards[card].name) {
                                title = cards[card].name;
                            }else if(cards[card].description) {
                                title = cards[card].description;
                            }else {
                                return;
                            }
                            if(title.length>0)
                            title = title.substring(0, 1).toUpperCase()+title.slice(1);
                            var div = document.createElement("div");//
                            var text = $("<div/>").html("<p>"+title+"</P>").text();//.substring(0, 1).toUpperCase()+title.slice(1);

                            $(div).attr("id", "card_"+cards[card].id).addClass("cardinset").text(text);

                            for(var key in cards[card]) {
                                var item = $("<input/>");
                                $(item).attr({"type":"hidden", "id": key, "value": cards[card][key]});
                                $(div).append(item);
                                console.log(item);
                            }
                            $("#cardscontainer").append(div);
                            $(div).on('click', function (e) {
                                var items = $(this).find("input[type='hidden']");
                                $(items).each(function (indx) {

                                    var elem = $("#editcard [name='"+$(this).attr("id")+"']");
                                    console.log($(elem));
                                    if($(elem).prop("nodeName")=="SELECT") {
                                        var option = $(elem).find("option[value='"+$(this).prop("value")+"']");
                                        $(option).prop("selected", true);
                                    }else if($(elem).prop("nodeName")=="TEXTAREA") {
                                        $(elem).text($("<div/>").html("<p>"+$(this).prop("value")+"</P>").text());
                                    }else if($(elem).is("input")) {
                                        $(elem).val($(this).prop("value"));
                                    }else {
                                        $(elem).text($(this).prop("value"));
                                    }
                                });
                                $(e.toElement).animate({
                                    'background-color':"#00ffff"
                                },100, function () {
                                    $(e.toElement).animate({
                                        'background-color':"#f0ffff"
                                    },100, function () {
                                        hideElements();
                                        $("#editcard").show();
                                    });
                                });

                            });

                        }
                        hideElements();
                        $('div#mbody').find("#editcardset").show();

                        break;
                    case "card" :

                        break;
                    case "category" :

                        break;
                }

            });
    		
    	}else {
    		// Error
    		return -1;
    	}      	
    }
    
    function getQuizTime() {

        stopQuizTimer();
        var d = new Date();
        var endTimeString = (d.getHours() < 10) ? "0" + d.getHours() : d.getHours();
        endTimeString += ":";
        endTimeString += (d.getMinutes() < 10) ? "0" + d.getMinutes() : d.getMinutes();
        endTimeString += ":";
        endTimeString += (d.getSeconds() < 10) ? "0" + d.getSeconds() : d.getSeconds();

        quiz['end_time'] = endTimeString;
        quiz['date_taken'] = d.getFullYear() + "-" + ((d.getMonth() < 9) ? "0" + (d.getMonth()+1) : d.getMonth()+1) + "-" + d.getDate();


    }

    function stopQuizTimer() {

        quizTimer.stop();

        var timerVal = quizTimer.getTime();
        console.log(timerVal);
        timerVal = timerVal.split(' ');
        timerVal = parseInt(timerVal[0]);
        var hours = 0;
        var mins = 0;
        var secs = 0;
        if (timerVal > 3600) {
            //hours
            hours = parseInt(timerVal / 3600);
            timerVal -= (hours * 3600);
            mins = parseInt(timerVal / 60);
            timerVal -= (mins * 60);
            secs = parseInt(timerVal);
        } else if (timerVal > 60) {
            //minutes
            // alert('mins');
            mins = parseInt(timerVal / 60);
            timerVal -= (mins * 60);
            secs = parseInt(timerVal);
        } else if (timerVal > 0) {
            // seconds
            // alert('secs');
            secs = parseInt(timerVal);
        }
        if (hours < 10) {
            hours = "0" + hours;
        }
        if (mins < 10) {
            mins = "0" + mins;
        }
        if (secs < 10) {
            secs = "0" + secs;
        }


        quiz['quiz_time'] = hours + ":" + mins + ":" + secs;

    }
    function checkWrongCards(wc) {

        var wrongCards = wc;
        console.log(wrongCards);
        console.log(wrongCards.length);
        //alert("wrong cards");
        //return;
        if(wrongCards.length>0) {
            var mesg;
            if(wrongCards.length>1) {
                mesg = "There are "+wrongCards.length+" cards that were graded as \"Incorrect\". Would you like to retest on those cards?";

            }else {
                mesg = "There was only one card that was graded as \"Incorrect\". Would you like to view that card?";
            }

            if(confirm(mesg)){


                returnedCardsHTML = "";
console.log(wrongCards);
                cardsInSet = wrongCards;
                wrongCards.length = 0;

                resetCardDeck();

                var cardCount = quiz['number_of_questions'] = cardsInSet.length;
                $("#totalcardnum").text(cardCount);
                $("#currcardnum").text("0");
                quiz['cardset_id'] = $('#selectCardset').val();

                var texts = $("div[id^='scoresRow'] > div[id^='display'] > span#text");

                $(texts).each(function(){
                    $(this).text("0");
                });

                if($('span#loadquiz').text().trim()=="Quiz finished.") {
                    $('span#loadquiz').text("");
                }
                $("#deckBlind").hide();

            }else {


            }
        }
    }

    function checkTimer(timer, trigger) {

        var timerVal = timer.getTime();
        var value = timerVal.split(" ");
        value = value[0];

        console.log(value);

        if(value < trigger) {
            console.log(allreadyWrong);
            console.log(trigger);
            console.log(linkage);
        }



        //console.log(timerVal);
        if(Math.floor(value)==trigger && !firstprompt) {
            console.log(firstprompt);
            console.log(linkage);
            console.log(currentCard.id);
            firstprompt = !firstprompt;
            for(var c in linkage) {
                console.log(linkage[c]);
                var card = linkage[c];
                if(currentCard.id==card.id){
                    /** **/
                    console.log("found");
                    console.log(card.prompt_text);
                    showPrompt(card, currentCard);
                }
            }

        }


    }

    function checkToCreatePrompt() {

        var qrybox = getMessageBox("Create Personal Cue?", "Would you like to create a Personal Cue?", ["Yes","No"]);
        $("body").append(qrybox);

        var buttons = $(qrybox).find("#queryButtons").children("button");
        var buttonClicked;

        buttons.each(function(indx, item){
            $(this).bind("click", function(){
                buttonClicked = $(this).attr("id");
                switch (buttonClicked) {
                    case "yes" :
                        showCreatePrompt();
                    case "no" :
                        break;
                }
                $(qrybox).remove();
                //return buttonClicked;
            });
        });

        $(qrybox).show();


        //return window.confirm("Would you like to create a prompt for this card?");

    }

    function showCreatePrompt() {

        if($("div.new-card-prompt-div").hasClass("hidden")) {
            $("div.new-card-prompt-div").removeClass("hidden")
        }
        $("div.new-card-prompt-div").toggle();
    }

    function showPrompt(linkage, card){
        console.log(linkage);
        var t = linkage.prompt_text?1:0;
        //alert("Show: "+t);

        var elem = $("<div></div>")
            .attr("id","promptWin")
            .addClass("prompt-window")
            .append("<div></div><div></div><div></div>")
            .appendTo("body");
        var divs = $("div#promptWin").find("div");
        $(divs).each(function(ind, val) {
            console.log(ind);
            console.log(val);
            var elvals = ["name","prompt-text","card"];
            $(val).attr("id",elvals[ind]);

            switch(ind){
                case 0:
                    break;
                case 1:
                    $(val).html(linkage.prompt_text);
                    break;
                case 2:
                    $(val).html("<input name='"+elvals[ind]+"' type='hidden' value='"+linkage.id+"' >");
                    break;
            }

        });

        var right = Math.floor((Math.random() * 2) + 1);
        var top = Math.floor((Math.random() * 2) + 1);
        var rpx = 0;
        var tpx = 0;
        console.log($(elem));
        //alert(right+":"+top);
        if(right==1) {
            rpx = Math.floor((Math.random() * 400) + 41);
            $("div#promptWin").css({"right":rpx+"px"});
            tpx = Math.floor((Math.random() * 225) + 201);
            //alert("right-1");
        }else {
            rpx = Math.floor((Math.random() * 300) + 41);
            $("div#promptWin").css({"left":rpx+"px"});
            tpx = Math.floor((Math.random() * 200) + 226);
            //alert("right-2");
        }

        if(top==1) {
            //alert("top-1");
            $("div#promptWin").css({"top":tpx+"px"});
        }else {
            //alert("top-2");
            $("div#promptWin").css({"bottom":tpx+"px"});
        }

        if(t)
            $(elem).show();
        else
            alert("default");
    }
});


function rotateLogos() {

    var src = "//memorytutor.scottdinwiddie.com/assets/flashcard/images/logos/";
    var picsLen = logos.length;
    var clone = null;
    //console.log(currLogoNum);

    if(currLogoNum==picsLen)
        currLogoNum = 0;

    currLogo = logos[currLogoNum++];

    $('img.logo-image').attr("src", src + currLogo);

    setTimeout(function(){rotateLogos();}, 10000);
}
rotateLogos();


$("#moveTop").on("click", function (e) {
    e.preventDefault();

    var clickedElem = e.toElement;
    var parentForm = $(clickedElem).parents("form");
    var ul = $(parentForm).find('#cardSetCategorization');
    var clickedIndex = $(ul).find('li.active').index();

    if(clickedIndex==0) {
        alert("Item is already at the top");
        return;
    }
    var moveElem = $(ul).children().eq(clickedIndex).detach();

    $(moveElem).insertBefore($(ul).children("li:first-child"));

    var newCode = getNewCatVal($(ul.children("li")));

    var newIndex = 0;

    getSelectCategorization(newCode,newIndex, e);


    // alert("newCode: "+newCode);
});
$("#moveUp").on("click", function (e) {
    e.preventDefault();
    var selIndex = $('#cardSetCategorization li.active').index();

    if(selIndex!=-1) {
        if(selIndex!=0){
            var clickedElem = e.toElement;
            var parentForm = $(clickedElem).parents("form");
            var ul = $(parentForm).find('#cardSetCategorization');
            var clickedIndex = $(ul).find('li.active').index();

            var moveElem = $(ul).children().eq(clickedIndex).detach();

            $(moveElem).insertBefore($(ul).children("li").eq(clickedIndex-1));

            var newCode = getNewCatVal($(ul.children("li")));

            var newIndex = selIndex - 1;

            getSelectCategorization(newCode,newIndex, e);

        } else { alert("Already Top item.");}
    } else { alert("Can't move something when nothing is selected...");}

    // console.log(selIndex);
    // console.log(catCode);
    // console.log(arr);
    // alert($('#cardSetCategorization option:selected').index());
});
$("#deleteCat").on("click", function (e) {
    e.preventDefault();

    var clickedElem = e.toElement;
    var parentForm = $(clickedElem).parents("form");
    var ul = $(parentForm).find('#cardSetCategorization');
    var clickedIndex = $(ul).find('li.active').index();

    var confirmDelete = confirm("Really delete this category from this Cardset?\n(Click Save Changes to make permanent.)");

    if (confirmDelete) {

        var moveElem = $(ul).children().eq(clickedIndex).remove();

        var newCode = getNewCatVal($(ul.children("li")));

        var newIndex = clickedIndex;

        getSelectCategorization(newCode,newIndex, e);

    }



// console.log(clickedSplit.pop());
    // alert("clickedVal: "+clickedVal+" - newCode: "+newCode+" - currCats: "+currCats);

});
$("#moveDown").on("click", function (e) {
    e.preventDefault();
    var selIndex = $('#cardSetCategorization').find("li.active").index();
    var lastIndex = $('#cardSetCategorization').children('li').length;
    var clkElem = e.toElement;
    var form = $(clkElem).parents("form");
 console.log(e);
    if(selIndex!=-1) {
        if(selIndex!=(lastIndex-1)){

            var clickedElem = e.toElement;
            var parentForm = $(clickedElem).parents("form");
            var ul = $(parentForm).find('#cardSetCategorization');
            var clickedIndex = $(ul).find('li.active').index();

            var moveElem = $(ul).children().eq(clickedIndex).detach();

            $(moveElem).insertAfter($(ul).children("li").eq(clickedIndex));

            var newCode = getNewCatVal($(ul.children("li")));

            var newIndex = selIndex + 1;

            getSelectCategorization(newCode,newIndex, e);

        } else { alert("Already Bottom item.");}
    } else { alert("Can't move something when nothing is selected...");}

    // console.log(selIndex);
    // console.log(catCode);

    // alert($('#cardSetCategorization option:selected').index());
});
$("#moveBottom").on("click", function (e) {
    e.preventDefault();

    var clickedElem = e.toElement;
    var parentForm = $(clickedElem).parents("form");
    var ul = $(parentForm).find('#cardSetCategorization');
    var clickedIndex = $(ul).find('li.active').index();

    if(clickedIndex==$(ul).children("li").length) {
        alert("Item is already at the botton");
        return;
    }
    var moveElem = $(ul).children().eq(clickedIndex).detach();

    $(moveElem).insertAfter($(ul).children("li:last-child"));

    var newCode = getNewCatVal($(ul.children("li")));

    var currCats = newCode.split('-');
    var newIndex = currCats.length - 1;

    getSelectCategorization(newCode,newIndex, e);


    // alert("newCode: "+newCode);
});

function getMessageBox(title, message, buttons) {
    title = title || null;
    message = message || null;
    buttons = (buttons instanceof Array)? buttons : null;

    if(!(buttons) || !(title) || !(message))
        return;

    var elem = document.createElement("div");
    var querybox = document.createElement("div");

    $(elem).attr("id","queryShader").addClass("query-shader").append(querybox);
    $(querybox).addClass("query-box");
    var xhtml = '\
        <div id="title" class="query-title" >'+title+'</div>\
        <div id="query" class="query-body">'+message+'</div>\
        <div id="queryButtons" class="query-buttons">';

    for(var i = 0; i < buttons.length; i++){
        xhtml += '\
            <button id="'+buttons[i].toLowerCase()+'" class="query-button">'+capitalize(buttons[i])+'</button>\
        ';
    }

    xhtml += '</div> \
        <div id="queryFooter" class="query-footer"></div>\
        ';
    $(querybox).html(xhtml);

    return elem;
}

function shuffleQuotes(myquotes) {


    var numberOfQuotes = myquotes.length;

    var n = Math.floor((numberOfQuotes*2) * Math.random());

    for (var i = 1; i < n; i++) {
        var quote1 = Math.floor(numberOfQuotes * Math.random());
        var quote2 = Math.floor(numberOfQuotes * Math.random());
        var temp = myquotes[quote2];
        myquotes[quote2] = myquotes[quote1];
        myquotes[quote1] = temp;
    }
    return myquotes;
}

function getCardDeckHTML(carddeck) {
    var built = "<div id='container'>";
    var deck = carddeck;

    $.each(deck, function (i, val) {

        built += '<div id="card_' + val.id + '" name="' + val.id + '" class="panel card shuffled">' +

            '<div id="hint" class="hint" style="">' +
            '<div class="pad"><p>' +
            val.hint +
            '</p></div></div>' +
            '<div id="front" class="front"><div class="pad"><p>' +
            val.question +
            '</p></div></div>' +
            '<div id="back" class="back" style="">' +
            '<div class="pad"><p>' +
            val.answer +
            '</p></div></div>' +
            '</div>';
    });
    built += "</div>";
    return built;
}

function sizeForHint(inccard) {
    console.log("sizing");
    var card = inccard;
    if (card && card.hint) {
        $("#showHint").show();
        $("div#cardDeck").height("700px");
    } else {
        $("#showHint").hide();
        $("div#cardDeck").height("800px");
    }
}

function arrUnique(array) {
    return array.filter(function (a, b, c) {
        // keeps first occurrence
        return c.indexOf(a) === b;
    });
}

function categorize(element, addto, item) {
    /*
     get category info (id, text, etc)*/
    var container = $(element).parent();
    var addToElem = addto;
    var header = $(element).parent().find("label[for='"+$(element).attr("id")+"']");
    var headerText = $(header).text();
    var itemtext = $(item).text();
    var id = $(element).val();

    if(headerText.indexOf("Main") > -1) {
        // alert("text is main");

        // add text to categorization select...
        // get category id over with text
        var html = '<li id="category_'+id+'" data-cat="'+id+'">'+itemtext+'</li>';

        $(addToElem).append(html);

        // change "Main Category" to "Sub-Category"
        $(header).text(headerText.replace("Main", "Sub"));

    } else if(headerText.indexOf("Sub") > -1) {

        var dashes = '';
        var newOptionText = '';
        var subId = '';
        var options = $(addToElem).children("li");
        var howMany = $(options).length;

        console.log(options);
        console.log($(addToElem).find("li:last-child").data("cat"));
        //console.log(($(addToElem).find("li").eq(howMany-1).split("_"))[1]);
        dashes = " ";
        for (var i = 0; i < howMany; i++) {
            dashes += "-";
        };
        dashes +=  " ";

        //console.log(dashes);

        /*
         add dash to
         next line before category name
         current category value
         add selected category id to current category value
         add text to categorization select... */
        var html = '<li id="category_'+id+'" data-cat="'+$(addToElem).find("li").eq(howMany-1).data("cat")+'-'+id+'">'+dashes+itemtext+'</li>';
        $(addToElem).append(html);

        // console.log(selectSize);
    } else if(headerText == "Add Category") {

        //console.log($(addToSelect).selected);
        /*
         If nothing selected, add to end of current list

         If something selected, ask if above or below selected.
         */

    }
    $("li#category_"+id).bind("click", function(e) {
        $('#cardSetCategorization').children('li').each(function() {
            $(this).removeClass("active");
        });
        $(this).addClass("active");
    });
}

function getSelectCategorization(categorizationCode, newSelIndex, e) {
    //console.log(e);
    var clickedElem = e.toElement ? e.toElement : e;
    var parentForm = $(clickedElem).parents("form");

    //console.log(parentForm);


    var elem = $(parentForm).find('ul#cardSetCategorization');
    //console.log(elem);
    $(elem).css({ "background-color" : "", "border-color" : ""});

    $.ajax({
        type: "POST",
        url: "/flashcard/getCategoriesById",
        data: {categorization: categorizationCode},
        success: function (data) {

            var returnedData = data.data;
            // console.log(returnedData);
            var selectOptions = "";
            var count = 0;
            var dashes = "";
            var idMod = "";
            var oldId = "";

            $(elem).empty();
            $.each(returnedData, function () {
                idMod = this.id;

                // for (var i = 0; i < count; i++) {
                if(count>0){
                    dashes += "-";
                    idMod = oldId + "-" + this.id;
                }
                // };


                $(elem).append('<li id="category_'+ this.id +
                    '" class="categorization-options" value="'+ idMod +'">'+ dashes +
                    this.name + '</li>');
                count++;
                oldId = idMod;

            });
            $(elem).find("li[id^='category_']").each(function() {
                $(this).bind("click", function(e) {
                    $(elem).children('li').each(function() {
                        $(this).removeClass("active");
                    });
                    $(this).addClass("active");
                })
            });

            $(parentForm).find('ul#cardSetCategorization').find("li").eq(newSelIndex).addClass('active', true);


        },
        dataType: 'json'
    });
}

function getNewCatVal(elem) {
    var catval = "";

    $(elem).each(function() {
        var atid = ($(this).attr("id").split("_"))[1];

        if(atid)
        catval += atid + "-";
    });

    catval = catval.substr(0, catval.length-1);

    return catval;
}

function validateLICategorization(Element) {
    var form;
    var ul;

    if($(Element).is("form")) {
        form = Element;
        ul = $(form).find("ul#cardSetCategorization");
    }else if($(Element).is("ul")) {
        ul = Element;
        form = $(ul).parents("form");
    }else {
        return false;
    }

    var csformvalid = validateCardsetForm(form);

    var livalid = $(ul).children("li").length? true : false;
    if(!(livalid)) {
        alert("Must select at least one category for a cardset presently.");
        $(ul).css('border', "5px solid #FF9933");
        //ul.style.borderColor = "#F5EDB4";
    }
    var fin;
    fin = (csformvalid==false || livalid==false)?false: true;
console.log(fin);
    return fin;//fin;
}

function capitalize(str) {
    strVal = '';
    str = str.split(' ');
    for (var chr = 0; chr < str.length; chr++) {
        strVal += str[chr].substring(0, 1).toUpperCase() + str[chr].substring(1, str[chr].length) + ' '
    }
    return strVal
}

function shuffleDeck(cardDeck) {

    var n = Math.floor(400 * Math.random() + 500);
    var numberOfCards = cardDeck.length;

    for (i = 1; i < n; i++) {
        var card1 = Math.floor(numberOfCards * Math.random());
        var card2 = Math.floor(numberOfCards * Math.random());
        var temp = cardDeck[card2];
        cardDeck[card2] = cardDeck[card1];
        cardDeck[card1] = temp;
    }
    return cardDeck;
}

function drawDeck() {

    var ZIdx = $('.carddeck > div').length;

    $.each($('.carddeck > div'), function (k, v) {

        $(this).css("z-index", (ZIdx));

        ZIdx -= 1;

    });
}

function cardTimer(elem, options, buttons) {

    buttons = buttons || 0;

    var offset,
        clock,
        interval;

    if (!(elem === undefined)) {
        var timer = createTimer();
        if (buttons) {
            var startButton = createButton("start", start),
                stopButton = createButton("stop", stop),
                resetButton = createButton("reset", reset);
        }
    }
    // default options
    options = options || {};
    options.delay = options.delay || 1;

    // append elements
    if (!(elem === undefined)) {
        elem.append(timer);
        if (buttons) {
            elem.append(startButton);
            elem.append(stopButton);
            elem.append(resetButton);
        }
    }

    // initialize
    reset();

    // private functions
    function createTimer() {
        return document.createElement("span");
    }

    function createButton(action, handler) {
        var a = document.createElement("a");
        a.href = "#" + action;
        a.innerHTML = action;
        a.addEventListener("click", function (event) {
            handler();
            event.preventDefault();
        });
        return a;
    }

    function getTime() {
        return clock / 1000 + " secs";
    }

    function start() {
        if (!interval) {
            offset = Date.now();
            interval = setInterval(update, options.delay);
        }
    }

    function stop() {
        //console.log(clock / 1000);

        if (interval) {
            clearInterval(interval);
            interval = null;
        }
    }

    function reset() {
        clock = 0;
        if (!(elem === undefined)) render(0);
    }

    function update() {
        clock += delta();
        if (!(elem === undefined)) render();
    }

    function render() {
        timer.innerHTML = clock / 1000 + " secs";
    }

    function delta() {
        var now = Date.now(),
            d = now - offset;

        offset = now;
        return d;
    }

    // public API
    this.start = start;
    this.stop = stop;
    this.reset = reset;
    this.getTime = getTime;
}