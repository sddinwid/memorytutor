function mixin(c, p) {
    for(k in p) if(p.hasOwnProperty(k)) c[k] = p[k];
}

function bind(o, f) {
    return function() { return f.apply(o, arguments); };
}

function inherits(c, p) {
    mixin(c, p);
    function f() { this.constructor = c; };
    f.prototype = c._super = p.prototype;
    c.prototype = new f();
}
function FCSystemModel () {
    var self = this();
    self.id = 0;
    self.name = '';
    self.description = '';
    self.categorization = '';
}
function Category () {
    inherits(Category, FCSystemModel);
}
function CardSet () {
    inherits(CardSet, FCSystemModel);
    var self = this();
    self.cardCount = 0;
    self.cards = [];
    self.lastUsed = '';
    self.addCard = function () {
        // AddCard function
    }
    self.removeCard = function () {
        
    }
    self.shuffle = function () {
        
    }
}
