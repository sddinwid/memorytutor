$(document).ready(function () {

    

    $("#categoryButton").click(function () {
        if ($("#settingsMenu").is(':visible')) {
            $("#settingsMenu").hide();
        }
        if ($("#categoryMenu").is(':visible')) {
            $("#categoryMenu").hide();
        } else {

            $("#categoryMenu").show();
        }
        //alert($("#cardTable").position().top);
    });

    $("#addCategory").click( function () {
        //if ($("#changingTable").is(':visible')) {
            //$("#changingTable").hide();
        //} else {
        $("#newCategoryForm>div:last-child").each(function() {
            $(this).val("");// = "";
        });
            $("#changingTable").show();
            $("#categoryMenu").hide();
        //}
    });

    $("#settingsButton").click(function () {
        if ($("#categoryMenu").is(':visible')) {
            $("#categoryMenu").hide();
        }
        if ($("#settingsMenu").is(':visible')) {
            $("#settingsMenu").hide();
        } else {
            $("#settingsMenu").show();
        }
    });

    
    $("#hintButton").click(function () {
        if ($("#hintArea").is(':visible')) {
            $("#hintArea").slideUp();
        } else {
            $("#hintArea").slideDown("2000");
        }
    });

    $("#categoryAddCard").click(function () {

    });

    $('#categorySelect option').on('click', function () {
        // body...
        // alert($(this).text());// works, same with $(this).val() to get categoryId

        // alert($(this).parent().prev().first().children('span').text());//works
        // console.log($(this).parent().parent().children().eq(1));

        //   So what now...
        categorize($(this));


    });

    $('#categoryViewCardSet').on('click', function (e) {
        window.location="viewcardsets";

        // $('#cardsInSetTable').DataTable({paging: false,
    // scrollY: 250});
    });


    $("#viewCardsetCloseButton").click(function (e) {

        $('#cardsInSetTable').DataTable().destroy();

        $('#cardsInSetTable tbody').empty();

    });

    $("#viewCardsetSubmitButton").click(function (e) {

        e.preventDefault();

    });

    $('#categoryLoadCardSet').on('click', function(e) {

        // var clone = $('#categoryListing').find(':last-child').clone();
        // clone.attr('id',"42").text("the answer to all");
        // $('#categoryListing').append(clone);
    });
    

    $('#categoryNewCardSet').on('click', function(e) {

        // var clone = $('#categoryListing').find(':last-child').clone();
        // clone.attr('id',"42").text("the answer to all");
        // $('#categoryListing').append(clone);newCardSetWindow
    });

    $('#newCategoryFormSubmit').on('click', function(e) {
        e.preventDefault();

        

        $.ajax({
            data: $('#newCategoryForm').serialize(),
            type: "POST",
            url: '/flashcard/addNewCategory',
            dataType: 'json',
            'success': function (resp) {

                
                var clone = $('#categoryListing').find(':last-child').clone();
                clone.attr('id',resp.data.id).text(resp.data.name);
                $('#categoryListing').append(clone);
                $('#categorySelect').append('<option id="category_'+resp.data.id+'" class="activeHover" value="'+resp.data.id+'" >'+resp.data.name+'</option>')
                                    .find(':last-child').bind('click', function() {
                                        //alert("clicked on "+$(this).text());
                                        console.log($(this));
                                        categorize($(this));
                                    });
                $('#userResponseArea').append('<li id="'+ resp.data.id +'" class="activeHover">' + resp.data.name + '</li>');
                $('#newCategoryFormQuit').trigger('click');
console.log($('#userResponseArea').find(':last-child'));
            },
            error: function(xhr, status, error) {
                console.log("(" + xhr.responseText + ")");

            }
        });
    });

    $("#newCardSubmitButton").click(function (e) {
        // To Do
        e.preventDefault();
        // var form = $('#newCardForm').serialize();
        $('#categorization').val($('#categorization option:last-child').val());
        console.log($('#categorization').val());
        
        $.ajax({
            type: "POST",
            url: "addNewCard",
            data: $('#newCardForm').serialize(),
            success: function (data) {
                // console.log(data);
                var display = '';
                for (var key in data.data) {
                    display += key+"-"+data.data[key]+"\n";
                }
                alert(display);
            },
            dataType: 'json'
        });
    });

    $("#newCardsetSubmitButton").click(function (e) {
        // To Do
        e.preventDefault();
        // var form = $('#newCardForm').serialize();
        $('#cardSetCategorization').val($('#cardSetCategorization option:last-child').val());
        console.log($('#categorization').val());
        
        $.ajax({
            type: "POST",
            url: "addNewCardSet",
            data: $('#newCardSetForm').serialize(),
            success: function (data) {
                // console.log(data);
                var display = '';
                for (var key in data.data) {
                    display += key+"-"+data.data[key]+"\n";
                }
                alert(display);
                window.location.reload();

            },
            dataType: 'json'
        });
    });


    $('#dumpButton').on('click', function (e) {
        e.preventDefault();
        // $('#cardSetCategorization').val($('#cardSetCategorization option:last-child').val());

        $.ajax({
            type: "POST",
            url: "getCategoriesById",
            data: $('#cardSetCategorization').val($('#cardSetCategorization option:last-child').val()),
            success: function (data) {
                console.log(data);
                var display = '';
                for (var key in data.data) {
                    for (var key2 in data.data[key]) {
                        display += key2+"-"+data.data[key][key2]+"\n";
                    }
                }
                alert(display);
                // window.location.reload();

            },
            dataType: 'json'
        });
    });

    $("#categoryMenu").hide()
        .css({ "top": ($("#categoryButtonDiv").position().top + $("#categoryButtonDiv").height()) + "px" })
        .css({ "left": ($("#categoryButtonDiv").position().left) + "px" })
        .menu();

    $("#settingsMenu").hide()
        .css({ "top": ($("#settingsButtonDiv").position().top + $("#settingsButtonDiv").height()) + "px" })
        .css({ "left": (($("#settingsButtonDiv").position().left + $("#settingsButtonDiv").width()) - $("#settingsMenu").width() - 2)
                 + "px"
        })
        .menu();

    
    tinymce.init({
        selector: 'textarea#cardSetDescription',
        width: 300,
        height: 120,
        menubar: false,
        statusbar: false,
        setup: function (editor) {
            editor.on('change', function () {
                tinymce.triggerSave();
            });
        }
    });

    tinymce.init({
        selector: 'textarea',
        width: 400,
        height: 180,
        menubar: false,
        statusbar: false,
        setup: function (editor) {
            editor.on('change', function () {
                tinymce.triggerSave();
            });
        }
    });


    
});

    function categorize(element) {
/*
        get category info (id, text, etc)*/
        var container = $(element).parent().parent();
        var addToSelect = $(element).parent().parent().children().eq(1);
        var headerText = $(container).children().eq(2).children().text();
        var text = $(element).text();
        var id = $(element).val();
        console.log(headerText);

        if(headerText == "Main Category") {
            // alert("text is main");

            // add text to categorization select...
            // get category id over with text                
            $(addToSelect).append('<option id="category_'+id+'" value="'+id+'">'+text+'</option>');
            
            // change "Main Category" to "Sub-Category"
            $(element).parent().prev().first().children('span').text("Sub-Category");

        } else if(headerText == "Sub-Category") {

            var dashes = '';
            var newOptionText = '';
            var subId = '';
            var options = $(addToSelect).children();
            var howMany = $(options).length;

            dashes = " ";
            for (var i = 0; i < howMany; i++) {
                dashes += "-";
            };
            dashes +=  " ";

        /*
            add dash to
                next line before category name
                current category value
            add selected category id to current category value
            add text to categorization select... */
            $(addToSelect).append('<option id="category_'+id+'" value="'+$(options[(howMany-1)]).val()+'-'+id+'">'+dashes+text+'</option>');

            // console.log(selectSize);
        }
    }

    