$(document).ready(function() {
	

	var currLogo = "";
	var currLogoNum = 0;
    var quotes = {};
    var scrw = window.screen.width;

    $(".activeHover").hover(
        function () {
            $(this).addClass("ui-state-hover").css({"cursor":"pointer"});
        },
        function () {
            $(this).removeClass("ui-state-hover").css({"cursor":""});
        }
    );

    function getQuotes() {
        //setup vars


        $.ajax({
            url: "/flashcard/getQuotes/",
            dataType: "json",
            asynch: false,
            method: "GET",
            success: function(ax_rs) {
                quotes = ax_rs.data;

            },
            error: function(data, xhr, error) {

            }
        }).done(function(resp){
            // Put quote in DIV element
            quotes = shuffleQuotes(quotes);

            loadQuote();
            // animate DIV to show - showQuote()
        });
    }

    function showQuote(elem) {
     /*****\\**//****/
        var time = 3; // set this variable in seconds to change time

        //fade in quote from right to stop in center of screen

        var center = (screen.width / 2) - ($(elem).parent().width() / 2);
        var elemStart = $(elem).position().left;
        var dist = elemStart - center;
        var speed = time * 1000;
        var banners = $(elem).parent().children()
        var halfparent = $(elem).parent().width() / 2;

        $(banners).each(function(k,v){

            if($(v).children('div').attr("id")===$(elem).children('div').attr("id")){
                //alert($(this).left);//$(this).width()
                $(this)
                    .css('visibility', 'visible')
                    .animate({opacity: 1.0, left: '0px'}, speed, function (){
                        if($(this).position.left < 0) {

                        }
                    });
            } else {
                //alert($(this).position().left);
                $(this)
                    .animate({opacity: 0.0, left: '-500px'}, speed/4, function (){
                        if($(this).position().left < 0) {
                            $(this).remove();
                        }
                    });
            }

        });

    }

    function loadQuote() {
        if(!quotes.length) {
            getQuotes();

        } else {
            var qcnt = quotes.length;

            var newquoteelem = createQuoteBanner();
            showQuote(newquoteelem);
            setTimeout(loadQuote, 9000);
        }
    }

    var file, n;

    file = window.location.pathname;
    file = file.split("/");
    page = null;
    if (file[2]) {
        page = file[2];
    }

    if(page=="index" || page==null)
    loadQuote();

    function createQuoteBanner(){
        var currQuotes = quotes;
        var lastquoteid = $('#quotesDiv > #quotes:last-child > .quote-container > div').attr("id");

        var dupe = "";
        var current = currQuotes.pop();
        if($(current).attr("id")==lastquoteid){
            dupe = current;
            current = currQuotes.pop();
            currQuotes.push(dupe);
        }

        var ltrBreak = 120;

        if(current.quote_text.length > ltrBreak) {
            var txt = current.quote_text;
            var rex = /(<([^>]+)>)/ig;
            //alert(txt.replace(rex , ""));
            var s = txt.replace(rex , "");
            var cnt = parseInt(s.length / ltrBreak);


            for(var i = 0; i < cnt; i++) {

                var middle = ltrBreak * (i+1);
                var before = s.lastIndexOf(' ', middle);
                var after = s.indexOf(' ', middle + 1);

                if (before == -1 || (after != -1 && middle - before >= after - middle)) {
                    middle = after;
                } else {
                    middle = before;
                }

                var s1 = s.substr(0, middle);
                var s2 = s.substr(middle + 1);
                console.log(s1);
                console.log(s2);

                s = s1 + "<br />" + s2;

            }
            current.quote_text = s;
            console.log(current.quote_text);

        }

        var d = new Date(current.quote_date);
        var newQuoteBanner = document.createElement('div');
        $(newQuoteBanner).attr("id","quote_container").addClass("quote-container").css({"left":(screen.width)+"px"});
        var html = "<div id='"+current.id+"' class='quote-info'>";
        html += "<div id='text' class='text'><span class='quote-mark fa fa-quote-left fa-2x'></span><span class='quote-text'>"+current.quote_text+"</span><span class='quote-mark fa fa-quote-right fa-2x'></span></div>";
        if(current.quote_credit!="")
        html += "<div id='credit' class='credit'>"+current.quote_credit+"</div>";
        if(current.quote_date!=null)
            html += "<div id='date' class='credit'> ~ "+ d.getFullYear()+"</div>";
        html += "</div>";
        $(newQuoteBanner).append(html);
        $(newQuoteBanner).appendTo($('#quotesDiv > #quotes'));

        return newQuoteBanner;
    }
    function rotateLogos() {
    	
    	var src = "//memorytutor.scottdinwiddie.com/assets/flashcard/images/logos/";
    	var picsLen = logos.length;
    	var clone = null;
    	//console.log(currLogoNum);
    	
    	if(currLogoNum==picsLen)
    		currLogoNum = 0;
    	
    	currLogo = logos[currLogoNum++];
    	
    	$('img.logo-image').attr("src", src + currLogo);
    	    	
    	setTimeout(function(){rotateLogos();}, 10000);
    }
    rotateLogos();

	$('#viewMyCardsetsButtonDiv').on('click', function (e) {
        window.location="/flashcard/viewcardsets";

    });

    $('#logoutLink').click(function () {
        window.location="/fclogin/logout";
    });

    $('#quizzesButtonDiv').on('click', function (e) {
        window.location="/flashcard/quizzes";

    });

    $('.cogbutton, .cogbutton > div, .cogbutton > div > span').on("mouseenter", function(e) {
        e.stopPropagation();
        $(".cogbutton").addClass("home-active");
    }).on("mouseout", function(e) {
        e.stopPropagation();
        $(".cogbutton").removeClass("home-active");
    });

    $('#categorySelect option').on('click', function () {

        categorize($(this));

    });

    $("#categoryMenu>#quiz>#categoryListing>li").on("click" ,".cardset", function (e) {
        // alert("name: "+$(this).text()+"\nid: "+$(this).attr("id"));
        var setName = $(this).html();
        var cardset = $(this).attr("data-cardsetid");
        window.location="/flashcard/quizzes/"+cardset;
    });

    $("#homeButton")
	    .on("click", function () {
	        if ($("#settingsMenu").is(':visible')) {
	            $("#settingsMenu").hide();
	        }
	        if ($("#categoryMenu").is(':visible')) {
	            $("#categoryMenu").hide();
	        } else {

	            $("#categoryMenu").show();
	        }
	        //alert($("#cardTable").position().top);
    	})
    	.on("focusout", function () {
    		if ($("#categoryMenu").is(':visible')) {
            	$("#categoryMenu").hide();
        	}
    	});

    $("#settingsButton")
    	.on("click", function () {

	        if ($("#categoryMenuDiv").is(':visible')) {
	            $("#categoryMenu").hide();
	        }
	        if ($("#settingsMenuDiv").is(':visible')) {
	            $("#settingsMenuDiv").hide();
	        } else {
                $("#settingsMenu").css({"display":"block"});
	            $("#settingsMenuDiv").css({
                    "display":"block",
                    "left":0,
                    "right": $("#settingsButton").css("right") + "px",
                    "top": ($("#settingsButton").css("top") + $("#settingsButton").height()) + "px"
                }).show();
	        }
	    })
	    .on("focusout", function () {
    		if ($("#settingsMenu").is(':visible')) {
            	$("#settingsMenu").hide();
        	}
    	});

	$("#categoryMenu").hide()
        .css({ "top": ($("#homeButtonDiv").position().top + $("#homeButtonDiv").height()) + "px" })
        .css({ "left": ($("#homeButtonDiv").position().left) + "px" })
        .menu();

    if($("#settingsButtonDiv").length)
    $("#settingsMenu").hide()
        .css({ "top": ($("#settingsButtonDiv").position().top + $("#settingsButtonDiv").height()) + "px" })
        .css({ "left": (($("#settingsButtonDiv").position().left + $("#settingsButtonDiv").outerWidth()) - $("#settingsMenu").outerWidth() - 2)
                 + "px"
        })
        .menu({
                focus: function (event, ui) {
                	// console.log(event);
                	// console.log(ui);
                	$("#settingsMenu").menu("option", "position", { of: $(ui.item.parent()), my: "right top", at: "left top" });
                }
            });
    thisurl = window.location.pathname;
    if (thisurl.indexOf("viewcardsets")>=0) {
        $(".nav-bottom").css({"background-color":"mediumpurple"});
        $("#headerBar").css({"background-color":"mediumpurple"});
        $("body").css({"background-color":"rgb(240, 240, 240)"});
        $("#viewMyCardsetsButtonDiv>.mtbutton").addClass("nav-active");
        $('#homeButton').unbind("click");
        $("#homeButtonDiv").bind("click", function() {
                window.location="/";
        });
    } else if(thisurl.indexOf("quizzes")>=0) {
        $(".nav-bottom").css({"background-color":"rgb(255,100,100)"});
        $("#headerBar").addClass("quiz-back");
        $("body").css({"background-color":"rgb(240, 240, 240)"});
        $("#quizzesButton").addClass("quizzes-active");
        $('#homeButton').unbind("click");
        $("#homeButtonDiv").bind("click", function () {
            window.location = "/";
        });
    } else if(thisurl=="/") {
        $("#homeButton").addClass("nav-active").css({"background-color":"midnightblue","color":"white"});

        $("#headerBar").css({"background-color":"midnightblue"});
        $("body").css({"background-color":"rgb(220,220,255)"});
    }
});

$("#moveTop").on("click", function (e) {
    e.preventDefault();

    var clickedVal = $('#cardSetCategorization').val();
    // console.log($(this).children('option:last-child').val());
    var catVal = $('#cardSetCategorization').children('option:last-child').val();

    var catToMove = clickedVal.split("-").pop();

    var currCats = catVal.split('-');

    var index = currCats.indexOf(catToMove);
    if(index > -1) {
        currCats.splice(index, 1);
        currCats.splice(0, 0, catToMove);
    }

    var newCode = "";
    for (var i = 0; i < currCats.length; i++) {
        newCode += currCats[i];
        if(i!=currCats.length - 1) {
            newCode += "-";
        }
    };

    getSelectCategorization(newCode);


    // alert("newCode: "+newCode);
});
$("#moveUp").on("click", function (e) {
    e.preventDefault();
    var selIndex = $('#cardSetCategorization option:selected').index();

    if(selIndex!=-1) {
        if(selIndex!=0){
            var catCode = $('#cardSetCategorization option:last-child').val();
            var arr = catCode.split('-');

            // console.log(catCode);

            var temp = arr[selIndex-1];
            arr[selIndex-1] = arr[selIndex];
            arr[selIndex] = temp;
            catCode = "";

            for (var i = 0; i < arr.length; i++) {
                catCode += arr[i];
                if((arr.length-1) != i) {
                    catCode += "-";
                }
            };

            getSelectCategorization(catCode);

        } else { alert("Already Top item.");}
    } else { alert("Can't move something when nothing is selected...");}

    // console.log(selIndex);
    // console.log(catCode);
    // console.log(arr);
    // alert($('#cardSetCategorization option:selected').index());
});
$("#deleteCat").on("click", function (e) {
    e.preventDefault();

    var clickedVal = $('#cardSetCategorization').val();
    // console.log($(this).children('option:last-child').val());
    var catVal = $('#cardSetCategorization').children('option:last-child').val();

    var catToRemove = clickedVal.split("-").pop();

    var currCats = catVal.split('-');

    var index = currCats.indexOf(catToRemove);

    var confirmDelete = confirm("Really delete this category from this Cardset?\n(Click Save Changes to make permanent.)");

    if (confirmDelete) {

        if(index > -1) {
            currCats.splice(index, 1);
        }
        var newCode = "";
        for (var i = 0; i < currCats.length; i++) {
            newCode += currCats[i];
            if(i!=currCats.length - 1) {
                newCode += "-";
            }
        };

        getSelectCategorization(newCode);

    }



// console.log(clickedSplit.pop());
    // alert("clickedVal: "+clickedVal+" - newCode: "+newCode+" - currCats: "+currCats);

});
$("#moveDown").on("click", function (e) {
    e.preventDefault();
    var selIndex = $('#cardSetCategorization option:selected').index();
    var lastIndex = $('#cardSetCategorization').children('option').length;
// console.log(lastIndex);
    if(selIndex!=-1) {
        if(selIndex!=(lastIndex-1)){
            var catCode = $('#cardSetCategorization option:last-child').val();
            var arr = catCode.split('-');

            var temp = arr[selIndex+1];
            arr[selIndex+1] = arr[selIndex];
            arr[selIndex] = temp;
            catCode = "";

            for (var i = 0; i < arr.length; i++) {
                catCode += arr[i];
                if((arr.length-1) != i) {
                    catCode += "-";
                }
            };

            getSelectCategorization(catCode);

        } else { alert("Already Bottom item.");}
    } else { alert("Can't move something when nothing is selected...");}

    // console.log(selIndex);
    // console.log(catCode);

    // alert($('#cardSetCategorization option:selected').index());
});
$("#moveBottom").on("click", function (e) {
    e.preventDefault();

    var clickedVal = $('#cardSetCategorization').val();
    // console.log($(this).children('option:last-child').val());
    var catVal = $('#cardSetCategorization').children('option:last-child').val();

    var catToMove = clickedVal.split("-").pop();

    var currCats = catVal.split('-');

    var index = currCats.indexOf(catToMove);
    if(index > -1) {
        currCats.splice(index, 1);
        currCats.splice(currCats.length, 0, catToMove);
    }

    var newCode = "";
    for (var i = 0; i < currCats.length; i++) {
        newCode += currCats[i];
        if(i!=currCats.length - 1) {
            newCode += "-";
        }
    };

    getSelectCategorization(newCode);


    // alert("newCode: "+newCode);
});

function getMessageBox(title, message, buttons) {
    title = title || null;
    message = message || null;
    buttons = (buttons instanceof Array)? buttons : null;

    if(!(buttons) || !(title) || !(message))
        return;

    var elem = document.createElement("div");
    var querybox = document.createElement("div");

    $(elem).attr("id","queryShader").addClass("query-shader").append(querybox);
    $(querybox).addClass("query-box");
    var xhtml = '\
        <div id="title" class="query-title" >'+title+'</div>\
        <div id="query" class="query-body">'+message+'</div>\
        <div id="queryButtons" class="query-buttons">';

    for(var i = 0; i < buttons.length; i++){
        xhtml += '\
            <button id="'+buttons[i].toLowerCase()+'" class="query-button">'+capitalize(buttons[i])+'</button>\
        ';
    }

    xhtml += '</div> \
        <div id="queryFooter" class="query-footer"></div>\
        ';
    $(querybox).html(xhtml);

    return elem;
}

function shuffleQuotes(myquotes) {


    var numberOfQuotes = myquotes.length;

    var n = Math.floor((numberOfQuotes*2) * Math.random());

    for (var i = 1; i < n; i++) {
        quote1 = Math.floor(numberOfQuotes * Math.random());
        quote2 = Math.floor(numberOfQuotes * Math.random());
        var temp = myquotes[quote2];
        myquotes[quote2] = myquotes[quote1];
        myquotes[quote1] = temp;
    }
    return myquotes;
}


function arrUnique(array) {
    return array.filter(function (a, b, c) {
        // keeps first occurrence
        return c.indexOf(a) === b;
    });
}

function categorize(element, addto, option) {
/*
    get category info (id, text, etc)*/
    var container = $(element).parent();
    var addToSelect = addto;
    var header = $(element).parent().find("label[for='"+$(element).attr("id")+"']");
    var headerText = $(header).text();
    var optiontext = $(option).text();
    var id = $(element).val();
    console.log(container);
    console.log(addToSelect);
    console.log(headerText);
    console.log(optiontext);
    console.log(headerText.indexOf("Sub"));
    console.log(id);


    //return;

    if(headerText.indexOf("Main") > -1) {
        // alert("text is main");

        // add text to categorization select...
        // get category id over with text
        $(addToSelect).append('<option id="category_'+id+'" value="'+id+'">'+optiontext+'</option>');

        // change "Main Category" to "Sub-Category"
        $(header).text(headerText.replace("Main", "Sub"));

    } else if(headerText.indexOf("Sub") > -1) {

        var dashes = '';
        var newOptionText = '';
        var subId = '';
        var options = $(addToSelect).children();
        var howMany = $(options).length;

        console.log(options);
        console.log(howMany);
        dashes = " ";
        for (var i = 0; i < howMany; i++) {
            dashes += "-";
        };
        dashes +=  " ";

        console.log(dashes);

    /*
        add dash to
            next line before category name
            current category value
        add selected category id to current category value
        add text to categorization select... */
        $(addToSelect).append('<option id="category_'+id+'" value="'+$(options[(howMany-1)]).val()+'-'+id+'">'+dashes+optiontext+'</option>');

        // console.log(selectSize);
    } else if(headerText == "Add Category") {

        console.log($(addToSelect).selected);
        /*
            If nothing selected, add to end of current list

            If something selected, ask if above or below selected.
        */

    }
}

function getSelectCategorization(categorizationCode) {

    $('#cardsetInfo #cardSetCategorization').empty();
    var elem = document.getElementById("cardsetInfo").querySelector("#cardSetCategorization");
    elem.style.backgroundColor = "";
    elem.style.borderColor = "";

    $.ajax({
        type: "POST",
        url: "/flashcard/getCategoriesById",
        data: {categorization: categorizationCode},
        success: function (data) {

            var returnedData = data.data;
            // console.log(returnedData);
            var selectOptions = "";
            var count = 0;
            var dashes = "";
            var idMod = "";
            var oldId = "";

            $.each(returnedData, function () {
                idMod = this.id;

                // for (var i = 0; i < count; i++) {
                    if(count>0){
                        dashes += "-";
                        idMod = oldId + "-" + this.id;
                    }
                // };

                $('#cardsetInfo #cardSetCategorization').append('<option id="category_'+ this.id +
                    '" class="categorization-options" value="'+ idMod +'">'+ dashes +
                    this.name + '</option>');
                count++;
                oldId = idMod;

            });


        },
        dataType: 'json'
    });
}

function capitalize(str) {
    strVal = '';
    str = str.split(' ');
    for (var chr = 0; chr < str.length; chr++) {
        strVal += str[chr].substring(0, 1).toUpperCase() + str[chr].substring(1, str[chr].length) + ' '
    }
    return strVal
}