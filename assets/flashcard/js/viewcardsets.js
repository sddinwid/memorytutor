$(document).ready(function () {
	$('#cardsInSet').hide();
	$('#selectCardset').val("-1");
	
	var cardsInSetDT = "";
	var carddata = {};

	$('#cardSetCategorization').on('click', function (e) {
		// e.preventDefault();

		// var clickedVal = $(this).val();
		// // console.log($(this).children('option:last-child').val());
		// var catVal = $(this).children('option:last-child').val();

		// alert("clickedVal: "+clickedVal+" - catVal: "+catVal);

	});

	$('#selectCardset').on('change', function (e) {

        var cardset_id = $(this).serialize();
        // var id = $(this).val();

        if ($(this).val()=='-1') {
            $('#cardsInSet').hide();
        } else {
        	$('#cardsInSet').show();
        

	        $.ajax({
	            type: "POST",
	            url: "/flashcard/getCardsetsById",
	            data: cardset_id,
	            success: function (data) {
	                // console.log(data);
	                var result = data.data;
	                var display = '';
	                for (var key in data.data) {
	                    for (var key2 in data.data[key]) {
	                        display += key2+"-"+data.data[key][key2]+"\n";
	                    }
	                }
	                // alert(result[0].name);

	                // console.log(result);

	                $('#cardsetInfo #name').val(result[0].name).css({"background-color":"white", "border-color":""});

	                $(tinymce.get('viewCardSetDescription').getBody()).html(result[0].description).css({"background-color":"white"});

	                // $('#viewCardSetDescription').val(result[0].description);

	                getSelectCategorization(result[0].categorization);



	                // window.location.reload();

	            },
	            dataType: 'json'
	        });

	        // if($.fn.DataTable.fnIsDataTable( $('#cardsInSetTable') )) {
	        // 	$('#cardsInSetTable').DataTable().destroy();
	        // }





			cardsInSetDT.ajax.reload();

	        // cardsInSetDT =

		}

        
    });

	$('#categorySelect option').on('click', function () {

        var dashes = '';
        var newOptionText = '';
        var subId = '';
        var options = $("#cardSetCategorization").children();
        var howMany = $(options).length;
        var text = $(this).text();
        var id = $(this).val();

        dashes = " ";
        for (var i = 0; i < howMany; i++) {
            dashes += "-";
        };
        dashes +=  " ";

    /*
        add dash to
            next line before category name
            current category value
        add selected category id to current category value
        add text to categorization select... */
        $("#cardSetCategorization").append('<option id="category_'+id+'" value="'+$(options[(howMany-1)]).val()+'-'+id+'">'+dashes+text+'</option>');


    });

	$("#updateCardsetInfoForm").on("submit", function(e) {
		e.preventDefault();
		if(document.getElementById("selectCardset").value==-1) return;
		if(!validateCardsetForm(document.getElementById("updateCardsetInfoForm")))
			return;
		var cardsetid = $("#selectCardset").val();

		$('#cardSetCategorization').val($('#cardSetCategorization option:last-child').val());
		$("#updateCardsetId").val(cardsetid);

		var d = new Date($.now());
		var curr_day = d.getDate();
		var curr_month = d.getMonth();
		var curr_year = d.getFullYear();
		var curr_hour = d.getHours();
		var curr_mins = d.getMinutes();
		var curr_secs = d.getSeconds();
		$("#updateCardsetModified").val(curr_year+"-"+curr_month+"-"+curr_day+" "+curr_hour+":"+curr_mins+":"+curr_secs);
        // console.log($('#cardSetCategorization').val());
        
        $.ajax({
            type: "POST",
            url: "/flashcard/updateCardset",
            data: $('#updateCardsetInfoForm').serialize(),
            success: function (data) {
                console.log(data);
                $("#selectCardset").val(0);
                $("#selectCardset").val(cardsetid);

            },
            dataType: 'json'
        });
	});

	$("#newCardWindow #cardResetButton").on("click", function() {

		resetForm(document.getElementById("newCardForm"), carddata);
	});

	$("#addNewCard").on("click", function (e) {
		$(".modal-title").text("Add Card");
		resetFormStyle(document.getElementById("addNewCard"));

		$("#newCardForm #addCardName").val('');
		$("#newCardForm #addCardDescription").val('');
		$("#newCardForm #addCardQuestion").val('');
		$("#newCardForm #addCardAnswer").val('');
		$("#newCardForm #addCardHint").val('');
		$("#newCardForm #addCardCardset_id").val($("#selectCardset").val());
	});

	$('#cardsInSetTable tbody')
        .on( 'mouseover', 'tr', function () {
            $(this).addClass('selected');
        } )
        .on( 'mouseleave', 'tr', function () {
            $(this).removeClass( 'selected' );
        } )
        .on( 'click', 'td button', function () {
        	// alert("gotcha "+$(this).text());
        	if($(this).text()=='Edit') {
        		/*
					Change text in Add Card window
        		*/
        		$(".modal-title").text("Edit Card");
        		var rowId = $(this).parent().parent().index();
        		var page = cardsInSetDT.page.info(); 
        		var data = null;
        		
        		if(page.page==0) {
        			data = cardsInSetDT.row(rowId).data();
        		} else if (page.page==page.pages) {
        			rowId = page.end;
        			data = cardsInSetDT.row((rowId)).data();
        		} else {
        			rowId = rowId + (page.page * page.length);
        			data = cardsInSetDT.row((rowId)).data();
        		}

				carddata["card"] = data;
        		var page = cardsInSetDT.page.info(); 
        		console.log(data);

        		$("#newCardForm #addCardName").val(data.name);
        		if(data.description!=null)
        		$(tinymce.get('addCardDescription').getBody()).html(data.description);
        		// $("#newCardForm #addCardDescription").val(data.description);
        		if(data.question!=null) {
					$(tinymce.get('addCardQuestion').getBody()).css({"background-color":"white"});
					$(tinymce.get('addCardQuestion').getBody()).html(data.question);
				}
        		// $("#newCardForm #addCardQuestion").val(data.question);
				if(data.answer!=null) {
					$(tinymce.get('addCardAnswer').getBody()).css({"background-color":"white"});
					$(tinymce.get('addCardAnswer').getBody()).html(data.answer);
				}
        		// $("#newCardForm #addCardAnswer").val(data.answer);
        		if(data.hint!=null)
        		$(tinymce.get('addCardHint').getBody()).html(data.hint);
        		// $("#newCardForm #addCardHint").val(data.hint);
                //if(data.active==1) {
					//$('#chkWrap #active').prop('checked', true).css({"background-color":"green"});
                //} else {
					//$('#chkWrap #active').prop('checked', false).css({"background-color":"red"});
                //}
                //
                //if(data.private==1) {
					//$('#prvWrap #private').prop('checked', true).css({"background-color":"green"});
                //} else {
					//$('#prvWrap #private').prop('checked', false).css({"background-color":"red"});
                //}
        		$("#newCardForm #addCardCardset_id").val(data.cardset_id);
        		$("#newCardForm").append("<input type='hidden' id='temp' name='id' value='"+ data.id +"'>");


				$.ajax({
					type: "POST",
					url: "/flashcard/getLinkageAJAX",
					data: {"cardId":data.id},
					success: function (data) {

							$(tinymce.get('addCardPrompt').getBody()).html("");
							$('#checks #chkWrap #active').prop('checked', false).css({"background-color": "red"});
							$('#checks #prvWrap #private').prop('checked', false).css({"background-color": "red"});
							$("#alarmCount option[value=1]").attr("selected", "selected");
							$("#prompt_added_date").val("");
							$("#modified_date").val("");
							$("#promptid").val("");

					},
					dataType: 'json'
				}).done(function(data) {
					var promptdata = data.data[0];
					console.log(promptdata);
					if(promptdata === "undefined")
						return;

					carddata["link"] = promptdata;

					if (promptdata.prompt_text != null)
						$(tinymce.get('addCardPrompt').getBody()).html(promptdata.prompt_text);
					document.getElementById("active").checked = promptdata.prompt_active?"on":"";
					document.getElementById("private").checked = promptdata.prompt_private?"on":"";

					promptdata.prompt_active?$('#checks #chkWrap #active').css({"background-color": "green"}):$('#checks #chkWrap #active').css({"background-color": "red"});
					promptdata.prompt_private?$('#checks #chkWrap #private').css({"background-color": "green"}):$('#checks #chkWrap #private').css({"background-color": "red"});

					if (promptdata.alarmWrong != null)
						$("input#wrong").val(promptdata.alarmWrong);
					if (promptdata.alarmTime != null)
						$("input#alarm").val(promptdata.alarmTime);
					if (promptdata.prompt_added_date != null)
						$("#prompt_added_date").val(promptdata.prompt_added_date);
					if (promptdata.prompt_modified_date != null)
						$("#modified_date").val(promptdata.prompt_modified_date);
					if (promptdata.id != null) {

					$("#promptid").val(promptdata.id);

					}else {
						$("#promptid").val("");
					}
				});

        		/*
					Open the window -using modal toggle
        		*/
        		

        	} else if($(this).text()=='Remove') {
        		/*
					get row id
					set cardset to -1 in DB
					reload DT
        		*/
        		if($('#selectCardset').val()=="0") {
        			alert("Sorry, cannot remove orphaned cards.");
        		} else {
	        		var retVal = confirm("Are you sure that you want to remove this card from this cardset?");
				   if( retVal == true ){
				      
				      var rowId = $(this).parent().parent().index();
		        		var data = cardsInSetDT.row(rowId).data();

		        		$.ajax({
				            type: "POST",
				            url: "/flashcard/removeCardFromCardset",
				            data: {"data":data.id},
				            success: function (data) {
				                // console.log(data);
				                cardsInSetDT.ajax.reload();
				            },
				            dataType: 'json'
				        });
					  
				   }else{
				      alert("Cancelled!");
					  return false;
				   }

				}
        		
        	}
        });

	
	$('#categorySelect option').on('click', function () {
        
        categorize($(this));


    });

	function checkNewCard() {

		if(!validateCardForm(document.getElementById("newCardForm")))
			return false;
		if($("input#wrong").val()=="0" &&
			$("input#alarm").val() == "0" ) {
			//pop alert
			alert("Error:\nBoth numbers cannot be zero.");
			return false;
		}else if(!$.isNumeric($("input#wrong").val()) ||
			!$.isNumeric($("input#alarm").val()) ) {
			alert("Non-number entered");
			return false;

		}else {
			return true;
		}
	}
	
	$("#cardSubmitButton").click(function (e) {
        // To Do
        e.preventDefault();

		if(!checkNewCard())
			return;

		resetFormStyle(document.getElementById("newCardForm"));

		var d = new Date($.now());
		var curr_day = (d.getDate()<10?"0"+ d.getDate(): d.getDate());
		var curr_month = (d.getMonth()<10?"0"+ d.getMonth(): d.getMonth());
		var curr_year = d.getFullYear();
		var curr_hour = (d.getHours()<10?"0"+ d.getHours(): d.getHours());
		var curr_mins = (d.getMinutes()<10?"0"+ d.getMinutes(): d.getMinutes());
		var curr_secs = (d.getSeconds()<10?"0"+ d.getSeconds(): d.getSeconds());

		//$('#cardModified ').val(curr_year+"-"+curr_month+"-"+curr_day+" "+curr_hour+":"+curr_mins+":"+curr_secs);

		$('div#dates input#modified_date ').val(curr_year+"-"+curr_month+"-"+curr_day+" "+curr_hour+":"+curr_mins+":"+curr_secs);

		$("#addCardPrompt").val($(tinymce.get('addCardPrompt').getBody()).html());
		$("#addCardQuestion").val($(tinymce.get('addCardQuestion').getBody()).html());
		$("#addCardAnswer").val($(tinymce.get('addCardAnswer').getBody()).html());
		$("#addCardHint").val($(tinymce.get('addCardHint').getBody()).html());
		$("#addCardDescription").val($(tinymce.get('addCardDescription').getBody()).html());


		console.log($('div#dates input#date ').val());//return;

        if($(".modal-header h4").text()=="Edit Card") {

        	$('#categorization').val($('#categorization option:last-child').val());

	        // console.log($('#categorization').val());
	        
	        $.ajax({
	            type: "POST",
	            url: "/flashcard/updateCard",
	            data: $('#newCardForm').serialize(),
	            success: function (data) {
	                console.log(data);
	                cardsInSetDT.ajax.reload();
	                
	                $('#temp').remove();
	                $('#newCardWindow #cardCloseButton').trigger("click");
	            },
				error: function(xhr, thing, other) {
					console.log(xhr);
					console.log(thing);
					console.log(other);

				},
	            dataType: 'json'
	        });

        } else if($(".modal-header h4").text()=="Add Card"){
        	// var form = $('#newCardForm').serialize();
	        $('#categorization').val($('#categorization option:last-child').val());

	        $('#newCardForm').append('<input type="hidden" id="cardCreated" value="" name="created_on">');
    		
	        var d = new Date($.now());
			var curr_day = d.getDate();
			var curr_month = d.getMonth();
			var curr_year = d.getFullYear();
			var curr_hour = d.getHours();
			var curr_mins = d.getMinutes();
			var curr_secs = d.getSeconds();
			$('#newCardForm #cardCreated').val(curr_year+"-"+curr_month+"-"+curr_day+" "+curr_hour+":"+curr_mins+":"+curr_secs);
	        // console.log($('#categorization').val());

			//return;

	        $.ajax({
	            type: "POST",
	            url: "/flashcard/addNewCard",
	            data: $('#newCardForm').serialize(),
	            success: function (data) {
	                // console.log(data);
	                var display = '';
	                for (var key in data.data) {
	                    display += key+"-"+data.data[key]+"\n";
	                }
	                alert(display);

	                $('#newCardForm').remove('#cardCreated');
	                
	                cardsInSetDT.ajax.reload();
	                
	            },
	            dataType: 'json'
	        });
        }
        
    });

	tinymce.init({
        selector: 'textarea#viewCardSetDescription',
        width: 300,
        height: 120,
        menubar: false,
        statusbar: false,
        setup: function (editor) {
            editor.on('change', function () {
                tinymce.triggerSave();
            });
        }
    });

	tinymce.init({
        selector: 'textarea#addCardDescription',
        width: 300,
        height: 120,
        menubar: false,
        statusbar: false,
        setup: function (editor) {
            editor.on('change', function () {
                tinymce.triggerSave();
            });
        }
    });

	tinymce.init({
		selector: 'textarea#addCardQuestion',
		width: 300,
		height: 120,
		menubar: false,
		statusbar: false,
		setup: function (editor) {
			editor.on('change', function () {
				tinymce.triggerSave();
			});
		}
	});

	tinymce.init({
		selector: 'textarea#addCardAnswer',
		width: 300,
		height: 120,
		menubar: false,
		statusbar: false,
		setup: function (editor) {
			editor.on('change', function () {
				tinymce.triggerSave();
			});
		}
	});

	tinymce.init({
		selector: 'textarea#addCardHint',
		width: 300,
		height: 120,
		menubar: false,
		statusbar: false,
		setup: function (editor) {
			editor.on('change', function () {
				tinymce.triggerSave();
			});
		}
	});

	tinymce.init({
		selector: 'textarea#addCardPrompt',
		height: 120,
		menubar: false,
		statusbar: false,
		setup: function (editor) {
			editor.on('change', function () {
				tinymce.triggerSave();
			});
		}
	});

    

    cardsInSetDT = $('#cardsInSetTable').DataTable({
		"order": [[ 1, 'asc' ]],
		"ajax": {
			"url": "/flashcard/getCardsByCardsetAJAX",
			"data": function ( d ) {
			  return $.extend( {}, d, {
				"selectCardset": $('#selectCardset').val()
			  } );
			},
			"type": "POST"
		},
		"columns": [
			{"data": "id"},
			{"data": "name"},
			{"data": "description"},
			{"data": "cardset_id"},
			{"data": "categorization"},
			{"data": "question"},
			{"data": "answer"},
			{"data": "hint"},
			{"data": "created_on"},
			{"data": "modified_on"}
		],
		"columnDefs": [
			{
				"className": "center", "targets": [1,2,5,6,7,8,9]
			},
			{
				"targets" : 0,
				"title": "ID",
				"visible": false,
				"render": function ( data, type, full, meta ) {

				  return data;
				}
			},
			{
				"targets" : 1,
				"title": "Card Name",
				"render": function ( data, type, full, meta ) {
					return data;
				}
			},
			{
				"targets" : 2,
				"title": "Card Description",
				"render": function ( data, type, full, meta ) {
					return data;
				}
			},
			{
				"targets" : 3,
				"title": "Cardset ID",
				"visible": false,
				"render": function ( data, type, full, meta ) {

				  return data;
				}
			},
			{
				"targets" : 4,
				"title": "Categorization",
				"visible": false,
				"render": function ( data, type, full, meta ) {

				  return data;
				}
			},
			{
				"targets" : 5,
				"title": "Question",
				"render": function ( data, type, full, meta ) {

				  return data;
				}
			},
			{
				"targets" : 6,
				"title": "Answer",
				"render": function ( data, type, full, meta ) {

				  return data;
				}
			},
			{
				"targets" : 7,
				"title": "Hint",
				"render": function ( data, type, full, meta ) {

				  return data;
				},
				"width": "100px"
			},
			{
				"targets" : 8,
				"title": "Created",
				"render": function ( data, type, full, meta ) {
					var tempdate = new Date(data);
					return data;//tempdate.toString();
				},
				"width": "95px"
			},
			{
				"targets" : 9,
				"title": "Last Modified",
				"render": function ( data, type, full, meta ) {
					var tempdate = Date(data);
					return data;//tempdate.toString();
				},
				"width": "95px"
			},
			{
				"targets" : 10,
				"title": "Admin",
				"render": function ( data, type, full, meta) {
					var html = '<button class="admin-button edit btn btn-primary"'+
					' data-toggle="modal" data-target="#newCardWindow">Edit</button><br />'+
								'<button class="admin-button remove btn btn-danger">Remove</button>';
					return html;
				},
				"className": "admin-buttons"
			}
		]
	})


});

function resetForm(formElement, data) {

	if(formElement==null)
		return;

	if(data==="undefined" ||(data instanceof Array && data.length == 0)) {

	}else {

		var els = formElement.elements;
		var dat = data['card'];
		var lnk = data['link'];

		for(var i =0;i< els.length;i++) {
			for(var j in dat) {
				if(els[i].name == j){
					els[i].value = dat[j];
					if(document.getElementById(els[i].id+"_ifr")) {
						$(tinymce.get(els[i].id).getBody()).html(dat[j]);
					}
				}
			}
			for( var k in lnk) {
				if(els[i].name == k || (k == "prompt_text" && els[i].name=="prompt")){
					els[i].value = lnk[k];
					if(document.getElementById(els[i].id+"_ifr")) {
						$(tinymce.get(els[i].id).getBody()).html(lnk[k]);
					}
				}else if("prompt_"+els[i].name == k) {
					if(els[i].name == "active" || els[i].name=="private") {
						els[i].checked = lnk[k]?"on":"";
					}else {
						els[i].value = lnk[k];
					}

					if(document.getElementById(els[i].id+"_ifr")) {
						$(tinymce.get(els[i].id).getBody()).html(lnk[k]);
					}
				}

			}
		}

	}


	resetFormStyle(formElement);

}

function showEditScreen(data) {
	console.log(data);
	// alert('Got it');prompt

	document.getElementById("updatePromptInfoForm").reset();
	$(tinymce.get('prompt').getBody()).html("");

	var promptWin = $("#modifyPromptWindow");

	var elem = $(promptWin).find(".modal-title");
	var text = $(elem).text();

	text = text.replace("Add","Edit");
	$(elem).text(text);

	$('input#id').val(data.id);
	$(tinymce.get('prompt').getBody()).html(data.prompt_text);
	$('input#credit').val(data.prompt_credit);
	$('input#date').val(data.prompt_added_date);
	$('input#modified_date').val(data.modified_date);
	$('input#alarm').val(data.alarmTime);
	$('input#wrong').val(data.alarmWrong);

	if(data.active==1) {
		$('#active').prop('checked', true).css({"background-color":"green"});
	} else {
		$('#active').prop('checked', false).css({"background-color":"red"});
	}

	if(data.private==1) {
		$('#private').prop('checked', true).css({"background-color":"green"});
	} else {
		$('#private').prop('checked', false).css({"background-color":"red"});
	}

	tinyMCE.triggerSave();

}