function FormValidator(formNode) {
    this.errorMessage = "";
    this.formNode = formNode;
}

FormValidator.prototype = {
    addRule: function (fieldName, message, validatorType, args) {
        validatorType = validatorType.toLowerCase(validatorType);
        var element = this.formNode[fieldName];
        if (element != null) {
            switch (validatorType) {
                case "required":
                    this.required(element, message);
                    break;
                case "minlength":
                    this.minlength(element, message, args[0]);
                    break;
                case "rangelength":
                    this.rangelength(element, message, args[0], args[1]);
                    break;
                case "rangevalues":
                    this.rangevalues(element, message, args[0], args[1]);
                    break;
                case "numeric":
                    this.numeric(element, message);
                    break;
                case "positivenumber":
                    this.positivenumber(element, message);
                    break;
                case "integer":
                    this.integer(element, message);
                    break;
                case "positiveinteger":
                    this.positiveinteger(element, message);
                    break;
                case "lettersonly":
                    this.lettersonly(element, message);
                    break;
                case "zipcode":
                    this.zipcode(element, message);
                    break;
                case "email":
                    this.email(element, message);
                    break;
                case "ischecked":
                    this.ischecked(element, message);
                    break;
                case "compare":
                    var elementPivot = this.formNode[args];
                    if (elementPivot != null) this.compare(element, message, elementPivot);
                    break;
                case "oneorother":
                    var other = this.formNode[args];
                    if (other != null) this.oneorother(element, message, other);
                    break;
                case "notboth":
                    var other = this.formNode[args];
                    if (other != null) this.notboth(element, message, other);
                    break;
                case "creditcard":
                    this.creditcard(element, message);
                    break;
                case "expression":
                    this.expression(element, message, args);
                    break;
                default:
            }
        }
    }, markField: function (element, bg_color, border_color) {
        if (element.length) {
            for (var j = 0; j < element.length; j++) {
                
                element[j].style.backgroundColor = bg_color;
                element[j].style.borderColor = border_color;
            }
        } else {
            element.style.backgroundColor = bg_color;
            element.style.borderColor = border_color;
            
        }
    }, invalidField: function (element, message) {
        this.markField(element, "#F5EDB4", "#FF9933");
        this.errorMessage = this.errorMessage + message + "\n";
    }, required: function (element, message) {
        var form = this.formNode;
        //alert(element.getElementsByTagName("option").length);
        console.log(element);
        if(element.tagName.toLowerCase()=="select") {
            if(element.getElementsByTagName("option").length==0) {
                this.invalidField(element, message);
            }
        }else  {
            if(this.iftinymce(element)) {
                var editarea;
                if(form.name=="newCardForm") {
                    editarea = form.querySelector("#"+element.name).querySelector(".mce-tinymce").querySelector(".mce-edit-area");
                }else {
                    editarea = form.querySelector(".mce-tinymce").querySelector(".mce-edit-area");
                }

                var iframe = editarea.querySelector("iframe");
                var ifr_body = iframe.contentDocument.querySelector("body");

                if (!/\S/.test(ifr_body.value)) {
                    this.invalidField(ifr_body, message);
                }
            }else {
                if (!/\S/.test(element.value)) {
                    this.invalidField(element, message);
                }
            }
        }
    }, minlength: function (element, message, minLength) {
        if (minLength != null && element.value.length < minLength) {
            this.invalidField(element, message);
        }
    }, rangelength: function (element, message) {
        if (element.getElementsByTagName('li').length < 1) {
            this.invalidField(element, message);
        }
    }, rangevalues: function (element, message, minValue, maxValue) {
        if ((minValue != null && element.value < minValue) || (maxValue != null && element.value > maxValue)) {
            this.invalidField(element, message);
        }
    }, numeric: function (element, message) {
        if (!/^(-?(\d+\.?\d+)|(\d+)){1}$/.test(element.value)) { this.invalidField(element, message); }
    }, positivenumber: function (element, message) {
        if (!/^((\d+\.?\d+)|(\d+)){1}$/.test(element.value)) { this.invalidField(element, message); }
    }, integer: function (element, message) {
        if (!/^-?\d+$/.test(element.value)) { this.invalidField(element, message); }
    }, positiveinteger: function (element, message) {
        if (!/^\d+$/.test(element.value)) { this.invalidField(element, message); }
    }, lettersonly: function (element, message) {
        if (!/^[a-zA-Z]+$/.test(element.value)) { this.invalidField(element, message); }
    }, zipcode: function (element, message) {
        if (!/^\d{5}$/.test(element.value)) { this.invalidField(element, message); }
    }, email: function (element, message) {
        if (!/^\S+@\S+(\.\S+)*\.(\S{2,3}|info)$/i.test(element.value) || /[\(\)\<\>\,\;\:\\\"\[\]]/.test(element.value)) {
            this.invalidField(element, message);
        }
    }, ischecked: function (element, message) {
        var i = 0, flag = false;
        if (element.length) {
            while ((i < element.length) && !flag) {
                if (element[i].checked == true) flag = true; i++;
            }
            if (!flag) {
                for (var j = 0; j < element.length; j++)
                    this.markField(element[j], "#F5EDB4", "#FF9933");
                this.invalidField(element[0], message);
            }
        } else { if (element.checked == false) this.invalidField(element, message); }
    }, compare: function (element, message, elementPivot) {
        if (element.value != elementPivot.value) {
            this.markField(elementPivot, "#F5EDB4", "#FF9933");
            this.invalidField(element, message);
        }
    }, oneorother: function (element, message, other) {
        if (!(element.value) && !(other.value)) {
            this.markField(other, "#F5EDB4", "#FF9933");
            this.invalidField(element, message);
        }
    }, notboth: function (element, message, other) {
        if (element.value && other.value) {
            this.markField(other, "#F5EDB4", "#FF9933");
            this.invalidField(element, message);
        }
    }, expression: function (element, message, regularExpression) {
        if (!regularExpression.test(element.value)) { this.invalidField(element, message); }
    }, creditcard: function (element, message, regularExpression) {
        var i; var transformString = "";
        var sum = 0; element.value = element.value.replace(/\D/g, "");
        var strLength = element.value.length;
        if (strLength > 0) {
            for (i = strLength; i > 0; i--) {
                if ((strLength - i) % 2 != 0) {
                    transformString = transformString + parseInt(element.value.charAt(i - 1), 10) * 2;
                } else {
                    transformString = transformString + element.value.charAt(i - 1);
                }
            }
            for (i = 0; i < transformString.length; i++) {
                sum = sum + parseInt(transformString.charAt(i), 10);
            }
            if (sum == 0 || sum % 10 != 0) this.invalidField(element, message);
        } else this.invalidField(element, message);
    }, iftinymce: function (element) {
        var elid = element.id;
        var ifr = (elid+'_ifr');
        var par = element.parentNode;
        if(par.getElementsByClassName("mce-tinymce").length){
            return true;
        }else {
            return false;
        }

    }, validate: function () {
        if (this.errorMessage != "") {
            alert(this.errorMessage);
            return false;
        }
        return true;
    }
};

function resetFormStyle(formElement) {

    if(formElement==null)
        return;

    var elems = formElement.elements;

    for(var i =0;i<elems.length;i++){
        if(elems[i].tagName.toLowerCase()=="button")
            continue;
        if(elems[i].tagName.toLowerCase()=="input") {
            elems[i].style.backgroundColor = "";
            elems[i].style.borderColor = "";
            elems[i].value = "";
        }
        if(elems[i].tagName.toLowerCase()=="select") {
            elems[i].style.backgroundColor = "";
            elems[i].style.borderColor = "";
            if(elems[i].size == 0 || elems[i].size == 1) {
                elems[i].selectedIndex = 0;
            }else if(elems[i].size > 1) {
                elems[i].length = 0;
            }
        }
        if(elems[i].tagName.toLowerCase()=="textarea") {
            elems[i].style.backgroundColor = "";
            elems[i].style.borderColor = "";
            if($(tinymce.get(elems[i].id)).length)
                $(tinymce.get(elems[i].id).getBody()).html("");
            elems[i].value = "";
        }
    }
}