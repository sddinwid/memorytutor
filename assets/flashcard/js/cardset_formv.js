// JavaScript Document
function validateCardsetForm(formNode){
  var formValidatorInstance = new FormValidator(formNode);
  formValidatorInstance.addRule('name', 'Name is required', 'required');
  return formValidatorInstance.validate();
}