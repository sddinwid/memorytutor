// JavaScript Document
function validateCategoryForm(formNode){
  var formValidatorInstance = new FormValidator(formNode);
  formValidatorInstance.addRule('cat_name', 'Category Name is required.', 'required');

  return formValidatorInstance.validate();
}