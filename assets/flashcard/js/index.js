$(document).ready(function () {
	/*

		vars

	*/
	var cardIndex = 0;
	var cardsInSet = "";
	
    $('#loginFormDiv').hide();
	$(".footer-div").addClass("text-white");


	tinymce.init({
		selector: 'textarea#newCategoryDescription',
		width: 300,
		height: 120,
		menubar: false,
		statusbar: false,
		setup: function (editor) {
			editor.on('change', function () {
				tinymce.triggerSave();
			});
		}
	});

	tinymce.init({
		selector: 'textarea#cardSetDescription',
		width: 300,
		height: 120,
		menubar: false,
		statusbar: false,
		setup: function (editor) {
			editor.on('change', function () {
				tinymce.triggerSave();
			});
		}
	});

	tinymce.init({
		selector: 'textarea#viewCardSetDescription',
		width: 300,
		height: 120,
		menubar: false,
		statusbar: false,
		setup: function (editor) {
			editor.on('change', function () {
				tinymce.triggerSave();
			});
		}
	});

	tinymce.init({
		selector: 'textarea#addCardDescription',
		width: 300,
		height: 120,
		menubar: false,
		statusbar: false,
		setup: function (editor) {
			editor.on('change', function () {
				tinymce.triggerSave();
			});
		}
	});

	tinymce.init({
		selector: 'textarea#addCardQuestion',
		width: 300,
		height: 120,
		menubar: false,
		statusbar: false,
		setup: function (editor) {
			editor.on('change', function () {
				tinymce.triggerSave();
			});
		}
	});

	tinymce.init({
		selector: 'textarea#addCardAnswer',
		width: 300,
		height: 120,
		menubar: false,
		statusbar: false,
		setup: function (editor) {
			editor.on('change', function () {
				tinymce.triggerSave();
			});
		}
	});

	tinymce.init({
		selector: 'textarea#addCardHint',
		width: 300,
		height: 120,
		menubar: false,
		statusbar: false,
		setup: function (editor) {
			editor.on('change', function () {
				tinymce.triggerSave();
			});
		}
	});

	tinymce.init({
		selector: 'textarea#addCardPrompt',
		height: 120,
		menubar: false,
		statusbar: false,
		setup: function (editor) {
			editor.on('change', function () {
				tinymce.triggerSave();
			});
		}
	});
    
    
    if (typeof newUser !== 'undefined') {
    // the variable is defined
        console.log(newUser);
        if (newUser) {
            //alert('Hello, New User!!');
        }
    } else {
        //alert('newUser is undefined');
    }

	$('.hover').hover(function(){
		$(this).addClass('flip');
	},function(){
		$(this).removeClass('flip');
	});

	$('.click').toggle(function(){
			$(this).addClass('flip');
		},function(){
			$(this).removeClass('flip');
		});

    $("#loginLink").click(function () {
		console.log($("#loginButtonDiv").position().top);
		console.log($("#loginButtonDiv").height());
		console.log($("#loginButtonDiv").position().left);
        $('#loginFormDiv').removeClass('fade');
        $('#loginFormDiv').load('/fclogin');
        if($("#loginFormDiv").is(":visible")) {
            $("#loginFormDiv").toggle();
        } else {
			$("#loginFormDiv").css({
				"top" : ($("#loginButtonDiv").position().top + $("#loginButtonDiv").height() ) + "px",
				"right" : "10px"
			}).toggle();
        }
    });

    $("#newCardSubmitButton").click(function (e) {
        // To Do
        e.preventDefault();
        // var form = $('#newCardForm').serialize();
        $('#categorization').val($('#categorization option:last-child').val());
        console.log($('#categorization').val());
        
        $.ajax({
            type: "POST",
            url: "/flashcard/addNewCard",
            data: $('#newCardForm').serialize(),
            success: function (data) {
                // console.log(data);
                var display = '';
                for (var key in data.data) {
                    display += key+"-"+data.data[key]+"\n";
                }
                alert(display);
            },
            dataType: 'json'
        });
    });

    $('#newCategoryFormSubmit').on('click', function(e) {
        e.preventDefault();

        if(!(validateCategoryForm(document.getElementById("newCategoryForm"))))
			return;

        $.ajax({
            data: $('#newCategoryForm').serialize(),
            type: "POST",
            url: '/flashcard/addNewCategory',
            dataType: 'json',
            'success': function (resp) {

				return;

				console.log(resp);
                
                var clone = $('#categoryListing').find(':last-child').clone();
                clone.attr('id',resp.data.id).text(resp.data.name);
                $('#categoryListing').append(clone);
                $('#newCardSetForm #categorySelect').append('<option id="category_'+resp.data.id+'" class="activeHover" value="'+ (resp.data.categorization>0)?(resp.data.categorization+"-"+resp.data.id) : resp.data.id+'" >'+resp.data.name+'</option>')
                                    .find(':last-child').bind('click', function() {
                                        alert("clicked on "+$(this).text());
                                        console.log($(this));
                                        categorize($(this));
                                    });
                // $('#userResponseArea').append('<li id="'+ resp.data.id +'" class="activeHover">' + resp.data.name + '</li>');
                $('#newCategoryFormQuit').trigger('click');
console.log($('#userResponseArea').find(':last-child'));
            },
            error: function(xhr, status, error) {
                console.log("(" + xhr.responseText + ")");

            }
        }).done(function(resp) {
			console.log(resp);
		});
    });

	$("#categorySelect").on("change", function(e) {
		var lbltext = $("label[for='categorySelect']").text();
		//console.log(this.options[this.selectedIndex]);
		//if(lbltext.indexOf("Main")) {
		//	$("label[for='categorySelect']").text(lbltext.replace("Main", "Sub"));
		//}

		var option = this.options[this.selectedIndex];
		//$("#cardSetCategorization").append(option);
		categorize($("#categorySelect"), $("#cardSetCategorization"), option);

		//alert("change: "+lbltext);
	});

	$("#newCardsetSubmitButton").click(function (e) {
        // To Do
        e.preventDefault();

		if(!(validateCardsetForm(document.getElementById("newCardSetForm"))))
			return;

        // var form = $('#newCardForm').serialize();
        $('#cardSetCategorization').val($('#cardSetCategorization option:last-child').val());
        
        $.ajax({
            type: "POST",
            url: "/flashcard/addNewCardSet",
            data: $('#newCardSetForm').serialize(),
            success: function (data) {
                // console.log(data);
                var display = '';
                for (var key in data.data) {
                    display += key+"\t"+data.data[key]+"\n";
                }

                $("#cardsetListing").append("<li id=\"cs_"+data.data["id"]+"\" class=\"activeHover ui-menu-item\">"+data.data["name"]+"</li>")
                alert(display);

                $("#newCardsetCloseButton").click();
                // window.location.reload();

            },
            dataType: 'json'
        });
    });

	$("#newCategoryFormQuit").on("click", function (e) {
		//alert("Close");
		resetFormStyle(document.getElementById("newCategoryForm"));
	});

	$("#newCardsetCloseButton").on("click", function (e) {
		var lbltext = $("label[for='categorySelect']").text();
		if(!(lbltext.indexOf("Main"))) {
			$("label[for='categorySelect']").text(lbltext.replace("Sub", "Main"));
		}
		resetFormStyle(document.getElementById("newCardSetForm"));
	});

	//$("#categoryMenu>#review>#categoryListing>li").on("click" ,".cardset", function (e) {
		// alert("name: "+$(this).text()+"\nid: "+$(this).attr("id"));
	//	var setName = $(this).html();
     //   var category = $(this).attr("data-cardsetid");
	//	var html = "";
     //   // console.log($(this));
     //   // alert("IE?");
	//	$.ajax({
	//            type: "POST",
	//            url: "/flashcard/getCardsByCardset",
     //           cache: false,
     //           dataType: 'json',
	//            data: {"selectCardset":category},
	//            success: function (data) {
	//            	cardsInSet = data.data;
     //               // alert("IE?");
	//            	cardIndex = 0;
	//            	$("#front .pad").html("<h2>Mouse Over Me!</h2><p><h3>This is the question side of the card....</h3></p>");
	//	    		$("#flipBack .pad").html("<h2>this is the hint side of the card.</h2><p>Click Reveal to show answer.</p>");
	//	    		$("#swingBack .pad").html("<h2>and this is the answer side of the card.</h2>");
	//                // console.log(cardsInSet);
	//                // alert("Cardset loaded.");
	//                $.each(cardsInSet, function (i, val) {
	//                	// console.log(val);
	//                	html += '<li>'+ val.question +'</li>'
	//                });
	//                var toReturn = "<ul>"+html+"</ul>";
	//                $("#leftSidebar").append(toReturn);
	//                $("#leftSidebar").css({"width":"19%"});
	//                $("#leftSidebar").show();
    	//			$("#centerArea").removeClass("center-area-full-screen");
    	//			$("#centerArea").css({"width":"80%"});
	//            }
	//        });
	//});

});