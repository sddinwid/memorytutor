$("body").hide();
$(document).ready(function () {

    var cardsInSet = [];
    var currentCard = [];
    var wrongCards = [];
    var inactiveY = 3;
    var quiz = {};
    var userAnswer = '';
    var drawOrder = [];
    var hintUsed = 0;
    var quizTimer = new cardTimer();
    var qtStarted = false;
    var speedBool = true;
    var speed = 1;
    var modifierKey = null;
    var lastKey = null;
    var prefsWidth = $('#configWin').width();
    var prefsHeight = $('#configWin').height();
    var screenWidth = $(window).width();
    var screenHeight = $(window).height();
    var allreadyWrong = null;
    var firstprompt = 0;
    var answered = 0;
    var linked = false;

    $('#configWin').css({
        "left": ((screenWidth / 2) - (prefsWidth / 2))
    });
    prefs = prefs[0];

    console.log(linkage);

    /** Put card numbers into array for checking purposes **
     **
     **
     ** **/

    var cardtimer = new cardTimer($('#scoresRow3 #displayTimer>span'));

    $('#configWin').hide();
    $('#notificationsDiv').detach().appendTo("body").hide();

    $('#quizMenuBar').detach().appendTo("#menuBar").css({"position":"absolute","right":0}).removeClass("hidden");
    $("#settingsButtonDiv").css({"z-index":100});

    //$("#headerBar").hide();

    $("body").show();

    setSpeedMode($("#speedEnabled"));

    //$("#quizMenuBar ul li").on("hover", function (e) {
    //
    //    alert("hover");
    //
    //});

    $("span#siteTitle").click(function(e){
        e.preventDefault();
        $('#notificationsDiv').css({

            "top": "20%",
            "left": "50%",

            "z-index":($('#notificationsDiv').css("z-index")*-1)
        }).toggle();
    });

    $("#closewindowbutton").click(function(e){
        $("span#siteTitle").click();
    });

    $("div#buttonsDiv > #close").click(function(e){
        $("span#siteTitle").click();
    });

    $("#speedEnabled").on('change', function (e) {

        setSpeedMode($(this));

    });

    $('#cardRatingDiv>div>div').on('click', function (e) {

        var which = $(this).text().toLowerCase();

        switch (which) {
            case "correct":
            case "incorrect":

                $('#rightWrong>div').each(function () {
                    $(this).removeClass('active');
                });
                $(this).addClass('active');


                break;
            case "easy":
            case "challenging":
            case "hard":
            case "impossible":

                $('#ratings>div').each(function () {
                    $(this).removeClass('active');
                });
                $(this).addClass('active');

                break;
        }

        if ($('#ratings').find('.active').length != 0 &&
            $('#rightWrong').find('.active').length != 0) {

            var selectedRightText = $('#rightWrong').find('.active').text();
            var selectedRatingText = $('#ratings').find('.active').text();

            var rightWrongElem = $('#scoresRow1').find('span:contains("' + (selectedRightText) + '")').next();
            rightWrongElem.text(parseInt(rightWrongElem.text()) + 1);

            var ratingElem = $('#scoresRow2').find('span:contains("' + (selectedRatingText) + '")').next();
            ratingElem.text(parseInt(ratingElem.text()) + 1);

            var numAnswered = $('#scoresRow3').find('#totalAnswered').find(':last-child');

            $(numAnswered).html($("#currcardnum").text());
            var percentValueElem = $('#scoresRow1').find('span:contains("Percent")').next();
            var currCorrect = $('#scoresRow1').find('span:contains("Correct")').next().text();

            var currAnswered = $(numAnswered).text();

            var currPercent = currCorrect / currAnswered;

            currPercent = currPercent * 10000;
            currPercent = Math.round(currPercent);
            currPercent = currPercent / 100;
            percentValueElem.text(parseFloat(currPercent) + "%");

            var rwBool = 0;
            if ($('#rightWrong').find('.active').text().toLowerCase() == "correct") {
                rwBool = 1;
            }

            /**
             * if rwBool is 0 here, card was incorrect.
             * add it to the wrong card stack to retest on
             */
            //console.log("test");
            if(rwBool==0) {
                /** Add in here a prompt if user
                 *   would like to add a prompt for this card.
                 */

                if($('#newHolder').parent().find('.card').length && (!linked)){

                    checkToCreatePrompt();
                }


                wrongCards.push(currentCard);
            }

            if($("div#promptWin").length==1) {
                $("div#promptWin").remove();
            }

            var quizdata = [
                {"rightWrong": (rwBool)},
                {"rating": ($('#ratings').find('.active').text().toLowerCase())},
                {"timeToAnswer": (cardtimer.getTime())},
                {"correct": ($('#scoresRow1').find('span:contains("Correct")').next().text())},
                {"incorrect": ($('#scoresRow1').find('span:contains("Incorrect")').next().text())},
                {"percent": (percentValueElem.text())},
                {"hintUsed": (hintUsed)},
                {"answer": (userAnswer)},
                {"qsAnswered": (numAnswered.text())}
            ];

            storeQuiz(quizdata, currentCard);
            gradedAndRated();

            //alert(parseInt($("#currcardnum").text())==parseInt($("#totalcardnum").text()));
            if(parseInt($("#currcardnum").text())==parseInt($("#totalcardnum").text())) {
                console.log(wrongCards);

                if (!quiz['end_time'] && !quiz['date_taken'])
                    endQuiz();

                    quiz['percent_correct'] = drawOrder[(drawOrder.length - 1)].info[5]['percent'];
                    quiz['quiz_length'] = $('#answeredCardsDiv').find('.inactive-card').length;
                    quiz['quiz_length'] += ($('#answeredCardsDiv').find('.inactive-card').length > 1) ? " cards viewed" : " card viewed";

                $.ajax({
                    type: "POST",
                    data: {"quiz": quiz, "drawOrder": drawOrder},
                    cache: false,
                    url: "/quizzes/saveQuiz",
                    success: function (data) {
                        console.log(data);

                        alert("Quiz has been saved.");
                    },
                    dataType: 'json'
                });

                if(wrongCards.length>0) {
                    if(wrongCards.length>1) {
                        mesg = "There are "+wrongCards.length+" cards that were graded as \"Incorrect\". Would you like to retest on those cards?";

                    }else {
                        mesg = "There is 1 card that was graded as \"Incorrect\". Would you like to retest on that card?";
                    }

                    if(confirm(mesg)){


                        var returnedCardsHTML = "";
                        cardsInSet = [];

                        cardsInSet = wrongCards.slice(0);
                        wrongCards.length = 0;

                        $.each(cardsInSet, function (i, val) {


                            returnedCardsHTML += '<div id="card_' + val.id + '" name="' + val.id + '" class="panel card shuffled">' +

                            '<div id="front" class="front"><div class="pad"><p>' +

                            '</p></div></div>' +
                            '<div id="back" class="back" style="">' +
                            '<div class="pad"><p>' +
                            val.question +
                            '</p></div></div>' +

                            '</div>'
                        });

                        $('#cardDeck').empty().html(returnedCardsHTML);
                        $("#answeredCardsDiv").find(".inactive-card").remove();

                        var cardCount = quiz['number_of_questions'] = cardsInSet.length;
                        $("#totalcardnum").text(cardCount);
                        $("#currcardnum").text("0");
                        quiz['cardset_id'] = $('#selectCardset').val();

                        var texts = $("div[id^='scoresRow'] > div[id^='display'] > span#text");

                        $(texts).each(function(){
                            $(this).text("0");
                        });


                        drawDeck();

                    }else {


                    }
                }

            }
        }
    });
    //$("div.new-card-prompt-div").hide();
    $('#configWin').on('click', 'input', function (e) {

        $(this).val("");

    }).on('change', 'input', function (e) {

        var txt = $(this).val();

        if (txt.length > 1) {
            txt = (txt.substring(0)).toUpperCase() + (txt.substring(1, txt.length - 2)) + (isNaN(txt.substring(txt.length - 1)) ? (txt.substring(txt.length - 1)).toUpperCase() : txt.substring(txt.length - 1));
            $(this.val(txt));
        } else {
            if (isNaN(txt)) {
                $(this).val(txt.toUpperCase());
            } else {
                $(this).val(txt);
            }
        }
    }).on('blur', 'input', function (e) {


    });

    $("#promptSubmitButton").on("click", function() {
        //alert("hi");
        $.ajax({
            type: "POST",
            url: "/flashcard/insertPromptWrongCard",
            data: $("form#addPromptForm").serialize(),
            success: function (data) {

                console.log(data);

            },
            dataType: 'json'
        });

    });

    $('#prefSubmit').on('click', function (e) {

        e.preventDefault();

        var first = [];

        if (validateForm(this)) {
            $('.key-config-input').each(function () {
                $(this).css('background-color', "white");
            });
            var userKeysInt = {};
            $('fieldset input').each(function () {

                userKeysInt[$(this).attr('id')] = $(this).val().charCodeAt(0);
            });

            $.ajax({
                type: "POST",
                url: "/flashcard/saveQuizPrefs",
                data: {data: userKeysInt},
                success: function (data) {

                    prefs = data.data;

                },
                dataType: 'json'
            });

            $('#closePrefs').trigger("click");
        } else {
            var elems = $('.key-config-input');
            $(elems).each(function () {
                var color = $(this).css("background-color");
                if (color == "rgb(238, 180, 180)") {
                    if (first == null) {

                    }
                    first.push($(this));

                }
            });
            confirm("Cannot have the same key assigned to two different buttons.");
            $(first[0]).val("").focus();
        }
    });

    $(document).keydown(function (event) {

        if($("#newPromptWin").is(":visible") || $("#userAnswer").is(":focus"))
            return;
        event.stopPropagation();
        if(answered) {
            if (prefs) {
                for (var i in prefs) {

                    if (event.which === parseInt(prefs[i])) {


                        var button = $('#' + i);
                        $(button).trigger('click');
                        break;
                    }
                }

                if ($('#newHolder').parent().find('.card').length) {
                    // There is a card in the answer box
                    switch (event.which) {
                        case 49: // the number one
                            switch (modifierKey) {
                                case "Ctrl":
                                    //alert("Ctrl-1");
                                    modifierKey = null;
                                    break;
                                case "Alt":
                                    //alert("Alt-1");
                                    modifierKey = null;
                                    break;
                                case null:
                                    //alert("1");
                                    modifierKey = null;
                                    break;
                            }
                            break;
                        case 50: // the number two
                            //alert("2");
                            break;
                        case 51: // the number three
                            //alert("3");
                            break;
                    }
                }
            }
        }
    });


    $("#cardDeck").on("click", 'div[id^="card_"]:last-child', function (e) {
        e.preventDefault();

        var lastnum = parseInt($("#currcardnum").text());
        lastnum = lastnum+1;

        $("#currcardnum").text(lastnum);

        if ($('#ratings').find('.active').length == 0 && $('#newHolder').parent().find('.card').length) {

            alert("Must rate this card.");

        } else if ($('#rightWrong').find('.active').length == 0 && $('#newHolder').parent().find('.card').length) {
            alert("Must select whether you were right or wrong.");
        } else if ($('#newHolder').parent().find('.card').length == 0 && $('#cardHolder').parent().find('.card').length) {
            alert("Can't draw a card until you submit your answer for the current card.");
        } else if ($('#cardHolder').parent().find('.card').length) {
            alert("Can't draw a card until you answer the current card.");
        } else {
            if (!qtStarted) {
                quizTimer.start();
                qtStarted = true;
            }
            var x = $('#cardHolder').offset().left - $("#cardDeck").offset().left;
            var y = $('#cardHolder').offset().top + $("#cardDeck").offset().top;
            var myx = $(this).offset().left;
            var self = this;
            hintUsed = 0;

            $.each(cardsInSet, function (i, val) {

                if (val.id == $(self).attr('name')) {
                    currentCard = this;
                    $("div#modified_datediv > input#cardid").val(val.id);
                    firstprompt = 0;
                }
            });


            $(this).animate({
                    //left: x
                }, speed, function () {

                    var thisx = $(this).offset().left;
                    var thisy = $(this).offset().top;
                    var clone = $(this).clone();
                    var cardHolder = $('#cardHolder').clone();
                    $('#cardHolder').remove();
                    $(this).remove();

                    for(var i = 0; i < linkage.length;i++){
                        var link = linkage[i];
                        var currcardwrong = 0;
                        if(link.id==currentCard.id){
                            console.log("found");
                            linked = true;
                            for (var k in allreadyWrong){
                                if (typeof allreadyWrong[k] !== 'function' &&
                                        k == currentCard.id) {
                                    currcardwrong = allreadyWrong[k];
                                }
                            }
                            // Below handles prompt triggers
                            if(link.alarmTime>0){
                                setInterval(function  () {
                                        checkTimer(cardtimer, parseInt(link.alarmTime))}, 100
                                );
                            }else if(parseInt(link.alarmWrong)<=parseInt(currcardwrong)){
                                setInterval(function  () {
                                        checkTimer(cardtimer, 5)}, 100
                                );
                            }
                            break;
                        }
                    }



                    $('#currentCardDiv').append('<div id="currentHintDiv" class="current-hint-div"><div class="current-hint">' +
                    ((currentCard.hint) ? currentCard.hint : "No Hint") + '</div></div><div id="hintButton" class="hint-button"><span>Show hint</span></div>');
                    $('#currentHintDiv').hide();
                    $(clone).removeClass('shuffled');
                    $('#currentCardDiv').append(cardHolder);
                    $(clone).css({
                        //"top": ($(cardHolder).position().top) + "px",
                        //"left": ($(cardHolder).position().left) + "px",
                        "z-index": "0"
                    });
                    $('#currentCardDiv').append(clone);
                    $('#currentCardDiv').append('<div id="currentUserAnswerDiv" class="current-user-answer-div"><div class="current-user-answer">' +
                    '<textarea id="userAnswer" class="user-answer"></textarea><span>Your answer <span class="fa fa-arrow-up"></span></span><button type="submit">Submit</button></div></div>');
                    $('#currentUserAnswerDiv').hide();

                    $('#hintButton>span').bind("click", function () {
                        hintUsed = 1;
                        $('#currentHintDiv').slideToggle();
                    });

                    $('.current-user-answer textarea').focus();

                    $('.current-user-answer > button').bind("click", function () {

                        $('.current-user-answer textarea').blur();

                        userAnswer = $('#userAnswer').val();
                        if ($('#answeredCardsDiv #newHolder').length == 0)
                            $('#answeredCardsDiv').append($('#cardHolder').clone().attr('id', 'newHolder').css('top', '50px'));
                        var x = $('#newHolder').offset().left - $('#currentCardDiv').offset().left;
                        var y = $('#newHolder').position().top;
                        cardtimer.stop();

                        if ($('#cardDeck .card').length == 0) {

                            endQuiz();
                        }

                        $('#currentCardDiv div[id^="card_"]').animate({
                            left: x,
                            top: y
                        }, speed, function () {

                            var thisx = $(this).offset().left;
                            var thisy = $(this).offset().top;
                            var cardClone = $(this).clone();
                            cardHolder = $('#newHolder').clone();
                            $(this).remove();
                            $('#answeredCardsDiv').append(cardClone);
                            answered = 1;
                            $(cardClone).css({
                                "top": ($('#newHolder').position().top) + "px",
                                "left": ($('#newHolder').position().left) + "px",
                                "z-index": "0"
                            });
                            $(cardClone).find('.front .pad').html(currentCard.answer);
                            $(cardClone).removeClass('flip');
                        });
                    });

                    $('.current-user-answer textarea').keydown(function (event) {

                        if (event.which == 13) {
                            event.preventDefault();
                            $('.current-user-answer > button').click();
                        }
                    });

                    $('#currentUserAnswerDiv').slideToggle(function () {
                        $('#userAnswer').css('background-color', '#CCEBCC').focus();
                    });

                    $(clone).addClass("flip");
                    setTimeout(cardtimer.start, 1000);
                }
            );

        }
    });

    $('#selectCardset').on('change', function (e) {

        if ($('#answeredCardsDiv .inactive-card').length > 0) {

            quizFinished();
            return;
        } else {
            window.location.href = "/flashcard/quizzes/" + $('#selectCardset').val();
        }

    });

    $('#closePrefs').on('click', function (e) {
        $('#configWin').toggle('slide', {direction: 'up'}, 1000);
    });

    $('.quiz-menu-bar ul li').on('click', function (e) {

        if ($(this).text() == "Preferences") {

            $('#configWin').toggle('slide', {direction: 'up'}, 500);

        }

    });



    if (incQuiz) {

        $("#selectCardset").find('option[value="0"]').removeAttr("selected");
        $('#selectCardset option[value=' + incQuiz + ']').attr('selected', 'selected');

        var returnedCardsHTML = "";

        if ($('#quizIndexContent li').length) {
            $('#quizIndexContent li').text($('#selectCardset option:selected').text());
        } else {
            $('#quizIndexContent').prepend('<li style="list-style-type:none">' + $('#selectCardset option:selected').text() + '    [Card: <span id="currcardnum">0</span> of <span id="totalcardnum">0</span>]</li>');
        }

        $('#selectCardset option[value=' + $('#selectCardset option:selected').val() + ']').attr('selected', 'selected');

        $('#currentCardDiv').find('*').not('.card-holder').remove();
        $('#answeredCardsDiv').find('*').not('.card-holder').remove();


        $.ajax({
            type: "POST",
            url: "/quizzes/getCardsByCardsetAJAX",
            data: $('#selectCardset').serialize(),
            success: function (data) {

                cardsInSet = data;

                cardsInSet = shuffleDeck(cardsInSet);

                var cardCount = quiz['number_of_questions'] = cardsInSet.length;
                $("#totalcardnum").text(cardCount);
                quiz['cardset_id'] = $('#selectCardset').val();


                $.each(cardsInSet, function (i, val) {

                    returnedCardsHTML += '<div id="card_' + val.id + '" name="' + val.id + '" class="panel card shuffled">' +

                        '<div id="front" class="front"><div class="pad"><p>' +

                        '</p></div></div>' +
                        '<div id="back" class="back" style="">' +
                        '<div class="pad"><p>' +
                        val.question +
                        '</p></div></div>' +

                        '</div>'
                });

                $('#cardDeck').empty().html(returnedCardsHTML);

                drawDeck();

                $('.select-quiz-set').hide();

            },
            dataType: 'json'
        });

        var judge = {"cardset_ID":incQuiz};
        //judge.push({"cardset_ID":incQuiz});

        /** how many this user can get wrong on this cardset **/
        $.ajax({
            type: "POST",
            url: "/flashcard/getWrongAJAX",
            data: judge,
            success: function (data) {

                console.log(data);

            },
            dataType: 'json'
        }).done(function(data) {
            var wrongcards = data.data;
            ids = new Array();
            for(var i = 0; i < wrongcards.length; i++) {
                //console.log(wrongcards[i].card_id);
                ids.push(wrongcards[i].card_id);
            }

            //console.log(ids);

            allreadyWrong = ids.reduce(function (acc, curr) {
                if (typeof acc[curr] == 'undefined') {
                    acc[curr] = 1;
                } else {
                    acc[curr] += 1;
                }

                return acc;
            }, {});
            //console.log(allreadyWrong);
        });
    }

    function checkTimer(timer, trigger) {

        var timerVal = timer.getTime();
        var value = timerVal.split(" ");
        value = value[0];

        console.log(value);

        if(value < trigger) {
            console.log(allreadyWrong);
            console.log(trigger);
            console.log(linkage);
        }



        //console.log(timerVal);
        if(Math.floor(value)==trigger && !firstprompt) {
            console.log(firstprompt);
            console.log(linkage);
            console.log(currentCard.id);
            firstprompt = !firstprompt;
            for(var c in linkage) {
                console.log(linkage[c]);
                var card = linkage[c];
                if(currentCard.id==card.id){
                    /** **/
                    console.log("found");
                    console.log(card.prompt_text);
                    showPrompt(card, currentCard);
                }
            }

        }


    }

    function checkToCreatePrompt() {

        var qrybox = getMessageBox("Create Personal Cue?", "Would you like to create a Personal Cue?", ["Yes","No"]);
        $("body").append(qrybox);

        var buttons = $(qrybox).find("#queryButtons").children("button");
        var buttonClicked;

        buttons.each(function(indx, item){
            $(this).bind("click", function(){
                buttonClicked = $(this).attr("id");
                switch (buttonClicked) {
                    case "yes" :
                        showCreatePrompt();
                    case "no" :
                        break;
                }
                $(qrybox).remove();
                //return buttonClicked;
            });
        });

        $(qrybox).show();


        //return window.confirm("Would you like to create a prompt for this card?");

    }

    function showCreatePrompt() {

        if($("div.new-card-prompt-div").hasClass("hidden")) {
            $("div.new-card-prompt-div").removeClass("hidden")
        }
        $("div.new-card-prompt-div").toggle();
    }

    function showPrompt(linkage, card){
        console.log(linkage);
        var t = linkage.prompt_text?1:0;
        //alert("Show: "+t);

        var elem = $("<div></div>")
            .attr("id","promptWin")
            .addClass("prompt-window")
            .append("<div></div><div></div><div></div>")
            .appendTo("body");
        var divs = $("div#promptWin").find("div");
        $(divs).each(function(ind, val) {
                console.log(ind);
                console.log(val);
                var elvals = ["name","prompt-text","card"];
                $(val).attr("id",elvals[ind]);

                switch(ind){
                    case 0:
                        break;
                    case 1:
                        $(val).html(linkage.prompt_text);
                        break;
                    case 2:
                        $(val).html("<input name='"+elvals[ind]+"' type='hidden' value='"+linkage.id+"' >");
                        break;
                }

            });

        var right = Math.floor((Math.random() * 2) + 1);
        var top = Math.floor((Math.random() * 2) + 1);
        var rpx = 0;
        var tpx = 0;
        console.log($(elem));
        //alert(right+":"+top);
        if(right==1) {
            rpx = Math.floor((Math.random() * 400) + 41);
            $("div#promptWin").css({"right":rpx+"px"});
            tpx = Math.floor((Math.random() * 225) + 201);
            //alert("right-1");
        }else {
            rpx = Math.floor((Math.random() * 300) + 41);
            $("div#promptWin").css({"left":rpx+"px"});
            tpx = Math.floor((Math.random() * 200) + 226);
            //alert("right-2");
        }

        if(top==1) {
            //alert("top-1");
            $("div#promptWin").css({"top":tpx+"px"});
        }else {
            //alert("top-2");
            $("div#promptWin").css({"bottom":tpx+"px"});
        }

        if(t)
            $(elem).show();
        else
            alert("default");
    }

    function shuffleDeck(cardDeck) {

        var n = Math.floor(400 * Math.random() + 500);
        var numberOfCards = cardDeck.length;

        for (i = 1; i < n; i++) {
            card1 = Math.floor(numberOfCards * Math.random());
            card2 = Math.floor(numberOfCards * Math.random());
            var temp = cardDeck[card2];
            cardDeck[card2] = cardDeck[card1];
            cardDeck[card1] = temp;
        }
        return cardDeck;
    }

    function drawDeck() {

        var topDeck = 150;
        var numCards = $('.card.shuffled').length;
        var leftDeck = $('.card-deck-shuffled').offset().left;
        var setLeft = parseInt($('.card-deck-shuffled').width() / 3);
        var startLeft = setLeft;
        var startTop = topDeck;
        var ZIdx = 0;
        var count = 0;
        var topVrnt = 3;
        var leftVrnt = 3;
        console.log("numcards= " + numCards);
        if (numCards > 40) {
            topDeck = 100;
            topVrnt = 2;
            leftVrnt = 2;
        } else if (numCards > 90) {
            topDeck = 50;
            topVrnt = 2;
        } else if (numCards > 140) {
            topDeck = 0;
            topVrnt = 1;
        }

        $.each($('.card.shuffled'), function (k, v) {

            //$(this).css("top", (topDeck + "px"));
            //$(this).css("left", (setLeft + "px"));
            $(this).css("z-index", (ZIdx));

            ZIdx += 1;

        });
        $('#cardHolder').css({"top": ((topDeck) + "px"), "left": ((setLeft) + "px")});

    }

    function setSpeedMode(elem) {

        if ($(elem).is(':checked')) {
            speedBool = true;

            if ($(document).find('.panel .back').length > 0 &&
                $(document).find('.panel .front').length > 0) {

                // 	alert('checked and cards found');
                $('.panel .back, .panel .front').css({
                    '-webkit-transition-duration': '0.01s',
                    'transition-duration': '0.01s'
                });
                $('.panel .back, .panel .front').css({
                    '-webkit-animation-duration': '0.01s',
                    'animation-duration': '0.01s'
                });
                speed = '1';
            } else {
                // 	alert("no cards");
            }

        } else {
            speedBool = false;
        }

    }

    /**
     * function to keep quiz data stored and up-to-date
     *
     * @param qData - quiz data for the current card
     */
    function storeQuiz(qData) {



        console.log(qData);

        cardsInSet[cardIndex]["response"] = qData;

    }
    /**
     * function to save quiz data to the database
     *
     * @param qData - quiz data for the current card
     */
    function saveQuiz() {
        var save = {};

        quiz['percent_correct'] = $("div#displayPercent > #text").text();
        quiz['quiz_length'] = $("div#totalAnswered > #text").text();
        quiz['quiz_length'] += (parseInt($("div#totalAnswered > #text").text()) > 1) ? " cards viewed" : " card viewed";

        save['quizdata'] = quiz;
        save['cards'] = cardsInSet;

        console.log(save);

        $.ajax({
            type: "POST",
            data: {"quiz": JSON.stringify(save)},
            url: "/quizzes/saveQuiz",
            success: function (data) {
                console.log(data);

                alert("Quiz has been saved.");

                window.location.href = "/flashcard/quizzes/" + $('#selectCardset').val();
            },
            dataType: 'json'
        });


    }
    function quizFinished() {
        getQuizTime();
        //alert("quiz finished at "+quiz['end_time']+" on " +quiz['date_taken']);
        saveQuiz();
    }
    function getQuizTime() {

        stopQuizTimer();
        var d = new Date();
        var endTimeString = (d.getHours() < 10) ? "0" + d.getHours() : d.getHours();
        endTimeString += ":";
        endTimeString += (d.getMinutes() < 10) ? "0" + d.getMinutes() : d.getMinutes();
        endTimeString += ":";
        endTimeString += (d.getSeconds() < 10) ? "0" + d.getSeconds() : d.getSeconds();

        quiz['end_time'] = endTimeString;
        quiz['date_taken'] = d.getFullYear() + "-" + ((d.getMonth() < 9) ? "0" + (d.getMonth()+1) : d.getMonth()+1) + "-" + d.getDate();


    }

    function stopQuizTimer() {

        quizTimer.stop();

        var timerVal = quizTimer.getTime();
        console.log(timerVal);
        timerVal = timerVal.split(' ');
        timerVal = parseInt(timerVal[0]);
        var hours = 0;
        var mins = 0;
        var secs = 0;
        alert(parseFloat(timerVal)+ "secs");
        if (timerVal > 3600) {
            //hours
            hours = parseInt(timerVal / 3600);
            timerVal -= (hours * 3600);
            mins = parseInt(timerVal / 60);
            timerVal -= (mins * 60);
            secs = parseInt(timerVal);
        } else if (timerVal > 60) {
            //minutes
            // alert('mins');
            mins = parseInt(timerVal / 60);
            timerVal -= (mins * 60);
            secs = parseInt(timerVal);
        } else if (timerVal > 0) {
            // seconds
            // alert('secs');
            secs = parseInt(timerVal);
        }
        if (hours < 10) {
            hours = "0" + hours;
        }
        if (mins < 10) {
            mins = "0" + mins;
        }
        if (secs < 10) {
            secs = "0" + secs;
        }


        quiz['quiz_time'] = hours + ":" + mins + ":" + secs;

    }

    function gradedAndRated() {
        $('#ratings').find('.active').removeClass('active');
        $('#rightWrong').find('.active').removeClass('active');
        var cardHolder = $('#cardHolder').clone();
        $('#currentCardDiv').empty().append(cardHolder);
        $('#newHolder').parent().find('.card').removeClass('card').addClass('inactive-card').animate({
            top: inactiveY,
            left: 3
        });
        inactiveY += 5;
        cardtimer.reset();

        // $('#cardDeck>div[id^="card_"]:last-child').trigger("click");
    }

    function cardTimer(elem, options, buttons) {

        buttons = buttons || 0;

        var offset,
            clock,
            interval;

        if (!(elem === undefined)) {
            var timer = createTimer();
            if (buttons) {
                var startButton = createButton("start", start),
                    stopButton = createButton("stop", stop),
                    resetButton = createButton("reset", reset);
            }
        }
        // default options
        options = options || {};
        options.delay = options.delay || 1;

        // append elements
        if (!(elem === undefined)) {
            elem.append(timer);
            if (buttons) {
                elem.append(startButton);
                elem.append(stopButton);
                elem.append(resetButton);
            }
        }

        // initialize
        reset();

        // private functions
        function createTimer() {
            return document.createElement("span");
        }

        function createButton(action, handler) {
            var a = document.createElement("a");
            a.href = "#" + action;
            a.innerHTML = action;
            a.addEventListener("click", function (event) {
                handler();
                event.preventDefault();
            });
            return a;
        }

        function getTime() {
            return clock / 1000 + " secs";
        }

        function start() {
            if (!interval) {
                offset = Date.now();
                interval = setInterval(update, options.delay);
            }
        }

        function stop() {
            console.log(clock / 1000);

            if (interval) {
                clearInterval(interval);
                interval = null;
            }
        }

        function reset() {
            clock = 0;
            if (!(elem === undefined)) render(0);
        }

        function update() {
            clock += delta();
            if (!(elem === undefined)) render();
        }

        function render() {
            timer.innerHTML = clock / 1000 + " secs";
        }

        function delta() {
            var now = Date.now(),
                d = now - offset;

            offset = now;
            return d;
        }

        // public API
        this.start = start;
        this.stop = stop;
        this.reset = reset;
        this.getTime = getTime;
    }

});