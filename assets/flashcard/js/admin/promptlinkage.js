/**
 * Created by Scott on 7/3/2015.
 */
$(document).ready(function () {


    var allPromptLinkageTableDT = $('#allPromptLinkageTable').DataTable({
        "ajax": {
            "url": "/fcadmin/getLinkageAJAX",
            // "data": (getAJAXtoken()),
            "type": "POST"
        },
        /**
         * flash_card_prompts.id as promptid,
         flash_card_prompts.prompt_text,
         flash_card_prompts.prompt_added_date,
         flash_card_prompts.modified_date as prompt_modified_date,
         flash_card_prompts.userid_added as prompt_userid_added,
         flash_card_prompts.private as prompt_private,
         flash_card_prompts.active as prompt_active,
         flash_card_users.username,
         flash_card_prompt_user_card.alarmWrong,
         flash_card_prompt_user_card.alarmTime,
         flash_card_prompt_user_card.last_modified as alarm_last_modified,
         flash_card_cards.question as card_question,
         flash_card_cards.id as card_id
         */
        "columns": [
            {"data": "promptid"},
            {"data": "username"},
            {"data": "card_question"},
            {"data": "card_id"},
            {"data": "prompt_text"},
            {"data": "prompt_added_date"},
            {"data": "prompt_modified_date"},
            {"data": "prompt_userid_added"},
            {"data": "alarmWrong"},
            {"data": "alarmTime"},
            {"data": "alarm_last_modified"},
            {"data": "prompt_private"},
            {"data": "prompt_active"}
        ],
        "columnDefs": [
            {
                "className": "center", "targets": [0,1,2,3,4,5,6,7,8,9,10,11,12,13]
            },
            {
                "targets" : 0,
                "title": "Prompt ID",
                "render": function ( data, type, full, meta ) {

                    return data;
                }
            },
            {
                "targets" : 1,
                "title": "Added by",
                "render": function ( data, type, full, meta ) {
                    return data;
                }
            },
            {
                "targets" : 2,
                "title": "Question",
                "render": function ( data, type, full, meta ) {

                    return data;
                }
            },
            {
                "targets" : 3,
                "title": "Card ID",
                "visible": false,
                "render": function ( data, type, full, meta ) {

                    return data;
                }
            },
            {
                "targets" : 4,
                "title": "Prompt Text",
                "render": function ( data, type, full, meta ) {

                    return data;
                }
            },
            {
                "targets" : 5,
                "title": "Prompt Added Date",
                "render": function ( data, type, full, meta ) {

                    return data;
                }
            },
            {
                "targets" : 6,
                "title": "Prompt Modified Date",
                "render": function ( data, type, full, meta ) {

                    return data;
                }
            },
            {
                "targets" : 7,
                "title": "Prompt User ID Added",
                "visible": false,
                "render": function ( data, type, full, meta ) {

                    return data;
                }
            },
            {
                "targets" : 8,
                "title": "Alarm on # Wrong",
                "render": function ( data, type, full, meta ) {
                    return parseInt(data);
                }
            },
            {
                "targets" : 9,
                "title": "Alarm on Seconds Passed",
                "render": function ( data, type, full, meta ) {
                    return parseInt(data)+" secs";
                }
            },
            {
                "targets" : 10,
                "title": "Alarm Last modified",
                "render": function ( data, type, full, meta ) {

                    return data;
                }
            },
            {
                "targets" : 11,
                "title": "Prompt Private",
                "render": function ( data, type, full, meta ) {

                    return data;
                }
            },
            {
                "targets" : 12,
                "title": "Prompt Active",
                "render": function ( data, type, full, meta ) {

                    return data;
                }
            },
            {
                "targets" : 13,
                "title": "Admin",
                "render": function ( data, type, full, meta) {
                    var html = '<button class="admin-button edit btn btn-primary"'+
                        ' data-toggle="modal" data-target="#modifyPromptWindow">Edit</button>';
                    return html;
                },
                "className": "admin-buttons"
            }
        ],
        "drawCallback": function( settings ) {
            // alert( 'DataTables has redrawn the table' );

            if ($('#mainArea').is(':animated')) {

            } else {
                setTimeout(function() {
                    $('#mainArea').slideDown(5000);
                },250);
            }


        }
    });

    $('#allPromptLinkageTable tbody')
        .on( 'mouseover', 'tr', function () {
            $(this).addClass('selected');
        } )
        .on( 'mouseleave', 'tr', function () {
            $(this).removeClass( 'selected' );
        } )
        .on( 'click', 'td button', function () {
            // alert("gotcha "+$(this).text());
            if($(this).text()=='Edit') {
                var page = allPromptLinkageTableDT.page.info();
                //rowNdx = $(this).parent().parent().prop('rowIndex') - 1;
                console.log(allPromptLinkageTableDT);
                //alert('Row index: ' + $(this).closest('tr').index());
                showEditScreen(allPromptLinkageTableDT.row( ($(this).closest('tr').index())+(page.start) ).data());
            }
        }
    );


    tinymce.init({
        selector: '#prompt',
        width: 300,
        height: 120,
        menubar: false,
        statusbar: false,
        setup: function (editor) {
            editor.on('change', function () {
                tinymce.triggerSave();
            });
        }
    });

    $("#updatePromptInfoForm").on("submit", function(e) {
        e.preventDefault();

        var promptWin = $("#modifyPromptWindow");

        var elem = $(promptWin).find(".modal-title");
        var text = $(elem).text();

        //console.log($('#updatePromptInfoForm #date input').val(new Date()));
        var d = new Date($.now());
        var curr_day = (d.getDate()<10?"0"+ d.getDate(): d.getDate());
        var curr_month = (d.getMonth()<10?"0"+ d.getMonth(): d.getMonth());
        var curr_year = d.getFullYear();
        var curr_hour = (d.getHours()<10?"0"+ d.getHours(): d.getHours());
        var curr_mins = (d.getMinutes()<10?"0"+ d.getMinutes(): d.getMinutes());
        var curr_secs = (d.getSeconds()<10?"0"+ d.getSeconds(): d.getSeconds());


        if(text.indexOf("Edit")>-1) {

            //$('#updatePromptInfoForm input#modified_date').val(curr_year+"-"+curr_month+"-"+curr_day+" "+curr_hour+":"+curr_mins+":"+curr_secs);
            $('#updatePromptInfoForm input#modified_date ').val(curr_year+"-"+curr_month+"-"+curr_day+" "+curr_hour+":"+curr_mins+":"+curr_secs);

            //console.log($('#updatePromptInfoForm').serialize());

            //return;

            $.ajax({
                type: "POST",
                url: "/fcadmin/updatePrompt/",
                data: $('#updatePromptInfoForm').serialize(),// + getAJAXtoken(),
                success: function (data) {
                    console.log(data);
                    // $("#selectCardset").val(0);
                    // $("#selectCardset").val(cardsetid);
                    $('#modifyPromptWindow').modal('toggle');
                    allPromptLinkageTableDT.clear();
                    allPromptLinkageTableDT.ajax.reload();
                },
                dataType: 'json'
            });
        } else if(text.indexOf("Add")>-1) {

            $('#updatePromptInfoForm #date input').val(curr_year+"-"+curr_month+"-"+curr_day+" "+curr_hour+":"+curr_mins+":"+curr_secs);
            $('#updatePromptInfoForm input#modified_date ').val(curr_year+"-"+curr_month+"-"+curr_day+" "+curr_hour+":"+curr_mins+":"+curr_secs);

            $.ajax({
                type: "POST",
                url: "/fcadmin/addPrompt/",
                data: $('#updatePromptInfoForm').serialize(),// + getAJAXtoken(),
                success: function (data) {
                    console.log(data);
                    // $("#selectCardset").val(0);
                    // $("#selectCardset").val(cardsetid);
                    $('#modifyPromptWindow').modal('toggle');
                    allPromptLinkageTableDT.clear();
                    allPromptLinkageTableDT.ajax.reload();
                },
                dataType: 'json'
            });
        }


    });

    $("#newPromptSubmitButton").click(function (e) {
        // To Do
        //alert("submit");
        e.preventDefault();

        console.log($.isNumeric($("input#alarm").val()));
        console.log($.isNumeric($("input#wrong").val()) &&
            $.isNumeric($("input#alarm").val()) );
        // alert("submit");
        // var form = $('#newCardForm').serialize();
        if($("input#wrong").val()=="0" &&
            $("input#alarm").val() == "0" ) {
            //pop alert
            alert("Error");
        }else if(!$.isNumeric($("input#wrong").val()) ||
            !$.isNumeric($("input#alarm").val()) ) {
            alert("Non-number entered");

        }else {

            $("#updatePromptInfoForm").submit();
        }
    });

    //$("input#alarm").on("focus", function(e){
    //
    //    e.preventDefault();
    //    $("input#wrong").val("0");
    //    alert("alarm focus");
    //});
    //
    //$("input#wrong").on("focus", function(e){
    //
    //    e.preventDefault();
    //    $("input#alarm").val("0");
    //    alert("wrong focus");
    //});


    function showEditScreen(data) {
        console.log(data);
        console.log($(promptWin).find('#alarmon > input#wrong'));
        // alert('Got it');prompt

        document.getElementById("updatePromptInfoForm").reset();
        $(tinymce.get('prompt').getBody()).html("");

        var promptWin = $("#modifyPromptWindow");

        var elem = $(promptWin).find(".modal-title");
        var text = $(elem).text();

        text = text.replace("Add","Edit");
        $(elem).text(text);

        $('input#id').val(data.id);
        $(tinymce.get('prompt').getBody()).html(data.prompt_text);
        $('input#credit').val(data.prompt_credit);
        $('input#date').val(data.prompt_added_date);
        $('input#alarm').val(data.alarmTime);
        $(promptWin).find('input#wrong').val(data.alarmWrong);

        if(data.active==1) {
            $('#active').prop('checked', true).css({"background-color":"green"});
        } else {
            $('#active').prop('checked', false).css({"background-color":"red"});
        }

        if(data.private==1) {
            $('#private').prop('checked', true).css({"background-color":"green"});
        } else {
            $('#private').prop('checked', false).css({"background-color":"red"});
        }

        tinyMCE.triggerSave();

    }


});

function openAddPromptLinkage() {

    document.getElementById("updatePromptInfoForm").reset();
    $(tinymce.get('prompt').getBody()).html("");

    var promptWin = $("#modifyPromptWindow");

    var elem = $(promptWin).find(".modal-title");
    var text = $(elem).text();

    text = text.replace("Edit","Add");
    $(elem).text(text);
    $(promptWin).modal('show');

}