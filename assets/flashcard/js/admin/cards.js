$(document).ready(function () {

	$('#allCardsTable tbody')
        .on( 'mouseover', 'tr', function () {
            $(this).addClass('selected');
        } )
        .on( 'mouseleave', 'tr', function () {
            $(this).removeClass( 'selected' );
        } )
        .on( 'click', 'td button', function () {
        	// alert("gotcha "+$(this).text());
        	if($(this).text()=='Edit') {
        		
        	}
        }
    );

	var allCardsTableDT = $('#allCardsTable').DataTable({
        "order": [[ 1, 'asc' ]],
        "ajax": {
        	"url": "/fcadmin/getAllCardsAJAX",
            // "data": (getAJAXtoken()),
        	"type": "POST"
        },

        "columns": [
        	{"data": "id"},
        	{"data": "name"},
        	{"data": "description"},
        	{"data": "categorization"},
            {"data": "hint"},
            {"data": "question"},
            {"data": "answer"},
            {"data": "userId"},
            {"data": "cardset_id"},
            {"data": "created_on"},
            {"data": "modified_on"}
        ],
        "columnDefs": [
        	{
    			"className": "center", "targets": [1,2,3,4]
        	},
        	{
        		"targets" : 0,
        		"title": "ID",
        		"render": function ( data, type, full, meta ) {
        			
			      return data;
			    }
        	},
        	{
        		"targets" : 1,
        		"title": "Card Name",
        		"render": function ( data, type, full, meta ) {
        			return data;
			    }
        	},
        	{
        		"targets" : 2,
        		"title": "Description",
        		"render": function ( data, type, full, meta ) {
        			return data;
			    }
        	},
        	{
        		"targets" : 3,
        		"title": "Categorization",
        		"render": function ( data, type, full, meta ) {
        			
			      return data;
			    }
        	},
            {
                "targets" : 4,
                "title": "Hint",
                "render": function ( data, type, full, meta ) {
                    return data;
                }
            },
            {
                "targets" : 5,
                "title": "Question",
                "render": function ( data, type, full, meta ) {
                    return data;
                }
            },
            {
                "targets" : 6,
                "title": "Answer",
                "render": function ( data, type, full, meta ) {
                    
                  return data;
                }
            },
            {
                "targets" : 7,
                "title": "UserId",
                "render": function ( data, type, full, meta ) {
                    return data;
                }
            },
            {
                "targets" : 8,
                "title": "Cardset ID",
                "render": function ( data, type, full, meta ) {
                    return data;
                }
            },
            {
                "targets" : 9,
                "title": "Created On",
                "render": function ( data, type, full, meta ) {
                    
                  return data;
                }
            },
            {
                "targets" : 10,
                "title": "Last Modified",
                "render": function ( data, type, full, meta ) {
                    return data;
                }
            },
        	{
        		"targets" : 11,
        		"title": "Admin",
        		"render": function ( data, type, full, meta) {
        			var html = '<button class="admin-button edit btn btn-primary"'+
        			' data-toggle="modal" data-target="#newCardWindow">Edit</button>';
        			return html;
        		},
        		"className": "admin-buttons"
        	}
        ],
        "drawCallback": function( settings ) {
	        // alert( 'DataTables has redrawn the table' );

	        if ($('#mainArea').is(':animated')) {
        		
        	} else {
        		setTimeout(function() {
		        	$('#mainArea').slideDown(5000);
		        },250);
        	}
	        

	    }
    });

	

});