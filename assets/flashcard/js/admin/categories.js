$(document).ready(function () {

	var allCategoriesTableDT = $('#allCategoriesTable').DataTable({
        "ajax": {
        	"url": "/fcadmin/getAllCategoriesAJAX",
            // "data": (getAJAXtoken()),
        	"type": "POST"
        },
		"order": [[ 1, 'asc' ]],
        "columns": [
        	{"data": "id"},
        	{"data": "name"},
        	{"data": "description"},
        	{"data": "categorization"},
            {"data": "userId"},
            {"data": "created_on"},
            {"data": "modified_on"}
        ],
        "columnDefs": [
        	{
    			"className": "center", "targets": [1,2,3,4]
        	},
        	{
        		"targets" : 0,
        		"title": "ID",
        		"render": function ( data, type, full, meta ) {
        			
			      return data;
			    }
        	},
        	{
        		"targets" : 1,
        		"title": "Category Name",
        		"render": function ( data, type, full, meta ) {
        			return data;
			    }
        	},
        	{
        		"targets" : 2,
        		"title": "Description",
        		"render": function ( data, type, full, meta ) {
        			return data;
			    }
        	},
        	{
        		"targets" : 3,
        		"title": "Categorization",
        		"render": function ( data, type, full, meta ) {
        			
			      return data;
			    }
        	},
            {
                "targets" : 4,
                "title": "Created On",
                "render": function ( data, type, full, meta ) {
                    return data;
                }
            },
            {
                "targets" : 5,
                "title": "Last Modified",
                "render": function ( data, type, full, meta ) {
                    
                  return data;
                }
            },
        	{
        		"targets" : 6,
        		"title": "Admin",
        		"render": function ( data, type, full, meta) {
        			var html = '<button class="admin-button edit btn btn-primary"'+
        			' data-toggle="modal" data-target="#modifyCategoryWindow">Edit</button>';
        			return html;
        		},
        		"className": "admin-buttons"
        	}
        ],
        "drawCallback": function( settings ) {
	        // alert( 'DataTables has redrawn the table' );

	        if ($('#mainArea').is(':animated')) {
        		
        	} else {
        		setTimeout(function() {
		        	$('#mainArea').slideDown(5000);
		        },250);
        	}
	        

	    }
    });

	tinymce.init({
		selector: '#categoryDescription',
		width: 300,
		height: 120,
		menubar: false,
		statusbar: false,
		setup: function (editor) {
			editor.on('change', function () {
				tinymce.triggerSave();
			});
		}
	});

	$("#updateCategoryInfoForm").on("submit", function(e) {
		e.preventDefault();

		$('#categoryCategorization').val($('#categoryCategorization option:last-child').val());

		var d = new Date($.now());
		var curr_day = d.getDate();
		var curr_month = d.getMonth();
		var curr_year = d.getFullYear();
		var curr_hour = d.getHours();
		var curr_mins = d.getMinutes();
		var curr_secs = d.getSeconds();
		$("#modified_on").val(curr_year+"-"+curr_month+"-"+curr_day+" "+curr_hour+":"+curr_mins+":"+curr_secs);
		// console.log($('#cardSetCategorization').val());

		$.ajax({
			type: "POST",
			url: "/flashcard/updateCardset",
			data: $('#updateCategoryInfoForm').serialize(),
			success: function (data) {
				console.log(data);
				// $("#selectCardset").val(0);
				// $("#selectCardset").val(cardsetid);
				$('#modifyCategoryWindow').modal('toggle');
				allCategoriesTableDT.clear();
				allCategoriesTableDT.ajax.reload();
			},
			dataType: 'json'
		});
	});

	$("#newCategorySubmitButton").click(function (e) {
		// To Do
		e.preventDefault();
		// alert("submit");
		// var form = $('#newCardForm').serialize();
		$("#updateCategoryInfoForm").submit();
	});

	$('#allCategoriesTable tbody')
		.on( 'mouseover', 'tr', function () {
			$(this).addClass('selected');
		} )
		.on( 'mouseleave', 'tr', function () {
			$(this).removeClass( 'selected' );
		} )
		.on( 'click', 'td button', function () {
			// alert("gotcha "+$(this).text());
			if($(this).text()=='Edit') {
				//rowNdx = $(this).parent().parent().prop('rowIndex') - 1;
				//console.log($(this));
				//alert('Row index: ' + $(this).closest('tr').index());
				var page = allCategoriesTableDT.page.info();
				showEditScreen(allCategoriesTableDT.row( ($(this).closest('tr').index())+(page.start) ).data());
			}
		}
	);

});

function showEditScreen(data) {

//var test = tinymce.get('categoryDescription');
 console.log(data);//alert("hi");

	$('#updateCategoryInfoForm > div > #id').val(data.id);
	$('#updateCategoryInfoForm > div > #name').val(data.name);
	$('#updateCategoryInfoForm > div > #created_on').val(data.created_on);
	getSelectCategorization(data.categorization);
	$(tinymce.get('categoryDescription').getBody()).html(data.description);
	$('#updateCategoryInfoForm > div > #modified_on').val(data.modified_on);


}