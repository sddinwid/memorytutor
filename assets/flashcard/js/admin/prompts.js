/**
 * Created by Scott on 7/3/2015.
 */
$(document).ready(function () {


    var allPromptsTableDT = $('#allPromptsTable').DataTable({
        "ajax": {
            "url": "/flashcard/getAllPromptsAJAX",
            // "data": (getAJAXtoken()),
            "type": "POST"
        },
        /**
         *  1   id	            int(12)
         2	prompt_text	    text
         3	prompt_category	int(11)
         4	prompt_added_date	    date
         5	modified_date	date
         6	userid_added	int(11)
         7	private	        tinyint(1)
         8	active          tinyint(1)
         */
        "columns": [
            {"data": "id"},
            {"data": "prompt_text"},
            {"data": "prompt_category"},
            {"data": "prompt_added_date"},
            {"data": "modified_date"},
            {"data": "userid_added"},
            {"data": "private"},
            {"data": "active"}
        ],
        "columnDefs": [
            {
                "className": "center", "targets": [1,2,3,4,5,6,7,8]
            },
            {
                "targets" : 0,
                "title": "ID",
                "render": function ( data, type, full, meta ) {

                    return data;
                }
            },
            {
                "targets" : 1,
                "title": "Prompt Text",
                "render": function ( data, type, full, meta ) {
                    return data;
                }
            },
            {
                "targets" : 2,
                "title": "Prompt Category",
                "visible": false,
                "render": function ( data, type, full, meta ) {
                    return data;
                }
            },
            {
                "targets" : 3,
                "title": "Prompt Added Date",
                "render": function ( data, type, full, meta ) {

                    return data;
                }
            },
            {
                "targets" : 4,
                "title": "Modified Date",
                "render": function ( data, type, full, meta ) {

                    return data;
                }
            },
            {
                "targets" : 5,
                "title": "User Added",
                "render": function ( data, type, full, meta ) {

                    return data;
                }
            },
            {
                "targets" : 6,
                "title": "Private",
                "render": function ( data, type, full, meta ) {

                    return parseInt(data)?"True":"False";
                }
            },
            {
                "targets" : 7,
                "title": "Active",
                "render": function ( data, type, full, meta ) {
                    return parseInt(data)?"True":"False";
                }
            },
            {
                "targets" : 8,
                "title": "Admin",
                "render": function ( data, type, full, meta) {
                    var html = '<button class="admin-button edit btn btn-primary"'+
                        ' data-toggle="modal" data-target="#modifyPromptWindow">Edit</button>';
                    return html;
                },
                "className": "admin-buttons"
            }
        ],
        "drawCallback": function( settings ) {
            // alert( 'DataTables has redrawn the table' );

            if ($('#mainArea').is(':animated')) {

            } else {
                setTimeout(function() {
                    $('#mainArea').slideDown(5000);
                },250);
            }


        }
    });

    $('#allPromptsTable tbody')
        .on( 'mouseover', 'tr', function () {
            $(this).addClass('selected');
        } )
        .on( 'mouseleave', 'tr', function () {
            $(this).removeClass( 'selected' );
        } )
        .on( 'click', 'td button', function () {
            // alert("gotcha "+$(this).text());
            if($(this).text()=='Edit') {
                var page = allPromptsTableDT.page.info();
                //rowNdx = $(this).parent().parent().prop('rowIndex') - 1;
                console.log(allPromptsTableDT);
                //alert('Row index: ' + $(this).closest('tr').index());
                showEditScreen(allPromptsTableDT.row( ($(this).closest('tr').index())+(page.start) ).data());
            }
        }
    );


    tinymce.init({
        selector: '#prompt',
        width: 300,
        height: 120,
        menubar: false,
        statusbar: false,
        setup: function (editor) {
            editor.on('change', function () {
                tinymce.triggerSave();
            });
        }
    });

    $("#updatePromptInfoForm").on("submit", function(e) {
        e.preventDefault();

        var promptWin = $("#modifyPromptWindow");

        var elem = $(promptWin).find(".modal-title");
        var text = $(elem).text();

        //console.log($('#updatePromptInfoForm #date input').val(new Date()));
        var d = new Date($.now());
        var curr_day = (d.getDate()<10?"0"+ d.getDate(): d.getDate());
        var curr_month = (d.getMonth()<10?"0"+ d.getMonth(): d.getMonth());
        var curr_year = d.getFullYear();
        var curr_hour = (d.getHours()<10?"0"+ d.getHours(): d.getHours());
        var curr_mins = (d.getMinutes()<10?"0"+ d.getMinutes(): d.getMinutes());
        var curr_secs = (d.getSeconds()<10?"0"+ d.getSeconds(): d.getSeconds());


        if(text.indexOf("Edit")>-1) {

            //$('#updatePromptInfoForm input#modified_date').val(curr_year+"-"+curr_month+"-"+curr_day+" "+curr_hour+":"+curr_mins+":"+curr_secs);
            $('#updatePromptInfoForm input#modified_date ').val(curr_year+"-"+curr_month+"-"+curr_day+" "+curr_hour+":"+curr_mins+":"+curr_secs);

            //console.log($('#updatePromptInfoForm').serialize());

            //return;

            $.ajax({
                type: "POST",
                url: "/fcadmin/updatePrompt/",
                data: $('#updatePromptInfoForm').serialize(),// + getAJAXtoken(),
                success: function (data) {
                    console.log(data);
                    // $("#selectCardset").val(0);
                    // $("#selectCardset").val(cardsetid);
                    $('#modifyPromptWindow').modal('toggle');
                    allPromptsTableDT.clear();
                    allPromptsTableDT.ajax.reload();
                },
                dataType: 'json'
            });
        } else if(text.indexOf("Add")>-1) {

            $('#updatePromptInfoForm #date input').val(curr_year+"-"+curr_month+"-"+curr_day+" "+curr_hour+":"+curr_mins+":"+curr_secs);
            $('#updatePromptInfoForm input#modified_date ').val(curr_year+"-"+curr_month+"-"+curr_day+" "+curr_hour+":"+curr_mins+":"+curr_secs);

            $.ajax({
                type: "POST",
                url: "/fcadmin/addPrompt/",
                data: $('#updatePromptInfoForm').serialize(),// + getAJAXtoken(),
                success: function (data) {
                    console.log(data);
                    // $("#selectCardset").val(0);
                    // $("#selectCardset").val(cardsetid);
                    $('#modifyPromptWindow').modal('toggle');
                    allPromptsTableDT.clear();
                    allPromptsTableDT.ajax.reload();
                },
                dataType: 'json'
            });
        }


    });

    $("#newPromptSubmitButton").click(function (e) {
        // To Do
        //alert("submit");
        e.preventDefault();
        // alert("submit");
        // var form = $('#newCardForm').serialize();
        $("#updatePromptInfoForm").submit();
    });


    function showEditScreen(data) {
        console.log(data);
        // alert('Got it');prompt

        document.getElementById("updatePromptInfoForm").reset();
        $(tinymce.get('prompt').getBody()).html("");

        var promptWin = $("#modifyPromptWindow");

        var elem = $(promptWin).find(".modal-title");
        var text = $(elem).text();

        text = text.replace("Add","Edit");
        $(elem).text(text);

        $('input#id').val(data.id);
        $(tinymce.get('prompt').getBody()).html(data.prompt_text);
        $('input#credit').val(data.prompt_credit);
        $('input#date').val(data.prompt_added_date);
        $('input#modified_date').val(data.modified_date);

        if(data.active==1) {
            $('#active').prop('checked', true).css({"background-color":"green"});
        } else {
            $('#active').prop('checked', false).css({"background-color":"red"});
        }

        if(data.private==1) {
            $('#private').prop('checked', true).css({"background-color":"green"});
        } else {
            $('#private').prop('checked', false).css({"background-color":"red"});
        }

        tinyMCE.triggerSave();

    }


});

function openAddPrompt() {

    document.getElementById("updatePromptInfoForm").reset();
    $(tinymce.get('prompt').getBody()).html("");

    var promptWin = $("#modifyPromptWindow");

    var elem = $(promptWin).find(".modal-title");
    var text = $(elem).text();

    text = text.replace("Edit","Add");
    $(elem).text(text);
    $(promptWin).modal('show');

}