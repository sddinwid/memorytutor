$(document).ready(function () {

	$('#allCodesTable tbody')
        .on( 'mouseover', 'tr', function () {
            $(this).addClass('selected');
        } )
        .on( 'mouseleave', 'tr', function () {
            $(this).removeClass( 'selected' );
        } )
        .on( 'click', 'td button', function () {
        	// alert("gotcha "+$(this).text());
        	if($(this).text()=='Edit') {
        		
        	}
        }
    );

    var spinner = $('#howmany').spinner();

	var allCodesTableDT = $('#allCodesTable').DataTable({
        "ajax": {
        	"url": "/fcadmin/getAllCodesAJAX",
            //"data": (getAJAXtoken()),
        	"type": "POST"
        },

        "columns": [
        	{"data": "id"},
        	{"data": "code"},
        	{"data": "reason_used"},
        	{"data": "date_used"}
        ],
        "columnDefs": [
        	{
    			"className": "center", "targets": [0,1,2,3,4]
        	},
        	{
        		"targets" : 0,
        		"title": "ID",
        		"render": function ( data, type, full, meta ) {
        			
			      return data;
			    },
                "width": "25px"
        	}, 
        	{
        		"targets" : 1,
        		"title": "Code",
        		"render": function ( data, type, full, meta ) {
        			return data;
			    },
                "width": "250px"
        	},
        	{
        		"targets" : 2,
        		"title": "Reason Used",
        		"render": function ( data, type, full, meta ) {
        			return data;
			    },
                "width": "200px"
        	},
        	{
        		"targets" : 3,
        		"title": "Date Used",
        		"render": function ( data, type, full, meta ) {
        			
			      return data;
			    },
                "width": "50px"
        	},
        	{
        		"targets" : 4,
        		"title": "Admin",
        		"render": function ( data, type, full, meta) {
        			var html = '<button class="admin-button edit btn btn-primary"'+
        			' data-toggle="modal" data-target="#newCardWindow">Edit</button>';
        			return html;
        		},
                "width": "80px",
        		"className": "admin-buttons"
        	}
        ],
        "drawCallback": function( settings ) {
	        // alert( 'DataTables has redrawn the table' );

	        if ($('#mainArea').is(':animated')) {
        		
        	} else {
        		setTimeout(function() {
		        	$('#mainArea').slideDown(5000);
		        },250);
        	}
	        

	    }
    });

	

    $('.code-generator button').on('click', function (e) {
        e.preventDefault();
        // alert('Make codes');

        $.ajax({
            type: "POST",
            data: {"howmany": $('#howmany').val()},
            url: "/fcadmin/createCodes",
            success: function (data) {
                console.log(data);
                setTimeout(allCodesTableDT.ajax.reload,500);
            },
            dataType: 'json'
        });

    });

});