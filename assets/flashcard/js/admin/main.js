$(document).ready(function () {

	window.setInterval(function () {
		var d = new Date();
		var timezone = d.toString().match(/\(([A-Za-z\s].*)\)/)[1];
		var tzone = timezone.split(" ");
		var tz = "";
		for (var i = 0; i < tzone.length; i++) {
			tz += tzone[i].substring(0,1);
		};

		$('.admin-clock .clock').text(d.toDateString() +
			 " " + d.toLocaleTimeString() + 
			 " " + tz);
	}, 250);

	$('#logos, .back-to-main').on('click', function (e) {
		e.preventDefault();

		window.location.href = "/";

	});

	$('div#userAdminOptions>ul>li span').on('click', function (e) {
		e.preventDefault();
		// console.log($(this));
		// alert($(this).text())
		switch($(this).text().toString().trim()) {
			case "Categories":
				//code
				window.location.href = "/fcadmin/categories";
				break;
			case "Card Sets":
				//code
				window.location.href = "/fcadmin/cardsets";
				break;
			case "Cards":
				//code
				window.location.href = "/fcadmin/cards";
				break;
			//case "Quotes":
			//	//code
			//	window.location.href = "/fcadmin/quotes";
			//	break;
			case "Site Codes":
				//code
				window.location.href = "/fcadmin/codes";
				break;
		}

	});

	$('#users').on('click', function (e) {
		window.location.href = "/fcadmin/users";
	});

	$('#quotes').on('click', function (e) {
		window.location.href = "/fcadmin/quotes";
	});

	$('#prompts').on('click', function (e) {
		window.location.href = "/fcadmin/prompts";
	});

	$('#promptlinkage').on('click', function (e) {
		window.location.href = "/fcadmin/promptlinkage";
	});

	$('#quizzes').on('click', function (e) {
		window.location.href = "/fcadmin/quizzes";
	});

	$('.left-sidebar>ul>li>span').hover(function() {	
			$('div#userAdminOptions').hide("slide", { direction: "left" }, 250);	
			var option = $(this).attr('title').split(' ').slice(1).toString().toLowerCase();
			// console.log(option);
			var listItem = $(this).parent('li');
			listItem.find('span').removeClass('menuHover');
			var sidebar = $(this).parents('div.left-sidebar');
			var content = $('div#userAdminOptions[option="'+option+'"]');
			// console.log(sidebar);
			// alert(sidebar.offset().left);
			// alert(option);
			if (option==="support" || option==="content" || option==="site") {	
				if (option==="support" || option==="payments") {
					$(content).css({
					"top": (($(sidebar).offset().top + $(sidebar).height()) - $(content).outerHeight()) + "px"
						, "left": ($(sidebar).offset().left + $(sidebar).innerWidth() - 4) + "px"
					})
					.show("slide", { direction: "left" }, 500);
				} else {
					$(content).css({
					"top": $(listItem).offset().top + "px"
						, "left": ($(sidebar).offset().left + $(sidebar).innerWidth() - 4) + "px"
					})
					.show("slide", { direction: "left" }, 500);
				}
			}
	
		}, function() {
			
		}
	
	);

	// $('div#userAdminOptions').hover(function() {	
	// 		var option = $(this).attr('option');
	// 		// alert(option[0].toUpperCase()+option.slice(1));
	// 		var parent = $(".left-sidebar>ul li[id~="+option+"]");
	// 		console.log(parent);
	// 		parent.find('span').addClass('menuHover');
	// 	}, function() {
	// 		var option = $(this).attr('option');
	// 		// alert(option[0].toUpperCase()+option.slice(1));
	// 		var parent = $(".left-sidebar>ul li[id~="+option+"]");
	// 		console.log(parent);
	// 		parent.find('span').removeClass('menuHover');
	// 		$('div#userAdminOptions').hide("slide", { direction: "left" }, 250);
	// 	}
	// );
});

function getCategorization(categorizationCode) {

        //$('#cardsetInfo #cardSetCategorization').empty();
        var returnString = '';

        $.ajax({
            type: "POST",
            url: "/flashcard/getCategoriesById",
            data: {categorization: categorizationCode},
            success: function (data) {
                
                var returnedData = data.data;
                // console.log(returnedData);
                var selectOptions = "";
                var count = 0;
                var dashes = "";
                var idMod = "";
                var oldId = "";

                $.each(returnedData, function () {
                    idMod = this.id;

                    // for (var i = 0; i < count; i++) {
                        if(count>0){
                            dashes += "-";
                            idMod = oldId + "-" + this.id;
                        }
                    // };

                    returnString += dashes + this.name + '<br>';
                    count++;
                    oldId = idMod;

                });
                // console.log(returnString);
                // returnString = returnString.substring(0, returnString.length-4);
                
                
				return returnString;
            },
            error: function(xhr, textStatus, errorThrown){
		       alert('request failed');
		    },
            dataType: 'json'
        });

    }

function getSelectCategorization(categorizationCode) {

    $('#cardSetCategorization').empty();

    $.ajax({
        type: "POST",
        url: "/flashcard/getCategoriesById",
        data: {categorization: categorizationCode},
        success: function (data) {
            
            var returnedData = data.data;
            // console.log(returnedData);
            var selectOptions = "";
            var count = 0;
            var dashes = "";
            var idMod = "";
            var oldId = "";

            $.each(returnedData, function () {
                idMod = this.id;

                // for (var i = 0; i < count; i++) {
                    if(count>0){
                        dashes += "-";
                        idMod = oldId + "-" + this.id;
                    }
                // };

                $('#cardSetCategorization').append('<option id="category_'+ this.id +
                    '" class="categorization-options" value="'+ idMod +'">'+ dashes +
                    this.name + '</option>');
                count++;
                oldId = idMod;

            });


        },
        dataType: 'json'
    });
}

 function getAJAXtoken() {
 	var csrf = $.cookie('csrf_scott_dinwiddie_com');
 	return {"csrf_sd_token":csrf};
 }