$(document).ready(function () {

    // if(allRoles) 
    //     alert('allRoles = '+allRoles);
    


	var allUsersTableDT = $('#allUsersTable').DataTable({
        "ajax": {
        	"url": "/fcadmin/getAllUsersAJAX",
            // "data": (getAJAXtoken()),
        	"type": "POST"
        },

        "columns": [
        	{"data": "id"},
        	{"data": "username"},
        	{"data": "name"},
        	{"data": "secVal"}
        ],
        "columnDefs": [
        	{
    			"className": "center", "targets": [0,1,2,3,4]
        	},
        	{
        		"targets" : 0,
        		"title": "ID",
        		"render": function ( data, type, full, meta ) {
        			
			      return data;
			    },
                "width":"100px"
        	},
        	{
        		"targets" : 1,
        		"title": "User Name",
        		"render": function ( data, type, full, meta ) {
        			return data;
			    }
        	},
        	{
        		"targets" : 2,
        		"title": "Role Name",
        		"render": function ( data, type, full, meta ) {
                    retHTML = "<select id='selRole'>";
                    for (var i = 0; i < allRoles.length; i++) {
                        // console.log(allRoles[i]);
                        retHTML += "<option id='"+allRoles[i].id+"' value='"+allRoles[i].name+"' "+((allRoles[i].name==data)? "selected":"");
                        retHTML += ">"+allRoles[i].name+"</option>";
                    };
                    retHTML += "</select>";
        			return retHTML;
			    }
        	},
        	{
        		"targets" : 3,
        		"title": "Sec Val",
        		"visible": false,
        		"render": function ( data, type, full, meta ) {
        			
			      return data;
			    }
        	},
        	{
        		"targets" : 4,
        		"title": "Admin",
        		"render": function ( data, type, full, meta) {
        			var html = '<button class="admin-button save btn btn-primary"'+
        			' >Save</button>';
        			return html;
        		},
        		"className": "admin-buttons"
        	}
        ],
        "drawCallback": function( settings ) {
	        // alert( 'DataTables has redrawn the table' );

	        if ($('#mainArea').is(':animated')) {
        		
        	} else {
        		setTimeout(function() {
		        	$('#mainArea').slideDown(5000);
		        },250);
        	}
	        

	    }
    });
    
    //var testTable = $('#allUsersTable').datatable();
	$('#allUsersTable tbody')
        .on( 'mouseover', 'tr', function () {
            $(this).addClass('selected');
        } )
        .on( 'mouseleave', 'tr', function () {
            $(this).removeClass( 'selected' );
        } )
        .on( 'click', 'td button', function () {
            // alert("gotcha "+$(this).text());
            if($(this).text()=='Save') {
                alert("Save");
                rowNdx = $(this).parent().parent().prop('rowIndex') - 1;
                // dtapi = allUsersTableDT.api();
                selVal = $(this).parent().parent().find('td select option:selected').attr('id');
                aData = allUsersTableDT.row(rowNdx).data();
                userid = aData.id;
                console.log(userid);

                $.ajax({
                    type: "POST",
                    url: "/fcadmin/updateUserRole",
                    data: {user: userid, secLvl: selVal},
                    success: function (data) {
                        
                        if(data) {
                            allUsersTableDT.ajax.reload();
                        } else {
                            alert('Error Updating User!');
                        }
                    },
                    dataType: 'json'
                });
            }
        } )
        .on( 'change', 'td select', function () {
            // alert("gotcha "+$(this).text());
            if($(this).attr('id')=='selRole') {
                alert("Role Changed");
                $(this).parent().parent().find('td button').removeClass('btn-primary').addClass('btn-success');
            }
        }
    );

});