/**
 * Created by Scott on 7/3/2015.
 */
$(document).ready(function () {


    var allQuotesTableDT = $('#allQuotesTable').DataTable({
        "ajax": {
            "url": "/fcadmin/getAllQuotesAJAX",
            // "data": (getAJAXtoken()),
            "type": "POST"
        },

        "columns": [
            {"data": "id"},
            {"data": "quote_text"},
            {"data": "quote_credit"},
            {"data": "quote_date"},
            {"data": "active"}
        ],
        "columnDefs": [
            {
                "className": "center", "targets": [1,2,3,4]
            },
            {
                "targets" : 0,
                "title": "ID",
                "render": function ( data, type, full, meta ) {

                    return data;
                }
            },
            {
                "targets" : 1,
                "title": "Quote Text",
                "render": function ( data, type, full, meta ) {
                    return data;
                }
            },
            {
                "targets" : 2,
                "title": "Quote Credit",
                "render": function ( data, type, full, meta ) {
                    return data;
                }
            },
            {
                "targets" : 3,
                "title": "Quote Date",
                "render": function ( data, type, full, meta ) {

                    return data;
                }
            },
            {
                "targets" : 4,
                "title": "Active",
                "render": function ( data, type, full, meta ) {
                    return data;
                }
            },
            {
                "targets" : 5,
                "title": "Admin",
                "render": function ( data, type, full, meta) {
                    var html = '<button class="admin-button edit btn btn-primary"'+
                        ' data-toggle="modal" data-target="#modifyQuoteWindow">Edit</button>';
                    return html;
                },
                "className": "admin-buttons"
            }
        ],
        "drawCallback": function( settings ) {
            // alert( 'DataTables has redrawn the table' );

            if ($('#mainArea').is(':animated')) {

            } else {
                setTimeout(function() {
                    $('#mainArea').slideDown(5000);
                },250);
            }


        }
    });

    $('#allQuotesTable tbody')
        .on( 'mouseover', 'tr', function () {
            $(this).addClass('selected');
        } )
        .on( 'mouseleave', 'tr', function () {
            $(this).removeClass( 'selected' );
        } )
        .on( 'click', 'td button', function () {
            // alert("gotcha "+$(this).text());
            if($(this).text()=='Edit') {
                var page = allQuotesTableDT.page.info();
                //rowNdx = $(this).parent().parent().prop('rowIndex') - 1;
                console.log(allQuotesTableDT);
                //alert('Row index: ' + $(this).closest('tr').index());
                showEditScreen(allQuotesTableDT.row( ($(this).closest('tr').index())+(page.start) ).data());
            }
        }
    );


    tinymce.init({
        selector: '#quote',
        width: 300,
        height: 120,
        menubar: false,
        statusbar: false,
        setup: function (editor) {
            editor.on('change', function () {
                tinymce.triggerSave();
            });
        }
    });

    $("#updateQuoteInfoForm").on("submit", function(e) {
        e.preventDefault();

        var quoteWin = $("#modifyQuoteWindow");

        var elem = $(quoteWin).find(".modal-title");
        var text = $(elem).text();


        if(text.indexOf("Edit")>-1) {
            $.ajax({
                type: "POST",
                url: "/fcadmin/updateQuote/",
                data: $('#updateQuoteInfoForm').serialize(),// + getAJAXtoken(),
                success: function (data) {
                    console.log(data);
                    // $("#selectCardset").val(0);
                    // $("#selectCardset").val(cardsetid);
                    $('#modifyQuoteWindow').modal('toggle');
                    allQuotesTableDT.clear();
                    allQuotesTableDT.ajax.reload();
                },
                dataType: 'json'
            });
        } else if(text.indexOf("Add")>-1) {
            $.ajax({
                type: "POST",
                url: "/fcadmin/addQuote/",
                data: $('#updateQuoteInfoForm').serialize(),// + getAJAXtoken(),
                success: function (data) {
                    console.log(data);
                    // $("#selectCardset").val(0);
                    // $("#selectCardset").val(cardsetid);
                    $('#modifyQuoteWindow').modal('toggle');
                    allQuotesTableDT.clear();
                    allQuotesTableDT.ajax.reload();
                },
                dataType: 'json'
            });
        }


    });

    $("#newQuoteSubmitButton").click(function (e) {
        // To Do
        //alert("submit");
        e.preventDefault();
        // alert("submit");
        // var form = $('#newCardForm').serialize();
        $("#updateQuoteInfoForm").submit();
    });


    function showEditScreen(data) {
        console.log(data);
        // alert('Got it');quote

        document.getElementById("updateQuoteInfoForm").reset();
        $(tinymce.get('quote').getBody()).html("");

        var quoteWin = $("#modifyQuoteWindow");

        var elem = $(quoteWin).find(".modal-title");
        var text = $(elem).text();

        text = text.replace("Add","Edit");
        $(elem).text(text);

        $('input#id').val(data.id);
        $(tinymce.get('quote').getBody()).html(data.quote_text);
        $('input#credit').val(data.quote_credit);
        $('input#date').val(data.quote_date);

        if(data.active==1) {
            $('#active').prop('checked', true).css({"background-color":"green"});
        } else {
            $('#active').prop('checked', false).css({"background-color":"red"});
        }

    }


});

function openAddQuote() {

    document.getElementById("updateQuoteInfoForm").reset();
    $(tinymce.get('quote').getBody()).html("");

    var quoteWin = $("#modifyQuoteWindow");

    var elem = $(quoteWin).find(".modal-title");
    var text = $(elem).text();

    text = text.replace("Edit","Add");
    $(elem).text(text);
    $(quoteWin).modal('show');

}