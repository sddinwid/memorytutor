$(document).ready(function () {

//console.log(cardsets);
	var allCardsetsTableDT = $('#allCardsetsTable').DataTable({
        "order": [[ 1, 'asc' ]],
        "ajax": {
        	"url": "/fcadmin/getAllCardsetsAJAX",
        	"type": "POST"
        },
        //data : cardsets,
        "columns": [
        	{"data": "id"},
        	{"data": "name"},
        	{"data": "description"},
        	{"data": "categorization"},
            {"data": "username"},
            {"data": "created_on"},
            {"data": "modified_on"},
            {"data": "active"},
            {"data": "count"}
        ],
        "columnDefs": [
        	{
    			"className": "center", "targets": [0,1,2,4,7,8]
        	},
        	{
        		"targets" : 0,
        		"title": "ID",
        		"render": function ( data, type, full, meta ) {
        			
			      return data;
			    }
        	},
        	{
        		"targets" : 1,
        		"title": "Cardset Name",
        		"render": function ( data, type, full, meta ) {
        			return data;
			    }
        	},
        	{
        		"targets" : 2,
        		"title": "Description",
        		"render": function ( data, type, full, meta ) {
        			return data;
			    }
        	},
        	{
        		"targets" : 3,
        		"title": "Categorization",
        		"render": function ( data, type, full, meta ) {

                  var returnString = '';

                  $.ajax({
                        type: "POST",
                        async: false,
                        url: "/flashcard/getCategoriesById",
                        data: {categorization: data},
                        success: function (data) {
                            
                            var returnedData = data.data;
                            // console.log(returnedData);
                            var selectOptions = "";
                            var count = 0;
                            var dashes = "";
                            var idMod = "";
                            var oldId = "";

                            $.each(returnedData, function () {
                                idMod = this.id;

                                // for (var i = 0; i < count; i++) {
                                    if(count>0){
                                        dashes += "-";
                                        idMod = oldId + "-" + this.id;
                                    }
                                // };

                                returnString += dashes + this.name + '<br>';
                                count++;
                                oldId = idMod;

                            });
                            // console.log(returnString);
                            // returnString = returnString.substring(0, returnString.length-4);
                            
                            
                            // return returnString;
                        },
                        error: function(xhr, textStatus, errorThrown){
                           alert('request failed');
                        },
                        dataType: 'json'
                    });
        			// console.log(returnString);
			      return returnString;
			    }
        	},
            {
                "targets" : 4,
                "title": "User",
                "render": function ( data, type, full, meta ) {
                    
                  return data;
                }
            },
            {
                "targets" : 5,
                "title": "Created On",
                "render": function ( data, type, full, meta ) {
                    var incDate = data;
                    var dateSplit = incDate.split(' ');
                    var date = dateSplit[0];
                    var time = dateSplit[1];
                    dateSplit = date.split('-');
                    var timeSplit = time.split(':');
                    var year = dateSplit[0];
                    var month = dateSplit[1];
                    var day = dateSplit[2];
                    var hour = timeSplit[0];
                    var minute = timeSplit[1];
                    var second = timeSplit[2];
                    var newDate = new Date(year, month, day, hour, minute, second, 0);
                    return newDate.toDateString()+' <br>'+newDate.toLocaleTimeString();
                },
                "width": "120px"
            },
            {
                "targets" : 6,
                "title": "Last Modified",
                "render": function ( data, type, full, meta ) {
                    var incDate = data;
                    var dateSplit = incDate.split(' ');
                    var date = dateSplit[0];
                    var time = dateSplit[1];
                    dateSplit = date.split('-');
                    var timeSplit = time.split(':');
                    var year = dateSplit[0];
                    var month = dateSplit[1];
                    var day = dateSplit[2];
                    var hour = timeSplit[0];
                    var minute = timeSplit[1];
                    var second = timeSplit[2];
                    var newDate = new Date(year, month, day, hour, minute, second, 0);
                    return newDate.toDateString()+' <br>'+newDate.toLocaleTimeString();
                },
                "width": "120px"
            },
            {
                "targets" : 7,
                "title": "Active?",
                "render": function ( data, type, full, meta ) {

                  if(data=='1') {
                    return 'Yes';
                  } else {
                    return 'No';
                  }

                }
            },
            {
                "targets" : 8,
                "title": "Card Count",
                "render": function ( data, type, full, meta ) {

                  return data;

                }
            },
        	{
        		"targets" : 9,
        		"title": "Admin",
        		"render": function ( data, type, full, meta) {
                    // console.log(meta);
        			var html = '<button class="admin-button edit btn btn-primary"'+
        			' data-toggle="modal" data-target="#modifyCardSetWindow" onClick="">Edit</button>';
        			return html;
        		},
        		"className": "admin-buttons"
        	}
        ],
        "drawCallback": function( settings ) {
	        // alert( 'DataTables has redrawn the table' );

	        if ($('#mainArea').is(':animated')) {
        		
        	} else {
        		setTimeout(function() {
		        	$('#mainArea').slideDown(5000);
		        },250);
        	}
	        

	    }
    });
    

    $('#allCardsetsTable tbody')
        .on( 'mouseover', 'tr', function () {
            $(this).addClass('selected');
        } )
        .on( 'mouseleave', 'tr', function () {
            $(this).removeClass( 'selected' );
        } )
        .on( 'click', 'td button', function () {
            // alert("gotcha "+$(this).text());
            if($(this).text()=='Edit') {
                //rowNdx = $(this).parent().parent().prop('rowIndex') - 1;
                //console.log($(this));
                //alert('Row index: ' + $(this).closest('tr').index());
                var page = allCardsetsTableDT.page.info();
                showEditScreen(allCardsetsTableDT.row( ($(this).closest('tr').index())+(page.start) ).data());
            }
        }
    );

    tinymce.init({
        selector: '#cardSetDescription',
        width: 300,
        height: 120,
        menubar: false,
        statusbar: false,
        setup: function (editor) {
            editor.on('change', function () {
                tinymce.triggerSave();
            });
        }
    });

    $("#updateCardsetInfoForm").on("submit", function(e) {
        e.preventDefault();

        $('#cardSetCategorization').val($('#cardSetCategorization option:last-child').val());

        var d = new Date($.now());
        var curr_day = (d.getDate()<10?"0"+ d.getDate(): d.getDate());
        var curr_month = (d.getMonth()<10?"0"+ d.getMonth(): d.getMonth());
        var curr_year = d.getFullYear();
        var curr_hour = (d.getHours()<10?"0"+ d.getHours(): d.getHours());
        var curr_mins = (d.getMinutes()<10?"0"+ d.getMinutes(): d.getMinutes());
        var curr_secs = (d.getSeconds()<10?"0"+ d.getSeconds(): d.getSeconds());
        $("#modified_on").val(curr_year+"-"+curr_month+"-"+curr_day+" "+curr_hour+":"+curr_mins+":"+curr_secs);
        // console.log($('#cardSetCategorization').val());
        
        $.ajax({
            type: "POST",
            url: "/flashcard/updateCardset",
            data: $('#updateCardsetInfoForm').serialize(),
            success: function (data) {
                console.log(data);
                // $("#selectCardset").val(0);
                // $("#selectCardset").val(cardsetid);
                $('#modifyCardSetWindow').modal('toggle');
                allCardsetsTableDT.clear();
                allCardsetsTableDT.ajax.reload();
            },
            dataType: 'json'
        });
    });

    $("#newCardsetSubmitButton").click(function (e) {
        // To Do
        e.preventDefault();
        // alert("submit");
        // var form = $('#newCardForm').serialize();
        $("#updateCardsetInfoForm").submit();
    });

    $("#moveTop").on("click", function (e) {
        e.preventDefault();

        var clickedVal = $('#cardSetCategorization').val();
        // console.log($(this).children('option:last-child').val());
        var catVal = $('#cardSetCategorization').children('option:last-child').val();

        var catToMove = clickedVal.split("-").pop();

        var currCats = catVal.split('-');

        var index = currCats.indexOf(catToMove);
        if(index > -1) {
            currCats.splice(index, 1);
            currCats.splice(0, 0, catToMove);
        }

        var newCode = "";
        for (var i = 0; i < currCats.length; i++) {
            newCode += currCats[i];
            if(i!=currCats.length - 1) {
                newCode += "-";
            }
        };

        getSelectCategorization(newCode);


        // alert("newCode: "+newCode);
    });
    $("#moveUp").on("click", function (e) {
        e.preventDefault();
        var selIndex = $('#cardSetCategorization option:selected').index();
        console.log(selIndex);
        if(selIndex!=-1) {
            if(selIndex!=0){
                var catCode = $('#cardSetCategorization option:last-child').val();
                var arr = catCode.split('-');

                // console.log(catCode);

                var temp = arr[selIndex-1];
                arr[selIndex-1] = arr[selIndex];
                arr[selIndex] = temp;
                catCode = "";

                for (var i = 0; i < arr.length; i++) {
                    catCode += arr[i];
                    if((arr.length-1) != i) {
                        catCode += "-";
                    }
                };

                getSelectCategorization(catCode);

            } else { alert("Already Top item.");}
        } else { alert("Can't move something when nothing is selected...");}

        // console.log(selIndex);
        // console.log(catCode);
        // console.log(arr);
        // alert($('#cardSetCategorization option:selected').index());
    });
    $("#deleteCat").on("click", function (e) {
        e.preventDefault();

        var clickedVal = $('#cardSetCategorization').val();
        // console.log($(this).children('option:last-child').val());
        var catVal = $('#cardSetCategorization').children('option:last-child').val();

        var catToRemove = clickedVal.split("-").pop();

        var currCats = catVal.split('-');

        var index = currCats.indexOf(catToRemove);

        var confirmDelete = confirm("Really delete this category from this Cardset?\n(Click Save Changes to make permanent.)");

        if (confirmDelete) {

            if(index > -1) {
                currCats.splice(index, 1);
            }
            var newCode = "";
            for (var i = 0; i < currCats.length; i++) {
                newCode += currCats[i];
                if(i!=currCats.length - 1) {
                    newCode += "-";
                }
            };

            getSelectCategorization(newCode);

        }



// console.log(clickedSplit.pop());
        // alert("clickedVal: "+clickedVal+" - newCode: "+newCode+" - currCats: "+currCats);

    });
    $("#moveDown").on("click", function (e) {
        e.preventDefault();
        var selIndex = $('#cardSetCategorization option:selected').index();
        var lastIndex = $('#cardSetCategorization').children('option').length;
// console.log(lastIndex);
        if(selIndex!=-1) {
            if(selIndex!=(lastIndex-1)){
                var catCode = $('#cardSetCategorization option:last-child').val();
                var arr = catCode.split('-');

                console.log(catCode);

                var temp = arr[selIndex+1];
                arr[selIndex+1] = arr[selIndex];
                arr[selIndex] = temp;
                catCode = "";

                for (var i = 0; i < arr.length; i++) {
                    catCode += arr[i];
                    if((arr.length-1) != i) {
                        catCode += "-";
                    }
                };

                getSelectCategorization(catCode);

            } else { alert("Already Bottom item.");}
        } else { alert("Can't move something when nothing is selected...");}

        // console.log(selIndex);
        // console.log(catCode);

        // alert($('#cardSetCategorization option:selected').index());
    });
    $("#moveBottom").on("click", function (e) {
        e.preventDefault();

        var clickedVal = $('#cardSetCategorization').val();
        // console.log($(this).children('option:last-child').val());
        var catVal = $('#cardSetCategorization').children('option:last-child').val();

        var catToMove = clickedVal.split("-").pop();

        var currCats = catVal.split('-');

        var index = currCats.indexOf(catToMove);
        if(index > -1) {
            currCats.splice(index, 1);
            currCats.splice(currCats.length, 0, catToMove);
        }

        var newCode = "";
        for (var i = 0; i < currCats.length; i++) {
            newCode += currCats[i];
            if(i!=currCats.length - 1) {
                newCode += "-";
            }
        };

        getSelectCategorization(newCode);


        // alert("newCode: "+newCode);
    });

    $('#categorySelect option').on('click', function () {

        var dashes = '';
        var newOptionText = '';
        var subId = '';
        var options = $("#cardSetCategorization").children();
        var howMany = $(options).length;
        var text = $(this).text();
        var id = $(this).val();

        dashes = " ";
        for (var i = 0; i < howMany; i++) {
            dashes += "-";
        };
        dashes +=  " ";

        /*
         add dash to
         next line before category name
         current category value
         add selected category id to current category value
         add text to categorization select... */
        $("#cardSetCategorization").append('<option id="category_'+id+'" value="'+$(options[(howMany-1)]).val()+'-'+id+'">'+dashes+text+'</option>');


    });

});

function showEditScreen(data) {
    console.log(data.modified_on);
    // alert('Got it');


    $('#updateCardsetInfoForm > div > #id').val(data.id);
    $('#updateCardsetInfoForm > div > #name').val(data.name);
    $('#updateCardsetInfoForm  #created_on').val(data.created_on);
    getSelectCategorization(data.categorization);
    $(tinymce.get('cardSetDescription').getBody()).html(data.description);
    $('#updateCardsetInfoForm  #modified_on').val(data.modified_on);

    if(data.active==1) {
        $('#infoCont #active').prop('checked', true).css({"background-color":"green"});
    } else {
        $('#infoCont #active').prop('checked', false).css({"background-color":"red"});
    }

}

