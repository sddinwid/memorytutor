$(document).ready(function () {

	var allQuizzesTableDT = $('#allQuizzesTable').DataTable({
        "ajax": {
        	"url": "/fcadmin/getAdminQuizzesAJAX",
            // "data": (getAJAXtoken()),
        	"type": "POST"
        },
        "order": [[ 5, 'desc' ],[ 7, 'desc' ]],
        "columns": [
        	{"data": "id"},
        	{"data": "cardset_id"},
            {"data": "cardsetname"},
        	{"data": "userId"},
            {"data": "username"},
        	{"data": "date_taken"},
            {"data": "quiz_length"},
            {"data": "end_time"},
            {"data": "quiz_time"},
            {"data": "percent_correct"},
            {"data": "number_of_questions"}
        ],
        "columnDefs": [
        	{
    			"className": "center", "targets": [0,1,2,3,4,5,6,7,8,9,10]
        	},
        	{
        		"targets" : 0,
        		"title": "ID",
                "visible": false,
        		"render": function ( data, type, full, meta ) {
        			
			      return data;
			    }
        	},
        	{
        		"targets" : 1,
        		"title": "Cardset ID",
                "visible": false,
        		"render": function ( data, type, full, meta ) {
        			return data;
			    }
        	},
            {
                "targets" : 2,
                "title": "Cardset Name",
                "render": function ( data, type, full, meta ) {
                    return data;
                }
            },
        	{
        		"targets" : 3,
        		"title": "User ID",
                "visible": false,
        		"render": function ( data, type, full, meta ) {
        			return data;
			    }
        	},
            {
                "targets" : 4,
                "title": "User",
                "render": function ( data, type, full, meta ) {
                    return data;
                }
            },
            {
                "targets" : 5,
                "title": "Date Taken",
                "render": function ( data, type, full, meta ) {
                    
                  return data;
                }
            },
            {
                "targets" : 6,
                "title": "Quiz Length",
                "render": function ( data, type, full, meta ) {
                    
                  return data;
                }
            },
            {
                "targets" : 7,
                "title": "End Time",
                "render": function ( data, type, full, meta ) {
                    return data;
                }
            },
            {
                "targets" : 8,
                "title": "Quiz Time",
                "render": function ( data, type, full, meta ) {
                    return data;
                }
            },
            {
                "targets" : 9,
                "title": "% Correct",
                "render": function ( data, type, full, meta ) {
                    return data;
                }
            },
            {
                "targets" : 10,
                "title": "# Questions",
                "render": function ( data, type, full, meta ) {
                    return data;
                }
            },
        	{
        		"targets" : 11,
        		"title": "Admin",
        		"render": function ( data, type, full, meta) {
        			var html = '<button class="admin-button edit btn btn-primary"'+
        			' data-toggle="modal" data-target="#newCardWindow">Edit</button>';
        			return html;
        		},
        		"className": "admin-buttons"
        	}
        ],
        "drawCallback": function( settings ) {
	        // alert( 'DataTables has redrawn the table' );

	        if ($('#mainArea').is(':animated')) {
        		
        	} else {
        		setTimeout(function() {
		        	$('#mainArea').slideDown(5000);
		        },250);
        	}
	        

	    }
    });

	
    $('#allQuizzesTable tbody')
        .on( 'mouseover', 'tr', function () {
            $(this).addClass('selected');
        } )
        .on( 'mouseleave', 'tr', function () {
            $(this).removeClass( 'selected' );
        } )
        .on('click', 'tr', function() {
            var data = allQuizzesTableDT.row(this).data();
            console.log(data);
            alert("Quiz '"+data.cardsetname+"' selected");
            window.location.href = "/fcadmin/quizzes/"+data.id;
        })
        .on( 'click', 'td button', function () {
            // alert("gotcha "+$(this).text());
            if($(this).text()=='Edit') {
                
            }
        }
    );

});