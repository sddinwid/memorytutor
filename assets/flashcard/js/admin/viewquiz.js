$(document).ready(function () {

	// $('table.cards > tbody').hide();


	$('table.draw-order').on('click', 'tbody tr', function() {
		// alert('Show card row.');

		$('table.draw-order tbody tr.draw-order-selected').removeClass('draw-order-selected');

		$(this).removeClass('draw-order-hover').addClass('draw-order-selected');

		var cardid = $(this).find('td.id').text();

		var resultrow = $('table.cards > tbody td:contains('+cardid+')').parent('tr');

		$.each($('table.cards tbody > tr'), function () {
			$(this).hide();
		});

		$(resultrow).show();

		// console.log(resultrow);

	}).on('mouseover', 'tbody tr',  function() {
		// alert('hover');
		if($(this).hasClass('draw-order-selected')) {

		} else {
			$(this).addClass('draw-order-hover');
		}
	}).on('mouseleave', 'tbody tr',  function() {
		// alert('hover');
		$(this).removeClass('draw-order-hover');
	});

	$.each($('table.cards tbody > tr'), function () {
		$(this).hide();
	});

});