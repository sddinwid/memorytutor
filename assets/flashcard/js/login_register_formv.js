// JavaScript Document
function validateloginForm(formNode){
  var formValidatorInstance = new FormValidator(formNode);
  formValidatorInstance.addRule('username', 'Username must be an email address', 'email');
  formValidatorInstance.addRule('password', 'Password must be at least 8 characters long.', 'minlength', '8');

  return formValidatorInstance.validate();
}